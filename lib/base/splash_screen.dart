// @dart=2.9
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:ui' as ui show window;

import 'package:liveapp/base/skip_down_time_progress.dart';
import 'package:liveapp/config/image/local_image_selector.dart';

class SplashPage extends StatefulWidget {
  const SplashPage();

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  String netImgUrl = '';
  Timer _timer;
  int count = 5;
  @override
  void initState() {
    super.initState();

    //模拟广告图接口请求
    Future.delayed(Duration(microseconds: 1000), () {
      netImgUrl = 'https://img2.baidu.com/it/u=1170834292,3580907519&fm=253&fmt=auto&app=138&f=JPG?w=500&h=889';
      // netImgUrl='';

      //有广告页：netImgUrl不为空
      if (netImgUrl.length > 0) {
        /**刷新页面使广告图显示**/
        // setState(() {});
        startTime();
        //无广告页：netImgUrl为空""
      } else {
        /**跳转到主页**/
        Future.delayed(Duration(microseconds: 10), goIndexPage);
      }
    });
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
    ;

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment(1.0, -1.0), // 右上角对齐
        children: <Widget>[
          ConstrainedBox(
            constraints: BoxConstraints.expand(),
            child: LocalImageSelector.getSingleImage('splash', bFitFill: BoxFit.fill),
          ),
          //倒计时：跳过
          // Visibility(
          //   visible: netImgUrl.length>0,
          //   child: Positioned(
          //      child: SkipDownTimeProgress(
          //          color: Colors.black,
          //          radius: 22.0,
          //          duration: Duration(seconds: 5),
          //          size: Size(25.0, 25.0),
          //          skipText: "跳过",
          //          onTap: () => goIndexPage(),
          //          onFinishCallBack: (bool value) {
          //            if (value) goIndexPage();
          //          }),
          //      top: MediaQueryData.fromWindow(ui.window).padding.top + 20,
          //      right: 30),
          //  )
          Padding(
            padding: EdgeInsets.fromLTRB(0.0, 46.0, 10.0, 0.0),
            child: FlatButton(
              padding: EdgeInsets.symmetric(horizontal: 0),
              color: Color(0xA5000000),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
              child: Container(
                width: 69,
                height: 24,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "跳过",
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5, top: 2),
                      child: Text(
                        '$count' + 's',
                        style: TextStyle(color: Color(0xFFF36704), fontSize: 15.0),
                      ),
                    )
                  ],
                ),
              ),
              onPressed: () {
                goIndexPage();
              },
            ),
          )
        ]);
  }

  // 跳转主页
  void goIndexPage() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
    Navigator.of(context).pushReplacementNamed('/index');
  }

  void startTime() async {
    //设置启动图生效时间
    var _duration = new Duration(seconds: 1);
    Timer(_duration, () {
      // 空等1秒之后再计时
      _timer = Timer.periodic(const Duration(milliseconds: 1000), (v) {
        count--;
        if (count == 0) {
          goIndexPage();
        } else {
          if (mounted) setState(() {});
        }
      });
      return _timer;
    });
  }
}
