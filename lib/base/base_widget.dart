// ignore_for_file: unused_import
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:liveapp/base/base_function.dart';
import 'package:liveapp/base/navigator_manger.dart';
import 'package:liveapp/main.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/style/live/base_color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef ShouldRebuildFunction<T> = bool Function(T oldWidget, T newWidget);

abstract class BaseWidget<T extends Widget> extends StatefulWidget {
  BaseWidget({Key? key}) : super(key: key);
  @override
  BaseWidgetState createState() => getState();
  BaseWidgetState getState();
}

abstract class BaseWidgetState<T extends BaseWidget> extends State<T>
    with WidgetsBindingObserver, BaseFunction, AutomaticKeepAliveClientMixin, AppColors, localStorage {
  BaseFunction getBaseFunction() => this;

  /// 页面暂停标记
  bool _onPause = false;

  ///是否进行页面缓存（默认不缓存界面）
  @override
  bool get wantKeepAlive => false;

  //- 是否进屏幕适配
  @override
  bool get isShowScreenSize => true;

  @override
  void initState() {
    initBaseCommon(this, this.context);
    NavigatorManger.addWidget(getClassName());
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  @override
  void deactivate() {
    //- 取消网络请求
    DioUtil.cancelRequest();
    if (NavigatorManger.isSecondTop(getClassName())) {
      if (!_onPause) {
        onPause();
        _onPause = true;
      } else {
        onResume();
        _onPause = false;
      }
    } else if (NavigatorManger.isTopPage(getClassName())) {
      if (!_onPause) {
        onPause();
      }
    }
    super.deactivate();
  }

  Widget PageBody(AppState state) => Container();

  @override
  Widget build(BuildContext context) {
    // 是否添加screenUtil
    if (isShowScreenSize) {
      ScreenUtil.init(navigatorKey.currentState!.context, designSize: Size(375, 812));
    }
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (context, state) {
        return PageBody(state);
      },
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    _onPause = false;
    NavigatorManger.removeWidget(getClassName());
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      onForeground(); //切回前台
      if (NavigatorManger.isTopPage(getClassName())) {
        onResume();
      }
    } else if (state == AppLifecycleState.paused) {
      // on pause
      if (NavigatorManger.isTopPage(getClassName())) {
        onBackground(); //后台切回
        onPause();
      }
    }
    super.didChangeAppLifecycleState(state);
  }
}
