// import 'dart:ffi';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/common_widget/base_app_bar.dart';
import 'package:liveapp/common_widget/custom_dialog.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import 'package:redux/redux.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';

abstract class BaseFunction {
  late State _stateBaseFunction;
  late BuildContext _contextBaseFunction;

  GlobalKey<ScaffoldState> baseScaffoldKey = new GlobalKey();

  /// 导航栏底部是否显示 默认不显示
  bool _isAppBarBottomShow = true;
  double _appBarBottomLineheight = 2;

  //- 作为内部页面距离底部的高度
  double bottomVertical = 0;

  //- 获取session应用存储内容
  Store<AppState> get sessionStorage => StoreProvider.of<AppState>(_contextBaseFunction);

  void initBaseCommon(State state, BuildContext context) {
    _stateBaseFunction = state;
    _contextBaseFunction = context;
  }

  /*
    @@@ 导航栏开始
   */
  //- 导航栏 appBar
  getAppBar({title, rightClickFn = null, rightContent = null, params = null, isShowBackIcon = true}) {
    return BaseAppBar(title: title, rightContent: rightContent, params: params, isShowBackIcon: isShowBackIcon);
  }

  //- 用户自定义导航栏
  Widget userAppBar({title, rightIcon, rightClickFn, noScale: false, isfloat: false, goIndex: false}) {
    return Container(
      padding: EdgeInsets.only(
        top: noScale ? 0 : 10.w,
        left: noScale ? 1 : 1.w,
        right: noScale ? 15 : 15.w,
      ),
      child: Row(
        children: [
          !isfloat
              ? FlatButton(
                  padding: EdgeInsets.symmetric(horizontal: 0),
                  minWidth: 40.w,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  child: Icon(Icons.arrow_back_ios, size: noScale ? 24 : 24.w, color: Color(0xffF7FAFF)),
                  onPressed: () {
                    if (goIndex) {
                      Navigator.of(_contextBaseFunction).popUntil(ModalRoute.withName('/index'));
                    } else {
                      Navigator.pop(_contextBaseFunction);
                    }
                  },
                )
              : Container(),
          Container(
            child: Text(
              title,
              style: TextStyle(fontSize: noScale ? 14 : 14.sp, color: Colors.white),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                child: Container(
                  width: noScale ? 20 : 20.w,
                  height: noScale ? 20 : 20.w,
                  child: LocalImageSelector.getSingleImage(rightIcon),
                ),
                onTap: rightClickFn,
              ),
            ),
          )
        ],
      ),
    );
  }

  //- 错误提示样式的toast
  showWarnToast(String text) {
    EasyLoading.showError(text);
  }

  //- 普通提示样式的toast
  showToast(String text, [type = "smallScreen"]) {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 800)
      ..loadingStyle = EasyLoadingStyle.custom
      ..indicatorSize = 60
      ..textStyle = TextStyle(color: Color(0xffF7FAFF), fontSize: 16)
      ..textColor = Color(0xffF7FAFF)
      ..backgroundColor = Color.fromRGBO(0, 0, 0, 0.6)
      ..indicatorColor = Color.fromRGBO(0, 0, 0, 0.6)
      ..userInteractions = false
      ..dismissOnTap = false
      ..boxShadow = <BoxShadow>[]
      ..indicatorType = EasyLoadingIndicatorType.cubeGrid;

    EasyLoading.showToast(text);
  }

  //- 淡入弹窗
  showFadeDialog(
      {String title = '公告消息',
      Color headerBGColor = const Color(0xffF7FAFF),
      Color headerTxtColor = const Color(0xFF657180),
      int headerHeight = 63,
      bool isShowHeaderCloseIcon = false,
      bool isShowTitle = false,
      String cancelTxt = '取消',
      bool isShowCancelBtn = false,
      Color cancelTextColor = const Color(0xff657180),
      Color confirmTextColor = const Color(0xffF36704),
      bool isShowConfirmBtn = false,
      String confirmTxt = '确认',
      Function? confirmCallback,
      int btnHeight = 40,
      double borderRadius = 10,
      double contentPadding = 40,
      bool barrierDismissible = false, //- 空白区域是否关闭弹窗(默认不开启)
      content = Container}) {
    showCustomDialog<bool>(
      barrierDismissible: barrierDismissible, // 空白区域禁止关闭弹窗
      context: _contextBaseFunction,
      builder: (context) {
        return SimpleDialog(
          insetPadding: EdgeInsets.symmetric(horizontal: contentPadding, vertical: 24.0),
          contentPadding: EdgeInsets.all(0),
          backgroundColor: Colors.transparent,
          titlePadding: EdgeInsets.only(top: 0),
          title: isShowTitle
              ? Container(
                  height: 63.w,
                  decoration: BoxDecoration(
                    color: headerBGColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(borderRadius),
                      topRight: Radius.circular(borderRadius),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(left: isShowHeaderCloseIcon ? 52 : 0),
                          child: Text(
                            title,
                            style: TextStyle(color: headerTxtColor, fontSize: 18.sp, fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      if (isShowHeaderCloseIcon)
                        InkWell(
                          child: Container(
                            width: 40,
                            height: 40,
                            alignment: Alignment.center,
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        )
                    ],
                  ),
                )
              : null,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Color(0xffF7FAFF),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(isShowTitle ? 0 : borderRadius.w),
                  topRight: Radius.circular(isShowTitle ? 0 : borderRadius.w),
                  bottomLeft: Radius.circular(borderRadius.w),
                  bottomRight: Radius.circular(borderRadius.w),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 24.w, right: 24.w, top: isShowTitle ? 0 : 25.w, bottom: 25.w),
                    child: content,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Color.fromRGBO(0, 0, 0, 0.15), width: 1),
                      ),
                    ),
                    child: Row(
                      children: [
                        if (isShowCancelBtn)
                          Expanded(
                            child: GestureDetector(
                              child: Container(
                                color: Colors.transparent,
                                alignment: Alignment.center,
                                height: btnHeight.w,
                                width: double.infinity,
                                child: Text(
                                  cancelTxt,
                                  style: TextStyle(color: cancelTextColor, fontSize: 16.sp),
                                ),
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                          ),
                        if (isShowCancelBtn && isShowConfirmBtn)
                          Container(
                            color: Color.fromRGBO(0, 0, 0, 0.15),
                            width: 1.w,
                            height: 40.w,
                          ),
                        if (isShowConfirmBtn)
                          Expanded(
                            child: GestureDetector(
                              child: Container(
                                color: Colors.transparent,
                                width: double.infinity,
                                alignment: Alignment.center,
                                height: btnHeight.w,
                                child: Text(
                                  confirmTxt,
                                  style: TextStyle(fontSize: 16.sp, color: confirmTextColor),
                                ),
                              ),
                              onTap: () {
                                if (confirmCallback != null) confirmCallback();
                              },
                            ),
                          ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  /// 返回屏幕高度
  double getScreenHeight() {
    return MediaQuery.of(_contextBaseFunction).size.height;
  }

  /// 返回状态栏高度
  double getTopBarHeight() {
    double paddingTop = MediaQuery.of(_contextBaseFunction).padding.top;
    // if (paddingTop > 0 && localStorage.getItem('screenTop') != paddingTop) {
    //   localStorage.setItem('screenTop', paddingTop);
    // }
    return paddingTop;
  }

  /// 返回底部safeArea高度
  double getBottomSafeAreaHeight() {
    double paddingBottom = MediaQuery.of(_contextBaseFunction).padding.bottom;
    // localStorage.setItem('screenBottom', paddingBottom);
    return paddingBottom;
  }

  /// 返回appbar高度，也就是导航栏高度
  double getAppBarHeight() {
    return kToolbarHeight + (_isAppBarBottomShow ? _appBarBottomLineheight : 0);
  }

  /// 返回屏幕宽度
  double getScreenWidth() {
    return MediaQuery.of(_contextBaseFunction).size.width;
  }

  /// 关闭最后一个 flutter 页面 ， 如果是原生跳过来的则回到原生，否则关闭app
  void finishDartPageOrApp() {
    exit(0);
  }

  /// 初始化一些变量 相当于 onCreate ， 放一下 初始化数据操作
  // void onCreate();

  /// 相当于onResume, 只要页面来到栈顶， 都会调用此方法，网络请求可以放在这个方法
  void onResume() {
    log("${getClassName()}页面onResume");
  }

  /// 路由跳转
  void goPage({url, params: ''}) {
    Navigator.pushNamed(_contextBaseFunction, '/onlive/${url}', arguments: params);
    // Get.to()
  }

  /// 页面被覆盖,暂停
  void onPause() {
    log("页面被覆盖,暂停");
  }

  /// 返回UI控件 相当于setContentView()
  // Widget buildWidget(BuildContext context);

  /// app切回到后台
  void onBackground() {
    log("回到后台");
  }

  /// app切回到前台
  void onForeground() {
    // if (sessionStorage.state.user.currentRouteName != '/onlive/live_detail' && sessionStorage.state.live.isMiniPlay) {
    //   sessionStorage.dispatch(UpdateIsMiniPlay(payload: {'isMiniPlay': false}));
    //   goPage(url: 'live_detail', params: {
    //     'userId': sessionStorage.state.live.videoDetail.userId,
    //     'item': sessionStorage.state.live.videoDetail,
    //   });
    // } else if (sessionStorage.state.user.currentRouteName == '/onlive/live_detail' &&
    //     sessionStorage.state.live.isMiniPlay) {
    //   if (VideoPlayerUtils.state == VideoPlayerState.playing) {
    //     VideoPlayerUtils.playerHandle(VideoPlayerUtils.url);
    //   }
    // }
  }

  /// 页面注销方法
  void onDestroy() {
    log("destroy");
  }

  /// 输入日志
  void log(String content) {
    // Log.d(getClassName() + "------:" + content);
  }

  // 获取当前路由界面context
  String getClassName() {
    try {
      String className = _contextBaseFunction.widget.toStringShort();
      return className;
    } catch (e) {
      String className = _contextBaseFunction.toString();
      className = className.substring(0, className.indexOf("("));
      return className;
    }
  }

  //- 检测用户登录状态
  Future<bool> async_checkedUserStatus() async {
    Map? r = await localStorage.getItem('userInfo');
    return r != null;
  }
}
