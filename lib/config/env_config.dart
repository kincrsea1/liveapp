// @dart=2.9

class EnvConfig {
  String APP_VERSION;
  String APP_TEMPLATE;
  String APP_TITLE;
  String APP_CODE;
  String APP_TEXT;
  String APP_DOWNLOAD_LDY;
  List<String> APP_API_URL;
  String DEFAULT_DOMAIN;
  String LOGANLOG_URL;
  List<String> APP_DOWNLOAD_URL_IOS;
  String APP_DOWNLOAD_URL_ANDROID;
  String APP_DEFAULT_SPTVTOKEN;
  String APP_TIM_SDK_APP_ID;
  Map<String, String> APP_PRE_STOMP_CONFIG;
  String APP_TOOGLETO_DOMAIN;
  String APP_SPORTS_SOCKET;
  String APP_H5_WEBVIEWURL;
  String APP_DEFAULT_STOKEN;
  static String APP_BEST__API_URL = '';
  String APP_SHARE_URL = '';
  EnvConfig({
    this.APP_VERSION,
    this.APP_TEMPLATE,
    this.APP_TITLE,
    this.APP_CODE,
    this.APP_TEXT,
    this.APP_API_URL,
    this.APP_DOWNLOAD_LDY,
    this.DEFAULT_DOMAIN,
    this.LOGANLOG_URL,
    this.APP_DOWNLOAD_URL_IOS,
    this.APP_DOWNLOAD_URL_ANDROID,
    this.APP_DEFAULT_SPTVTOKEN,
    this.APP_TIM_SDK_APP_ID,
    this.APP_PRE_STOMP_CONFIG,
    this.APP_TOOGLETO_DOMAIN,
    this.APP_SPORTS_SOCKET,
    this.APP_DEFAULT_STOKEN,
    this.APP_H5_WEBVIEWURL,
    this.APP_SHARE_URL,
  });
}

EnvConfig GetDcsConfig() {
  return EnvConfig(
      APP_VERSION: '1.0.4',
      APP_TEMPLATE: 'onlive',
      APP_TITLE: 'SDY体育',
      APP_CODE: 'dcs',
      APP_API_URL: [
        'https://adminapitest.51renqijie.com',
        'https://adminapitestbk.51renqijie.com',
        'https://adminapitestbk.99yipintao.com',
      ],
      APP_DOWNLOAD_LDY: 'https://www.lsdy1001.com/', // 落地页 集成openinstall
      DEFAULT_DOMAIN: 'www.wsdy.net', // 给app用的主站点域名
      LOGANLOG_URL: 'https://loganapi.wsdy.net/logan', // 前端日志url
      APP_DOWNLOAD_URL_IOS: ['https://sdytest.sdydownload.com', 'https://sdytest.sdydownload.com'],
      APP_DOWNLOAD_URL_ANDROID: 'https://download.51renqijie.com/wsdy-testdcs.apk',
      APP_DEFAULT_SPTVTOKEN: 'dcs_',
      APP_TIM_SDK_APP_ID: '1400482456',
      APP_DEFAULT_STOKEN: '1e4303d0ab8096cee63b204100fd4a869c2bfd87a6586c74',
      APP_PRE_STOMP_CONFIG: {
        'brokerURL': '//mq.wsdy.net/',
        'login': 'user',
        'passcode': '96VMN3RASlS10lwn6WAfTA',
      },
      APP_TOOGLETO_DOMAIN: 'https://agt.wsdy.net',
      APP_SPORTS_SOCKET: 'api.sportxxx278gwf4.com/yewuws2/push',
      APP_H5_WEBVIEWURL: 'https://download.51renqijie.com/webview2',
      APP_SHARE_URL: 'https://www.ccn15.com/#/');
}

EnvConfig GetPreConfig() {
  return EnvConfig(
      APP_VERSION: '1.0.4',
      APP_TEMPLATE: 'onlive',
      APP_TITLE: 'SDY体育',
      APP_CODE: 'pre',
      APP_API_URL: [
        'https://adminapipre.51renqijie.com',
        'https://adminapipre.51renqijie.com',
      ],
      DEFAULT_DOMAIN: 'www.presdy.net', // 给app用的主站点域名
      LOGANLOG_URL: 'https://loganapi.sdyng.net/logan', // 前端日志url
      APP_DOWNLOAD_URL_IOS: [
        'https://sdypre.sdydownload.com',
        'https://sdypre.sdydownload.com',
        'https://sdycthree.sdydownload.com'
      ],
      APP_DOWNLOAD_URL_ANDROID: 'https://download.51renqijie.com/wsdy-prodpre.apk',
      APP_DEFAULT_SPTVTOKEN: 'pre_',
      APP_TIM_SDK_APP_ID: '1400458964',
      APP_DEFAULT_STOKEN: '2d5e2ceaf09472f91f7e8de25f315ea4842942f31331e875',
      APP_PRE_STOMP_CONFIG: {
        'brokerURL': '//mq.sdyng.net',
        'login': 'user',
        'passcode': 'ChtEr9uSh8ui5hlSxAE',
      },
      APP_TOOGLETO_DOMAIN: 'https://agt.presdy.net',
      APP_SPORTS_SOCKET: 'api.02s6shxa.com/yewuws2/push',
      APP_H5_WEBVIEWURL: 'https://download.51renqijie.com/webview2',
      APP_SHARE_URL: 'https://www.ccn15.com/#/');
}

EnvConfig GetGocConfig() {
  return EnvConfig(
      APP_VERSION: '1.0.4',
      APP_TEMPLATE: 'onlive',
      APP_TITLE: 'SDY体育',
      APP_CODE: 'm99',
      APP_TEXT: 'SDY',
      APP_API_URL: ['https://adminapi.51renqijie.com', 'https://adminapibk.51renqijie.com'],
      DEFAULT_DOMAIN: 'www.m99live.net', // 给app用的主站点域名
      APP_DOWNLOAD_LDY: 'https://www.lsdy1001.com/', // 落地页 集成openinstall
      LOGANLOG_URL: 'https://loganapi.sdyng.net/logan', // 前端日志url
      APP_DEFAULT_STOKEN: '9289e20b79ea0abe152b5547d1d1fc4cad810b9936d7613c',
      APP_DOWNLOAD_URL_IOS: [
        'itms-services://?action=download-&url=https://download.51renqijie.com/prodgoc.plist',
        'https://sdycone.sdydownload.com',
        'https://sdyctwo.sdydownload.com',
        'https://sdycthree.sdydownload.com'
      ],
      APP_DOWNLOAD_URL_ANDROID: 'https://download.51renqijie.com/wsdy-prodgoc.apk',
      APP_DEFAULT_SPTVTOKEN: 'm99_',
      APP_TIM_SDK_APP_ID: '1400458964',
      APP_PRE_STOMP_CONFIG: {
        'brokerURL': '//mq.sdyng.net',
        'login': 'user',
        'passcode': 'ChtEr9uSh8ui5hlSxAE',
      },
      APP_TOOGLETO_DOMAIN: 'https://agt.sdy88.net',
      APP_SPORTS_SOCKET: 'api.02s6shxa.com/yewuws2/push',
      APP_H5_WEBVIEWURL: 'https://download.51renqijie.com/webview2',
      APP_SHARE_URL: 'https://www.ccn15.com/#/');
}

EnvConfig ConfigUtil() {
  return GetDcsConfig();
  // return GetPreConfig();
  // return GetGocConfig();
}
