//-  万位分割
String filterNum(sourceNumber) {
  return sourceNumber.truncate() > 9999
      ? '${(sourceNumber.truncate() / 10000).toStringAsFixed(2)}w'
      : sourceNumber.truncate().toString();
}

//-  万位分割
String filterNumChina(sourceNumber) {
  if(sourceNumber == null ){
    sourceNumber = 0;
  }
  return sourceNumber.truncate() > 9999
      ? '${(sourceNumber.truncate() / 10000).toStringAsFixed(2)}万'
      : sourceNumber.truncate().toString();
}
