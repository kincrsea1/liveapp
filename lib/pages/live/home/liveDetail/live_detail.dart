// ignore_for_file: import_of_legacy_library_into_null_safe, unused_import

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/api/index.dart' as api;
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/main.dart';
import 'package:liveapp/model/anchor_event_anchorId_mod.dart' as ListMiqAnchorEventByAnchorIdMod;
import 'package:liveapp/model/anchor_video_list_mod.dart';
import 'package:liveapp/model/tab_data_mod.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/anchor_list.dart';
import 'package:liveapp/pages/live/home/liveDetail/chat_content.dart';
import 'package:liveapp/pages/live/home/liveDetail/live_video.dart';
import 'package:liveapp/pages/live/home/liveDetail/recommend_list.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/store/session_storage/state_user.dart';
import 'package:liveapp/utils/my_underline_indicator.dart';
import 'package:liveapp/utils/number_format.dart';

class LiveDetail extends BaseWidget {
  LiveDetail({Key? key}) : super(key: key);

  @override
  BaseWidgetState<LiveDetail> getState() => _LiveDetailState();
}

class _LiveDetailState extends BaseWidgetState<LiveDetail> with TickerProviderStateMixin {
  bool get _isFullScreen => MediaQuery.of(context).orientation == Orientation.landscape;
  late TabController _tabController;
  List<DataMap> _tabs = [
    DataMap(val: '聊天', key: 0),
    DataMap(val: '主播', key: 1),
    DataMap(val: '推荐', key: 2),
  ];
  int tabIndex = 0; //- 当前滑动索引
  String enterType = ''; //- 其他界面i进入，关闭弹窗
  int _followTotal = 0;
  List anchorList = [];
  Map? followInfo; //- 用户登录关注主播信息

  String leagueName = '';
  String playerUrl = '';
  AnchorList? videoDetail;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    _init();
    _watchSmallEnter(); //- 监听是否返回直播
    Future.delayed(Duration.zero, () {
      sessionStorage.dispatch(UpdateIsGoLiveDetail(payload: {'isGoLiveDetail': false}));
    });
    super.initState();
  }

  @override
  void deactivate() {
    if (sessionStorage.state.live.isMiniPlay) {
      sessionStorage.dispatch(UpdateIsGoLiveDetail(payload: {'isGoLiveDetail': true}));
      sessionStorage.dispatch(saveVideoDetail(payload: {'videoDetail': videoDetail}));
    }
    super.deactivate();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      resizeToAvoidBottomInset: !_isFullScreen,
      body: Column(
        children: [
          if (Platform.isIOS || !_isFullScreen)
            Container(
              height: getTopBarHeight(),
              color: Colors.black87,
            ),
          Expanded(
            child: Stack(
              children: [
                Column(
                  children: [
                    if (playerUrl.length > 0)
                      LiveVideo(
                        enterType: enterType,
                        playerUrl: playerUrl,
                        leagueName: leagueName,
                        item: videoDetail,
                        routeType: '',
                      ),
                    if (!_isFullScreen) _navContainer(),
                    Expanded(child: _contentView(enterType))
                  ],
                ),
                if ((!_isFullScreen && state.live.isMiniPlay) || (!_isFullScreen && playerUrl.length < 1))
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      decoration: BoxDecoration(image: LocalImageSelector.getBgImage('common_live_bg')),
                      height: 210.w,
                    ),
                  ),
                if (state.live.isMiniPlay)
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: userAppBar(
                      title: leagueName,
                      rightIcon: 'common_share',
                      rightClickFn: () {
                        _shareMethod();
                      },
                    ),
                  ),
              ],
            ),
          )
        ],
      ),
    );
  }

  //! 导航容器
  Widget _navContainer() {
    //- 导航切换按钮
    return Container(
      height: 47.w,
      padding: EdgeInsets.only(left: 20.w),
      decoration: BoxDecoration(
        color: c_white,
        border: Border(bottom: BorderSide(color: Color(0xffF1E1DA))),
      ),
      child: Row(
        children: [
          Expanded(
            child: Theme(
              data: ThemeData(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
              ),
              child: TabBar(
                tabs: _tabs.map((item) {
                  return Tab(
                    child: Container(
                      alignment: Alignment.center,
                      width: 50.w,
                      child: Text(item.val),
                    ),
                  );
                }).toList(),
                isScrollable: true,
                labelColor: c_o,
                unselectedLabelColor: c_6,
                indicator: TabSizeIndicator(
                  wantWidth: 32.w,
                  borderSide: BorderSide(width: 3.0.w, color: c_o),
                ),
                labelStyle: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
                controller: _tabController,
                onTap: (index) {
                  tabIndex = index;
                  if (mounted) setState(() {});
                },
              ),
            ),
          ),
          GestureDetector(
            child: Container(
              width: 80.w,
              decoration: BoxDecoration(
                color: _getFollow ? c_o : c_w,
                border: Border(left: BorderSide(color: Color(0xffF1E1DA))),
              ),
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LocalImageSelector.getSingleImage('home_live_star${_getFollow ? '_active' : ''}',
                          imageWidth: 15.w, imageHeight: 14.w),
                      SizedBox(width: 2.w),
                      Text(
                        _getFollow ? '已关注' : '关注',
                        style:
                            TextStyle(color: _getFollow ? c_white : c_6, fontSize: 14.sp, fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                  Text(filterNumChina(followInfo != null ? followInfo!['followTotal'] : _followTotal),
                      style: TextStyle(color: _getFollow ? c_white : c_6, fontSize: 12.sp))
                ],
              ),
            ),
            onTap: () async {
              Map? userInfo = await localStorage.getItem('userInfo');
              if (userInfo == null) {
                bus.emit(EventBus.GOTARGETPAGE, 'go');
                return goPage(url: 'login');
              }
              if (_getFollow) {
                showFadeDialog(
                    content: Text(
                      '确定取消关注?',
                      style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    isShowCancelBtn: true,
                    isShowConfirmBtn: true,
                    cancelTextColor: c_o,
                    confirmCallback: () async {
                      await _cancelFollow();
                      showToast('已取消关注');
                      Navigator.pop(context);
                    });
              } else {
                await _followAnchor();
                showToast('关注成功');
              }
            },
          )
        ],
      ),
    );
  }

  //! 滑动容器
  _contentView(enterType) {
    return TabBarView(
      children: [
        Container(
          child: ChatContent(
            enterType: (ModalRoute.of(this.context)!.settings.arguments as Map)['enterType'] ?? '',
            videoDetail: (ModalRoute.of(this.context)!.settings.arguments as Map)['item'],
          ),
        ),
        Container(
          child: AnchorListContainer(
            followInfo: followInfo,
            userId: (ModalRoute.of(this.context)!.settings.arguments as Map)['userId'],
            item: (ModalRoute.of(this.context)!.settings.arguments as Map)['item'],
            reloadFollowInfo: () {
              _isFollow((ModalRoute.of(this.context)!.settings.arguments as Map)['userId']);
            },
          ),
        ),
        Container(child: RecommendList()), //-  推荐内容
      ],
      controller: _tabController,
    );
  }

  //- 检测是否为小窗口进入
  void _watchSmallEnter() {
    bus.on(EventBus.CLOSETARGETPAGE, (arg) {
      sessionStorage.dispatch(UpdateIsMiniPlay(payload: {'isMiniPlay': false}));
      if (VideoPlayerUtils.state == VideoPlayerState.paused) VideoPlayerUtils.playerHandle(VideoPlayerUtils.url);
    });
  }

  //- 请求入口
  void _init() async {
    await Future.delayed(Duration.zero);
    int userId = (ModalRoute.of(this.context)!.settings.arguments as Map)['userId'];
    getFollowOfTotalByUserId(userId);
    //- 获取直播间基础信息
    _listMiqAnchorEventByAnchorId(userId);
    //- 查询关注主播列表(登录使用)
    _isFollow(userId);

    videoDetail = (ModalRoute.of(this.context)!.settings.arguments as Map)['item'];
    leagueName = videoDetail?.leagueName ?? '';
    String url = videoDetail?.playUrls!.first.id ?? '';
    playerUrl = url + '.m3u8';
    sessionStorage.dispatch(SaveFloatVideo(payload: {'floatInfo': playerUrl}));
  }

  //-  关注内容获取
  Future getFollowOfTotalByUserId(userId) async {
    int n = await api.getFollowOfTotalByUserId({
      'userId': userId,
      'fakeState': ((ModalRoute.of(this.context)!.settings.arguments as Map)['item'] as AnchorList).fakeState,
    });
    if (mounted) setState(() => _followTotal = n);
  }

  //- 获取直播间基础信息
  Future _listMiqAnchorEventByAnchorId(userId) async {
    String? matchId = ((ModalRoute.of(this.context)!.settings.arguments as Map)['item'] as AnchorList).matchId;
    String? fakeState = ((ModalRoute.of(this.context)!.settings.arguments as Map)['item'] as AnchorList).fakeState;
    ListMiqAnchorEventByAnchorIdMod.Data roomInfo = await api.listMiqAnchorEventByAnchorId({
      'terminal': 1,
      'anchorId': userId,
      'matchId': fakeState == '0' ? null : matchId,
      'fakeState': fakeState,
    });
    if (sessionStorage.state.user.userInfo.token != null) {
//- 更新直播间人数
      _updateAnchorTotal(userId, roomInfo.matchScheduleId);
    }
    sessionStorage.dispatch(SaveRoomInfo(payload: {'roomInfo': roomInfo}));
  }

  //- 是否关注当前主播
  Future _isFollow(userId) async {
    Map? userInfo = await localStorage.getItem('userInfo');
    if (userInfo == null) return;
    followInfo = await api.isFollow({
      'userId': userId,
      'terminal': 1,
      'fakeState': ((ModalRoute.of(this.context)!.settings.arguments as Map)['item'] as AnchorList).fakeState,
    });
    if (mounted) setState(() {});
  }

  //- 是否关注当前主播
  get _getFollow => followInfo != null && followInfo!['isFollow'] == 1;

  //- 关注主播
  Future _followAnchor() async {
    await api.follow({
      'userId': (ModalRoute.of(this.context)!.settings.arguments as Map)['userId'],
      'terminal': 1,
      'fakeState': ((ModalRoute.of(this.context)!.settings.arguments as Map)['item'] as AnchorList).fakeState,
    });
    _isFollow((ModalRoute.of(this.context)!.settings.arguments as Map)['userId']);
    bus.emit(EventBus.RELOADAUDIENCELENGTH);
  }

//- 取消关注主播
  Future _cancelFollow() async {
    await api.cancleFollow({
      'userId': (ModalRoute.of(this.context)!.settings.arguments as Map)['userId'],
      'terminal': 1,
      'fakeState': ((ModalRoute.of(this.context)!.settings.arguments as Map)['item'] as AnchorList).fakeState,
    });
    _isFollow((ModalRoute.of(this.context)!.settings.arguments as Map)['userId']);
    bus.emit(EventBus.RELOADAUDIENCELENGTH);
  }

  //-分享
  void _shareMethod() {
    Clipboard.setData(ClipboardData(text: ConfigUtil().APP_SHARE_URL));
    EasyLoading.showToast('已经成功为你复制地址，快去分享吧！');
  }

  //- 更新直播间人数
  Future _updateAnchorTotal(int userId, String? matchScheduleId) async {
    await api.updateAnchorTotal({
      'userId': userId,
      'matchScheduleId': matchScheduleId,
    });
  }
}
