// ignore_for_file: import_of_legacy_library_into_null_safe, unused_import

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:redux/redux.dart';
import 'package:liveapp/main.dart';

import '../component/e_mine/suggestion/event_bus_util.dart';

class ChannelManager {
  // static const MethodChannel _channel = MethodChannel('pip');
  static const MethodChannel _channel = MethodChannel('login_page/method');
  // Store<AppState> store;
  // ChannelManager(this.store);

  /// 跳转到画中画页面
  static Future<void> gotoPipPage(String url) async {
    _channel.invokeMapMethod('miniPlay', {'url': url});
    _channel.setMethodCallHandler(_watchHandler);
    // return await _channel.invokeMethod('gotoPipPage');
  }

  /// 通知安卓关闭画中画
  static Future<void> closePage() async {
    return await _channel.invokeMethod('closePage');
  }

  /// 退出画中画页面
  static Future<void> dismissPipPage() async {
    return await _channel.invokeMethod('dismissPipPage');
  }

  //- 监听画中画关闭事件
  static Future<String> _watchHandler(MethodCall call) async {
    Store<AppState> sessionStorage = StoreProvider.of<AppState>(navigatorKey.currentContext as BuildContext);
    String result = call.method.toString();
    String currentRouteName = sessionStorage.state.user.currentRouteName;

    sessionStorage.dispatch(UpdateIsMiniPlay(payload: {'isMiniPlay': false}));
    if (VideoPlayerUtils.state == VideoPlayerState.paused) VideoPlayerUtils.playerHandle(VideoPlayerUtils.url);

    if (result == 'closePage') {
    } else if (result == 'bigPlay' && currentRouteName != '/onlive/live_detail') {
      Navigator.pushNamed(navigatorKey.currentContext as BuildContext, '/onlive/live_detail', arguments: {
        'userId': sessionStorage.state.live.videoDetail.userId,
        'item': sessionStorage.state.live.videoDetail,
      });
    }
    return result;
  }
}
