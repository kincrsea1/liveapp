// ignore_for_file: unused_import, import_of_legacy_library_into_null_safe, must_be_immutable

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/temp_value.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class VideoPlayerTop extends BaseWidget {
  String leagueName;
  VideoPlayerTop({Key? key, required this.leagueName}) : super(key: key);
  late Function(bool) opacityCallback;

  @override
  BaseWidgetState<VideoPlayerTop> getState() => _VideoPlayerTopState();
}

class _VideoPlayerTopState extends BaseWidgetState<VideoPlayerTop> {
  double _opacity = TempValue.isLocked ? 0.0 : 1.0;
  bool get _isFullScreen => MediaQuery.of(context).orientation == Orientation.landscape;

  @override
  void initState() {
    super.initState();
    widget.opacityCallback = (appear) {
      _opacity = appear ? 1.0 : 0.0;
      if (!mounted) return;
      setState(() {});
    };
  }

  @override
  Widget PageBody(AppState state) {
    return Positioned(
      left: 0,
      top: 0,
      right: 0,
      child: AnimatedOpacity(
        opacity: _opacity,
        duration: const Duration(milliseconds: 250),
        child: _isFullScreen
            ? Container(
                width: double.maxFinite,
                height: 40,
                alignment: Alignment.centerLeft,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Color.fromRGBO(0, 0, 0, .7), Color.fromRGBO(0, 0, 0, 0)],
                  ),
                ),
                child: Row(
                  children: [
                    IconButton(
                      padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                      onPressed: () {
                        // VideoPlayerUtils.setPortrait();
                        SystemChrome.setPreferredOrientations(
                            [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
                        bus.emit(EventBus.SHOWCHATSLIDER, false);
                      }, // 切换竖屏
                      icon: const Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      widget.leagueName,
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () {
                        _shareMethod();
                      },
                      icon: LocalImageSelector.getSingleImage('common_share', imageWidth: 20),
                    )
                  ],
                ),
              )
            : Container(
                margin: (Platform.isAndroid && _isFullScreen) ? EdgeInsets.only(top: getTopBarHeight() - 10) : null,
                width: double.maxFinite,
                child: userAppBar(
                  title: widget.leagueName,
                  rightIcon: 'common_share',
                  goIndex: true,
                  rightClickFn: () {
                    _shareMethod();
                  },
                  noScale: true,
                ),
              ),
      ),
    );
  }

  ///分享
  void _shareMethod() {
    Clipboard.setData(ClipboardData(text: ConfigUtil().APP_SHARE_URL));
    EasyLoading.showToast('已经成功为你复制地址，快去分享吧！');
  }
}
