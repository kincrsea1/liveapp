// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart';
import 'package:liveapp/model/room_mod.dart' as Room;
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/api/index.dart' as api;
import 'package:liveapp/utils/number_format.dart';

import 'package:liveapp/model/live_preview_modal.dart' as LivePreMod;
import 'package:pull_to_refresh/pull_to_refresh.dart';

//- 主播滑动内容
class AnchorListContainer extends BaseWidget {
  Map? followInfo;
  int userId;
  AnchorList item;
  Function reloadFollowInfo;
  AnchorListContainer(
      {Key? key, required this.followInfo, required this.item, required this.userId, required this.reloadFollowInfo})
      : super(key: key);

  @override
  BaseWidgetState<AnchorListContainer> getState() {
    return _AnchorListContainerState();
  }
}

class _AnchorListContainerState extends BaseWidgetState<AnchorListContainer> {
  List<LivePreMod.PrevList> _prevList = [];
  bool _isLoading = false;
  Room.Data roomInfo = Room.Data();

  int _pageSize = 10; //- 当前列表长度
  int _pageNo = 1; //- 当前请求页数

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async {
    _pageNo = 1;
    _refreshController.resetNoData();
    _listMiqAnchorLivePreview();
  }

  void _onLoading() async {
    _pageNo++;
    _listMiqAnchorLivePreview();
  }

  @override
  void initState() {
    bus.on(EventBus.RELOADAUDIENCELENGTH, (arg) {
      _getAnchorDetails();
    });
    _init();
    super.initState();
  }

  @override
  void dispose() {
    bus.off(EventBus.RELOADAUDIENCELENGTH);
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget PageBody(AppState state) {
    return Container(
      margin: EdgeInsets.only(bottom: 14.w, top: 10.w),
      child: _anchorList(),
    );
  }

  Widget _anchorList() {
    return Column(
      children: [
        Container(
          height: 68.w,
          color: c_white,
          padding: EdgeInsets.all(15.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  ClipOval(
                    child: roomInfo.userLogo == null
                        ? LocalImageSelector.getSingleImage('common_team_default', imageHeight: 35.w)
                        : CachedNetworkImage(
                            width: 38.w,
                            height: 38.w,
                            errorWidget: (BuildContext, String, dynamic) {
                              return Container(
                                width: 38.w,
                                height: 38.w,
                                child: LocalImageSelector.getSingleImage('common_user_avatar'),
                              );
                            },
                            placeholder: (BuildContext, String) {
                              return LocalImageSelector.getSingleImage('common_user_avatar', imageWidth: 38.w);
                            },
                            fit: BoxFit.cover,
                            imageUrl: roomInfo.userLogo,
                          ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          roomInfo.nickName ?? '',
                          style: TextStyle(fontSize: 16.sp, color: c_6, fontWeight: FontWeight.w500),
                        ),
                        DefaultTextStyle(
                          style: TextStyle(fontSize: 10.sp, color: c_6),
                          child: Row(
                            children: [
                              Text('ID:${roomInfo.userId ?? ''}'),
                              SizedBox(width: 10.w),
                              Text('粉丝数 ${filterNumChina(roomInfo.follow ?? 0)}')
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              GestureDetector(
                child: Container(
                  width: 50.w,
                  height: 22.w,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border.all(color: _getFollow ? c_9 : c_o),
                    borderRadius: BorderRadius.circular(11.w),
                  ),
                  child: Text(
                    _getFollow ? '已关注' : '关注',
                    style: TextStyle(fontSize: 10.sp, color: _getFollow ? c_9 : c_o),
                  ),
                ),
                onTap: () async {
                  Map? userInfo = await localStorage.getItem('userInfo');
                  if (userInfo == null) {
                    bus.emit(EventBus.GOTARGETPAGE, 'go');
                    return goPage(url: 'login');
                  }
                  if (_getFollow) {
                    showFadeDialog(
                        content: Text(
                          '确定取消关注?',
                          style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                        ),
                        isShowCancelBtn: true,
                        isShowConfirmBtn: true,
                        cancelTextColor: c_o,
                        confirmCallback: () async {
                          await _cancelFollow();
                          showToast('已取消关注');
                          Navigator.pop(context);
                        });
                  } else {
                    await _followAnchor();
                    showToast('关注成功');
                  }
                },
              )
            ],
          ),
        ),
        Expanded(
          child: SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
              header: ClassicHeader(
                height: 45.0,
                releaseText: '松开手刷新',
                refreshingText: '刷新中',
                completeText: '已更新最新主播列表！',
                failedText: '刷新失败',
                idleText: '下拉刷新',
              ),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus? mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text("没有更多");
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text("加载失败，请点击重新加载");
                  } else {
                    body = Text("没有更多");
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              controller: _refreshController,
              child: _isLoading
                  ? LoadingContainer()
                  : _prevList.length < 0
                      ? EmptyContainer(txt: '暂无更多赛事预告')
                      : SingleChildScrollView(
                          child: Column(children: _prevWidget()),
                        )),
        )
      ],
    );
  }

  //- 赛事列表
  List<Widget> _prevWidget() {
    List<Widget> tempList = [];
    for (var i = 0; i < _prevList.length; i++) {
      tempList.add(
        Container(
          margin: EdgeInsets.only(bottom: 5.w, top: 10.w),
          color: c_white,
          height: 90.w,
          child: Row(
            children: [
              Container(
                width: 260.w,
                padding: EdgeInsets.only(top: 14.w, bottom: 15.w, left: 28.w, right: 24.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _teamInfo(_prevList[i].homeLogo, _prevList[i].homeName),
                        _teamInfo(_prevList[i].awayLogo, _prevList[i].awayName),
                      ],
                    ),
                    // if (_prevList[i].resva != null)
                    /*   GestureDetector(
                        child: Container(
                          alignment: Alignment.center,
                          width: 50.w,
                          height: 22.w,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50.w),
                            border: Border.all(color: c_9),
                          ),
                          child: Text(
                            _prevList[i].resva == 0 ? '预约' : "已预约",
                            style: TextStyle(color: c_9, fontSize: 10.sp),
                          ),
                        ),
                        onTap: () {
                          showFadeDialog(
                              content: Text(
                                '确定取消预约本场比赛？',
                                style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                              ),
                              isShowCancelBtn: true,
                              isShowConfirmBtn: true,
                              cancelTextColor: c_o,
                              confirmCallback: () {
                                showToast('取消预约成功');
                                Navigator.pop(context);
                              });
                        },
                      ), */
                  ],
                ),
              ),
              Container(width: 1.w, height: 71.w, color: c_b1),
              Container(
                alignment: Alignment.centerRight,
                width: 100.w,
                child: DefaultTextStyle(
                  style: TextStyle(fontSize: 14.sp, color: c_6),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: 100.w),
                        padding: EdgeInsets.only(left: 4.w),
                        child: Text(
                          _prevList[i].leagueName as String,
                          style: TextStyle(overflow: TextOverflow.ellipsis),
                        ),
                      ),
                      Text((_prevList[i].matchDateTime as String).substring(5, _prevList[i].matchDateTime!.length - 3)),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }
    return tempList;
  }

  //-
  Future _init() async {
    await Future.delayed(Duration.zero);
    _getAnchorDetails();
    _listMiqAnchorLivePreview();
  }

  //- 获取主播信息
  void _getAnchorDetails() async {
    roomInfo = await api.getAnchorDetails({
      'userId': sessionStorage.state.live.roomInfo.userId, //todo
      'fakeState': widget.item.fakeState,
    });
    print('332行打印：============ ${roomInfo}');
    setState(() {});
  }

  //- 获取直播间信息列表
  void _listMiqAnchorLivePreview() async {
    _isLoading = true;
    LivePreMod.Data data = await api.listMiqAnchorLivePreview({
      'matchId': sessionStorage.state.live.roomInfo.matchId,
      'pageNo': _pageNo.toString(),
      'pageSize': _pageSize.toString(),
      'roomId': sessionStorage.state.live.roomInfo.roomId
    });
    _isLoading = false;
    // setState(() => _prevList = data.list!);

    if (_pageNo == 1) {
      _prevList.clear();
      _prevList = data.list!;
      _refreshController.refreshCompleted();
    } else {
      if (data.list!.length > 0) {
        _prevList.addAll(data.list!);
        _refreshController.loadComplete();
      } else {
        _pageNo -= 1;
        _refreshController.loadNoData();
      }
    }
    if (mounted) setState(() {});
  }

  Widget _teamInfo(logo, name) {
    return Container(
      child: Row(
        children: [
          logo != null
              ? Image.network(logo, width: 24.w)
              : LocalImageSelector.getSingleImage('common_team_default', imageWidth: 24.w),
          Container(
            constraints: BoxConstraints(maxWidth: 130.w),
            margin: EdgeInsets.only(left: 5.w),
            child: Text(
              name,
              style: TextStyle(overflow: TextOverflow.ellipsis),
            ),
          ),
        ],
      ),
    );
  }

  //- 是否关注当前主播
  get _getFollow => widget.followInfo != null && widget.followInfo!['isFollow'] == 1;

  //- 关注主播
  Future _followAnchor() async {
    await api.follow({
      'userId': widget.userId,
      'terminal': 1,
      'fakeState': widget.item.fakeState,
    });
    widget.reloadFollowInfo();
    _getAnchorDetails();
  }

//- 取消关注主播
  Future _cancelFollow() async {
    await api.cancleFollow({
      'userId': widget.userId,
      'terminal': 1,
      'fakeState': widget.item.fakeState,
    });
    widget.reloadFollowInfo();
    _getAnchorDetails();
  }
}
