import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/slider_chat_content.dart';
import 'package:liveapp/pages/live/home/liveDetail/temp_value.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';

// ignore: must_be_immutable
class VideoPlayerCenterContainer extends StatefulWidget {
  VideoPlayerCenterContainer({Key? key, required this.lockCallback}) : super(key: key);
  final Function lockCallback;
  late Function(bool) opacityCallback;
  @override
  _VideoPlayerCenterContainerState createState() => _VideoPlayerCenterContainerState();
}

class _VideoPlayerCenterContainerState extends State<VideoPlayerCenterContainer> with TickerProviderStateMixin {
  double _opacity = 1.0;
  late AnimationController _animationController;
  late Animation<Offset> _animation;
  bool isShow = false;
  Size get _window => MediaQueryData.fromWindow(window).size;
  bool get _isFullScreen => MediaQuery.of(context).orientation == Orientation.landscape;
  bool isPause = false;
  @override
  void initState() {
    super.initState();
    _watchIsShowSlider();

    ///- 播放状态监听
    VideoPlayerUtils.statusListener(
        key: this,
        listener: (state) {
          setState(() => isPause = state == VideoPlayerState.paused);
        });
    widget.opacityCallback = (appear) {
      if (TempValue.isLocked) return; // 如果当前isLocked，不会触发，防止快速点击误触
      _opacity = appear ? 1.0 : 0.0;
      if (!mounted) return;
      setState(() {});
    };
    _animationController = AnimationController(duration: const Duration(milliseconds: 280), vsync: this);
    _animation = Tween(
      begin: Offset(1, 0),
      end: Offset.zero,
    ).animate(_animationController);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: _opacity,
      duration: const Duration(milliseconds: 250),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            child: Row(
              children: [
                if (_isFullScreen)
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: SlideTransition(
                          position: _animation,
                          child: Container(
                            constraints: BoxConstraints(minHeight: _window.height),
                            color: Color.fromRGBO(0, 0, 0, 0.7),
                            width: 370,
                            child: SliderChatContent(),
                          ),
                        ),
                      ),
                      onTap: () {},
                    ),
                  )
              ],
            ),
          ),
          if (_isFullScreen && isPause)
            Positioned(
              child: GestureDetector(
                child: LocalImageSelector.getSingleImage('play_btn', imageWidth: 41, imageHeight: 50),
                onTap: () {
                  VideoPlayerUtils.playerHandle(VideoPlayerUtils.url);
                },
              ),
            )
        ],
      ),
    );
  }

  _watchIsShowSlider() {
    bus.on(EventBus.SHOWCHATSLIDER, (data) {
      if (mounted) {
        if (_animationController.status == AnimationStatus.completed && !data) {
          _animationController.reverse();
        } else if (data) {
          _animationController.forward();
        }
        setState(() {});
      }
    });
  }
}
