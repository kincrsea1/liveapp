// ignore_for_file: import_of_legacy_library_into_null_safe

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class ChatInput extends BaseWidget {
  final Function sendMsg;
  final AnchorList videoDetail;
  ChatInput({Key? key, required this.sendMsg, required this.videoDetail}) : super(key: key);

  @override
  BaseWidgetState<ChatInput> getState() {
    return _ChatInputState();
  }
}

class _ChatInputState extends BaseWidgetState<ChatInput> {
  TextEditingController _textFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget PageBody(AppState state) {
    return Container(
      padding: EdgeInsets.only(
          bottom: (Platform.isAndroid && getBottomSafeAreaHeight() == 0) ? 20.w : getBottomSafeAreaHeight().w,
          top: 14.w,
          left: 15.w,
          right: 15.w),
      color: c_white,
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 30.w,
              padding: EdgeInsets.only(left: 10.w),
              decoration: BoxDecoration(
                border: Border.all(color: c_6),
                borderRadius: BorderRadius.circular(15.w),
              ),
              alignment: Alignment.center,
              child: TextField(
                readOnly: state.user.userInfo.token == null,
                controller: _textFieldController,
                cursorColor: c_9,
                inputFormatters: [LengthLimitingTextInputFormatter(60)],
                style: TextStyle(color: c_9, fontSize: 14.sp),
                enabled: (!state.chat.charDisable || state.user.userInfo.token == null),
                decoration: InputDecoration(
                  isCollapsed: true,
                  fillColor: Color(0XFF2C2C2E),
                  border: OutlineInputBorder(borderSide: BorderSide.none),
                  hintText: state.chat.charDisable ? '直播间禁言中' : '说点什么...',
                  hintStyle: TextStyle(fontSize: 14.sp, color: c_9),
                ),
                onTap: () async {
                  Map? userInfo = await localStorage.getItem('userInfo');
                  if (userInfo == null) {
                    if (sessionStorage.state.live.isMiniPlay) {
                      sessionStorage.dispatch(UpdateIsGoLiveDetail(payload: {'isGoLiveDetail': true}));
                      sessionStorage.dispatch(saveVideoDetail(payload: {'videoDetail': widget.videoDetail}));
                    }
                    _unLoginDialog();
                  }
                },
              ),
            ),
          ),
          GestureDetector(
            child: Container(
              width: 70.w,
              height: 30.w,
              margin: EdgeInsets.only(left: 15.w),
              alignment: Alignment.center,
              decoration: BoxDecoration(color: c_o, borderRadius: BorderRadius.circular(15.w)),
              child: Text(
                '发送',
                style: TextStyle(fontSize: 12.sp, color: c_white),
              ),
            ),
            onTap: () async {
              if (state.chat.charDisable && state.user.userInfo.token != null) return;
              //- 登录用户不需要显示弹窗
              Map? userInfo = await localStorage.getItem('userInfo');
              if (userInfo == null) {
                if (sessionStorage.state.live.isMiniPlay) {
                  sessionStorage.dispatch(UpdateIsGoLiveDetail(payload: {'isGoLiveDetail': true}));
                  sessionStorage.dispatch(saveVideoDetail(payload: {'videoDetail': widget.videoDetail}));
                }
                return _unLoginDialog();
              }
              if (_textFieldController.text.trim().isEmpty) return showToast("不能发送空消息哦！");
              widget.sendMsg(
                  txt: _textFieldController.text,
                  callback: () {
                    _textFieldController.clear();
                  });
            },
          )
        ],
      ),
    );
  }

  //- 未登录用户弹窗
  void _unLoginDialog() {
    showFadeDialog(
      isShowCancelBtn: true,
      isShowConfirmBtn: true,
      cancelTxt: '取消',
      confirmTxt: '立即登录',
      cancelTextColor: Color(0xff666666),
      content: Container(
        child: Column(children: [
          Text(
            '登录立享高清体验',
            style: TextStyle(color: Color(0xff657180), fontWeight: FontWeight.w600, fontSize: 18.sp),
          ),
          Text('登录以获取更多丰富功能权限！', style: TextStyle(color: Color(0xff657180), fontSize: 14.sp, height: 1.5)),
        ]),
      ),
      confirmCallback: () {
        bus.emit(EventBus.GOTARGETPAGE, 'go');
        Navigator.pop(context);
        goPage(url: 'login');
      },
    );
  }
}
