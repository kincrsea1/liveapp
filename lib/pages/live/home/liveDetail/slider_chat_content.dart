import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/pages/live/home/liveDetail/slider_caht_input.dart';
import 'package:liveapp/store/session_storage/state_chat.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/model/history_chat_mod.dart' as HistoryListMod;
import 'package:liveapp/utils/icon_map.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/api/index.dart' as api;

class SliderChatContent extends BaseWidget {
  SliderChatContent({Key? key}) : super(key: key);

  @override
  BaseWidgetState<SliderChatContent> getState() {
    return _SliderChatContentState();
  }
}

class _SliderChatContentState extends BaseWidgetState<SliderChatContent> {
  ScrollController _scrollController = new ScrollController();
  List renderList = [];
  int actionIndex = -1;
  int previousListLength = 0; //- 初始化计数器

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    _getChatHistoryList();
    super.initState();
  }

  @override
  Widget PageBody(AppState state) {
    List renderList = [{}, ...state.chat.historyList];
    //- 设置滚动距离
    _settingScrollDistance(renderList);
    return Column(
      children: [
        Expanded(
          child: Container(
            child: SingleChildScrollView(
              controller: _scrollController,
              child: Container(
                padding: EdgeInsets.only(top: 22, bottom: 10, right: 10),
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: renderList.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return announcement();
                    } else {
                      return chatWidget(renderList[index]);
                    }
                  },
                ),
              ),
            ),
          ),
        ),
        _welcomeWidget,
        SliderChatInput(),
      ],
    );
  }

  //! 聊天文本内容结构
  Widget chatWidget(HistoryListMod.Data item) {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 15, right: 15),
      child: Row(
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${item.nickName}：',
                  style: TextStyle(color: Color(0xff8de7ff), fontSize: 14, fontWeight: FontWeight.w500),
                ),
                Expanded(
                  child: Wrap(
                    children: [
                      for (var i = 0; i < renderContent(item.msgContent).length; i++)
                        ContentWrapper(renderContent(item.msgContent)[i], item.ident, false)
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  //- 系统公告内容
  Widget announcement() {
    return Container(
      padding: EdgeInsets.only(bottom: 10, left: 5, right: 5),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: c_b1)),
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: Text(
        '系统公告：本平台是优质体育直播绿色平台，禁止一切赌博行为。',
        style: TextStyle(color: c_white, fontSize: 14),
      ),
    );
  }

  //- 文本图片内容项
  ContentWrapper(item, ident, isGift) {
    return item['name'] == 'text'
        ? Text(
            '${item['text']}',
            style: TextStyle(
              color: Color(isGift
                  ? 0XFF5CC264
                  : ident == 'self'
                      ? 0xffffffff
                      : 0xffffffff),
              fontSize: 13,
            ),
          )
        : Image(
            width: 20,
            image: NetworkImage(item['src']),
          );
  }

  //- 拆分文本图片
  List renderContent(message) {
    List tempList = [];
    String temp = message;
    int left = -1;
    int right = -1;
    while (temp.isNotEmpty) {
      left = temp.indexOf('[');
      right = temp.indexOf(']');
      switch (left) {
        case 0:
          if (right == -1) {
            tempList.add({
              'name': 'text',
              'text': temp,
            });
            temp = '';
          } else {
            var myEmoji = temp.substring(0, right + 1);
            if (IconMap.emojiMap.containsKey(myEmoji)) {
              tempList.add({
                'name': 'img',
                'src': '${IconMap.baseUrl}${IconMap.emojiMap[myEmoji]}',
              });
              temp = temp.substring(right + 1);
            } else {
              tempList.add({
                'name': 'text',
                'text': '[',
              });
              temp = temp.substring(1);
            }
          }
          break;
        case -1:
          tempList.add({
            'name': 'text',
            'text': temp,
          });
          temp = '';
          break;
        default:
          tempList.add({
            'name': 'text',
            'text': temp.substring(0, left),
          });
          temp = temp.substring(left);
          break;
      }
    }
    return tempList;
  }

  //- 初始化的滚动距离设置
  void _settingScrollDistance(renderList) {
    Future.delayed(Duration(seconds: 0), () {
      if (_scrollController.hasClients && (previousListLength != renderList.length)) {
        previousListLength = renderList.length;
        _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
      }
    });
  }

  //- 获取聊天室历史信息
  Future _getChatHistoryList() async {
    Map? userData = await localStorage.getItem('userInfo');
    List<HistoryListMod.Data> list = await api.getHistoryChatList({
      'groupId': sessionStorage.state.live.roomInfo.groupId,
      'msgSeq': 0,
      'pageNo': 1,
      'pageSize': 10,
    });
    list.retainWhere((element) => element.msgContent.indexOf('giftDynamicUrl') < 0);
    list = List.from(list.reversed);
    sessionStorage.dispatch(SaveHistoryList(payload: {'historyList': list}));
  }

  Widget get _welcomeWidget => AnimatedOpacity(
        opacity: sessionStorage.state.chat.isShowWelcomeInfo ? 1.0 : 0.0,
        duration: const Duration(milliseconds: 500),
        child: Container(
          margin: EdgeInsets.only(left: 15),
          height: 30,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                  sessionStorage.state.chat.welcomeListArr.length > 0
                      ? sessionStorage.state.chat.welcomeListArr[0]
                      : '',
                  style: TextStyle(color: Color(0xffFFF067), fontSize: 14)),
              Text(sessionStorage.state.chat.welcomeListArr.length > 0 ? ': 进入直播间' : '',
                  style: TextStyle(fontSize: 14, color: c_white)),
            ],
          ),
        ),
      );
}
