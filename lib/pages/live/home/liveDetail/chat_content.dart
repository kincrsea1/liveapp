// ignore_for_file: unused_import, import_of_legacy_library_into_null_safe
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart';
import 'package:liveapp/model/tab_data_mod.dart';
import 'package:liveapp/model/user_info_model.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/chat_input.dart';
import 'package:liveapp/pages/live/home/liveDetail/event.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_chat.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/api/index.dart' as api;
import 'package:liveapp/utils/encrypt.dart';
import 'package:liveapp/utils/icon_map.dart';
import 'package:tencent_im_sdk_plugin/enum/V2TimAdvancedMsgListener.dart';
import 'package:tencent_im_sdk_plugin/enum/V2TimGroupListener.dart';
import 'package:tencent_im_sdk_plugin/enum/V2TimSDKListener.dart';
import 'package:tencent_im_sdk_plugin/enum/V2TimSimpleMsgListener.dart';
import 'package:tencent_im_sdk_plugin/enum/callbacks.dart';
import 'package:tencent_im_sdk_plugin/enum/log_level.dart';
import 'package:tencent_im_sdk_plugin/manager/v2_tim_manager.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_callback.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_group_member_info.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_message.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_message_receipt.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_user_full_info.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_value_callback.dart';
import 'package:tencent_im_sdk_plugin/tencent_im_sdk_plugin.dart';
import 'package:provider/provider.dart';
import 'package:liveapp/model/history_chat_mod.dart' as HistoryListMod;

//- 聊天室界面
class ChatContent extends BaseWidget {
  String enterType;
  final AnchorList videoDetail;
  ChatContent({Key? key, required this.enterType, required this.videoDetail}) : super(key: key);

  @override
  BaseWidgetState<ChatContent> getState() {
    return _ChatContentState();
  }
}

class _ChatContentState extends BaseWidgetState<ChatContent> {
  ScrollController _scrollController = new ScrollController();
  int actionIndex = -1;
  V2TIMManager timManager = TencentImSDKPlugin.v2TIMManager; //- 腾讯SDK入口
  late V2TimSimpleMsgListener simpleMsgListener;
  List sensitiveList = []; //- 敏感词汇
  int previousListLength = 0; //- 初始化计数器

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    Future.delayed(Duration.zero, _initRequest);
    super.initState();
    _watchBigScreenSendMessage();
  }

  @override
  void deactivate() {
    List<HistoryListMod.Data> emptyList = [];
    sessionStorage.dispatch(SaveHistoryList(payload: {'historyList': emptyList}));
    sessionStorage.dispatch(UpdateIsShowWelcomeInfo(payload: {'isShowWelcomeInfo': false}));
    sessionStorage.dispatch(UpdateWelcomeListArr(payload: {'welcomeListArr': []}));
    bus.off(EventBus.SENDCHATMESSAGE);
    super.deactivate();
  }

  @override
  Widget PageBody(AppState state) {
    List renderList = [{}, ...state.chat.historyList];
    //- 设置滚动距离
    _settingScrollDistance(renderList);
    return Stack(
      children: [
        Container(
          child: SingleChildScrollView(
            controller: _scrollController,
            child: Container(
              padding: EdgeInsets.only(top: 10.w, bottom: 90.w),
              width: double.infinity,
              child: MediaQuery.removePadding(
                removeTop: true,
                context: context,
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: renderList.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return announcement();
                    } else {
                      return chatWidget(renderList[index]);
                    }
                  },
                ),
              ),
            ),
          ),
        ),
        Positioned(bottom: Platform.isIOS ? 70.w : 60.w, left: 15.w, right: 15.w, child: _welcomeWidget),
        Positioned(bottom: 0, left: 0, right: 0, child: ChatInput(sendMsg: _sendMsg, videoDetail: widget.videoDetail))
      ],
    );
  }

  //! 聊天文本内容结构
  Widget chatWidget(HistoryListMod.Data item) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(top: 10.w, left: 15.w, right: 15.w),
        child: Row(
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Container(
                  //   width: 38.w,
                  //   height: 15.w,
                  //   margin: EdgeInsets.only(right: 4.w, top: 1.w),
                  //   alignment: Alignment.center,
                  //   decoration: BoxDecoration(color: c_o, borderRadius: BorderRadius.circular(8.w)),
                  //   child: Text(index == 2 ? '主播' : '超管', style: TextStyle(color: c_white, fontSize: 12.sp)),
                  // ),
                  Text(
                    '${item.nickName}：',
                    style: TextStyle(
                      color: Color.fromRGBO(43, 68, 177, 1),
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Expanded(
                    child: Wrap(
                      children: [
                        for (var i = 0; i < renderContent(item.msgContent).length; i++)
                          ContentWrapper(renderContent(item.msgContent)[i], item.ident, false)
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      onTap: () {
        /*       showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.w),
                    topRight: Radius.circular(10.w),
                  ),
                ),
                barrierColor: Colors.transparent,
                backgroundColor: Colors.white,
                builder: (BuildContext context) {
                  return StatefulBuilder(
                    builder: (context, setState) {
                      return SafeArea(
                        child: Wrap(
                          children: actionList(setState),
                        ),
                      );
                    },
                  );
                },
              ); */
      },
    );
  }

  //- 操作内容
  /*  actionList(setState) {
    List<DataMap> actList = [
      DataMap(val: '选择控场', key: 0),
      DataMap(val: '', key: 1),
      DataMap(val: '永久禁言', key: 2, isShowLine: true),
      DataMap(val: '踢出直播间', key: 3, isShowLine: true),
      DataMap(val: '永久封号', key: 4, isShowLine: true),
      DataMap(val: '取消', key: 5),
    ];
    return actList
        .map(
          (e) => GestureDetector(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.w),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                image: e.isShowLine != null && e.key == actionIndex
                    ? LocalImageSelector.getBgImage('common_select', scale: 3, alignment: Alignment(0.9, 0))
                    : null,
                color: e.key == 1 ? c_w : c_white,
                border: e.isShowLine != null ? Border(bottom: BorderSide(color: Color(0xffFAFAFA))) : null,
              ),
              height: e.key == 0
                  ? 50.w
                  : e.key == 1
                      ? 10.w
                      : 40.w,
              width: double.infinity,
              child: Text(
                e.val,
                style: TextStyle(
                    color: e.key == 0
                        ? c_6
                        : e.key == 5 || e.key == actionIndex
                            ? c_o
                            : c_9,
                    fontSize: 14.sp,
                    fontWeight: e.key == 0 ? FontWeight.w500 : FontWeight.w400),
              ),
            ),
            onTap: () {
              if (e.isShowLine == true) {
                actionIndex = e.key;
                setState(() {});
              } else if (e.key == 5) {
                Navigator.pop(context);
              }
            },
          ),
        )
        .toList();
  } */

  //- 系统公告内容
  Widget announcement() {
    return Container(
      padding: EdgeInsets.only(bottom: 10.w, left: 5.w, right: 5.w),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: c_b1)),
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 15.w,
      ),
      child: Text(
        '系统公告：本平台是优质体育直播绿色平台，禁止一切赌博行为。',
        style: TextStyle(color: c_6, fontSize: 14.sp),
      ),
    );
  }

  //- 初始化请求入口
  void _initRequest() {
    _initSDK();
    _getChatHistoryList();
    _getSensitiveWordsReq(); //- 获取聊天室敏感词汇
    _getOutsideLinkAllow(); //- 聊天时是否可以发送外部链接
  }

  //- 获取聊天SDK
  Future _initSDK() async {
    String sdk = await api.getSdkAppId();
    _initChat(int.parse(sdk));
  }

  void _initChat(int sdkAppID) async {
    V2TimValueCallback<bool> res = await timManager.initSDK(
      sdkAppID: sdkAppID,
      loglevel: LogLevel.V2TIM_LOG_NONE, // LogLevelEnum.V2TIM_NULL,
      listener: V2TimSDKListener(
        onConnectFailed: Provider.of<Event>(context, listen: false).onConnectFailed,
        onConnectSuccess: Provider.of<Event>(context, listen: false).onConnectSuccess,
        onConnecting: Provider.of<Event>(context, listen: false).onConnectSuccess,
        onKickedOffline: Provider.of<Event>(context, listen: false).onKickedOffline,
        onSelfInfoUpdated: Provider.of<Event>(context, listen: false).onSelfInfoUpdated,
        onUserSigExpired: Provider.of<Event>(context, listen: false).onUserSigExpired,
      ),
    );

    simpleMsgListener = V2TimSimpleMsgListener(
      onRecvGroupTextMessage: Provider.of<Event>(context, listen: false).onRecvGroupTextMessage,
    );

    timManager.addSimpleMsgListener(listener: simpleMsgListener);
    timManager.setGroupListener(
        listener: new V2TimGroupListener(
      onMemberEnter: Provider.of<Event>(context, listen: false).onMemberEnter,
    ));

    timManager.getMessageManager().addAdvancedMsgListener(
      listener: V2TimAdvancedMsgListener(
        onRecvNewMessage: (V2TimMessage message) {
          HistoryListMod.Data lastData = sessionStorage.state.chat.historyList.last;
          var enStr = JhEncryptUtils.aesDecrypt(message.customElem!.data);
          //- 当前用户
          if ((lastData.fromAccount == message.sender && lastData.msgContent == enStr) ||
              message.groupID != widget.videoDetail.groupId) return;
          try {
            Map delInfo = jsonDecode(enStr);
            _delMsg(delInfo);
          } catch (e) {
            _addChatList(HistoryListMod.Data(
              nickName: message.nickName,
              msgContent: enStr,
              msgTime: (DateTime.now().millisecondsSinceEpoch / 1000).toString(),
              fromAccount: message.sender,
              msgSeq: message.seq,
            ));
          }
        }, // 收到新消息
      ),
    );

    //- 进入群组监听
    _onMemberEnterWatch();

    if (res.toJson()['code'] != 0) return;
    //- 是否进行登录聊天室操作
    _isLoginChatRoom();
  }

  Future _isLoginChatRoom() async {
    Map? userInfo = await localStorage.getItem('userInfo');
    if (userInfo == null) return;

    _timLogin();
  }

  //- 检测用户是否加入群组
  Future _checkGroup() async {
    Map res = await api.checkGroup({'groupId': widget.videoDetail.groupId});
    if (res['code'] != 0 || !mounted) return;

    sessionStorage.dispatch(UpdateChatDisable(payload: {
      'charDisable': res['data'] != null
          ? res['data']['charDisable']
          : widget.videoDetail.fakeState == '1'
              ? true
              : false
    }));

    V2TimCallback joinResponse = await timManager.joinGroup(groupID: widget.videoDetail.groupId as String, message: '');
  }

  //- 登录TIM
  Future _timLogin() async {
    UserInfo userInfo = UserInfo.fromJson(await localStorage.getItem('userInfo'));
    String sign = await _getUserSig(userInfo.loginName as String);

    V2TimCallback loginResponse = await timManager.login(
      userID: '${ConfigUtil().APP_DEFAULT_SPTVTOKEN}${userInfo.loginName}',
      userSig: sign,
    );

    if (loginResponse.code != 0) return;
    timManager.setSelfInfo(
        userFullInfo:
            V2TimUserFullInfo(nickName: userInfo.nickName!.isNotEmpty ? userInfo.nickName : userInfo.loginName));

    if (mounted) {
      //- 二期登录
      api.addAccount({'userID': '${ConfigUtil().APP_CODE}_${userInfo.loginName}'});
    }
    //- 禁言列表获取
    // api.getGroupShut({'groupId': widget.videoDetail.groupId});
    // 获取登录状态
    _checkGroup();
  }

  //- 获取用户签名
  Future _getUserSig(String loginName) async {
    return await api.getUserSig({
      'terminal': 1,
      'userid': '${ConfigUtil().APP_CODE}_${loginName}',
      'expire': 604800,
    });
  }

  //- 获取聊天室历史信息
  Future _getChatHistoryList() async {
    Map? userData = await localStorage.getItem('userInfo');
    List<HistoryListMod.Data> list = await api.getHistoryChatList({
      'groupId': widget.videoDetail.groupId,
      'msgSeq': 0,
      'pageNo': 1,
      'pageSize': 10,
    });
    list.retainWhere((element) => element.msgContent.indexOf('giftDynamicUrl') < 0);
    list = List.from(list.reversed);
    sessionStorage.dispatch(SaveHistoryList(payload: {'historyList': list}));
  }

  //- 获取聊天室敏感词汇
  Future _getSensitiveWordsReq() async {
    sensitiveList = await api.getSensitiveWordsReq();
  }

  //- 发送信息过滤
  String _filterSendText(str) {
    sensitiveList.forEach((item) {
      if (str.contains(item)) {
        String star = item.split('').map((item) => '*').join('');
        str = str.replaceAll(item, star);
      }
    });
    return str;
  }

  //- 发送聊天信息
  Future _sendMsg({txt, callback, disabledCheck = false}) async {
    //- 大屏幕禁用检查
    if (!disabledCheck) {
      if (!_checkIsHasUr(txt.trim())) return;
    }
    String filterTxt = _filterSendText(txt.trim());
    callback();

    V2TimValueCallback<V2TimMessage> sendResponse = await timManager.getMessageManager().sendCustomMessage(
          data: JhEncryptUtils.aesEncrypt(filterTxt),
          receiver: '',
          extension: '',
          groupID: widget.videoDetail.groupId ?? '',
          desc: 'encrypt',
        );

    if (sendResponse.code == 10017) {
      return showToast('你已被禁言，请联系客服!');
    }

    if (sendResponse.code != 0) {
      return showToast('加入直播间失败');
    }

    String? nickName = sendResponse.data!.nickName;
    _addChatList(HistoryListMod.Data(
      nickName: nickName ?? sendResponse.data!.sender!.split('_')[1],
      msgContent: filterTxt,
      msgSeq: sendResponse.data!.seq,
      msgTime: (DateTime.now().millisecondsSinceEpoch / 1000).toString(),
      fromAccount: sendResponse.data!.sender,
    ));
  }

  //- 追加文本内容
  _addChatList(params) {
    List<HistoryListMod.Data> temArrList = new List.empty(growable: true);
    temArrList = [...sessionStorage.state.chat.historyList, params];
    sessionStorage.dispatch(SaveHistoryList(payload: {'historyList': temArrList}));
  }

  //- 初始化的滚动距离设置
  void _settingScrollDistance(renderList) {
    Future.delayed(Duration(seconds: 0), () {
      if (_scrollController.hasClients && (previousListLength != renderList.length)) {
        previousListLength = renderList.length;
        _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
      }
    });
  }

  //- 文本图片内容项
  ContentWrapper(item, ident, isGift) {
    return item['name'] == 'text'
        ? Text(
            '${item['text']}',
            style: TextStyle(
              color: Color(isGift
                  ? 0XFF5CC264
                  : ident == 'self'
                      ? 0xffe4d38f
                      : 0xff333333),
              fontSize: 13.sp,
            ),
          )
        : Image(
            width: 20,
            image: NetworkImage(item['src']),
          );
  }

  //- 拆分文本图片
  List renderContent(message) {
    List tempList = [];
    String temp = message;
    int left = -1;
    int right = -1;
    while (temp.isNotEmpty) {
      left = temp.indexOf('[');
      right = temp.indexOf(']');
      switch (left) {
        case 0:
          if (right == -1) {
            tempList.add({
              'name': 'text',
              'text': temp,
            });
            temp = '';
          } else {
            var myEmoji = temp.substring(0, right + 1);
            if (IconMap.emojiMap.containsKey(myEmoji)) {
              tempList.add({
                'name': 'img',
                'src': '${IconMap.baseUrl}${IconMap.emojiMap[myEmoji]}',
              });
              temp = temp.substring(right + 1);
            } else {
              tempList.add({
                'name': 'text',
                'text': '[',
              });
              temp = temp.substring(1);
            }
          }
          break;
        case -1:
          tempList.add({
            'name': 'text',
            'text': temp,
          });
          temp = '';
          break;
        default:
          tempList.add({
            'name': 'text',
            'text': temp.substring(0, left),
          });
          temp = temp.substring(left);
          break;
      }
    }
    return tempList;
  }

  //- 监听大窗口消息发送
  void _watchBigScreenSendMessage() {
    bus.on(EventBus.SENDCHATMESSAGE, (arg) {
      _sendMsg(txt: arg['txt'], callback: arg['callback'], disabledCheck: true);
    });
  }

  //- 聊天室是否可以发送外部链接
  Future _getOutsideLinkAllow() async {
    int n = await api.getOutsideLinkAllow();
    sessionStorage.dispatch(SetOutsideLinkAllow(payload: {'outsideLinkAllow': n}));
  }

  //- 直播间链接
  bool _checkIsHasUr(str) {
    if (sessionStorage.state.chat.outsideLinkAllow == 1) return true;
    RegExp urlReg = new RegExp(r"^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+");
    RegExp ipReg = new RegExp(r"([0-9]{1,3}\.{1}){3}[0-9]{1,3}");
    RegExp suffixReg = new RegExp(
        r"(\.(com|tv|co|cn|net|org|gov|mil|edu|biz|info|pro|name|coop|travel|xxx|aero|idv|museum|mobi|asia|tel|int|post|jobs|cat))");
    if (urlReg.hasMatch(str) || ipReg.hasMatch(str) || suffixReg.hasMatch(str)) {
      showToast('直播间不允许发送带有链接的信息');
      return false;
    } else {
      return true;
    }
  }

  //- 进入聊天室监听
  void _onMemberEnterWatch() {
    timManager.groupListener.onMemberEnter = ((groupID, memberList) async {
      if (widget.videoDetail.groupId != groupID) return;
      if (!sessionStorage.state.chat.welcomeListArr.contains(memberList[0].nickName)) {
        sessionStorage.state.chat.welcomeListArr.add(memberList[0].nickName);
        _updateWelcomeInfo();
      }
    });
  }

  Widget get _welcomeWidget => AnimatedOpacity(
        opacity: sessionStorage.state.chat.isShowWelcomeInfo ? 1.0 : 0.0,
        duration: const Duration(milliseconds: 500),
        child: Container(
          height: 30.w,
          color: c_w,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                  sessionStorage.state.chat.welcomeListArr.length > 0
                      ? sessionStorage.state.chat.welcomeListArr[0]
                      : '',
                  style: TextStyle(color: c_o, fontSize: 14.sp)),
              Text(sessionStorage.state.chat.welcomeListArr.length > 0 ? ': 进入直播间' : '',
                  style: TextStyle(fontSize: 14.sp, color: c_6)),
            ],
          ),
        ),
      );

  //- 删除欢迎语
  void _delWelcomeInfo() {
    Future.delayed(Duration(seconds: 3), () {
      sessionStorage.dispatch(UpdateIsShowWelcomeInfo(payload: {'isShowWelcomeInfo': false}));
      Future.delayed(Duration(seconds: 3), () {
        sessionStorage.state.chat.welcomeListArr.removeAt(0);
        if (sessionStorage.state.chat.welcomeListArr.length > 0) _updateWelcomeInfo();
      });
    });
  }

  //- 修改欢迎语
  void _updateWelcomeInfo() {
    sessionStorage.dispatch(UpdateIsShowWelcomeInfo(payload: {'isShowWelcomeInfo': true}));
    _delWelcomeInfo();
  }

  void _delMsg(Map delInfo) {
    List<HistoryListMod.Data> chatList = List.from(sessionStorage.state.chat.historyList);
    if (delInfo['msgSeq'] != null) {
      chatList.removeWhere((element) => element.msgSeq.toString() == delInfo['msgSeq'].toString());
    } else {
      chatList.removeWhere((element) => element.fromAccount == delInfo['fromAccount']);
    }
    sessionStorage.dispatch(SaveHistoryList(payload: {'historyList': chatList}));
  }
}
