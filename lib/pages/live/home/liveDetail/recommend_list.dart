import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/pages/live/home/component/common/video_item.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart' as AnchorListMod;
import 'package:liveapp/api/index.dart' as api;
import 'package:pull_to_refresh/pull_to_refresh.dart';

class RecommendList extends BaseWidget {
  RecommendList({Key? key}) : super(key: key);

  @override
  BaseWidgetState<RecommendList> getState() {
    return _RecommendListState();
  }
}

class _RecommendListState extends BaseWidgetState<RecommendList> {
  // ScrollController _scrollController = new ScrollController();

  bool isLoading = false;
  int _pageSize = 10; //- 当前列表长度
  int _pageNo = 1; //- 当前请求页数
  var _catId = ''; //- 当前直播列表分类
  int _totalCount = 0;

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async {
    _pageNo = 1;
    _refreshController.resetNoData();
    _initRecommendList();
  }

  //- 下拉
  void _onLoading() async {
    if (_totalCount <= _recommendList.length) {
      _refreshController.loadNoData();
      setState(() {});
      return;
    }
    _pageNo++;
    _initRecommendList();
  }

  List<AnchorListMod.AnchorList> _recommendList = [];

  @override
  void initState() {
    Future.delayed(Duration.zero, _initRecommendList);
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget PageBody(AppState state) {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '已更新最新推荐！',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请点击重新加载");
            } else {
              body = Text("没有更多");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        controller: _refreshController,
        child: Container(
          margin: EdgeInsets.only(bottom: 14.w, top: 10.w),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 11.w),
            width: double.infinity,
            child: isLoading
                ? LoadingContainer()
                : _recommendList.length == 0
                    ? EmptyContainer(txt: '暂无推荐主播')
                    : SingleChildScrollView(
                        child: Wrap(
                          alignment: WrapAlignment.spaceBetween,
                          runSpacing: 17.w,
                          children: cardItem(),
                        ),
                      ),
          ),
        ));
  }

  //- 卡片列表
  cardItem() {
    return _recommendList
        .map(
          (item) => VideoItem(
              renderItem: item,
              clickCallBack: () {
                if (sessionStorage.state.live.roomInfo.groupId == item.groupId) return showToast('您已在当前直播间');
                if (sessionStorage.state.live.isMiniPlay) {
                  Navigator.popUntil(context, ModalRoute.withName('/index'));
                  goPage(url: 'live_detail', params: {
                    'enterType': 'playNormalVideo',
                    'userId': item.userId,
                    'item': item,
                  });
                } else {
                  // VideoPlayerUtils.removeInitializedListener(this);
                  // VideoPlayerUtils.dispose();
                  // Navigator.popUntil(context, ModalRoute.withName('/index'));
                  goPage(url: 'live_detail', params: {
                    'userId': item.userId,
                    'enterType': 'playNormalVideo',
                    'item': item,
                  });
                }
              }),
        )
        .toList();
  }

  Future _initRecommendList() async {
    List listArr = ['1', '2', '37'];
    isLoading = true;
    AnchorListMod.Data data = await api.listMiqEvent({
      'pageSize': _pageSize,
      'pageNo': _pageNo,
      'catId': listArr.contains(sessionStorage.state.live.roomInfo.catId)
          ? sessionStorage.state.live.roomInfo.catId
          : 'other',
    });
    isLoading = false;
    _totalCount = data.totalCount;

    if (_pageNo == 1) {
      _recommendList.clear();
      _recommendList = data.list!;
      _refreshController.refreshCompleted();
    } else {
      if (data.list!.length > 0) {
        _recommendList.addAll(data.list!);
        _refreshController.loadComplete();
      } else {
        _pageNo -= 1;
        _refreshController.loadNoData();
      }
    }

    _recommendList.sort((pre, next) => next.online!.compareTo(pre.online!));
    setState(() {});
  }
}
