// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';

class SliderChatInput extends BaseWidget {
  SliderChatInput({Key? key}) : super(key: key);

  @override
  BaseWidgetState<SliderChatInput> getState() {
    return _SliderChatInputState();
  }
}

class _SliderChatInputState extends BaseWidgetState<SliderChatInput> {
  TextEditingController _textFieldController = TextEditingController();

  @override
  Widget PageBody(AppState state) {
    return Container(
      padding: EdgeInsets.only(bottom: 15, top: 14, left: 15, right: 15),
      // color: c_white,
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 30,
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                border: Border.all(color: c_6),
                borderRadius: BorderRadius.circular(15),
              ),
              alignment: Alignment.center,
              child: state.user.userInfo.token == null
                  ? Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        '登录以获取更多丰富功能权限！',
                        style: TextStyle(color: c_white),
                      ),
                    )
                  : TextField(
                      controller: _textFieldController,
                      inputFormatters: [LengthLimitingTextInputFormatter(60)],
                      cursorColor: Color(0xffC1C2C4),
                      style: TextStyle(color: Color(0xffC1C2C4), fontSize: 14),
                      enabled: !state.chat.charDisable,
                      decoration: InputDecoration(
                        isCollapsed: true,
                        fillColor: Color(0XFF2C2C2E),
                        border: OutlineInputBorder(borderSide: BorderSide.none),
                        hintText: state.chat.charDisable ? '直播间禁言中' : '说点什么...',
                        hintStyle: TextStyle(fontSize: 14, color: Colors.white),
                      ),
                      onChanged: (val) {
                        // if (state.user.userInfo.token == null) return bus.emit(EventBus.OPENLOGINTIPDIALOG, true);
                      },
                    ),
            ),
          ),
          if (state.user.userInfo.token != null)
            GestureDetector(
              child: Container(
                width: 70,
                height: 30,
                margin: EdgeInsets.only(left: 15),
                alignment: Alignment.center,
                decoration: BoxDecoration(color: c_o, borderRadius: BorderRadius.circular(15)),
                child: Text(
                  '发送',
                  style: TextStyle(fontSize: 12, color: c_white),
                ),
              ),
              onTap: () async {
                if (state.chat.charDisable) return;
                if (_textFieldController.text.trim().isEmpty) return showToast("不能发送空消息哦！");
                if (!_checkIsHasUr(_textFieldController.text.trim())) return;
                bus.emit(EventBus.SENDCHATMESSAGE, {
                  'txt': _textFieldController.text,
                  'outsideLinkAllow': sessionStorage.state.chat.outsideLinkAllow == 1,
                  'callback': () {
                    _textFieldController.clear();
                  }
                });
              },
            )
        ],
      ),
    );
  }

  //- 直播间链接
  bool _checkIsHasUr(str) {
    if (sessionStorage.state.chat.outsideLinkAllow == 1) return true;
    RegExp urlReg = new RegExp(r"^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+");
    RegExp ipReg = new RegExp(r"([0-9]{1,3}\.{1}){3}[0-9]{1,3}");
    RegExp suffixReg = new RegExp(
        r"(\.(com|tv|co|cn|net|org|gov|mil|edu|biz|info|pro|name|coop|travel|xxx|aero|idv|museum|mobi|asia|tel|int|post|jobs|cat))");
    if (urlReg.hasMatch(str) || ipReg.hasMatch(str) || suffixReg.hasMatch(str)) {
      showToast('直播间不允许发送带有链接的信息', 'bigScreen');
      return false;
    } else {
      return true;
    }
  }
}
