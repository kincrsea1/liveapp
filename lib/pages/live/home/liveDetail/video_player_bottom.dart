// ignore_for_file: import_of_legacy_library_into_null_safe, must_be_immutable

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/ChannelManager.dart';
import 'package:liveapp/pages/live/home/liveDetail/temp_value.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class VideoPlayerBottom extends BaseWidget {
  String leagueName;
  String playerUrl;
  dynamic item;
  String routeType;
  VideoPlayerBottom({
    Key? key,
    required this.leagueName,
    required this.playerUrl,
    required this.item,
    required this.routeType,
  }) : super(key: key);
  late Function(bool) opacityCallback;
  @override
  _VideoPlayerBottomState getState() => _VideoPlayerBottomState();
}

class _VideoPlayerBottomState extends BaseWidgetState<VideoPlayerBottom> {
  double _opacity = TempValue.isLocked ? 0.0 : 1.0;
  bool get _isFullScreen => MediaQuery.of(context).orientation == Orientation.landscape;

  @override
  void initState() {
    super.initState();
    widget.opacityCallback = (appear) {
      _opacity = appear ? 1.0 : 0.0;
      if (!mounted) return;
      setState(() {});
    };
    bus.on(EventBus.SHOWCHATSLIDER, (data) {
      _opacity = data ? 0 : 1;
      if (mounted) setState(() {});
    });
  }

  @override
  Widget PageBody(AppState state) {
    return Positioned(
      bottom: 0,
      right: 0,
      left: 0,
      child: AnimatedOpacity(
        opacity: _opacity,
        duration: const Duration(milliseconds: 250),
        child: Container(
          width: double.maxFinite,
          height: _opacity == 0 ? 0 : 40,
          padding: const EdgeInsets.only(right: 0),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [Color.fromRGBO(0, 0, 0, .7), Color.fromRGBO(0, 0, 0, 0)],
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              VideoPlayerButton(),
              Row(
                children: [
                  if (_isFullScreen && widget.routeType.length == 0)
                    GestureDetector(
                      child: Text('聊天', style: TextStyle(fontSize: 12, color: Colors.white)),
                      onTap: () => bus.emit(EventBus.SHOWCHATSLIDER, true),
                    ),
                  IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      if (VideoPlayerUtils.state == VideoPlayerState.playing) {
                        VideoPlayerUtils.playerHandle(VideoPlayerUtils.url);
                      }
                      ChannelManager.gotoPipPage(widget.playerUrl);
                      sessionStorage.dispatch(UpdateIsMiniPlay(payload: {'isMiniPlay': true}));
                    },
                    icon: Container(
                      child: LocalImageSelector.getSingleImage(
                        'home_live_screen',
                        imageHeight: 14,
                        imageWidth: 15,
                      ),
                    ),
                  ),
                  IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      if (_isFullScreen) {
                        if (Platform.isAndroid) {
                          SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
                        }
                        bus.emit(EventBus.SHOWCHATSLIDER, false);
                        SystemChrome.setPreferredOrientations(
                            [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
                      } else {
                        if (Platform.isAndroid) {
                          SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
                        }
                        SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight]);
                      }
                    },
                    icon: Icon(
                      _isFullScreen ? Icons.fullscreen_exit : Icons.fullscreen,
                      color: Colors.white,
                      size: 26,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class VideoPlayerButton extends StatefulWidget {
  const VideoPlayerButton({Key? key}) : super(key: key);
  @override
  _VideoPlayerButtonState createState() => _VideoPlayerButtonState();
}

class _VideoPlayerButtonState extends State<VideoPlayerButton> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 250));
    if (VideoPlayerUtils.state == VideoPlayerState.playing) {
      _animationController.forward();
    }

    //- 播放状态监听
    VideoPlayerUtils.statusListener(
        key: this,
        listener: (state) {
          state == VideoPlayerState.playing ? _animationController.forward() : _animationController.reverse();
        });
    super.initState();
  }

  @override
  void dispose() {
    VideoPlayerUtils.removeStatusListener(this);
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: IconButton(
        padding: EdgeInsets.zero,
        onPressed: () => VideoPlayerUtils.playerHandle(VideoPlayerUtils.url),
        icon: AnimatedIcon(
          icon: AnimatedIcons.play_pause,
          progress: _animationController,
          color: Colors.white,
          size: 32,
        ),
      ),
    );
  }
}
