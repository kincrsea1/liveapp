// ignore_for_file: import_of_legacy_library_into_null_safe, must_be_immutable

import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/main.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/temp_value.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_bottom.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_center.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_gestures.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_top.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:m_loading/m_loading.dart';

class LiveVideo extends BaseWidget {
  String enterType;
  String leagueName;
  String playerUrl;
  dynamic item;
  String routeType;
  LiveVideo(
      {Key? key,
      required this.enterType,
      required this.routeType,
      required this.leagueName,
      required this.playerUrl,
      required this.item})
      : super(key: key);

  @override
  BaseWidgetState<LiveVideo> getState() => _LiveVideoState();
}

class _LiveVideoState extends BaseWidgetState<LiveVideo> {
  // 是否全屏
  bool get _isFullScreen => MediaQuery.of(context).orientation == Orientation.landscape;
  Size get _window => MediaQueryData.fromWindow(window).size;
  double get _width => _isFullScreen ? _window.width : _window.width;
  double get _height => _isFullScreen ? _window.height : _window.width * 9 / 16;
  Widget? _playerUI;
  VideoPlayerTop? _top;
  VideoPlayerBottom? _bottom;
  VideoPlayerCenterContainer? _center; // 控制是否沉浸式的widget
  bool loadingStatus = true; //- 视频加载状态
  String leagueName = '';
  String playerUrl = '';
  dynamic item;
  String routeType = '';

  @override
  void initState() {
    super.initState();
    if (widget.enterType == 'otherPageEnter' || widget.enterType == 'playNormalVideo') {
      VideoPlayerUtils.dispose();
    }
    //todo
    VideoPlayerUtils.playerHandle(widget.playerUrl, autoPlay: true);
    leagueName = widget.leagueName;
    playerUrl = widget.playerUrl;
    item = widget.item;
    routeType = widget.routeType;
    VideoPlayerUtils.initializedListener(
        key: this,
        listener: (initialize, widget) {
          if (initialize) {
            _top ??= VideoPlayerTop(leagueName: leagueName);
            _center ??= VideoPlayerCenterContainer(
              lockCallback: () {
                _top!.opacityCallback(!TempValue.isLocked);
                _bottom!.opacityCallback(!TempValue.isLocked);
              },
            );
            _bottom ??= VideoPlayerBottom(
              leagueName: leagueName,
              playerUrl: playerUrl,
              item: item,
              routeType: routeType,
            );
            _playerUI = widget;
            if (!mounted) return;
            setState(() {});
          }
        });
    if (widget.enterType == 'otherPageEnter') {
      // VideoPlayerUtils.setLandscape();
      SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight]);
    }
    _watchLoadingStatus();
    _watchIsChangePage();
  }

  @override
  void dispose() {
    VideoPlayerUtils.removeInitializedListener(this);
    VideoPlayerUtils.dispose();
    bus.off(EventBus.GOTARGETPAGE);
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    return SizedBox(
      height: _height,
      width: _width,
      child: _playerUI != null
          ? VideoPlayerGestures(
              appearCallback: (appear) {
                _top!.opacityCallback(appear);
                _center!.opacityCallback(appear);
                _bottom!.opacityCallback(appear);
              },
              children: [
                Center(child: _playerUI),
                _top!,
                _center!,
                _bottom!,
              ],
            )
          : Stack(
              children: [
                Container(
                  width: double.infinity,
                  color: Colors.black,
                  padding: Platform.isAndroid ? EdgeInsets.only(top: getTopBarHeight()) : null,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      !loadingStatus
                          ? Text('比赛加载失败', style: TextStyle(color: Color.fromRGBO(255, 255, 255, .7), fontSize: 16.sp))
                          : Container(
                              height: 30.w,
                              width: 30.w,
                              child: BallCircleOpacityLoading(
                                ballStyle: BallStyle(
                                  size: 4.w,
                                  color: Color.fromRGBO(255, 255, 255, .7),
                                  ballType: BallType.solid,
                                ),
                              ),
                            ),
                      !loadingStatus
                          ? GestureDetector(
                              child: Container(
                                margin: EdgeInsets.only(top: 15.w),
                                width: 80.w,
                                height: 24.w,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(color: c_6, borderRadius: BorderRadius.circular(12.w)),
                                child: Text('立即刷新', style: TextStyle(color: c_white, fontSize: 12.sp)),
                              ),
                              onTap: () {
                                //- 播放器重置
                                VideoPlayerUtils.playerHandle(widget.playerUrl);
                                _playerUI = null;
                                loadingStatus = true;
                                setState(() {});
                              },
                            )
                          : Container(
                              margin: EdgeInsets.only(top: 15.w),
                              child: Text('正在加载中...',
                                  style: TextStyle(color: Color.fromRGBO(255, 255, 255, .7), fontSize: 16.sp)),
                            )
                    ],
                  ),
                ),
                Positioned(
                  top: (Platform.isAndroid && _isFullScreen) ? getTopBarHeight() - 20 : null,
                  left: 0,
                  right: 0,
                  child: userAppBar(
                    title: widget.leagueName,
                    rightIcon: 'common_share',
                    rightClickFn: () {
                      _shareMethod();
                    },
                  ),
                ),
              ],
            ),
    );
  }

  //- 视频加载失败监听
  void _watchLoadingStatus() {
    bus.on(EventBus.GENERATEERRORTEXT, (arg) {
      Future.delayed(Duration(seconds: 0), () {
        loadingStatus = false;
        if (mounted) setState(() {});
      });
    });
  }

  ///分享
  void _shareMethod() {
    Clipboard.setData(ClipboardData(text: ConfigUtil().APP_SHARE_URL));
    EasyLoading.showToast('已经成功为你复制地址，快去分享吧！');
  }

  //- 监听是否离开当前界面
  void _watchIsChangePage() {
    bus.on(EventBus.GOTARGETPAGE, (arg) {
      if ((VideoPlayerUtils.state == VideoPlayerState.playing && arg == 'go') ||
          (VideoPlayerUtils.state == VideoPlayerState.paused && arg == 'return')) {
        VideoPlayerUtils.playerHandle(VideoPlayerUtils.url);
      }
    });
  }
}
