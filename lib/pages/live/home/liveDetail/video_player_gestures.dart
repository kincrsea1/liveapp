// ignore_for_file: import_of_legacy_library_into_null_safe

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/temp_value.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class VideoPlayerGestures extends BaseWidget {
  VideoPlayerGestures({Key? key, required this.children, required this.appearCallback}) : super(key: key);
  final List<Widget> children;
  final Function(bool) appearCallback;
  @override
  _VideoPlayerGesturesState getState() => _VideoPlayerGesturesState();
}

class _VideoPlayerGesturesState extends BaseWidgetState<VideoPlayerGestures> {
  bool _appear = true; // 控件隐藏与显示
  Timer? _timer;
  final List<Widget> _children = [];
  bool get _isFullScreen => MediaQuery.of(context).orientation == Orientation.landscape;

  @override
  void initState() {
    _children.addAll(widget.children);
    super.initState();
  }

  @override
  Widget PageBody(AppState state) {
    return GestureDetector(
      behavior: HitTestBehavior.deferToChild,
      onTap: _onTap, // 单击上下widget隐藏与显示
      child: Container(
        // 保证手势全屏
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.black,
        child: Stack(
          children: _children,
        ),
      ),
    );
  }

  void _onTap() {
    if (sessionStorage.state.live.isMiniPlay) return;
    if (_isFullScreen) {
      bus.emit(EventBus.SHOWCHATSLIDER, false);
    } else {
      if (TempValue.isLocked) return;
      _appear = !_appear;
      widget.appearCallback(_appear);
      if (_appear == true && VideoPlayerUtils.state == VideoPlayerState.playing) {
        _setupTimer();
      }
    }
  }

  // 开启定时器
  void _setupTimer() {
    _timer?.cancel();
    _timer = Timer(const Duration(seconds: 3), () {
      _appear = false;
      widget.appearCallback(_appear);
      _timer?.cancel();
    });
  }
}
