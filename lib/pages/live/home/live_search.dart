// ignore_for_file: import_of_legacy_library_into_null_safe, unused_import

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/mo/score_home_model.dart';
import 'package:liveapp/pages/live/home/component/common/video_item.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/utils/my_underline_indicator.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart' as AnchorListMod;
import 'package:liveapp/api/index.dart' as api;
import 'package:liveapp/model/match_list_mod.dart' as MatchListMod;
import 'package:liveapp/model/anchor_search_mod.dart' as AnchorSearchMod;
import 'package:liveapp/utils/number_format.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LiveSearchPage extends BaseWidget {
  LiveSearchPage({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _LiveSearchPageState();
  }
}

class _LiveSearchPageState extends BaseWidgetState<LiveSearchPage> with TickerProviderStateMixin {
  TextEditingController _searchController = TextEditingController();
  FocusNode searchFocusNode = FocusNode();
  /* 
  '直播', '赛事', '主播'
   */
  List _navList = [
    {'txt': '直播', 'index': 0},
    // {'txt': '赛事', 'index': 1},
    {'txt': '主播', 'index': 1},
  ];
  late TabController tabController;
  int _currentSelectIndex = 0;
  bool searchHistory = true;
  bool showSearchResultList = false;
  List _historySearchList = [];
  bool _isLoading = false; //- 搜索加载状态
  bool _isSearching = false;

  late Timer? timer = null; //- 防抖设置
  List<AnchorListMod.AnchorList>? _anchorList; //- 主播列表
  List<MatchListMod.MatchList>? _matchList;
  Map<String, List<MatchListMod.MatchList>> _matchFilterData = {};
  List<AnchorSearchMod.AnchorList>? _anchorUserList;
  List _alreadyFollowLost = [];

  RefreshController _refreshController_live = RefreshController(initialRefresh: false);
  RefreshController _refreshController_mathch = RefreshController(initialRefresh: false);
  RefreshController _refreshController_Anchor = RefreshController(initialRefresh: false);
  int currPage_live = 1;
  int currPage_mathch = 1;
  int currPage_Anchor = 1;
  int _matchTotal = 0;

  void _onRefresh() async {
    _getData(false);
  }

  //- 下拉刷新内容
  void _onLoading() async {
    _getData(true);
  }

  @override
  void initState() {
    _initTabController(); //- 切换滑动
    _getSearchHistoryList();
    _getAlreadyFollowList();
    super.initState();
  }

  @override
  void dispose() {
    _searchController.clear();
    _searchController.dispose();
    if (timer != null) {
      timer!.cancel();
    }
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      appBar: getAppBar(
        isShowBackIcon: false,
        title: Container(
          width: 301.w,
          height: 36.w,
          child: TextField(
              textInputAction: TextInputAction.done,
              focusNode: searchFocusNode,
              controller: _searchController,
              cursorColor: c_9,
              decoration: InputDecoration(
                fillColor: c_border,
                filled: true,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: c_b1),
                  borderRadius: BorderRadius.all(Radius.circular(100.w)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: c_b1),
                  borderRadius: BorderRadius.all(
                    Radius.circular(100.w),
                  ),
                ),
                contentPadding: EdgeInsets.all(10.w),
                prefixIcon: Icon(
                  Icons.search_outlined,
                  color: c_9,
                  size: 30.w,
                ),
                hintText: _currentSelectIndex == 0 || _currentSelectIndex == 1 ? "搜索主播/直播" : '请输入联赛或者球队名称',
                hintStyle: TextStyle(color: c_9, fontSize: 14.sp),
              ),
              onChanged: (String value) {
                if (value.isEmpty) {
                  _isSearching = false;
                  _isLoading = false;
                  return setState(() {});
                }
                if (value.trim().length == 0) return;
                _isSearching = true;
                _isLoading = true;
                setState(() => null);
                if (timer != null) timer!.cancel();
                timer = new Timer(Duration(seconds: 2), () async {
                  await _getData(false);
                  _historySearchList.add(value);
                  localStorage.setItem('history', {'searchHistoryList': _historySearchList});
                });
                showSearchResultList = value.isNotEmpty;
              }),
        ),
        rightContent: GestureDetector(
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(right: 14.w),
            child: Text("取消",
                style: TextStyle(
                  color: c_6,
                  fontSize: 16.sp,
                )),
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 18.w),
            color: c_white,
            height: 44.w,
            width: double.infinity,
            child: TabBar(
              labelPadding: EdgeInsets.symmetric(horizontal: 40.w),
              labelColor: c_o,
              labelStyle: TextStyle(fontSize: 16.sp),
              unselectedLabelColor: c_6,
              unselectedLabelStyle: TextStyle(fontSize: 16.sp),
              isScrollable: true,
              indicator: TabSizeIndicator(
                wantWidth: 32.w,
                borderSide: BorderSide(width: 4.0.w, color: c_o),
              ),
              tabs: _navList.map((e) {
                return Tab(text: e['txt']);
              }).toList(),
              controller: tabController,
            ),
          ),
          _isSearching
              ? Expanded(
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 10.w,
                      bottom: 30.w,
                    ),
                    child: TabBarView(
                      controller: tabController,
                      children: [
                        liveStreamingListWidget(),
                        // competitionListWidget(),
                        anchorListWidget(),
                      ],
                    ),
                  ),
                )
              : searchHistoryOrHotListWidget(),
        ],
      ),
    );
  }

  //! 直播列表
  Widget liveStreamingListWidget() {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '已更新最新直播！',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多数据");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请点击重新加载");
            } else {
              body = Text("没有更多数据");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        controller: _refreshController_live,
        child: _isLoading
            ? LoadingContainer(txt: '正在搜索中...')
            : _anchorList != null && _anchorList!.length > 0
                ? SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.only(
                        left: 10.w,
                        right: 10.w,
                      ),
                      child: Wrap(
                        spacing: 10.w,
                        runSpacing: 17.w,
                        children: cardItem(),
                      ),
                    ),
                  )
                : EmptyContainer(txt: '暂无搜索结果'));
  }

  //! 赛事列表
  competitionListWidget() {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '已更新最新赛事！',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多数据");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请点击重新加载");
            } else {
              body = Text("没有更多数据");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        controller: _refreshController_mathch,
        child: _matchList == null
            ? LoadingContainer(txt: '正在搜索中...')
            : _matchList!.length < 1
                ? EmptyContainer(txt: '暂无搜索结果')
                : ListView.builder(
                    itemCount: _matchFilterData.length,
                    itemBuilder: (BuildContext context, int i) {
                      String dateStr = _matchFilterData.keys.toList()[i];
                      return Column(
                        children: [
                          Container(
                            child: Text(
                              dateStr.substring(0, dateStr.length),
                              style: TextStyle(fontSize: 14.sp, color: c_9),
                            ),
                          ),
                          ListView.builder(
                            padding: EdgeInsets.only(bottom: 10),
                            shrinkWrap: true, //解决无限高度问题
                            physics: NeverScrollableScrollPhysics(), //禁用滑动事件
                            itemCount: _matchFilterData.values.toList()[i].length,
                            itemBuilder: ((BuildContext context, index) {
                              MatchListMod.MatchList item = _matchFilterData.values.toList()[i][index];
                              return GestureDetector(
                                child: Container(
                                  color: c_white,
                                  margin: EdgeInsets.only(top: 10.w),
                                  padding: EdgeInsets.symmetric(vertical: 10.w),
                                  child: Column(
                                    children: [
                                      DefaultTextStyle(
                                        style: TextStyle(color: c_9, fontSize: 14.sp),
                                        child: Container(
                                          child: Stack(children: [
                                            Container(
                                              alignment: Alignment.center,
                                              width: double.infinity,
                                              child: Container(
                                                margin: EdgeInsets.symmetric(horizontal: 80.w),
                                                child: Text(item.leagueName as String),
                                              ),
                                            ),
                                            Positioned(
                                              left: 24.w,
                                              child: Text(item.matchTime!.substring(0, item.matchTime!.length - 3)),
                                            ),
                                          ]),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(vertical: 10.w),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: [
                                                  Container(
                                                    constraints: BoxConstraints(maxWidth: 100.w),
                                                    child: Text(
                                                      item.homeName as String,
                                                      style: TextStyle(
                                                          fontSize: 16.w, color: c_6, overflow: TextOverflow.ellipsis),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 5.w),
                                                    child: item.homeLogo != null
                                                        ? Image(
                                                            image: NetworkImage(item.homeLogo as String),
                                                            width: 24.w,
                                                            height: 24.w,
                                                          )
                                                        : LocalImageSelector.getSingleImage('common_team_default',
                                                            imageWidth: 24.w),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.symmetric(horizontal: 14.w),
                                              child: Text(
                                                '${item.homeScore ?? ''}-${item.awayScore ?? ''}',
                                                style:
                                                    TextStyle(fontSize: 24.sp, fontWeight: FontWeight.w600, color: c_6),
                                              ),
                                            ),
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(right: 5.w),
                                                    child: item.awayLogo != null
                                                        ? Image(
                                                            image: NetworkImage(item.awayLogo as String),
                                                            width: 24.w,
                                                            height: 24.w,
                                                          )
                                                        : LocalImageSelector.getSingleImage('common_team_default',
                                                            imageWidth: 24.w),
                                                  ),
                                                  Container(
                                                    constraints: BoxConstraints(maxWidth: 100.w),
                                                    child: Text(
                                                      item.awayName ?? '',
                                                      style: TextStyle(
                                                          fontSize: 16.w, color: c_6, overflow: TextOverflow.ellipsis),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        child: Container(
                                          width: 70.w,
                                          height: 28.w,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            border: Border.all(color: item.resva == 1 ? c_9 : c_o),
                                            borderRadius: BorderRadius.circular(14.w),
                                            color: item.matchState == '2' ? c_o : c_white,
                                          ),
                                          child: Text(
                                            item.matchState == '2'
                                                ? '直播中'
                                                : item.resva == 0
                                                    ? '预约'
                                                    : '已预约',
                                            style: TextStyle(
                                                color: item.matchState == '2'
                                                    ? c_w
                                                    : item.resva == 1
                                                        ? c_9
                                                        : c_o,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        onTap: () async {
                                          if (item.matchState == '2') {
                                            goPage(url: 'score_detail', params: {'scoreModel': item, 'type': 'search'});
                                          } else {
                                            Map? userInfo = await localStorage.getItem('userInfo');
                                            if (userInfo == null) {
                                              goPage(url: 'login');
                                            } else {
                                              //- 添加预约功能
                                              if (item.resva == 0) {
                                                _userReserve(item);
                                              } else if (item.resva == 1) {
                                                //- 取消预约
                                                showFadeDialog(
                                                    content: Text(
                                                      '确定取消预约本场比赛？',
                                                      style: TextStyle(
                                                          color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                                                    ),
                                                    isShowCancelBtn: true,
                                                    isShowConfirmBtn: true,
                                                    cancelTextColor: c_o,
                                                    confirmCallback: () async {
                                                      await _cancelUserReserve(item);
                                                      showToast('已取消本场赛事预约');
                                                      Navigator.pop(context);
                                                    });
                                              }
                                            }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  if (item.matchState == '2') {
                                    goPage(url: 'score_detail', params: {'scoreModel': item, 'type': 'search'});
                                  } else {
                                    goPage(url: 'score_detail', params: {'scoreModel': item, 'type': 'reserve'});
                                  }
                                },
                              );
                            }),
                          )
                        ],
                      );
                    }));
  }

  //! 主播列表
  anchorListWidget() {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '已更新最新主播！',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多数据");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请点击重新加载");
            } else {
              body = Text("没有更多数据");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        controller: _refreshController_Anchor,
        child: _anchorUserList == null
            ? LoadingContainer(txt: '正在搜索中...')
            : _anchorUserList!.length < 1
                ? EmptyContainer(txt: '暂无搜索结果')
                : SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.only(top: 10.w),
                      padding: EdgeInsets.only(bottom: 30.w),
                      child: Column(children: [
                        for (int i = 0; i < _anchorUserList!.length; i++)
                          Container(
                            padding: EdgeInsets.only(top: 12.w, bottom: 12.w, left: 15.w, right: 15.w),
                            margin: EdgeInsets.only(bottom: 5.w),
                            height: 68.w,
                            color: c_white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    ClipOval(
                                      child: _anchorUserList![i].userLogo == null
                                          ? LocalImageSelector.getSingleImage('common_team_default',
                                              imageWidth: 38.w, imageHeight: 38.w)
                                          : Image(
                                              image: NetworkImage(
                                                _anchorUserList![i].userLogo as String,
                                              ),
                                              width: 38.w,
                                              height: 38.w,
                                              fit: BoxFit.cover,
                                            ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 10.w),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            _anchorUserList![i].nickName as String,
                                            style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                "ID:${_anchorUserList![i].userId.toString()}",
                                                style: TextStyle(color: c_6, fontSize: 14.sp),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(left: 10.w),
                                                child: Text(
                                                  "粉丝数 ${filterNumChina(_anchorUserList![i].follow)}",
                                                  style: TextStyle(color: c_6, fontSize: 14.sp),
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                GestureDetector(
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: 50.w,
                                    height: 22.w,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50.w),
                                      border: Border.all(color: _anchorUserList![i].isFollow ? c_9 : c_o),
                                    ),
                                    child: Text(
                                      _anchorUserList![i].isFollow ? '已关注' : "关注",
                                      style:
                                          TextStyle(color: _anchorUserList![i].isFollow ? c_9 : c_o, fontSize: 10.sp),
                                    ),
                                  ),
                                  onTap: () async {
                                    Map? userInfo = await localStorage.getItem('userInfo');
                                    if (userInfo == null) return goPage(url: 'login');

                                    if (_anchorUserList![i].isFollow) {
                                      showFadeDialog(
                                          content: Text(
                                            '确定取消关注?',
                                            style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                                          ),
                                          isShowCancelBtn: true,
                                          isShowConfirmBtn: true,
                                          cancelTextColor: c_o,
                                          confirmCallback: () async {
                                            await _cancelFollowAnchor(_anchorUserList![i]);
                                            showToast('已取消关注');
                                            Navigator.pop(context);
                                          });
                                    } else {
                                      await _followAnchor(_anchorUserList![i]);
                                      showToast('关注成功');
                                    }
                                  },
                                ),
                              ],
                            ),
                          )
                      ]),
                    ),
                  ));
  }

  //! 历史搜索记录
  Widget searchHistoryOrHotListWidget() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("历史记录", style: TextStyle(color: c_6, fontSize: 18.sp, fontWeight: FontWeight.w500)),
                if (searchHistory)
                  GestureDetector(
                    child: Row(
                      children: [
                        LocalImageSelector.getSingleImage('home_live_delete_icon', imageWidth: 20.w, imageHeight: 20.w),
                        Text("清空", style: TextStyle(color: c_9, fontSize: 16.sp, fontWeight: FontWeight.w500))
                      ],
                    ),
                    onTap: () {
                      localStorage.deleteItem('history');
                      _historySearchList = [];
                      setState(() {});
                    },
                  )
              ],
            ),
            _historySearchList.length > 0
                ? Container(
                    margin: EdgeInsets.only(top: 20.w),
                    alignment: Alignment.topLeft,
                    child: Wrap(
                      spacing: 10.w,
                      runSpacing: 10.w,
                      children: [
                        for (int i = 0; i < _historySearchList.length; i++)
                          GestureDetector(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 12.w),
                              height: 24.w,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2.w),
                                border: Border.all(color: c_b1),
                              ),
                              child: Text(
                                _historySearchList[i],
                                style: TextStyle(color: c_6, fontSize: 14.sp, height: 1.5),
                              ),
                            ),
                            onTap: () {
                              _searchController.value = _searchController.value.copyWith(
                                text: _historySearchList[i],
                              );
                              _isSearching = true;
                              _isLoading = true;
                              setState(() {});
                              _getData(false);
                            },
                          ),
                      ],
                    ),
                  )
                : Expanded(
                    child: EmptyContainer(
                      txt: '暂无历史搜索记录',
                    ),
                  )
          ],
        ),
      ),
    );
  }

  //! 直播卡片列表
  cardItem() {
    return _anchorList!
        .map(
          (item) => VideoItem(
              renderItem: item,
              clickCallBack: () {
                if (sessionStorage.state.live.isMiniPlay) {
                  goPage(url: 'live_detail', params: {
                    'enterType': 'playNormalVideo',
                    'userId': item.userId,
                    'item': item,
                  });
                } else {
                  goPage(url: 'live_detail', params: {
                    'userId': item.userId,
                    'item': item,
                  });
                }
              }),
        )
        .toList();
  }

  //- 获取历史搜索记录
  void _getSearchHistoryList() async {
    var history = await localStorage.getItem('history');
    List result = history != null ? history['searchHistoryList'] : [];
    //- 去除重复项跟空值
    result.removeWhere((element) => (element as String).trim().length < 1);
    var r = result.toSet().toList();
    setState(() => _historySearchList = r);
  }

  //- 初始化tabController
  void _initTabController() {
    tabController = TabController(length: _navList.length, vsync: this)
      ..addListener(() {
        if (tabController.index.toDouble() == tabController.animation!.value) {
          _currentSelectIndex = _navList[tabController.index]['index'];
          setState(() {});
          if (_searchController.text.isNotEmpty) _getData(false);
        }
      });
  }

  //- 获取各分类列表数据
  Future _getData(bool loadMore) async {
    switch (_currentSelectIndex) {
      case 0:
        if (loadMore) {
          currPage_live++;
          _refreshController_live.resetNoData();
        } else {
          currPage_live = 1;
          _refreshController_live.resetNoData();
        }
        await _getListMiqAnchorEvent();
        break;
      // case 1:
      //   if (loadMore) {
      //     int hasLen = 0;
      //     _matchFilterData.values.forEach((element) => hasLen += element.length);
      //     if (_matchTotal != 0 && _matchTotal <= hasLen) {
      //       return _refreshController_mathch.loadNoData();
      //     }
      //     currPage_mathch++;
      //     _refreshController_mathch.resetNoData();
      //   } else {
      //     currPage_mathch = 1;
      //     _refreshController_mathch.resetNoData();
      //     _matchFilterData = {}; //- 清空保存的原数据
      //   }
      //   await _getListMiqAllEvent();
      //   break;
      case 1:
        if (loadMore) {
          currPage_Anchor++;
          _refreshController_mathch.resetNoData();
        } else {
          currPage_Anchor = 1;
          _refreshController_Anchor.resetNoData();
        }
        await _getListMiqAnchor();
        break;
    }
  }

  //- 获取直播数据
  Future _getListMiqAnchorEvent() async {
    AnchorListMod.Data data = await api.listMiqEvent(
        {'pageSize': 20, 'pageNo': currPage_live.toString(), 'catId': '', 'content': _searchController.text});
    _isLoading = false;

    if (currPage_live == 1) {
      _anchorList?.clear();
      _anchorList = data.list;
      _refreshController_live.refreshCompleted();
    } else {
      if (data.list!.length > 0) {
        _anchorList!.addAll(data.list!);
        _refreshController_live.loadComplete();
      } else {
        currPage_live -= 1;
        _refreshController_live.loadNoData();
      }
    }
    setState(() => null);
  }

  //- 获取赛事数据
  _getListMiqAllEvent() async {
    if (_matchFilterData.isEmpty)
      setState(() {
        _matchFilterData = {};
        _matchList = null;
      });

    MatchListMod.Data r = await api.listMiqAllEvent(
        {'pageSize': 10, 'pageNo': currPage_mathch.toString(), 'catId': '', 'content': _searchController.text});

    if (currPage_mathch == 1) {
      _matchList?.clear();
      _matchList = r.list;
      _refreshController_mathch.refreshCompleted();
    } else {
      if (r.list!.length > 0) {
        _matchList = [];
        _matchList!.addAll(r.list!);
        _refreshController_mathch.loadComplete();
      } else {
        currPage_mathch -= 1;
        _refreshController_mathch.loadNoData();
      }
    }
    _matchList!.forEach((item) {
      if (_matchFilterData[item.matchDate] == null) {
        _matchFilterData[item.matchDate as String] = [];
      }
      _matchFilterData[item.matchDate as String]!.add(item);
    });

    _matchTotal = r.totalCount ?? 0;
    setState(() => null);
  }

  //- 获取主播数据
  _getListMiqAnchor() async {
    AnchorSearchMod.Data anchorData = await api.listMiqAnchor(
        {'pageSize': 100, 'pageNo': currPage_Anchor.toString(), 'catId': '', 'content': _searchController.text});
    anchorData.list!.forEach((element) {
      element.isFollow = _alreadyFollowLost.contains(element.userId);
    });

    if (currPage_Anchor == 1) {
      _anchorUserList?.clear();
      _anchorUserList = anchorData.list;
      _refreshController_Anchor.refreshCompleted();
    } else {
      if (anchorData.list!.length > 0) {
        _anchorUserList!.addAll(anchorData.list!);
        _refreshController_Anchor.loadComplete();
      } else {
        currPage_Anchor -= 1;
        _refreshController_Anchor.loadNoData();
      }
    }
    setState(() {});
  }

  //- 添加预约
  Future _userReserve(MatchListMod.MatchList item) async {
    await api.resva({'matchId': item.matchId});
    item.resva = 1;
    setState(() {});
    showToast('预约成功');
  }

  //- 取消预约
  Future _cancelUserReserve(MatchListMod.MatchList item) async {
    bool r = await api.cancleResva({'matchId': item.matchId});
    if (r) {
      showToast('已取消本场比赛赛事预约');
      item.resva = 0;
      setState(() {});
    }
  }

  //- 关注主播
  Future _followAnchor(AnchorSearchMod.AnchorList anchorList) async {
    await api.follow({
      'userId': anchorList.userId,
      'terminal': 1,
      'fakeState': anchorList.fakeState,
    });
    _anchorUserList!.forEach((element) {
      if (element.userId == anchorList.userId && element.isFollow == false) {
        element.isFollow = true;
      }
    });
    setState(() {});
  }

  //- 取消主播关注
  Future _cancelFollowAnchor(AnchorSearchMod.AnchorList anchorList) async {
    await api.cancleFollow({'userId': anchorList.userId, 'terminal': 1, 'fakeState': anchorList.fakeState});
    _anchorUserList!.forEach((element) {
      if (element.userId == anchorList.userId && element.isFollow == true) {
        element.isFollow = false;
      }
    });
    setState(() {});
  }

  //- 获取已经关注主播列表
  Future _getAlreadyFollowList() async {
    Map? userInfo = await localStorage.getItem('userInfo');
    if (userInfo == null) return;
    List tmpList = await api.listFollow({'pageNo': 1, 'pageSize': 120});
    if (tmpList.length == 0) return;
    _alreadyFollowLost = tmpList.map((e) => e['userId']).toList();
    setState(() {});
  }
}
