// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/api/index.dart' as api;
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/model/follow_list_mod.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/utils/my_underline_indicator.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/model/reserve_list_mod.dart' as ReserveMod;
import 'package:liveapp/utils/number_format.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class FocusPage extends BaseWidget {
  FocusPage({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _FocusPageState();
  }
}

class _FocusPageState extends BaseWidgetState<FocusPage> with TickerProviderStateMixin {
  late TabController tabController;
  List _navList = ['我的预约', '我关注的主播'];
  int initialIndex = 0;
  List<ReserveMod.ReserveList> _tempList = [];
  Map<String, List<ReserveMod.ReserveList>> _reserveData = {};
  bool _isLoading = false;
  List<FollowListMod> _followList = [];

  int _pageSize = 20; //- 当前列表长度
  int _pageNo = 1; //- 当前请求页数
  int _verbPageNo = 1; //- 预约当前页

  RefreshController _refreshController = RefreshController(initialRefresh: false);
  RefreshController _verbRefreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async {
    _pageNo = 1;
    _refreshController.resetNoData();
    _listFollow();
  }

  void _onLoading() async {
    _pageNo++;
    _listFollow();
  }

  void _verbOnRefresh() async {
    _verbPageNo = 1;
    _verbRefreshController.resetNoData();
    _initReserveList();
  }

  void _verbOnLoading() async {
    _verbPageNo++;
    _initReserveList();
  }

  @override
  void initState() {
    tabController = TabController(initialIndex: initialIndex, length: _navList.length, vsync: this);
    _init();

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    initialIndex = int.parse((ModalRoute.of(this.context)!.settings.arguments as Map)['type'] ?? '0');
    tabController = TabController(initialIndex: initialIndex, length: _navList.length, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      appBar: getAppBar(
        title: TabBar(
          labelColor: c_6,
          labelStyle: TextStyle(fontSize: 16.sp),
          unselectedLabelColor: c_9,
          unselectedLabelStyle: TextStyle(fontSize: 16.sp),
          isScrollable: true,
          indicator: TabSizeIndicator(
            wantWidth: 68.w,
            borderSide: BorderSide(width: 4.0.w, color: c_o),
            insets: EdgeInsets.only(
              bottom: -3.w,
            ),
          ),
          tabs: _navList.map((e) => Tab(text: e)).toList(),
          controller: tabController,
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          reserve(),
          followAnchor(),
        ],
      ),
    );
  }

  //! 预约列表
  Widget reserve() {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '已更新最新预约列表！',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请点击重新加载");
            } else {
              body = Text("没有更多");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _verbOnRefresh,
        onLoading: _verbOnLoading,
        controller: _verbRefreshController,
        child: _isLoading
            ? LoadingContainer()
            : _reserveData.isEmpty
                ? EmptyContainer(txt: '暂无预约赛事')
                : Container(
                    padding: EdgeInsets.only(bottom: 30.w),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          for (var i = 0; i < _reserveData.length; i++)
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(top: 10.w),
                              child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(bottom: 5.w),
                                    child: Text(
                                      _reserveData.keys.toList()[i],
                                      style: TextStyle(color: c_9),
                                    ),
                                  ),
                                  for (int j = 0; j < _reserveData.values.toList()[i].length; j++)
                                    Container(
                                      margin: EdgeInsets.only(bottom: 5.w),
                                      color: c_white,
                                      height: 90.w,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 260.w,
                                            padding: EdgeInsets.only(top: 14.w, bottom: 15.w, left: 28.w, right: 24.w),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    _teamInfo(_reserveData.values.toList()[i][j].homeLogo,
                                                        _reserveData.values.toList()[i][j].homeName),
                                                    _teamInfo(_reserveData.values.toList()[i][j].awayLogo,
                                                        _reserveData.values.toList()[i][j].awayName),
                                                  ],
                                                ),
                                                GestureDetector(
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    width: 50.w,
                                                    height: 22.w,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(50.w),
                                                      border: Border.all(color: c_9),
                                                    ),
                                                    child: Text(
                                                      "已预约",
                                                      style: TextStyle(color: c_9, fontSize: 10.sp),
                                                    ),
                                                  ),
                                                  onTap: () {
                                                    showFadeDialog(
                                                        content: Text(
                                                          '确定取消预约本场比赛？',
                                                          style: TextStyle(
                                                              color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                                                        ),
                                                        isShowCancelBtn: true,
                                                        isShowConfirmBtn: true,
                                                        cancelTextColor: c_o,
                                                        confirmCallback: () {
                                                          _deleteCancelMatch(_reserveData.keys.toList()[i],
                                                              _reserveData.values.toList()[i][j].matchId);
                                                          Navigator.pop(context);
                                                        });
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(width: 1.w, height: 71.w, color: c_b1),
                                          Container(
                                            alignment: Alignment.centerRight,
                                            width: 90.w,
                                            margin: EdgeInsets.only(left: 10.w, right: 10.w),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                Container(
                                                  constraints: BoxConstraints(maxWidth: 100.w),
                                                  child: Text(
                                                    _reserveData.values.toList()[i][j].leagueName ?? '',
                                                    style: TextStyle(
                                                        overflow: TextOverflow.ellipsis,
                                                        fontWeight: FontWeight.w500,
                                                        color: c_6),
                                                  ),
                                                ),
                                                Text((_reserveData.values.toList()[i][j].matchTime as String).substring(
                                                    0, _reserveData.values.toList()[i][j].matchTime!.length - 3)),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                ],
                              ),
                            ),
                        ],
                      ),
                    ),
                  ));
  }

  void _init() {
    _initReserveList();
    _listFollow();
  }

  //- 关注列表
  Widget followAnchor() {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '已更新最新关注列表！',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请点击重新加载");
            } else {
              body = Text("没有更多");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        controller: _refreshController,
        child: _followList.length == 0
            ? EmptyContainer(txt: '暂无关注主播')
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.only(top: 10.w),
                  padding: EdgeInsets.only(bottom: 30.w),
                  child: Column(children: [
                    for (int i = 0; i < _followList.length; i++)
                      Container(
                        padding: EdgeInsets.only(top: 12.w, bottom: 12.w, left: 15.w, right: 15.w),
                        margin: EdgeInsets.only(bottom: 5.w),
                        height: 68.w,
                        color: c_white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                _followList[i].userLogo == null
                                    ? ClipOval(
                                        child:
                                            LocalImageSelector.getSingleImage('common_team_default', imageWidth: 38.w))
                                    : ClipOval(
                                        child: Image.network(
                                          _followList[i].userLogo,
                                          width: 38.w,
                                          height: 38.w,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                Container(
                                  margin: EdgeInsets.only(left: 10.w),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        _followList[i].nickName ?? '',
                                        style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "ID:${_followList[i].userId}",
                                            style: TextStyle(color: c_6, fontSize: 14.sp),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10.w),
                                            child: Text(
                                              "粉丝数 ${filterNumChina(_followList[i].follow)}",
                                              style: TextStyle(color: c_6, fontSize: 14.sp),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            GestureDetector(
                              child: Container(
                                alignment: Alignment.center,
                                width: 50.w,
                                height: 22.w,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50.w),
                                  border: Border.all(color: c_9),
                                ),
                                child: Text(
                                  "已关注",
                                  style: TextStyle(color: c_9, fontSize: 10.sp),
                                ),
                              ),
                              onTap: () {
                                showFadeDialog(
                                    content: Text(
                                      '确定取消关注?',
                                      style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                                    ),
                                    isShowCancelBtn: true,
                                    isShowConfirmBtn: true,
                                    cancelTextColor: c_o,
                                    confirmCallback: () async {
                                      await _cancelFollow(_followList[i]);
                                      showToast('已取消关注');
                                      Navigator.pop(context);
                                    });
                              },
                            ),
                          ],
                        ),
                      )
                  ]),
                ),
              ));
  }

  //- 预约记录获取
  Future _initReserveList() async {
    _isLoading = true;
    ReserveMod.Data? data =
        await api.listResvaEvent({'pageNo': _verbPageNo.toString(), 'pageSize': _pageSize.toString()});
    _isLoading = false;

    if (data == null) {
      _verbRefreshController.refreshFailed();
      return;
    }
    ;
    _tempList = data.list as List<ReserveMod.ReserveList>;

    if (_verbPageNo == 1) {
      _reserveData.clear();
      _tempList.forEach((item) {
        if (_reserveData[item.matchDate] == null) {
          _reserveData[item.matchDate as String] = [];
        }
        _reserveData[item.matchDate as String]!.add(item);
      });
      _verbRefreshController.refreshCompleted();
    } else {
      if (_tempList.length > 0) {
        _tempList.forEach((item) {
          if (_reserveData[item.matchDate] == null) {
            _reserveData[item.matchDate as String] = [];
          }
          _reserveData[item.matchDate as String]!.add(item);
        });
        _verbRefreshController.loadComplete();
      } else {
        _verbPageNo -= 1;
        _verbRefreshController.loadNoData();
      }
    }
    if (mounted) setState(() {});
  }

  Widget _teamInfo(logo, temName) {
    return Container(
      child: Row(
        children: [
          logo != null
              ? Image.network(logo, width: 24.w)
              : LocalImageSelector.getSingleImage('common_team_default', imageWidth: 24.w),
          Container(
            constraints: BoxConstraints(maxWidth: 120.w),
            margin: EdgeInsets.only(left: 5.w),
            child: Text(temName,
                style: TextStyle(
                  overflow: TextOverflow.ellipsis,
                  fontWeight: FontWeight.w500,
                  color: c_6,
                  fontSize: 15.sp,
                )),
          ),
        ],
      ),
    );
  }

  Future _deleteCancelMatch(key, matchId) async {
    bool r = await api.cancleResva({'matchId': matchId});
    if (r) {
      showToast('已取消本场比赛赛事预约');
      _reserveData[key]!.removeWhere((item) => item.matchId == matchId);
      bool isEmpty = _reserveData.values.toList().every((item) => item.length < 1);
      if (isEmpty) _reserveData = {};
      setState(() {});
    }
  }

  //- 关注列表
  Future _listFollow() async {
    List tmpList = await api.listFollow({
      'pageNo': _pageNo.toString(),
      'pageSize': _pageSize.toString(),
    });

    if (_pageNo == 1) {
      _followList.clear();
      tmpList.forEach((element) {
        _followList.add(FollowListMod.fromJson(element));
      });
      _refreshController.refreshCompleted();
    } else {
      if (tmpList.length > 0) {
        tmpList.forEach((element) {
          _followList.add(FollowListMod.fromJson(element));
        });
        _refreshController.loadComplete();
      } else {
        _pageNo -= 1;
        _refreshController.loadNoData();
      }
    }
    if (mounted) setState(() {});
  }

  //- 取消关注主播
  Future _cancelFollow(FollowListMod followList) async {
    await api.cancleFollow({
      'userId': followList.userId,
      'terminal': 1,
      'fakeState': followList.fakeState,
    });
    _followList.removeWhere((element) => element.userId == followList.userId);
    setState(() {});
  }
}
