// ignore_for_file: import_of_legacy_library_into_null_safe, unused_import, unnecessary_import

import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/main.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart' as AnchorListMod;
import 'package:liveapp/model/banner_mod.dart' as BannerModal;
import 'package:liveapp/model/recommend_mod.dart' as RecommendMod;
import 'package:liveapp/pages/live/home/component/b_onlive/video_list.dart';
import 'package:liveapp/pages/live/home/component/common/video_item.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/store/session_storage/state_user.dart';
import 'package:liveapp/utils/my_underline_indicator.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:liveapp/model/tab_data_mod.dart';
import 'package:liveapp/api/index.dart' as api;
import 'package:liveapp/model/live_preview_modal.dart' as LivePreMod;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class OnLivePage extends BaseWidget {
  OnLivePage({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _OnLivePageState();
  }
}

class _OnLivePageState extends BaseWidgetState<OnLivePage> with TickerProviderStateMixin, RouteAware {
  late TabController tabController;
  List<DataMap> tabs = [
    DataMap(val: '推荐', key: ''),
    DataMap(val: '足球', key: '1'),
    DataMap(val: '篮球', key: '2'),
    DataMap(val: '电竞', key: '37'),
    DataMap(val: '其他', key: 'other'),
  ];
  List<RecommendMod.AnchorList> _recommendList = [];
  List<BannerModal.Page> _bannerList = [];

  var _catId = ''; //- 当前直播列表分类
  bool isLoading = false; //- 当前是否在进行数据请求

  RefreshController _refreshController = RefreshController(initialRefresh: false);
  int _pageSize = 10; //- 当前列表长度
  int _pageNo = 1; //- 当前请求页数
  String reloadType = '';
  Timer? timer;
  bool isFirstOpen = true;
  int _totalCount = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    tabController = TabController(length: 5, vsync: this)
      ..addListener(() {
        if (tabController.index.toDouble() == tabController.animation!.value) {
          _catId = tabs[tabController.index].key;
          _pageNo = 1; //- 切换界面调整起始页
          _totalCount = 0;
          _initLiveList();
        }
      });
    _initLiveList();
    _getBannerList();
    // _getRecommendList();
    _watchLoop();
    _loopRecommendList();
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    tabController.dispose();
    if (timer != null) timer!.cancel();
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(child: LocalImageSelector.getSingleImage("home_live_bg")),
          Container(
            padding: EdgeInsets.only(top: getTopBarHeight() + 10.w),
            width: double.infinity,
            child: Column(
              children: [
                _searchContainer,
                _TabNav,
                SizedBox(height: 10.w),
                _viewBody(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  BoxDecoration get decoration => BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 0.15),
        borderRadius: BorderRadius.circular(15.w),
      );

  //- 头部内容区域
  Widget get _searchContainer => Container(
        padding: EdgeInsets.only(left: 10.w, right: 15.w, bottom: 10.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
                child: Container(
                  width: 280.w,
                  height: 30.w,
                  decoration: decoration,
                  child: Row(children: [
                    Container(
                      margin: EdgeInsets.only(left: 10.w, right: 10.w),
                      width: 18.w,
                      height: 18.w,
                      child: Image(image: AssetImage('assets/images/onlive/home_live_search.png')),
                    ),
                    Text(
                      '主播，赛事',
                      style: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.4), fontSize: 14.sp),
                    )
                  ]),
                ),
                onTap: () {
                  goPage(url: 'live_search');
                }),
            GestureDetector(
              child: Container(
                width: 60.w,
                height: 30.w,
                alignment: Alignment.center,
                decoration: decoration,
                child: Text('关注', style: TextStyle(fontSize: 14.sp, color: c_w)),
              ),
              onTap: () async {
                if (await async_checkedUserStatus()) {
                  goPage(url: 'focus', params: {
                    'type': '0',
                  });
                } else {
                  goPage(url: 'login');
                }
              },
            )
          ],
        ),
      );

  //- 导航切换按钮
  Widget get _TabNav => Theme(
        data: ThemeData(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
        child: TabBar(
          tabs: tabs.map((item) {
            var tp = TextPainter(
              textDirection: TextDirection.ltr,
              text: TextSpan(
                text: item.val,
                style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
              ),
            )..layout();
            return Tab(
              child: Container(
                alignment: Alignment.center,
                width: 50.w,
                child: Text(item.val),
              ),
            );
          }).toList(),
          labelPadding: EdgeInsets.symmetric(horizontal: 10.w),
          isScrollable: true,
          labelColor: c_w,
          unselectedLabelColor: c_w,
          indicator: TabSizeIndicator(
            wantWidth: 30.w,
            borderSide: BorderSide(width: 4.0.w, color: c_w),
          ),
          labelStyle: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
          unselectedLabelStyle: TextStyle(fontSize: 16.sp),
          controller: tabController,
        ),
      );

  //- 容器
  Widget _viewBody() {
    return Flexible(
      child: TabBarView(
        controller: tabController,
        children: [
          for (int i = 0; i < tabs.length; i++)
            Container(
              child: Column(
                children: [
                  if (i == 0 && (tabs[i].contentList == null || tabs[i].contentList!.length < 1))
                    _bannerList.length < 1
                        ? LocalImageSelector.getSingleImage('common_default_bg',
                            imageHeight: 155.w, bFitFill: BoxFit.fitWidth, imageWidth: double.infinity)
                        : _bannerWidget(),
                  // if (i == 0 &&
                  //     _recommendList.length > 0 &&
                  //     (tabs[i].contentList == null || tabs[i].contentList!.length < 1))
                  //   _recommendWidget(),
                  if (i != 0 || tabs[i].contentList == null || tabs[i].contentList!.length < 1)
                    Container(
                      margin: EdgeInsets.all(10.w),
                      child: Row(
                        children: [
                          Container(
                            width: 19.w,
                            height: 18.w,
                            child: LocalImageSelector.getSingleImage('home_live_icon'),
                            margin: EdgeInsets.only(right: 4.w),
                          ),
                          Text(
                            '正在直播',
                            style: TextStyle(color: c_3, fontSize: 16.sp, fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ),
                  isLoading || tabs[i].contentList == null
                      ? Expanded(child: LoadingContainer())
                      : tabs[i].contentList!.length < 1
                          ? Expanded(child: EmptyContainer(txt: '暂无直播'))
                          : VideoList(
                              contentList: tabs[i].contentList as List,
                              onRefresh: _onRefresh,
                              onLoading: _onLoading,
                              reloadType: reloadType,
                              selectIndex: i,
                              bannerWidget: _bannerList.length < 1
                                  ? LocalImageSelector.getSingleImage('common_default_bg',
                                      imageHeight: 155.w, bFitFill: BoxFit.fitWidth, imageWidth: double.infinity)
                                  : _bannerWidget(),
                              // recommendWidget: _recommendWidget(),
                            )
                ],
              ),
            )
        ],
      ),
    );
  }

  //-!banner图片
  Widget _bannerWidget() {
    List renderList = _bannerList
        .map(
          (e) => ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl: e.picMbPath as String,
              placeholder: (BuildContext, String) {
                return LocalImageSelector.getSingleImage('common_team_default');
              },
            ),
          ),
        )
        .toList();

    return Container(
      height: 155.w,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.w)),
      child: Swiper(
        autoplayDelay: 5000,
        itemBuilder: (BuildContext context, int index) => renderList[index],
        itemCount: renderList.length,
        autoplay: true,
        itemWidth: 336.w,
        viewportFraction: 0.85,
        scale: 0.9,
        onTap: (index) {
          if (_bannerList[index].outStation != null && _bannerList[index].picTarget == 1) {
            // final url = Uri.parse(_bannerList[index].outStation);
            // launchUrl(
            //   url, /*  forceSafariVC: false */
            // );
            launch(_bannerList[index].outStation, forceSafariVC: false);
          }
        },
      ),
    );
  }

  //! 推荐赛事列表
/*   Widget _recommendWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            for (var i = 0; i < _recommendList.length; i++)
              GestureDetector(
                child: Container(
                  width: 180.w,
                  height: 80.w,
                  margin: EdgeInsets.only(left: 5.w, right: 5.w, top: 10.w),
                  padding: EdgeInsets.all(10.w),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.w),
                    color: c_white,
                  ),
                  child: Stack(children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        for (int j = 0; j < 2; j++)
                          Expanded(
                            child: Row(
                              children: [
                                j == 0
                                    ? Container(
                                        width: 16.w,
                                        height: 16.w,
                                        child: _recommendList[i].homeLogo == null
                                            ? LocalImageSelector.getSingleImage('common_team_default')
                                            : CachedNetworkImage(
                                                imageUrl: _recommendList[i].homeLogo,
                                                errorWidget: (BuildContext, String, dynamic) {
                                                  return LocalImageSelector.getSingleImage('common_team_default');
                                                },
                                              ),
                                      )
                                    : Container(
                                        width: 16.w,
                                        height: 16.w,
                                        child: _recommendList[i].awayLogo == null
                                            ? LocalImageSelector.getSingleImage('common_team_default')
                                            : CachedNetworkImage(
                                                imageUrl: _recommendList[i].awayLogo,
                                                errorWidget: (BuildContext, String, dynamic) {
                                                  return LocalImageSelector.getSingleImage('common_team_default');
                                                },
                                              ),
                                      ),
                                Container(
                                  margin: EdgeInsets.only(left: 5.w),
                                  constraints: BoxConstraints(maxWidth: 89.w),
                                  child: Text(
                                    j == 0
                                        ? _recommendList[i].homeName as String
                                        : _recommendList[i].awayName as String,
                                    style: TextStyle(fontSize: 12.sp, color: c_6, overflow: TextOverflow.ellipsis),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        Container(
                          margin: EdgeInsets.only(left: 21.w),
                          child: Text(
                            (_recommendList[i].matchDateTime as String)
                                .substring(0, _recommendList[i].matchDateTime!.length - 3),
                            style: TextStyle(fontSize: 12.sp, color: c_9),
                          ),
                        )
                      ],
                    ),
                    Positioned(
                      right: 0.w,
                      top: 0.w,
                      child: GestureDetector(
                        child: Container(
                          width: 50.w,
                          height: 20.w,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: _recommendList[i].matchState == '2' || _recommendList[i].matchState == '4'
                                  ? c_o
                                  : c_white,
                              borderRadius: BorderRadius.circular(10.w),
                              border: Border.all(color: _recommendList[i].resva == 1 ? c_9 : c_o)),
                          child: Text(
                            _recommendList[i].matchState == '1'
                                ? '已结束'
                                : _recommendList[i].resva == 0
                                    ? '预约'
                                    : '已预约',
                            style: TextStyle(
                                color: _recommendList[i].matchState == '2' || _recommendList[i].matchState == '4'
                                    ? c_white
                                    : _recommendList[i].resva == 1
                                        ? c_6
                                        : c_o,
                                fontSize: 10.sp),
                          ),
                        ),
                        onTap: () async {
                          Map? userInfo = await localStorage.getItem('userInfo');
                          if (userInfo == null) return goPage(url: 'login');
                          if (_recommendList[i].resva == 0) {
                            _userReserve(_recommendList[i]);
                          } else if (_recommendList[i].resva == 1) {
                            showFadeDialog(
                                content: Text(
                                  '确定取消预约本场比赛？',
                                  style: TextStyle(color: c_6, fontSize: 16.sp, fontWeight: FontWeight.w500),
                                ),
                                isShowCancelBtn: true,
                                isShowConfirmBtn: true,
                                cancelTextColor: c_o,
                                confirmCallback: () async {
                                  await _cancelUserReserve(_recommendList[i]);
                                  showToast('已取消本场赛事预约');
                                  Navigator.pop(context);
                                });
                          }
                        },
                      ),
                    )
                  ]),
                ),
                onTap: () => _goScoreDetailPage(_recommendList[i]),
              )
          ],
        ),
      ),
    );
  } */

  //- 直播列表获取
  Future _initLiveList() async {
    isLoading = true;
    try {
      AnchorListMod.Data? data = await api.listMiqEvent({
        'pageSize': _pageSize,
        'pageNo': _pageNo,
        'catId': _catId,
        'anchorId': null,
      });
      isLoading = false;
      _totalCount = data!.totalCount;
      if (_pageNo == 1) {
        tabs.firstWhere((e) => e.key == _catId).contentList?.clear();
        tabs.firstWhere((e) => e.key == _catId).contentList = data.list!;
        reloadType = 'refreshCompleted';
      } else {
        if (data != null && data.list!.length > 0) {
          tabs.firstWhere((e) => e.key == _catId).contentList!.addAll(data.list!);
          reloadType = 'loadComplete';
          _refreshController.loadComplete();
        } else {
          _pageNo -= 1;
          reloadType = 'loadNoData';
          _refreshController.loadNoData();
        }
      }

      setState(() {});
    } catch (e) {
      isLoading = false;
      setState(() {});
    }
  }

  //- 获取轮播图数据
  Future _getBannerList() async {
    List<BannerModal.Page> res = await api.getBannerList();
    if (res.length < 1) return;
    setState(() => {_bannerList = res});
  }

  //- 获取推荐列表
  // Future _getRecommendList() async {
  //   try {
  //     RecommendMod.Data data = await api.getReserveLiveEventList({
  //       'matchId': null,
  //       'pageNo': 1,
  //       'pageSize': 15,
  //       'roomId': null,
  //     });
  //     _recommendList = data.list ?? [];
  //     _sortRecommendList();
  //     setState(() => null);
  //   } catch (e) {
  //     print('509行打印：============ ${e}');
  //   }
  // }

  //-上拉
  _onRefresh() {
    _pageNo = 1;
    _refreshController.resetNoData();
    _initLiveList();
  }

  //- 下拉
  _onLoading() {
    DataMap data = tabs.firstWhere((e) => e.key == _catId);
    if (_totalCount <= data.contentList!.length) {
      reloadType = 'loadNoData';
      _refreshController.loadNoData();
      setState(() {});
      return;
    }
    _pageNo++;
    _initLiveList();
  }

  //- 添加预约
  Future _userReserve(RecommendMod.AnchorList recommendList) async {
    await api.resva({'matchId': recommendList.matchId});
    recommendList.resva = 1;
    _sortRecommendList();
    setState(() {});
    showToast('预约成功');
  }

  //- 取消预约
  Future _cancelUserReserve(RecommendMod.AnchorList recommendList) async {
    bool r = await api.cancleResva({'matchId': recommendList.matchId});
    if (r) {
      showToast('已取消本场比赛赛事预约');
      recommendList.resva = 0;
      _sortRecommendList();
      setState(() {});
    }
  }

  //- 排序推荐列表
  void _sortRecommendList() {
    _recommendList.sort((pre, next) => next.resva!.compareTo(pre.resva!));
  }

  //- 跳转比分详情界面
  _goScoreDetailPage(RecommendMod.AnchorList recommendList) {
    goPage(url: 'score_detail', params: {'scoreModel': recommendList, 'type': 'reserve'});
  }

  //- 循环推荐列表
  void _loopRecommendList() {
    timer = Timer.periodic(Duration(seconds: 5), (timer) {
      // _getRecommendList();
    });
  }

  void _watchLoop() {
    bus.on(EventBus.WATCHRECOMMENDLOOP, (arg) {
      if (isFirstOpen) {
        isFirstOpen = false;
      } else {
        if (arg == 'stop') {
          timer!.cancel();
        } else {
          // _loopRecommendList();
        }
      }
    });
  }
}
