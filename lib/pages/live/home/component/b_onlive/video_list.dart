import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/pages/live/home/component/common/video_item.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:liveapp/pages/live/home/liveDetail/ChannelManager.dart';

class VideoList extends BaseWidget {
  final List contentList;
  Function onRefresh;
  Function onLoading;
  final reloadType;
  int selectIndex;
  final Widget? bannerWidget;
  // final Widget? recommendWidget;
  VideoList({
    Key? key,
    required this.contentList,
    required this.onRefresh,
    required this.onLoading,
    required this.reloadType,
    required this.selectIndex,
    this.bannerWidget,
    // this.recommendWidget,
  }) : super(key: key);

  @override
  BaseWidgetState<VideoList> getState() => _VideoListState();
}

class _VideoListState extends BaseWidgetState<VideoList> {
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void didUpdateWidget(covariant VideoList oldWidget) {
    switch (widget.reloadType) {
      case 'refreshCompleted':
        _refreshController.refreshCompleted();
        break;
      case 'loadComplete':
        _refreshController.loadComplete();
        break;
      case 'loadNoData':
        _refreshController.loadNoData();
        break;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget PageBody(AppState state) {
    return Expanded(
      child: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: ClassicHeader(
          height: 45.0,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '刷新完成',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请点击重新加载");
            } else {
              body = Text("没有更多");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: () => widget.onRefresh(),
        onLoading: () => widget.onLoading(),
        controller: _refreshController,
        child: SingleChildScrollView(
          child: widget.selectIndex == 0
              ? Container(
                  child: Column(
                    children: [
                      widget.bannerWidget as Widget,
                      // widget.recommendWidget as Widget,
                      Container(
                        margin: EdgeInsets.all(10.w),
                        child: Row(
                          children: [
                            Container(
                              width: 19.w,
                              height: 18.w,
                              child: LocalImageSelector.getSingleImage('home_live_icon'),
                              margin: EdgeInsets.only(right: 4.w),
                            ),
                            Text(
                              '正在直播',
                              style: TextStyle(color: c_3, fontSize: 16.sp, fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        child: Wrap(
                          spacing: 10.w,
                          runSpacing: 17.w,
                          children: cardItem(widget.contentList),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: 10.w),
                  child: Wrap(
                    spacing: 10.w,
                    runSpacing: 17.w,
                    children: cardItem(widget.contentList),
                  ),
                ),
        ),
      ),
    );
  }

  //- 卡片列表
  List<Widget> cardItem(list) {
    return list
        .map<Widget>(
          (item) => VideoItem(
              renderItem: item,
              clickCallBack: () {
                if (sessionStorage.state.live.isMiniPlay) {
                  ChannelManager.closePage();
                  sessionStorage.dispatch(UpdateIsMiniPlay(payload: {'isMiniPlay': false}));
                }
                goPage(url: 'live_detail', params: {
                  'userId': item.userId,
                  'item': item,
                });
              }),
        )
        .toList();
  }
}
