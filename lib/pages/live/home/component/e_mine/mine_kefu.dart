import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import 'common/base_navigation_view.dart';

class Minekefu extends BaseWidget {
  Minekefu({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _Minekefu();
  }
}

class _Minekefu extends BaseWidgetState<Minekefu> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        backgroundColor: c_w,
        appBar: BaseNavigation("在线客服", "assets/images/mine/mine_nav_kefu.png"),
        body: Container(child: Text("在线客服")));
  }
}