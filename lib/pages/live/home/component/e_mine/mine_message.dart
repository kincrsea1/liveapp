// @dart=2.9
// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors_in_immutables, prefer_const_constructors, missing_return
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mo/message2_model.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mo/message_model.dart';
import 'package:liveapp/pages/live/home/component/e_mine/tools/common/notice_alert.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import '../../../../../common_widget/empty_container.dart';
import '../../../../../config/image/local_image_selector.dart';
import '../../../../../network/dio_util/dio_method.dart';
import '../../../../../network/dio_util/dio_response.dart';
import '../../../../../network/dio_util/dio_util.dart';
import '../../../../../utils/my_underline_indicator.dart';
import 'common/base_navigation_view.dart';

class MineMessage extends BaseWidget {

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _MineMessage();
  }
}

class _MineMessage extends BaseWidgetState<MineMessage> with TickerProviderStateMixin{
  TabController _tabController;
  int _selectedIndex = 0;
  List<String> items = ['收件箱', '通知', '公告'];
  MessageNoticeModel recieveModel;//收件箱
  MessageNoticeModel noticeModel;//通知
  MessageModel gongGaoModel; //公告
  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: items.length, vsync: this);
    _tabController.addListener(() {
      if (_tabController.index == _tabController.animation.value) {
        /// 当然只是给index赋值影响不大,最多重复赋值
        print(_tabController.index);
        if (_tabController.index == 0) {
          //数据
          requestData1();
        }
        if (_tabController.index == 1) {
          //数据
         requestData2();
        }

        if (_tabController.index == 2) {
          //数据
          requestData3();
        }


        setState(() {
          _selectedIndex = _tabController.index;
        });
      }
    });

    requestData();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        backgroundColor: c_border,
        appBar: BaseNavigation("消息中心", "assets/images/mine/mine_nav_kefu.png"),
        body: _mianbody( items));
  }


  _mianbody( List<String> items) {
    return MediaQuery.removePadding(
        removeTop: true,
        removeBottom: true,
        context: context,
        child: Column(
          children: <Widget>[
            Container(
              color:c_border ,
                height: 44.w,
                child: TabBar(
                  controller: _tabController,
                  isScrollable: false,
                  physics: NeverScrollableScrollPhysics(),
                  labelColor: c_6,
                  unselectedLabelColor: c_9,
                  // labelPadding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                  indicator: TabSizeIndicator(

                    ///自定义指示器
                    ///指示器的宽度
                      wantWidth: 40,
                      borderSide: BorderSide(width: 2.0, color: c_o)),
                  tabs: items.map<Tab>((String tab) {
                    return Tab(
                      child: Container(
                        child: Stack(
                          children: [
                            Text(tab,
                                style: TextStyle(
                                  fontSize: 15,
                                )),
                          ],
                        ),
                      ),
                    );
                  }).toList(),
                )),

            ///填充剩下的部分
            Flexible(
              child: TabBarView(
                  controller: _tabController,
                  children: items.map((String Name) {
                    if (Name == '收件箱') {
                      return itemView2();
                    } else if (Name == '通知') {
                      return itemView();
                    } else {
                      //公告
                      return itemView3();
                    }
                  }).toList()),
            )
          ],
        ));
  }


  Widget itemView(){
    if (noticeModel == null ||
        noticeModel.data == null ||
        noticeModel.data.list == null ||
        noticeModel.data.list.length == 0){
     return Container(
       child: EmptyContainer(txt: '暂无通知'),
     );
    }
      return ListView(
        children: [
          Column(
              children: noticeModel.data.list.map((e) {
            return item(e);
          }).toList()),
        ],
      );
  }
//公告
  Widget itemView3(){
    if (gongGaoModel == null ||
        gongGaoModel.page == null ||
        gongGaoModel.page.list == null ||
        gongGaoModel.page.list.length == 0){
      return Container(
        child: EmptyContainer(txt: '暂无公告'),
      );
    }
    return ListView(
      children: [
        Column(
            children: gongGaoModel.page.list.map((e) {
              return item3(e);
            }).toList()),
      ],
    );
  }
  Widget itemView2(){
    if (recieveModel == null ||
        recieveModel.data == null ||
        recieveModel.data.list == null ||
        recieveModel.data.list.length == 0){
      return Container(
        child: EmptyContainer(txt: '暂无消息'),
      );
    }
    return ListView(
      children: [
        Column(
            children:
            recieveModel.data.list.reversed.map((e){
              return item2(e);
            }).toList()

        ),
      ],
    );
  }

  Widget item(List2 list2) {
    String time = '';
    String dateStr = '';
    if (list2.createTime.length > 0) {
      List<String> TimeList = list2.createTime.split(' ').reversed.toList();
      time = TimeList.first;
      dateStr = TimeList.last;
    }
    return Column(
      children: [
        GestureDetector(
          child: Container(
              margin: EdgeInsets.only(left: 0, right: 0),
              decoration: BoxDecoration(color: c_border),
              child: Container(
                height: 56.w,
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.7,
                      margin: EdgeInsets.only(left: 10),
                      child: Text(list2.textContent.replaceAll('<p>', '').replaceAll('</p>', ''),
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 13.sp, color:  c_3,fontWeight:FontWeight.w600 )),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Container(
                        child: Text(time + '\n' + dateStr,
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                color: c_3, fontSize: 12.sp)),
                      ),
                    )
                  ],
                ),
              ),
            ),
          onTap: (){
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return NoticeDetailStyleDialog(NoticeDelegateConfirmCallback,list2);
                });
          },
        ),
        Container(height: 4.w, color: c_b1,)
      ],
    );
  }


  Widget item3(List1 list1) {
    print("createTime:${list1.createTime}||updateTime:${list1.updateTime}");
    String time = '';
    String dateStr = '';
    if(list1.updateTime == null){
    if (list1.createTime.length > 0) {
      List<String> TimeList = list1.createTime.split(' ').reversed.toList();
      time = TimeList.first;
      dateStr = TimeList.last;
    }
    }else{
      if (list1.updateTime.length > 0) {
        List<String> TimeList = list1.updateTime.split(' ').reversed.toList();
        time = TimeList.first;
        dateStr = TimeList.last;
      }
    }
    return Column(
      children: [
        GestureDetector(
          child: Container(
            margin: EdgeInsets.only(left: 0, right: 0),
            decoration: BoxDecoration(color: c_border),
            child: Container(
              height: 56.w,
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    margin: EdgeInsets.only(left: 10),
                    child: Text(list1.noticeTitle,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 13.sp, color:  c_3,fontWeight:FontWeight.w600 )),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    child: Container(
                      child: Text(time + '\n' + dateStr,
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              color: c_3, fontSize: 12.sp)),
                    ),
                  )
                ],
              ),
            ),
          ),
          onTap: (){
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return NoticeDetailStyleDialog(NoticeDelegateConfirmCallback,list1);
                });
          },
        ),
        Container(height: 4.w, color: c_b1,)
      ],
    );
  }

  Widget item2(List2 list2){
    return Column(
      children: [
        Container(height: 30,color: c_border,child: Text(list2.createTime,style: TextStyle(color: c_9,fontSize: 11.sp),),),
          Container(
            margin: EdgeInsets.only(left: 10.w, right: 0),
            decoration: BoxDecoration(color: c_border),
            child: Container(
              // height: 56.w,
              // color: Colors.red,
              alignment: Alignment.center,
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment:CrossAxisAlignment.start,
                children: [
                  LocalImageSelector.getSingleImage("img",imageHeight: 35.w,imageWidth: 35.w),
                  Container(
                    decoration: BoxDecoration(color: c_white,borderRadius:BorderRadius.circular(5) ),
                    width: MediaQuery.of(context).size.width * 0.7,
                    margin: EdgeInsets.only(left: 10,),
                    padding:EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10) ,
                    child: Text(list2.textContent.replaceAll('<p>', '').replaceAll('</p>', ''),

                        style: TextStyle(
                            fontSize: 13.sp, color:  c_3,fontWeight:FontWeight.w600 )),
                  ),

                ],
              ),
            ),
          ),
        Container(height: 20,color: c_border,)


      ],
    );
  }

  ///删除消息
  void NoticeDelegateConfirmCallback() {

  }


  requestData ()async {
    DioUtil().request('/api/user/messageInfo', method: DioMethod.get,
        params: {
          "pageSize": "2000",
          "pageNo": "1",
          "msgType": "1",
          "isRead": "0",
          "mbrIsRead": "0"
        }).then((res) {
      print("收件箱");

      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      print("收件箱${res.data}");
      recieveModel = MessageNoticeModel.fromJson(res.data);
      setState(() {

      });
    }).catchError((error) {
      print('747行打印：============ ${error}');
      // print(error);
    });


    ///api/user/messageInfo
    DioUtil().request('/api/user/messageInfo', method: DioMethod.get,
        params: {
          "pageSize": "200",
          "pageNo": "1",
          "msgType": "2",
          "isRead": "1",
          "mbrIsRead": "0"
        }).then((res) {
      print("通知");

      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      print("通知${res.data}");
      noticeModel = MessageNoticeModel.fromJson(res.data);
      setState(() {

      });
    }).catchError((error) {
      print('747行打印：============ ${error}');
      // print(error);
    });
    ///api/sys/noticeList
    DioUtil().request('/api/sys/noticeList', method: DioMethod.get,
        params: {
          "pageSize": "200",
          "pageNo": "1",
        }).then((res) {
      print("公告");

      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      print("公告${res.data}");
      gongGaoModel = MessageModel.fromJson(res.data);
      setState(() {

      });
    }).catchError((error) {
      print('747行打印：============ ${error}');
      // print(error);
    });

    setState(() {

    });
  }

  requestData1 ()async {
    DioUtil().request('/api/user/messageInfo', method: DioMethod.get,
        params: {
          "pageSize": "2000",
          "pageNo": "1",
          "msgType": "1",
          "isRead": "0",
          "mbrIsRead": "0"
        }).then((res) {
      print("收件箱");

      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      print("收件箱${res.data}");
      recieveModel = MessageNoticeModel.fromJson(res.data);
      setState(() {

      });
    }).catchError((error) {
      print('747行打印：============ ${error}');
      // print(error);
    });
  }


  requestData2 ()async {
    ///api/user/messageInfo
    DioUtil().request('/api/user/messageInfo', method: DioMethod.get,
        params: {
          "pageSize": "200",
          "pageNo": "1",
          "msgType": "2",
          "isRead": "1",
          "mbrIsRead": "0"
        }).then((res) {
      print("通知");

      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      print("通知${res.data}");
      noticeModel = MessageNoticeModel.fromJson(res.data);
      setState(() {

      });
    }).catchError((error) {
      print('747行打印：============ ${error}');
      // print(error);
    });
  }


  requestData3 ()async{
    ///api/sys/noticeList
    DioUtil().request('/api/sys/noticeList', method: DioMethod.get,
        params: {
          "pageSize": "200",
          "pageNo": "1",
        }).then((res) {
      print("公告");

      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      print("公告${res.data}");
      gongGaoModel = MessageModel.fromJson(res.data);
      setState(() {

      });
    }).catchError((error) {
      print('747行打印：============ ${error}');
      // print(error);
    });
  }


  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
    super.dispose();


  }
}