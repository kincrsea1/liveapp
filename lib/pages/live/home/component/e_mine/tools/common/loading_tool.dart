// @dart=2.9
// ignore_for_file: prefer_const_constructors

import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class Loading {
  static bool isShow = false;

  static showLoading(BuildContext context,
      {int milliseconds = 150, dynamic LoadingText = null, double width = 100.00, showGif = false}) {
    if (!isShow) {
      isShow = true;
      showGeneralDialog(
          context: context,
          // barrierColor: Colors.white, // 背景色
          // barrierLabel: '',
          barrierDismissible: false, // 是否能通过点击空白处关闭
          transitionDuration: Duration(milliseconds: milliseconds), // 动画时长
          pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return Align(
              child: showGif
                  ? Container(
                      width: 200,
                      child: Image(image: AssetImage('assets/images/enterGameLoading.gif')),
                    )
                  : Container(
                      width: width,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Theme(
                        data: ThemeData(
                          cupertinoOverrideTheme: CupertinoThemeData(
                            brightness: Brightness.dark,
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CupertinoActivityIndicator(
                              radius: 14,
                            ),
                            if (LoadingText != null) LoadingText
                          ],
                        ),
                      ),
                    ),
            );
          }).then((value) {
        isShow = false;
      });
    }
  }

  static hideLoading(BuildContext context) {
    if (isShow) {
      Navigator.of(context).pop();
    }
  }
}

void showBetInfoResult(BuildContext context, String betAmount, String winAmount, String orderId, Function callback) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return WillPopScope(
          child: AlertDialog(
            title: null,
            backgroundColor: Colors.grey,
            contentPadding: EdgeInsets.only(left: 0, right: 0),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Container(
                    color: Colors.black87,
                    height: 50,
                    child: Column(
                      children: [
                        Icon(
                          Icons.access_time_outlined,
                          size: 16,
                          color: Colors.white,
                        ),
                        Text(
                          '订单确认中',
                          style: TextStyle(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15, bottom: 0, right: 15),
                    child: Column(
                      children: [
                        Container(
                          height: 30,
                          child: Row(
                            children: [
                              Container(
                                width: 70,
                                child: Text(
                                  "投注金额：",
                                  style: TextStyle(color: Colors.white, fontSize: 12),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    "¥$betAmount",
                                    style: TextStyle(color: Colors.white, fontSize: 12),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 30,
                          child: Row(
                            children: [
                              Container(
                                width: 70,
                                child: Text(
                                  "可盈金额：",
                                  style: TextStyle(color: Colors.white, fontSize: 12),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    "$winAmount",
                                    style: TextStyle(color: Colors.white, fontSize: 12),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 30,
                          child: Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                  "注单号：",
                                  style: TextStyle(color: Colors.white, fontSize: 12),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    "$orderId",
                                    style: TextStyle(color: Colors.white, fontSize: 12),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          child: Container(
                            width: 100,
                            alignment: Alignment.center,
                            height: 30,
                            child: Text(
                              "查看详情",
                              style: TextStyle(color: Colors.white, fontSize: 13),
                            ),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(5)),
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          behavior: HitTestBehavior.opaque,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        GestureDetector(
                          child: Container(
                            width: 100,
                            height: 30,
                            alignment: Alignment.center,
                            child: Text(
                              "继续投注",
                              style: TextStyle(color: Colors.black, fontSize: 13),
                            ),
                            decoration:
                                BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(5)),
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                            callback();
                          },
                          behavior: HitTestBehavior.opaque,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          onWillPop: () async {
            return false;
          });
    },
  ).then((val) {
    log("$val");
  });
}

void betShowToast(BuildContext context, {dynamic LoadingText = null, double width = 100.00}) {}
