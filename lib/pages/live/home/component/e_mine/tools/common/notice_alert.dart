// ignore: file_names
// @dart=2.9

// ignore_for_file: file_names, prefer_typing_uninitialized_variables, prefer_const_constructors, must_be_immutable, use_key_in_widget_constructors, avoid_unnecessary_containers, unnecessary_new

import 'package:flutter/material.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mo/message2_model.dart';

import '../../mo/message_model.dart';

class NoticeDetailStyleDialog extends StatefulWidget {
  final confirmCallback;
  final model;


  NoticeDetailStyleDialog(this.confirmCallback,this.model);

  @override
  _NoticeDetailStyleDialogState createState() =>
      _NoticeDetailStyleDialogState();
}

class _NoticeDetailStyleDialogState extends State<NoticeDetailStyleDialog> {
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      titlePadding: EdgeInsets.only(top: 10),
      title: Container(
        height: 35,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 20,
            ),
            Center(
              child: Text('消息通知',
                  style: new TextStyle(
                    color: Color(0xFF333333),
                    fontSize: 18,
                  )),
            ),
            Container(
              margin: EdgeInsets.only(right: 10),

            )
          ],
        ),
      ),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          alignment: Alignment.center,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 0),
                child: Text((widget.model is List1)?(widget.model.updateTime??widget.model.createTime):widget.model.createTime,
                    style: TextStyle(fontSize: 14, color: Color(0xFF333333))),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 20),
                child: Text(
                    (widget.model is List2)?widget.model.textContent.replaceAll('<p>', '').replaceAll('</p>', ''):widget.model.noticeContent,
                    style: TextStyle(fontSize: 15, color: Color(0xFF333333))),
              ),
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: 15),
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Container(
                  alignment: Alignment.center,
                  width: 110,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: Color(0xFFF36704),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      widget.confirmCallback();
                    },
                    child: Text('我知道了',
                        style:
                            TextStyle(color: Colors.white, fontSize: 16)),
                  )),
            ))
      ],
    );
  }
}
