import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/common_widget/base_web_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/common/base_navigation_view.dart';

import '../../../../../base/base_widget.dart';
import '../../../../../config/image/local_image_selector.dart';
import '../../../../../store/session_storage/state_main.dart';

class MineAboutUs extends BaseWidget {
  MineAboutUs({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _MineAboutUs();
  }
}

class _MineAboutUs extends BaseWidgetState<MineAboutUs> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget PageBody(AppState state) {
    ///外部设置导航颜色和title

    return Scaffold(
      appBar: BaseNavigation("关于我们","assets/images/mine/mine_nav_kefu.png"),
      backgroundColor: Colors.white,
      body:Stack(
        children: [
          Column(
            // physics: const ScrollPhysics(),
            children: [
              //头部代码省略
              Container(
                  margin: EdgeInsets.only(top: 27.w),
                  height: 104.w,
                  width: 98.w,
                  child: Image(
                    image: AssetImage('assets/images/mine/img.png'),
                    // fit: BoxFit.fill,
                    height: 40,
                  )
              ),
              Container(
                width: getScreenWidth(),
                margin: EdgeInsets.only(left: 35.w,right: 35.w,top: 27.w),
                child: Text(
                  "      一个直播（以下称“我们”、“一个直播”）目前已经正式上线运营，我们为广大体育爱好者提供了丰富的比赛信息，及时的比赛内容，目前已经成为全国规模及影响力标杆的体育内容平台，在球迷中得到了广泛的认可。我们一直致力于为中国体育的发展贡献自己的力量。"
                  ,style: TextStyle(color:Color(0xFF999999)),
                ),
              )
            ],
          ),
          // Positioned(
          //   left: 0,
          //   right: 0,
          //   bottom:90.w,
          //   child: _bottomWidget(),
          // ),

        ],
      ),
    );
  }

  _bottomWidget(){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(child: Text("《用户协议》",
        style: TextStyle(color:Color(0xFFF36704)),)),
          Container(margin:  EdgeInsets.only(left:5.w,right: 5.w),color: Color(0xFF657180),width: 1.w,height: 10.w,),
          Container(child: Text("《隐私协议》",
            style: TextStyle(color:Color(0xFFF36704)),))
        ],
      ),
      // color: Colors.red,
      width: getScreenWidth(),
    );
  }

// _mianbody(
//     String statusBarColorStr, Color backButtonColor, List<String> stateList) {
//   return MediaQuery.removePadding(
//       removeTop: true,
//       removeBottom: true,
//       context: context,
//       child: Column(
//         children: <Widget>[
//           Container(
//             // padding: EdgeInsets.only(top: 10, left: 10, right: 10),
//               child: TabBar(
//                 controller: _tabController,
//                 isScrollable: true,
//                 labelColor: Color(0xFFD3C294),
//                 unselectedLabelColor: Color(0xFFC1C2C4),
//                 labelPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
//                 indicator: TabSizeIndicator(
//
//                   ///自定义指示器
//                   ///指示器的宽度
//                     wantWidth: 26,
//                     borderSide: BorderSide(width: 2.0, color: Color(0xFFD3C294))),
//                 tabs: stateList.map<Tab>((String tab) {
//                   return Tab(
//                     child: Container(
//                       child: Text(tab),
//                     ),
//                   );
//                 }).toList(),
//               )),
//
//           ///填充剩下的部分
//           Flexible(
//             child: TabBarView(
//                 controller: _tabController,
//                 children: StateType.map((int buttonShow) {
//                   return CoupTabPage(buttonShow: buttonShow);
//                 }).toList()),
//           )
//         ],
//       ));
// }
}
