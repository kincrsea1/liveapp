import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final String title;
  final bool enable;
  final VoidCallback? onPressed;

  const LoginButton(this.title, {Key? key, this.enable = true, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 1,
      child: MaterialButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22.5)),
        height: 45,
        minWidth: 20,
        onPressed: enable ? onPressed : null,
        disabledColor: Color(0xffF36704).withAlpha(100),
        color: Color(0xffF36704),
        child: Text(title, style: TextStyle(color: enable ? Colors.white : Colors.white, fontSize: 14)),
      ),
    );
  }
}
