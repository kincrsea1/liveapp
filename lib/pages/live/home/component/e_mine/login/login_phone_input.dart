import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../../../config/image/local_image_selector.dart';


typedef GesturetTapCallback = void Function(bool a);

///登录输入框，自定义widget
class LoginPhoneInput extends StatefulWidget {
  final String title;
  final String hint;
  final ValueChanged<String>? onChanged;
  final ValueChanged<bool>? focusChanged;
  final bool lineStretch;
  final bool obscureText;
  final TextInputType? keyboardType;
  final String? guojia;
  final GesturetTapCallback? onTap;
  final ValueChanged<dynamic>? onTapVif;
  final int limitLength;
  final bool eableClickCode;
  String codeTips;
  final ValueChanged<String>? deleteChanged;
  LoginPhoneInput(this.title, this.hint,
      {Key? key,
      this.onChanged,
      this.focusChanged,
      this.lineStretch = false,
      this.obscureText = false,
      this.keyboardType,
      this.guojia,
      this.onTap,
      this.onTapVif,
      this.limitLength = 100,
      this.eableClickCode = false,
      this.codeTips = "获取验证码",
      this.deleteChanged})
      : super(key: key);

  @override
  _LoginPhoneInput createState() => _LoginPhoneInput();
}

class _LoginPhoneInput extends State<LoginPhoneInput> {
  final _focusNode = FocusNode();
  Timer? _timer;

  //倒计时数值
  var _enable = true;
  var _time = 0;

  final controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    print("57行==============${widget.hint} ---- initState");

    ///是否获取光标的监听
    _focusNode.addListener(() {
      print("Has focus:${widget.hint}==== ${_focusNode.hasFocus}");
      if (widget.focusChanged != null) {
        widget.focusChanged!(_focusNode.hasFocus);
      }
    });

// ///手机号码每次初始化 目前在这添加初始化的值
// if(widget.hint == '请输入手机号'){
//   controller.value = TextEditingValue(
//       text: widget.title,
//       selection: TextSelection.fromPosition(
//           TextPosition(
//               affinity: TextAffinity.downstream,
//               offset: widget.title.length)));
// }

  }

  @override
  void dispose() {
    print("销毁....");
    _focusNode.dispose();
    if (_timer != null) {
      _timer?.cancel();
      _timer = null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [_input()],
        ),
      ],
    );
  }

  _input() {
    var rightWidget;
    var phonelocation;
    if (widget.hint == "手机号" || widget.hint == "请输入手机号") {
      rightWidget = Positioned(
        right: 20.0,
        top: 14,
        child: LocalImageSelector.getSingleImage("avatar",
            imageHeight: 20, imageWidth: 20),
      );

      phonelocation = Positioned(
        left: 43.0,
        top: 14,
        child: GestureDetector(
          child: Container(
            padding: EdgeInsets.only(left: 5),
            margin: EdgeInsets.only(left: 5, right: 5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Color.fromRGBO(62, 63, 68, 1),
                borderRadius: BorderRadius.circular(3.0)),
            child: Row(
              children: [
                Text(widget.guojia!,
                    style: TextStyle(fontSize: 13, color: Color(0XFFc1c2c4))),
                Text(" "),
                LocalImageSelector.getSingleImage("arrow_down",
                    imageHeight: 15, imageWidth: 15),
              ],
            ),
            width: 52,
            height: 20,
          ),
          onTap: () {
            widget.onTap!(true);
          },
        ),
      );
    } else if (widget.hint == "请输入验证码") {
      rightWidget = Positioned(
        right: 20.0,
        top: 14,
        child: GestureDetector(
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(),
            child: Text(_time == 0 ? widget.codeTips : "${_time}s(重新发送)",
                style: TextStyle(
                    fontSize: 15,
                    color: widget.eableClickCode
                        ? Color(0XFFF36704)
                        : Color(0XFFF36704).withOpacity(0.7))),
          ),
          onTap: () => setState(() {
            print("获取验证码");
            if (_enable) {
              widget.onTapVif!(() {
                _enable ? startCountdown(60) : null;
              });
            }
          }),
        ),
      );
    }else if (widget.hint == "请输入验证码 ") {
      rightWidget = Positioned(
        right: 20.0,
        top: 14,
        child: GestureDetector(
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(),
            child: Text(_time == 0 ? widget.codeTips : "${_time}s(重新发送)",
                style: TextStyle(
                    fontSize: 15,
                    color: widget.eableClickCode
                        ? Color(0XFFF36704)
                        : Color(0XFFF36704).withOpacity(0.7))),
          ),
          onTap: () => setState(() {
            print("获取验证码");
            if (_enable) {
              widget.onTapVif!(() {
                _enable ? startCountdown(60) : null;
              });
            }
          }),
        ),
      );
    }
    return Expanded(
        child: Container(
            height: 55,
            child: Stack(
                children: (widget.hint == "手机号" || widget.hint == "请输入手机号")
                    ? [
                        textField(),
//                  choiceNatinoal,
                        Positioned(
                          left: 20.0,
                          top: 14,
                          child: LocalImageSelector.getSingleImage(
                              "icon-Cell-phone@2x",
                              imageHeight: 20,
                              imageWidth: 20),
                        ),
                        phonelocation,
                      ]
                    : [
                        textField(),
                        Positioned(
                          left: 35.0,
                          top: 14,
                          child: LocalImageSelector.getSingleImage(
                              widget.hint == "请输入验证码"?"login_vifie":"mine_vifie",
                              imageHeight: 20,
                              imageWidth: 20),
                        ),
                        rightWidget
                      ]
        )
    )
    );
  }

  TextField textField() {
    return TextField(
      focusNode: _focusNode,
      onChanged: widget.onChanged,
      obscureText: widget.obscureText,
      keyboardType: widget.keyboardType,
      autofocus: !widget.obscureText,
      cursorColor: Colors.grey,
      style: TextStyle(
          fontSize: 16, fontWeight: FontWeight.w300, color: Color(0xFF666666)),
      //输入框的样式
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.transparent,
        contentPadding: EdgeInsets.only(
            left: (widget.hint == "手机号" || widget.hint == "请输入手机号") ? 110 : 80,
            right: 20,
            top: 5,
            bottom: 10),
        border: OutlineInputBorder(
            // borderRadius: BorderRadius.circular(40),
            borderSide: BorderSide.none),
        hintText: widget.hint,
        hintStyle: TextStyle(fontSize: 16, color: Color(0xFF999999)),
        suffixIcon: controller.text.length > 0 && widget.deleteChanged != null
            ? IconButton(

                ///阴影的效果
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  controller.clear();
                  if (widget.deleteChanged != null) {
                    widget.deleteChanged!("");
                  }
                },
                icon: Icon(
                  Icons.close,
                  color: Color(0xFFC1C2C4),
                  size: 17,
                ))
            : null,
//      focusedBorder: OutlineInputBorder(
//
//        borderRadius: BorderRadius.circular(25.7),
//      ),
//      enabledBorder: UnderlineInputBorder(
//
//        borderRadius: BorderRadius.circular(25.7),
//      ),
      ),
      inputFormatters: widget.limitLength == 100
          ? []
          : <TextInputFormatter>[
              LengthLimitingTextInputFormatter(widget.limitLength)
            ],
      controller: controller,
    );
  }

  //倒计时方法
  void startCountdown(int count) {
    if (!_enable) return;
    setState(() {
      _enable = false;
      _time = count;
    });
    //倒计时时间
    _timer = Timer.periodic(Duration(seconds: 1), (Timer it) {
      print(it.tick);
      setState(() {
        if (it.tick >= count) {
          _enable = true;
          it.cancel();
        }
        if (it.tick >= count) {
          _time = 0;
        } else {
          _time = count - it.tick;
        }
        widget.codeTips = "重新发送";
      });
    });
  }
}
