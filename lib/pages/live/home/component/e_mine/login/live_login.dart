// @dart=2.9
// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, avoid_print, unnecessary_this
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/util/string_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/video_player_utils.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/store/session_storage/state_user.dart';

import '../../../../../../bottom_nav_bar.dart';
import '../../../../../../config/env_config.dart';
import '../../../../../../config/image/local_image_selector.dart';
import '../../../../../../network/dio_util/dio_method.dart';
import '../../../../../../network/dio_util/dio_response.dart';
import '../../../../../../network/dio_util/dio_util.dart';
import '../../../../../../tools/db/hi_cache.dart';
import '../common/login_error_password_alert.dart';
import '../tools/common/loading_tool.dart';
import 'block_puzzle_captcha.dart';
import 'login_button.dart';
import 'login_input.dart';
import 'login_phone_input.dart';
import 'dart:io';
import '../../../../../../model/user_info_model.dart' as UserModal;

class LiveLogin extends BaseWidget {
  @override
  BaseWidgetState<BaseWidget> getState() {
    return _LiveLogin();
  }
}

class _LiveLogin extends BaseWidgetState<LiveLogin> {
  bool isPhoneLogin = true; //默认手机登录
  String userName;
  String password;
  String userTips = "";
  String passwordTips = "";
  String phoneNumber;
  String vCodeNumver;
  String phoneTips = "";
  String phoneCodeTips = "";
  bool loginEnable = false;
  bool eableClickCode = false;
  String codeTips = "获取验证码";
  bool hasMobileCode = false;
  String captchaVerification;
  String guojia = "中国";
  int phoneNumberlength = 11;
  String captchaVerification2;
  MethodChannel _methodChannel = MethodChannel('login_page/method');
  @override
  void initState() {
    //- 详情页面跳转后查看视频播放状态
    Future.delayed(Duration(seconds: 2), () {
      if (VideoPlayerUtils.state == VideoPlayerState.playing && !sessionStorage.state.live.isMiniPlay) {
        VideoPlayerUtils.playerHandle(VideoPlayerUtils.url);
      }
    });
    super.initState();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
//      appBar: appBar('密码登录', '注册', () {
//        Navigator.push(context, MaterialPageRoute(builder: (_) {
//          return RegistrationPage();
//        }));
//      }),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/onlive/login_bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          // color: Colors.red,
          child: buildListView(),
          width: getScreenWidth(),
        ),
      ),
    );
  }

  buildListView() {
    print("几把刷新${codeTips}");
    return Column(
      children: [
        GestureDetector(
          child: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(top: getAppBarHeight() + 10.w, left: 50.w),
            child: LocalImageSelector.getSingleImage("login_close", imageHeight: 14.w, imageWidth: 14.w),
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        Container(
          margin: EdgeInsets.only(top: 110.w, left: 50.w),
          child: Row(
            children: [
              GestureDetector(
                child: Container(
                  child: Column(
                    children: [
                      Text(
                        "手机登录",
                        style: TextStyle(fontSize: 20.sp, color: isPhoneLogin ? Color(0xFF666666) : Color(0xFF999999)),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5.w),
                        color: isPhoneLogin ? Color(0xFFF36704) : Colors.transparent,
                        width: 30.w,
                        height: 2.w,
                      )
                    ],
                  ),
                ),
                onTap: () {
                  print("手机登录");
                  setState(() {
                    if (isPhoneLogin) return;

                    isPhoneLogin = true;
                    this.loginEnable = false;
                    userTips = "请输入账号";
                    passwordTips = "请输入密码";
                    phoneTips = "请输入手机号";
                    phoneCodeTips = "请输入验证码";

                    hasMobileCode = false;
                    captchaVerification = null;
                    captchaVerification2 = null;
                    // guojiaChoiceVisible = false;
                    guojia = "中国";
                    phoneNumberlength = 11;
                    eableClickCode = false;
                    codeTips = "获取验证码";
                    clearAllInput();
                  });
                },
              ),
              GestureDetector(
                child: Container(
                  margin: EdgeInsets.only(left: 40.w),
                  child: Column(
                    children: [
                      Text("密码登录",
                          style:
                              TextStyle(fontSize: 20.sp, color: !isPhoneLogin ? Color(0xFF666666) : Color(0xFF999999))),
                      Container(
                        margin: EdgeInsets.only(top: 5.w),
                        color: !isPhoneLogin ? Color(0xFFF36704) : Colors.transparent,
                        width: 30.w,
                        height: 2.w,
                      )
                    ],
                  ),
                ),
                onTap: () {
                  print("密码登录");
                  setState(() {
                    if (!isPhoneLogin) return;
                    isPhoneLogin = false;
                    loginEnable = false;
                    userTips = "请输入账号";
                    passwordTips = "请输入密码";
                    // isRemeberCode = false;
                    phoneTips = "请输入手机号";
                    phoneCodeTips = "请输入验证码";
                    // Global.isFirstRefresh = true;
                    hasMobileCode = false;
                    captchaVerification = null;
                    captchaVerification2 = null;
                    // guojiaChoiceVisible = false;
                    guojia = "中国";
                    phoneNumberlength = 11;
                    eableClickCode = false;
                    codeTips = "获取验证码";
                    clearAllInput();
                  });
                },
              )
            ],
          ),
        ),
        Container(
          height: 10.w,
        ),
        isPhoneLogin
            ? LoginInput(
                '',
                '请输入手机号',
                key: Key("1"),
                onChanged: (text) {
                  phoneNumber = text;
                  setState(() {});
                  checkPhoneInput();
                },
                deleteChanged: (n) {
                  phoneNumber = "";
                  setState(() {});
                  checkPhoneInput();
                },
                limitLength: phoneNumberlength,
                keyboardType: TextInputType.number,
              )
            : LoginInput(
                '',
                '请输入用户名',
                key: Key("2"),
                onChanged: (text) {
                  userName = text;
                  setState(() {});
                  checkInput();
                },
                deleteChanged: (n) {
                  userName = "";
                  setState(() {});
                  checkInput();
                },
              ),
        Container(
          margin: EdgeInsets.only(left: 12.w, right: 12.w),
          height: 1.w,
          width: getScreenWidth(),
          color: Color(0xFF999999).withAlpha(153),
        ),
        Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 20),
            child: Text(
              isPhoneLogin ? phoneTips : userTips,
              style: TextStyle(fontSize: 13, color: Colors.red),
            )),
        Container(
          height: 0,
        ),
        isPhoneLogin
            ? LoginPhoneInput(
                '',
                '请输入验证码',
                // obscureText: true,
                onChanged: (text) {
                  vCodeNumver = text;
                  checkPhoneInput();
                },
                focusChanged: (focus) {
//                    this.setState(() {
////                    protect = focus;
//                    });
                },
                onTapVif: (a) {
//     print("点击了验证码哦");
                  bool isCorrect = phoneCheck(phoneNumber);

                  if (isCorrect) {
                    //合法
                    loadingBlockPuzzle2(context, countDown: a);
                  } else {
                    // showToast("请输入正确的手机号");
                  }
                },
                limitLength: 5,
                eableClickCode: eableClickCode,
                codeTips: codeTips,
              )
            : LoginInput(
                '',
                '请输入密码',
                obscureText: true,
                onChanged: (text) {
                  password = text;
                  checkInput();
                },
                focusChanged: (focus) {
//                    this.setState(() {
////                    protect = focus;
//                    });
                },
                deleteChanged: (n) {
                  password = "";
                  checkInput();
                },
              ),
        Container(
          margin: EdgeInsets.only(left: 12.w, right: 12.w),
          height: 1.w,
          width: getScreenWidth(),
          color: Color(0xFF999999).withAlpha(153),
        ),
        Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 20),
            child: Text(
              isPhoneLogin ? phoneCodeTips : passwordTips,
              style: TextStyle(fontSize: 13, color: Colors.red),
            )),
        false
            ? Container(
                // height: 12.w,
                margin: EdgeInsets.only(left: 12.w, right: 12.w),
                child: Row(
                  children: [
                    Text("登录即代表您同意"),
                    GestureDetector(
                        child: Text(
                          "《用户协议》",
                          style: TextStyle(color: Color(0xFFF36704)),
                        ),
                        onTap: () {
                          print("用户协议");
                        }),
                    Text("以及"),
                    GestureDetector(
                        child: Text(
                          "《隐私协议》",
                          style: TextStyle(color: Color(0xFFF36704)),
                        ),
                        onTap: () {
                          print("隐私协议");
                        }),
                  ],
                ),
              )
            : Container(
                height: 16.w,
              ),
        Container(
          height: 22.w,
        ),
        Padding(
            padding: EdgeInsets.only(left: 12.w, right: 12.w, top: 10.w),
            child: LoginButton('登 录', enable: loginEnable, onPressed: () {
              if (isPhoneLogin) {
                //手机登录
                send();
              } else {
                //用户名登录
                send2();
              }
            }))
      ],
    );
  }

  void checkInput() {
    bool enable;
//    if (isNotEmpty(userName) && isNotEmpty(password)) {
//      enable = true;
//    } else {
//      enable = false;
//    }
    bool isCorrect = userNameCheck(userName);

    if (isCorrect) {
      //合法
      userTips = "";
    } else {
      //不合法
      if (isEmpty(userName)) {
        userTips = "请输入账号";
      } else {
        userTips = "6~16位字母或数字组合";
      }
    }
    bool isCorrect2 = passwordCheck(password);

    if (isCorrect2) {
      //合法
      passwordTips = "";
    } else {
      //不合法
      if (isEmpty(password)) {
        passwordTips = "请输入密码";
      } else {
        passwordTips = "6~18位数字或字母组合";
      }
    }

    if (isCorrect && isCorrect2) {
      enable = true;
    } else {
      enable = false;
    }
    setState(() {
      loginEnable = enable;
    });
  }

  void checkPhoneInput() {
    bool enable;

    bool isCorrect = phoneCheck(phoneNumber);

    if (isCorrect) {
      //合法
      phoneTips = "";
      eableClickCode = true;
    } else {
      //不合法
      if (isEmpty(phoneNumber)) {
        phoneTips = "请输入手机号";
      } else {
        phoneTips = "手机号格式不正确";
      }
      eableClickCode = false;
    }
    bool isCorrect2 = code(vCodeNumver);

    if (isCorrect2) {
      //合法
      phoneCodeTips = "";
    } else {
      //不合法
      if (isEmpty(vCodeNumver)) {
        phoneCodeTips = "请输入验证码";
      } else {
        phoneCodeTips = "验证码格式不正确";
      }
    }

    if (!hasMobileCode) {
      phoneCodeTips = "请完成滑块验证";
    }

    if (isCorrect && isCorrect2 && hasMobileCode) {
      enable = true;
    } else {
      enable = false;
    }
    setState(() {
      loginEnable = enable;
    });
  }

  //滑动拼图
  void loadingBlockPuzzle(
    BuildContext context, {
    barrierDismissible = true,
  }) {
    showDialog<Null>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return BlockPuzzleCaptchaPage(
          onSuccess: (v) {
            // print("22222" + v);
            captchaVerification = v.toString();
            //关闭滑块
            //write something
            Future.delayed(Duration(seconds: 0), () {
              // send2();
              LoginRequestFunction1();
            });
          },
          onFail: () {},
        );
      },
    );
  }

  //滑动拼图
  void loadingBlockPuzzle2(BuildContext context, {barrierDismissible = true, Function countDown}) {
    showDialog<Null>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return BlockPuzzleCaptchaPage(
          onSuccess: (v) {
            print("22222:" + v);
            captchaVerification2 = v.toString();
            Future.delayed(Duration(seconds: 0), () {
              checkphone(countDown);
            });
            //关闭滑块
            //write something
          },
          onFail: () {},
        );
      },
    );
  }

  void checkphone(Function countDown) async {
    Loading.hideLoading(context);
    codeTips = "正在发送验证码";
    setState(() {});
    sendMobileCode(countDown);
    return;
    //短信登录
    String distrct = guojia == "中国" ? "86" : "886";
    DioUtil()
        .request(
            "/api/user/chkMobile?terminal=1&smsCaptcha=${captchaVerification2}&mobile=${phoneNumber}&mobileAreaCode=${distrct}")
        .then((res) {
      print(res);
      Loading.hideLoading(context);
      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      bool isRight = res.data["msg"];
      if (isRight) {
        //发送验证码

        setState(() {
          codeTips = "正在发送验证码";
        });
        sendMobileCode(countDown);
      } else {
        //没有注册
        showToast("该手机未注册");
      }
    }).catchError((error) {
      Loading.hideLoading(context);
      print(error);
    });
  }

  sendMobileCode(Function countDown) {
    hasMobileCode = false;
    Loading.showLoading(context);
//    String SToken = HiCache.getInstance().get("SToken");
    String distrct = guojia == "中国" ? "86" : "886";
    Map<String, dynamic> Parms = {
      "mobile": phoneNumber,
      "mobileAreaCode": distrct,
      "captchaVerification": captchaVerification2,
    };
    DioUtil().request('/api/user/sendMobCodeForLbpn?terminal=1', method: DioMethod.post, data: Parms).then((res) {
      Loading.hideLoading(context);
      print(res.data['msg']);
      if (res.code != 0 || res.data['code'] != 0) {
        // showToast(res.data['msg']);
        EasyLoading.showToast(res.data['msg']);
        setState(() {
          codeTips = "重新发送";
        });
        return;
      }

      hasMobileCode = true;
      // showToast("短信发送成功");
      EasyLoading.showToast("短信发送成功");
      codeTips = "重新发送";
      countDown();
    }).catchError((error) {
      print(error);
      Loading.hideLoading(context);
      setState(() {
        codeTips = "重新发送";
      });
    });
  }

  void send() {
    //手机登录
    print("手机登录");
    if (captchaVerification2 != null) {
      LoginRequestFunction2();
    } else {
      // showToast("请先进行滑块验证");
      EasyLoading.showToast("请先进行滑块验证");
    }
  }

  void send2() {
    //用户名登录
    print("用户名登录");

    loadingBlockPuzzle(context);
  }

  void LoginRequestFunction1() async {
    Loading.showLoading(context);

    ///判断当前设备
    String loginType = '';
    String dev = '';
    if (Platform.isIOS) {
      loginType = '2';
      dev = 'iOS';
    } else if (Platform.isAndroid) {
      loginType = '3';
      dev = 'Android';
    } else {
      loginType = '1';
      dev = 'wap';
    }

    Map<String, dynamic> Parms = {
      "loginName": userName,
      "password": password,
      "captchaVerification": captchaVerification,
      'loginType': loginType,
      'dev': dev
    };

    print("密码登录参数:${Parms}}");
    DioUtil().request('/api/user/login', method: DioMethod.post, data: Parms).then((res) {
      Loading.hideLoading(context);
      if (res.code != 0 || res.data['code'] != 0) {
        if (res.data['code'] == 10086) {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RequestErrorStyleDialog(
                  '您输入密码错误次数过多，账号已锁定15分钟，请稍后再试，你可以点击忘记密码，来修改密码或者咨询在线客服，帮您解锁。',
                );
              });
        } else if (res.data['code'] == 2000 || res.data['code'] == 500) {
          // showToast('请确认账号和密码是否正确');
          EasyLoading.showToast('请确认账号和密码是否正确');
        } else {
          // showToast('网络异常，请稍后再试，谢谢');
          EasyLoading.showToast('网络异常，请稍后再试，谢谢');
        }
        return;
      }
      // EasyLoading.showToast('成功登录');
      Map<String, dynamic> m = {...res.data['userInfo'], 'token': res.data['token']};
      getUserBaseInfo(m);
    }).catchError((error) {
      Loading.hideLoading(context);
      print('605行============ ${error}');
      // print(error);
    });
  }

  void LoginRequestFunction2() async {
    bool isCorrect = phoneCheck(phoneNumber);

    if (isCorrect) {
    } else {
      // showToast("请输入正确的手机号");
      EasyLoading.showToast('请输入正确的手机号');
      return;
    }
    Loading.showLoading(context);
    String distrct = guojia == "中国" ? "86" : "886";
    String SToken = HiCache.getInstance().get("SToken");

    ///判断当前设备
    String loginType = '';
    String dev = '';
    if (Platform.isIOS) {
      loginType = '2';
      dev = 'iOS';
    } else if (Platform.isAndroid) {
      loginType = '3';
      dev = 'Android';
    } else {
      loginType = '1';
      dev = 'wap';
    }

    Map<String, dynamic> Parms = {
      "mobile": phoneNumber,
      "mobileAreaCode": distrct,
      "loginType": loginType,
      "mobileCaptcha": vCodeNumver,
      "kaptcha": captchaVerification2,
      "smsCaptcha": captchaVerification2,
      "registerDevice": "e32ebc5a6d082efa1780eb95a299dedc"
    };

    print("短信登录参数:${Parms}}");
    DioUtil.getInstance()?.openLog();
    DioUtil().request('/api/user/loginByPhoneNumber', method: DioMethod.post, data: Parms).then((res) {
      Loading.hideLoading(context);
      if (res.code != 0 || res.data['code'] != 0) {
        // showToast(res.data['msg']);
        EasyLoading.showToast(res.data['msg']);
        return;
      }

      print(res.data);
      log('=============注册标识 ${res.data['userInfo']['isNewMember']}');
      // _methodChannel.invokeMapMethod('regiest');
      String isNewMember = res.data['userInfo']['isNewMember'].toString();
      if(isNewMember == '1'){
        _methodChannel.invokeMapMethod('regiest');
      }
      EasyLoading.showToast('成功登录');
      Map<String, dynamic> m = {...res.data['userInfo'], 'token': res.data['token']};
      getUserBaseInfo(m);

      // Navigator.of(context).pop();
    }).catchError((error) {
      Loading.hideLoading(context);
      print(error);
    });
  }

  ///- 登录完成后获取用户基础信息
  void getUserBaseInfo(Map<String, dynamic> loginInfo) async {
    String url = "/splive/app/user/getUser?terminal=1";
    localStorage.setItem('userInfo', loginInfo);
    DioResponse result = await DioUtil().request("/splive/app/user/getUser", method: DioMethod.get);
    if (result.code != 0 || result.data['code'] != 0) return;

    loginInfo = {...loginInfo, ...result.data['data'] as Map};
    ///调取openinstall注册

    localStorage.setItem('userInfo', loginInfo);
    sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': loginInfo}));
    //- 登录成功销毁视频播放器
    if (!sessionStorage.state.live.isMiniPlay) VideoPlayerUtils.dispose();
    /*  VideoPlayerUtils.dispose(); */
    ///取消返回按钮跳转
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
      return Bottomnavigationbar(
        selectPage: 1,
      );
    }));
  }

  bool phoneCheck(String input) {
    if (input == null || input.isEmpty) return false;
    //手机正则验证

    String regexPhoneNumber = null;
    if (guojia == "中国") {
      regexPhoneNumber =
          // "^((13[0-9])|(15[^4])|(166)|(17[0-8])|(18[0-9])|(19[8-9])|(147,145))\\d{8}\$";
          "^((1[3-9]))\\d{9}\$";
    } else {
      regexPhoneNumber = "^[0][9]\\d{8}\$";
    }

    return RegExp(regexPhoneNumber).hasMatch(input);
  }

  bool code(String input) {
    if (input == null || input.isEmpty) return false;
    //手机正则验证
    String regexcode = "^\\d{5}\$";
    return RegExp(regexcode).hasMatch(input);
  }

  bool userNameCheck(String input) {
    if (input == null || input.isEmpty) return false;

    String regexUserName = "^[a-z0-9A-Z]{6,16}\$";

    return RegExp(regexUserName).hasMatch(input);
  }

  bool passwordCheck(String input) {
    if (input == null || input.isEmpty) return false;

    String regexPassword = "^[a-z0-9A-Z]{6,18}\$";

    return RegExp(regexPassword).hasMatch(input);
  }

  clearAllInput() {
    userName = "";
    password = "";
    phoneNumber = "";
    vCodeNumver = "";
  }
}
