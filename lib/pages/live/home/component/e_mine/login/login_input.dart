import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_sdy/public_tools/tools/db/hi_cache.dart';
// import 'package:flutter_sdy/public_tools/utils/Global.dart';

///登录输入框，自定义widget
class LoginInput extends StatefulWidget {
  final String title;
  final String hint;
  final ValueChanged<String>? onChanged;
  final ValueChanged<bool>? focusChanged;
  final bool lineStretch;
  bool obscureText;
  final TextInputType? keyboardType;
  final ValueChanged<String>? deleteChanged;
  final int limitLength;
  LoginInput(this.title, this.hint,
      {Key? key,
      this.onChanged,
      this.focusChanged,
      this.lineStretch = false,
      this.obscureText = false,
      this.keyboardType,
      this.deleteChanged,
      required this.limitLength})
      : super(key: key);

  @override
  _LoginInputState createState() => _LoginInputState();
}

class _LoginInputState extends State<LoginInput> {
  final _focusNode = FocusNode();
  String hideString = "assets/images/onlive/login_hide.png";
  bool obscureText = false;
  bool obscureTextCode = true;
  final controller = TextEditingController();
  @override
  void initState() {
    super.initState();

    print("${widget.hint} ---- initState");
    //是否获取光标的监听
    _focusNode.addListener(() {
      print("Has focus:${widget.title}==== ${_focusNode.hasFocus}");
      if (widget.focusChanged != null) {
        widget.focusChanged!(_focusNode.hasFocus);
      }
    });
  }

  @override
  void dispose() {
    print("销毁....");
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [_input()],
        ),
      ],
    );
  }

  _input() {
    var rightWidget;
    if (widget.hint == "请输入用户名" ||
        widget.hint == "请填写地址" ||
        widget.hint == "请填写qq号码" ||
        widget.hint == "请填写姓名" ||
        widget.hint == "请填写微信号" ||
        widget.hint == "请输入手机号") {
      rightWidget = Positioned(
          right: 20.0,
          top: 14,
          child: Container(
            height: 20,
            width: 20,
          ));
    } else if (widget.hint == "6~10位字母或数字组合") {
      rightWidget = Positioned(
        right: 20.0,
        top: 14,
        child: GestureDetector(
          child: Container(
            height: 20,
            width: 20,
          ),
          onTap: () => setState(() {
//            obscureText = !obscureText;
          }),
        ),
      );
    } else if (widget.hint == "6~16位数字或字母组合") {
      rightWidget = Positioned(
        right: 20.0,
        top: 14,
        child: GestureDetector(
          child: Container(
            height: 20,
            width: 20,
          ),
          onTap: () => setState(() {
//            obscureText = !obscureText;
          }),
        ),
      );
    } else if (widget.hint == "6~18位数字或字母组合") {
      rightWidget = Positioned(
        right: 50.0,
        top: 15,
        child: GestureDetector(
          child: Image(height: 17, width: 17, image: AssetImage(hideString)),
          onTap: () => setState(() {
            obscureTextCode = !obscureTextCode;
            hideString = !obscureTextCode
                ? "assets/images/onlive/login_hide.png"
                : "assets/images/onlive/login_hide.png";
          }),
        ),
      );
    } else if (widget.hint == "6~18位字母或数字组合") {
      rightWidget = Positioned(
        right: 50.0,
        top: 15,
        child: GestureDetector(
          child: Image(height: 17, width: 17, image: AssetImage(hideString)),
          onTap: () => setState(() {
            obscureTextCode = !obscureTextCode;
            hideString = !obscureTextCode
                ? "assets/images/onlive/login_hide.png"
                : "assets/images/onlive/login_hide.png";
          }),
        ),
      );
    } else if (widget.hint == "请再次输入密码") {
      rightWidget = Positioned(
        right: 50.0,
        top: 15,
        child: GestureDetector(
          child: Image(height: 17, width: 17, image: AssetImage(hideString)),
          onTap: () => setState(() {
            obscureTextCode = !obscureTextCode;
            hideString = !obscureTextCode
                ? "assets/images/onlive/login_show.png"
                : "assets/images/onlive/login_hide.png";
          }),
        ),
      );
    } else if (widget.hint == "请输入密码" ||widget.hint == "请输入新密码"||widget.hint == "请再次输入新密码") {
      rightWidget = Positioned(
        right: 50.0,
        top: 15,
        child: GestureDetector(
          child: Image(height: 17, width: 17, image: AssetImage(hideString)),
          onTap: () => setState(() {
            obscureTextCode = !obscureTextCode;
            hideString = !obscureTextCode
                ? "assets/images/onlive/login_show.png"
                : "assets/images/onlive/login_hide.png";
          }),
        ),
      );
    }

    String avatorIcon = 'assets/images/onlive/login_account_logo.png';
    if (widget.hint == "请填写地址") {
      avatorIcon = 'assets/images/icon-address.png';
    } else if (widget.hint == '请填写qq号码') {
      avatorIcon = 'assets/images/icon-qq.png';
    } else if (widget.hint == '请填写微信号') {
      avatorIcon = 'assets/images/icon-wechat.png';
    } else if (widget.hint == '请输入手机号') {
      avatorIcon = 'assets/images/onlive/login_phone.png';
    } else if (widget.hint == '请输入新密码' ||widget.hint == '请再次输入新密码') {
      avatorIcon = 'assets/images/onlive/mine_password.png';
    }
    return Expanded(
        child: Container(
            height: 50,
            child: Stack(children: [
              textField(),
              Positioned(
                left: 35.0,
                top: 14,
                child: Image(
                    height: 20,
                    width: 20,
                    image: widget.hint == "请输入用户名" ||
                            widget.hint == "6~16位数字或字母组合" ||
                            widget.hint == "6~10位字母或数字组合" ||
                            widget.hint == "请填写地址" ||
                            widget.hint == "请填写qq号码" ||
                            widget.hint == "请填写姓名" ||
                            widget.hint == "请填写微信号"||
                            widget.hint == "请输入手机号"||
                            widget.hint == "请输入新密码"||
                            widget.hint == "请再次输入新密码"

                        ? AssetImage(avatorIcon)
                        : AssetImage('assets/images/onlive/login_password.png')),
              ),
              rightWidget
            ])));
  }

  TextField textField() {
    return TextField(
      focusNode: _focusNode,
      onChanged: widget.onChanged,
      obscureText: widget.hint == "请输入密码" ||
              widget.hint == "6~18位数字或字母组合" ||
              widget.hint == "6~18位字母或数字组合" ||
              widget.hint == "请再次输入密码"
          ? obscureTextCode
          : obscureText,
      keyboardType: widget.keyboardType,
      autofocus: !widget.obscureText,
      inputFormatters: <TextInputFormatter>[
        LengthLimitingTextInputFormatter(widget.limitLength) //限制长度
      ],
      cursorColor: Colors.grey,
      style: TextStyle(
          fontSize: 16, color: Color(0xFF666666)),
      //输入框的样式
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.transparent,
        contentPadding:
            EdgeInsets.only(left: 80, right: 20,top: 5,bottom: 10),
        // border: InputBorder.none,
        border: OutlineInputBorder(
            // borderRadius: BorderRadius.circular(40),
            borderSide: BorderSide.none),
        hintText: widget.hint,
        hintStyle: TextStyle(fontSize: 16, color: Color(0xFF999999)),
        suffixIcon: controller.text.length > 0
            ? IconButton(

                ///阴影的效果
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  controller.clear();
                  if (widget.deleteChanged != null) {
                    widget.deleteChanged!("");
                  }
                },
                icon: Icon(
                  Icons.close,
                  color: Color(0xFFC1C2C4),
                  size: 20,
                ))
            : null,
//      focusedBorder: OutlineInputBorder(
//
//        borderRadius: BorderRadius.circular(25.7),
//      ),
//      enabledBorder: UnderlineInputBorder(
//
//        borderRadius: BorderRadius.circular(25.7),
//      ),
      ),
      controller: controller,
    );
  }
}
