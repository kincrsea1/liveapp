import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdateDialog extends Dialog {

  final bool isfocusUpdate;

  final data;

  UpdateDialog(this.isfocusUpdate,this.data);
  @override
  Widget build(BuildContext context) {
    print(data);
    // TODO: implement build
    return UnconstrainedBox(
      // constrainedAxis: Axis.horizontal,
      child: Center(
        child: Container(

          decoration: new BoxDecoration(
//背景
            color: Colors.white,
            //设置四周圆角 角度
            borderRadius: BorderRadius.all(Radius.circular(10.0)),

          ),
          alignment: Alignment.center,
          child: Container(width: 300,
              decoration: new BoxDecoration(
//背景
                color: Colors.transparent,
                //设置四周圆角 角度
                borderRadius: BorderRadius.all(Radius.circular(10.0)),

              ),
              child: Column(
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    top: -15,
                    child: Container(
                        width: 300,
                        height: 144.5,
                        child: Image.asset('assets/images/onlive/topupdateimg.png')
                    ),
                  ),

                    Container(
                      padding: EdgeInsets.only(left: 20, top: 30),
                      width: double.infinity,

                      height: 144.5,
                      child: Text(
                        '发现新版本\nV${data["newVersion"]}',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),

                ],
              ),
              Container(
                // color: Colors.white,
                decoration: new BoxDecoration(
//背景
                  color: Colors.white,
                  //设置四周圆角 角度
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),

                ),
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [


                                Container(
                                  color: Colors.white,
                                  padding: EdgeInsets.only(left: 20, top: 0),
                                  width: double.infinity,
                                  child: Text(
                                    '更新内容',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Color(0xFF657180),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Container(
                                      color: Colors.white,
                                      padding: EdgeInsets.only(left: 20, top: 20),
                                      width: 150,
                                      child: Column(
                                        crossAxisAlignment:CrossAxisAlignment.start ,
                                        children: data["updateContent1"].map<Widget>((e) => Text(e,textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: Color(0xFF657180),
                                            fontSize: 14,
                                          ),)).toList()
                                      ),
                                    ),
                                    Container(
                                      color: Colors.white,
                                      padding: EdgeInsets.only(left: 20, top: 20),
                                      width: 150,
                                      child: Column(
                                        crossAxisAlignment:CrossAxisAlignment.start ,
                                        children: data["updateContent2"].map<Widget>((e) => Text(e,textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: Color(0xFF657180),
                                            fontSize: 14,
                                          ),)).toList()
                                      ),
                                    ),
                                  ],
                                ),

                                Container(height: 20,color: Colors.white,),

                                isfocusUpdate?Container(
                                  decoration: new BoxDecoration(
//背景
                                    color: Colors.white,
                                    //设置四周圆角 角度
                                    borderRadius: BorderRadius.all(Radius.circular(10.0)),

                                  ),
                                  padding: EdgeInsets.only(bottom: 20),

                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [

                                      GestureDetector(
                                        child: Container(
                                            width: 110,
                                            height: 30,
                                            child: Image.asset('assets/images/onlive/update.png')
                                        ),
                                        onTap: (){
                                          _launchUrl();
                                        },
                                      )
                                    ],),
                                ):Container(
                                  padding: EdgeInsets.only(bottom: 20),
                                  decoration: new BoxDecoration(
//背景
                                    color: Colors.white,
                                    //设置四周圆角 角度
                                    borderRadius: BorderRadius.all(Radius.circular(10.0)),

                                  ),

                                    child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      GestureDetector(
                                        child: Container(
                                            width: 110,
                                            height: 30,
                                            child: Image.asset('assets/images/onlive/cancel.png')
                                        ),
                                        onTap: (){
                                          Navigator.pop(context);
                                        },
                                      ),
                                      GestureDetector(
                                        child: Container(
                                            width: 110,
                                            height: 30,
                                            child: Image.asset('assets/images/onlive/update.png')
                                        ),
                                        onTap: (){
                                          _launchUrl();
                                          // Navigator.pop(context);
                                        },
                                      )
                                    ],),
                                  )


                  ],
                ),
              ),
            ],
          )),
        ),
      ),
    );

  }
  Future<void> _launchUrl() async {
    final Uri _url = Uri.parse(data["url"]);
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }
}
