// @dart=2.9
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';

import '../../../../../../../config/env_config.dart';
import 'object_utils.dart';
/**
 * @Author: thl
 * @GitHub: https://github.com/Sky24n
 * @Email: 863764940@qq.com
 * @Email: sky24no@gmail.com
 * @Description: Widget Util.
 * @Date: 2018/9/10
 */

/// Widget Util.
class WidgetUtil {
  bool _hasMeasured = false;
  double _width;
  double _height;
  int tempTime = 0;
  String tempUrl = '';

  /// Widget rendering listener.
  /// Widget渲染监听.
  /// context: Widget context.
  /// isOnce: true,Continuous monitoring  false,Listen only once.
  /// onCallBack: Widget Rect CallBack.
  void asyncPrepare(
      BuildContext context, bool isOnce, ValueChanged<Rect> onCallBack) {
    if (_hasMeasured) return;
    WidgetsBinding.instance.addPostFrameCallback((Duration timeStamp) {
      RenderBox box = context.findRenderObject();
      if (box != null && box.semanticBounds != null) {
        if (isOnce) _hasMeasured = true;
        double width = box.semanticBounds.width;
        double height = box.semanticBounds.height;
        if (_width != width || _height != height) {
          _width = width;
          _height = height;
          if (onCallBack != null) onCallBack(box.semanticBounds);
        }
      }
    });
  }

  /// Widget渲染监听.
  void asyncPrepares(bool isOnce, ValueChanged<Rect> onCallBack) {
    if (_hasMeasured) return;
    WidgetsBinding.instance.addPostFrameCallback((Duration timeStamp) {
      if (isOnce) _hasMeasured = true;
      if (onCallBack != null) onCallBack(null);
    });
  }

  ///get Widget Bounds (width, height, left, top, right, bottom and so on).Widgets must be rendered completely.
  ///获取widget Rect
  static Rect getWidgetBounds(BuildContext context) {
    RenderBox box = context.findRenderObject();
    return (box != null && box.semanticBounds != null)
        ? box.semanticBounds
        : Rect.zero;
  }

  ///Get the coordinates of the widget on the screen.Widgets must be rendered completely.
  ///获取widget在屏幕上的坐标,widget必须渲染完成
  static Offset getWidgetLocalToGlobal(BuildContext context) {
    RenderBox box = context.findRenderObject();
    return box == null ? Offset.zero : box.localToGlobal(Offset.zero);
  }

  /// get image width height，load error return Rect.zero.（unit px）
  /// 获取图片宽高，加载错误情况返回 Rect.zero.（单位 px）
  /// image
  /// url network
  /// local url , package
  static Future<Rect> getImageWH(
      {Image image, String url, String localUrl, String package}) {
    if (ObjectUtils.isEmpty(image) &&
        ObjectUtils.isEmpty(url) &&
        ObjectUtils.isEmpty(localUrl)) {
      return Future.value(Rect.zero);
    }
    Completer<Rect> completer = Completer<Rect>();
    Image img = image != null
        ? image
        : ((url != null && url.isNotEmpty)
            ? Image.network(url)
            : Image.asset(localUrl, package: package));
    img.image
        .resolve(new ImageConfiguration())
        .addListener(new ImageStreamListener(
          (ImageInfo info, bool _) {
            completer.complete(Rect.fromLTWH(0, 0, info.image.width.toDouble(),
                info.image.height.toDouble()));
          },
          onError: (dynamic exception, StackTrace stackTrace) {
            completer.completeError(exception, stackTrace);
          },
        ));
    return completer.future;
  }

  /// get image width height, load error throw exception.（unit px）
  /// 获取图片宽高，加载错误会抛出异常.（单位 px）
  /// image
  /// url network
  /// local url (full path/全路径，example："assets/images/ali_connors.png"，""assets/images/3.0x/ali_connors.png"" );
  /// package
  static Future<Rect> getImageWHE(
      {Image image, String url, String localUrl, String package}) {
    if (ObjectUtils.isEmpty(image) &&
        ObjectUtils.isEmpty(url) &&
        ObjectUtils.isEmpty(localUrl)) {
      return Future.error("image is null.");
    }
    Completer<Rect> completer = Completer<Rect>();
    Image img = image != null
        ? image
        : ((url != null && url.isNotEmpty)
            ? Image.network(url)
            : Image.asset(localUrl, package: package));
    img.image
        .resolve(new ImageConfiguration())
        .addListener(new ImageStreamListener(
          (ImageInfo info, bool _) {
            completer.complete(Rect.fromLTWH(0, 0, info.image.width.toDouble(),
                info.image.height.toDouble()));
          },
          onError: (dynamic exception, StackTrace stackTrace) {
            completer.completeError(exception, stackTrace);
          },
        ));

    return completer.future;
  }

  ///将字符串切割成金额样式  比如1000000转成1.000.000  或  200000转成200.000
  ///也可以将所有的.替换成,   这样就是以,分隔 比如1,000,000或者200,000
  static String getMoneyStyleStr(String text) {
    try {
      if (text == null || text.isEmpty) {
        return "";
      } else {
        String temp = "";
        if (text.length <= 3) {
          temp = text;
          return temp;
        } else {
          int count = ((text.length) ~/ 3); //切割次数
          int startIndex = text.length % 3; //开始切割的位置
          if (startIndex != 0) {
            if (count == 1) {
              temp = text.substring(0, startIndex) +
                  "," +
                  text.substring(startIndex, text.length);
            } else {
              temp = text.substring(0, startIndex) + ","; //第一次切割0-startIndex
              int syCount = count - 1; //剩余切割次数
              for (int i = 0; i < syCount; i++) {
                temp += text.substring(
                        startIndex + 3 * i, startIndex + (i * 3) + 3) +
                    ",";
              }
              temp += text.substring(
                  (startIndex + (syCount - 1) * 3 + 3), text.length);
            }
          } else {
            for (int i = 0; i < count; i++) {
              if (i != count - 1) {
                temp += text.substring(3 * i, (i + 1) * 3) + ",";
              } else {
                temp += text.substring(3 * i, (i + 1) * 3);
              }
            }
          }
          return temp;
        }
      }
    } catch (e) {
      print(e);
    }
  }

  ///线路检测，选出最优的线路
  CheckBestUrl() async {
    await Future.wait<dynamic>(ConfigUtil().APP_API_URL.map((String Url) {
      _RequesloadFormUrl(Url);
    }).toList())
        .then((e) {})
        .catchError((e) {});
  }

  _RequesloadFormUrl(String url) async {
    ///初始化请求类
    Dio dio = Dio();
    //请求协议 post 、get
    Options option = Options(method: 'post');
    option.sendTimeout = 15000;
    option.contentType = "application/json;charset=UTF-8";

    Response response;
    try {
      ///开始请求

      String requestUrl = '${url}/splive/app/getCurrentTimeMillis?terminal=1';
      response = await dio.request(requestUrl, options: option);
    } on DioError catch (e) {
      Response errorResponse;
      if (e.response != null) {}
    }
    try {
      var responseJson = response.data;
      if (response.statusCode == 200) {
        if (responseJson['code'] == 0) {
          String requestUrl = url;
          Map<String, String> urlMap = Map<String, String>();
          String key = responseJson['time'].toString();
          urlMap[key] = url;
          MatchBestUrl(urlMap);
        }
      }
    } catch (e) {
      throw e;
    }
  }

  MatchBestUrl(Map<String, String> urlMap) {
    if (tempTime != 0) {
      int timeTemp = int.parse(urlMap.keys.first);
      if (timeTemp < tempTime) {
        tempUrl = urlMap[timeTemp];
      }
    } else {
      tempTime = int.parse(urlMap.keys.first);
      tempUrl = urlMap[tempTime];
    }
    if (tempUrl.length > 0) EnvConfig.APP_BEST__API_URL = tempUrl;
  }
}

class BottomJuageNotification extends Notification {
  BottomJuageNotification(this.index);
  final int index;
}
