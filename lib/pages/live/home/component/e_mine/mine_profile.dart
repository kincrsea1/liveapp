// @dart=2.9
// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, avoid_print, unnecessary_this

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/base_web_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/common/base_navigation_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/util/string_util.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/style/live/base_color.dart';

import '../../../../../config/image/local_image_selector.dart';
import '../../../../../store/session_storage/state_main.dart';
import '../../../../../store/session_storage/state_user.dart';
import '../../../../../tools/db/hi_cache.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';

class MineProfile extends BaseWidget {
  @override
  BaseWidgetState<BaseWidget> getState() {
    return _MineProfile();
  }
}

class _MineProfile extends BaseWidgetState<MineProfile> {
  final List<MineModel> _list = [
    MineModel(
        iconName: 'assets/images/mine/mine_iocn-user.png',
        title: '用户名',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "modify_nickname"),
    MineModel(
        iconName: 'assets/images/onlive/mine_nickname.png',
        title: '昵称',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "modify_nickname"),
    MineModel(
        iconName: 'assets/images/onlive/mine_date.png',
        title: '出生日期',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "date"),
    MineModel(
        iconName: 'assets/images/onlive/mine_phone.png',
        title: '手机号码',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "modify_phone"),
  ];

  String TelNum = '';
  String MailNum = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 2),_checkUserInfo);
  }

  @override
  Widget PageBody(AppState state) {
    print("刷新了哟");
    bus.on(EventBus.WiDGETSETSTATE, (arg) {
      setState(() {});
    });

    // Map userInfo = json.decode(HiCache.getInstance().get('userInfoStr'));
    // print(userInfo);
    String nickName = null;
    String brithday = null;
    return Scaffold(
        backgroundColor: c_w,
        appBar: BaseNavigation("个人中心", "assets/images/mine/mine_nav_kefu.png"),
        body: StoreConnector<AppState, AppState>(
            converter: (store) => store.state,
            builder: (context, state) {
              return Container(
                margin: EdgeInsets.only(top: 15),
                height: double.infinity,
                color: c_w,
                child: ListView.separated(
                    shrinkWrap: true,
                    // physics: const ScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      String showName = "";
                      if (_list[index].title == "昵称") {
                        showName = isEmpty(sessionStorage.state.user.userInfo.nickName)?"":sessionStorage.state.user.userInfo.nickName;
                      } else if (_list[index].title == "出生日期") {
                        showName = isEmpty(sessionStorage.state.user.userInfo.birthday)?"请添加日期,确保您已满18周岁":sessionStorage.state.user.userInfo.birthday;
                      } else if (_list[index].title == "用户名") {
                        showName = isEmpty(sessionStorage.state.user.userInfo.username)?"":sessionStorage.state.user.userInfo.username;
                      } else if (_list[index].title == "手机号码") {
                        showName = isEmpty(sessionStorage.state.user.userInfo.mobile)?"请绑定手机号码":sessionStorage.state.user.userInfo.mobile;
                      }
                      print(showName);
                      bool isPadding = index == _list.length - 1;
                      return GestureDetector(
                        onTap: () async {
                          // goPage(url: _list[index].target);
                          if(_list[index].title == "用户名") {
                            return;
                          }

                          if(_list[index].title == "手机号码" && isNotEmpty(showName) && showName != "请绑定手机号码" ) {
                            return;
                          }

                          if(_list[index].title == "出生日期" && isNotEmpty(showName) && showName != "请添加日期,确保您已满18周岁") {
                            return;
                          }


                          if (_list[index].target != "") {
                            goPage(url: _list[index].target);
                          }
                        },
                        child: Padding(
                          padding: EdgeInsets.only(right: 0, left: 0, bottom: isPadding ? 15 : 0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            clipBehavior: Clip.antiAlias,
                            child: Container(
                              color: c_white,
                              height: 40,
                              child: Row(
                                children: [
                                  Container(
                                    width: 50,
                                    padding: const EdgeInsets.only(left: 20),
                                    alignment: Alignment.centerLeft,
                                    child: Image(
                                      image: AssetImage(_list[index].iconName),
                                      width: 18,
                                      height: 18,
                                    ),
                                  ),
                                  Expanded(
                                      child: Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      _list[index].title,
                                      style: TextStyle(
                                          color: Color(0xff666666), fontSize: 12.sp, fontWeight: FontWeight.w400),
                                    ),
                                  )),

                                    Container(
                                      child: Text(
                                        showName,
                                        style: TextStyle(color: c_9, fontSize: 14, fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                 isEmpty(showName)||showName=="请绑定手机号码"||showName=="请添加日期,确保您已满18周岁"||_list[index].title == "昵称"? Container(
                                    padding: const EdgeInsets.only(right: 20),
                                    width: 35,
                                    // color: Colors.red,
                                    alignment: Alignment.centerRight,
                                    child: Image(
                                      image: AssetImage(
                                        _list[index].rightIconName,
                                      ),
                                      fit: BoxFit.fitHeight,
                                      width: 5.w,
                                      height: 10.w,
                                    ),
                                  ):Container( padding: const EdgeInsets.only(right: 15),),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return Divider(
                        height: 2,
                        color: c_b1,
                      );
                    },
                    itemCount: _list.length),
              );
            }));
  }

  ListView listView() {
    return ListView.separated(
        shrinkWrap: true,
//          physics: const ScrollPhysics(),

        itemBuilder: (BuildContext context, int index) {
          String rightContent = '';
          if (_list[index].title == "手机绑定") {
            if (TelNum.length > 0) {
              rightContent = realNameClip(TelNum, start: 3, end: 4) + TelNum.substring(TelNum.length - 4);
            }
          } else if (_list[index].title == "邮箱绑定") {
            if (MailNum.length > 0) {
              rightContent = realNameClip(MailNum, start: 3, end: 4) + MailNum.substring(MailNum.indexOf('@'));
            }
          }
          String title = _list[index].title;
          if (_list[index].title == "手机绑定" && TelNum.length > 0) {
            title = '手机号码';
          } else if (_list[index].title == "邮箱绑定" && MailNum.length > 0) {
            title = '电子邮箱';
          }

          return GestureDetector(
            onTap: () async {
              // showToast(_list[index].title);
              String titleName = _list[index].title;
              // if (titleName == "手机绑定") {
              //   if (TelNum.length > 0) return;
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (_) {
              //         return PhoneBlindView();
              //       }));
              // } else if (titleName == "邮箱绑定") {
              //   if (MailNum.length > 0) return;
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (_) {
              //         return MailSafeView();
              //       }));
              // } else if (titleName == "修改密码") {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (_) {
              //         return ModifyCodeView();
              //       }));
              // } else if (titleName == "绑定银行卡") {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (_) {
              //         return BindBankCardView();
              //       }));
              // } else if (titleName == "绑定USDT账户") {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (_) {
              //         return BindUSDTView();
              //       }));
              // }else if (titleName == "绑定支付宝账号") {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (_) {
              //         return BindAlipayView();
              //       }));
              // }
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 10, left: 10),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  clipBehavior: Clip.antiAlias,
                  child: Container(
                    color: const Color(0xff2C2C2E),
                    height: 40,
                    child: Row(
                      children: [
//                          Container(
//                            width: 50,
//                            padding: const EdgeInsets.only(left: 10),
//                            alignment: Alignment.centerLeft,
//                            child: Image(
//                              image: AssetImage(_list[index].iconName),
//                              width: 18,
//                              height: 18,
//                            ),
//                          ),
                        Expanded(
                            child: Container(
                          margin: EdgeInsets.only(left: 15),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            title,
                            style: const TextStyle(color: Color(0xffC1C2C4), fontSize: 14, fontWeight: FontWeight.w400),
                          ),
                        )),
                        (_list[index].title == "手机绑定" && TelNum.length > 0) ||
                                (_list[index].title == "邮箱绑定" && MailNum.length > 0)
                            ? Container(
                                child: Text(
                                  rightContent,
                                  style: TextStyle(color: Color(0xffC1C2C4), fontSize: 14, fontWeight: FontWeight.w400),
                                ),
                              )
                            : Container(),
                        Container(
                          padding: const EdgeInsets.only(right: 10),
                          width: 50,
                          alignment: Alignment.centerRight,
                          child: Image(
                            image: AssetImage(
                              _list[index].rightIconName,
                            ),
                            fit: BoxFit.fitHeight,
                            width: 18,
                            height: 18,
                          ),
                        ),
                      ],
                    ),
                  )),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            height: 10,
            color: Colors.transparent,
          );
        },
        itemCount: _list.length);
  }

  //- 字符串截取操作
  String realNameClip(String initVal, {start, end}) {
    if (initVal.length == 0) {
      return initVal;
    }
    String subString = initVal.substring(start);
    return initVal.replaceAll(subString, '*' * end);
  }
  Future _checkUserInfo() async {

    Map userInfo = await localStorage.getItem('userInfo');
    if(userInfo != null) {
      sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': userInfo}));
    }
  }
}

class MineModel {
  String iconName = '';
  String title = '';
  String rightIconName = '';
  String target;

  MineModel({this.iconName, this.title, this.rightIconName, this.target});
}
