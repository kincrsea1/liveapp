import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import 'common/base_navigation_view.dart';

class MineSystem extends BaseWidget {
  MineSystem({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _MineSystem();
  }
}

class _MineSystem extends BaseWidgetState<MineSystem> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        backgroundColor: c_w,
        appBar: BaseNavigation("系统设置", "assets/images/mine/mine_nav_kefu.png"),
        body: Container(child: Text("系统设置")));
  }
}