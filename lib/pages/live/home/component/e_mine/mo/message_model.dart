// @dart=2.9
// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors_in_immutables, prefer_const_constructors, missing_return
class MessageModel {
  String msg;
  int code;
  Page page;

  MessageModel({this.msg, this.code, this.page});

  MessageModel.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    return data;
  }
}

class Page {
  int totalCount;
  int pageSize;
  int totalPage;
  int currPage;
  List<List1> list;
  int pageTotalCount;

  Page(
      {this.totalCount,
        this.pageSize,
        this.totalPage,
        this.currPage,
        this.list,
        this.pageTotalCount});

  Page.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = new List<List1>();
      json['list'].forEach((v) {
        list.add(new List1.fromJson(v));
      });
    }
    pageTotalCount = json['pageTotalCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    data['pageTotalCount'] = this.pageTotalCount;
    return data;
  }
}

class List1 {
  int id;
  Null ids;
  String noticeTitle;
  String noticeContent;
  String startTime;
  String endTime;
  String showType;
  int available;
  String createUser;
  String createTime;
  String updateUser;
  String updateTime;
  String pcPath;
  String mbPath;
  Null createStart;
  Null createEnd;
  Null availables;
  Null showTypes;

  List1(
      {this.id,
        this.ids,
        this.noticeTitle,
        this.noticeContent,
        this.startTime,
        this.endTime,
        this.showType,
        this.available,
        this.createUser,
        this.createTime,
        this.updateUser,
        this.updateTime,
        this.pcPath,
        this.mbPath,
        this.createStart,
        this.createEnd,
        this.availables,
        this.showTypes});

  List1.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ids = json['ids'];
    noticeTitle = json['noticeTitle'];
    noticeContent = json['noticeContent'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    showType = json['showType'];
    available = json['available'];
    createUser = json['createUser'];
    createTime = json['createTime'];
    updateUser = json['updateUser'];
    updateTime = json['updateTime'];
    pcPath = json['pcPath'];
    mbPath = json['mbPath'];
    createStart = json['createStart'];
    createEnd = json['createEnd'];
    availables = json['availables'];
    showTypes = json['showTypes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ids'] = this.ids;
    data['noticeTitle'] = this.noticeTitle;
    data['noticeContent'] = this.noticeContent;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['showType'] = this.showType;
    data['available'] = this.available;
    data['createUser'] = this.createUser;
    data['createTime'] = this.createTime;
    data['updateUser'] = this.updateUser;
    data['updateTime'] = this.updateTime;
    data['pcPath'] = this.pcPath;
    data['mbPath'] = this.mbPath;
    data['createStart'] = this.createStart;
    data['createEnd'] = this.createEnd;
    data['availables'] = this.availables;
    data['showTypes'] = this.showTypes;
    return data;
  }
}