// @dart=2.9
// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors_in_immutables, prefer_const_constructors, missing_return
class MessageNoticeModel {
  String msg;
  int code;
  Data data;

  MessageNoticeModel({this.msg, this.code, this.data});

  MessageNoticeModel.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int totalCount;
  int pageSize;
  int totalPage;
  int currPage;
  List<List2> list;
  int pageTotalCount;

  Data(
      {this.totalCount,
        this.pageSize,
        this.totalPage,
        this.currPage,
        this.list,
        this.pageTotalCount});

  Data.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = new List<List2>();
      json['list'].forEach((v) {
        list.add(new List2.fromJson(v));
      });
    }
    pageTotalCount = json['pageTotalCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    data['pageTotalCount'] = this.pageTotalCount;
    return data;
  }
}

class List2 {
  int id;
  int messageId;
  String textContent;
  Null imageUrl;
  String createTime;
  String createUser;
  int isSign;
  int isRead;
  int isReadSys;
  int isDelete;
  Null expirationTime;
  Null accountId;
  Null loginName;
  Null loginNameList;
  Null agyList;
  Null groupList;
  Null msgType;
  Null mbrIsRead;
  Null infoId;
  Null isMessage;
  Null num;
  Null setReadType;
  Null isPush;
  Null isAllDevice;
  Null pushTitle;
  Null pushContent;
  Null siteCode;
  int timeToLive;

  List2(
      {this.id,
        this.messageId,
        this.textContent,
        this.imageUrl,
        this.createTime,
        this.createUser,
        this.isSign,
        this.isRead,
        this.isReadSys,
        this.isDelete,
        this.expirationTime,
        this.accountId,
        this.loginName,
        this.loginNameList,
        this.agyList,
        this.groupList,
        this.msgType,
        this.mbrIsRead,
        this.infoId,
        this.isMessage,
        this.num,
        this.setReadType,
        this.isPush,
        this.isAllDevice,
        this.pushTitle,
        this.pushContent,
        this.siteCode,
        this.timeToLive});

  List2.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    messageId = json['messageId'];
    textContent = json['textContent'];
    imageUrl = json['imageUrl'];
    createTime = json['createTime'];
    createUser = json['createUser'];
    isSign = json['isSign'];
    isRead = json['isRead'];
    isReadSys = json['isReadSys'];
    isDelete = json['isDelete'];
    expirationTime = json['expirationTime'];
    accountId = json['accountId'];
    loginName = json['loginName'];
    loginNameList = json['loginNameList'];
    agyList = json['agyList'];
    groupList = json['groupList'];
    msgType = json['msgType'];
    mbrIsRead = json['mbrIsRead'];
    infoId = json['infoId'];
    isMessage = json['isMessage'];
    num = json['num'];
    setReadType = json['setReadType'];
    isPush = json['isPush'];
    isAllDevice = json['isAllDevice'];
    pushTitle = json['pushTitle'];
    pushContent = json['pushContent'];
    siteCode = json['siteCode'];
    timeToLive = json['timeToLive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['messageId'] = this.messageId;
    data['textContent'] = this.textContent;
    data['imageUrl'] = this.imageUrl;
    data['createTime'] = this.createTime;
    data['createUser'] = this.createUser;
    data['isSign'] = this.isSign;
    data['isRead'] = this.isRead;
    data['isReadSys'] = this.isReadSys;
    data['isDelete'] = this.isDelete;
    data['expirationTime'] = this.expirationTime;
    data['accountId'] = this.accountId;
    data['loginName'] = this.loginName;
    data['loginNameList'] = this.loginNameList;
    data['agyList'] = this.agyList;
    data['groupList'] = this.groupList;
    data['msgType'] = this.msgType;
    data['mbrIsRead'] = this.mbrIsRead;
    data['infoId'] = this.infoId;
    data['isMessage'] = this.isMessage;
    data['num'] = this.num;
    data['setReadType'] = this.setReadType;
    data['isPush'] = this.isPush;
    data['isAllDevice'] = this.isAllDevice;
    data['pushTitle'] = this.pushTitle;
    data['pushContent'] = this.pushContent;
    data['siteCode'] = this.siteCode;
    data['timeToLive'] = this.timeToLive;
    return data;
  }
}