// ignore: file_names
// @dart=2.9

// ignore_for_file: file_names, prefer_typing_uninitialized_variables, prefer_const_constructors, must_be_immutable, use_key_in_widget_constructors, avoid_unnecessary_containers, unnecessary_new

import 'package:flutter/material.dart';

class RequestErrorStyleDialog extends StatefulWidget {
  String Msg;

  RequestErrorStyleDialog(this.Msg);

  @override
  _RequestErrorStyleDialogState createState() => _RequestErrorStyleDialogState();
}

class _RequestErrorStyleDialogState extends State<RequestErrorStyleDialog> {
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      titlePadding: EdgeInsets.only(top: 0),
      title: Container(
        height: 35,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0))),
        child: Center(
          child: Text(
            '提示',
            style: new TextStyle(
              color:  Color(0xFF333333),
              fontSize: 15,
            ),
          ),
        ),
      ),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          alignment: Alignment.center,
          child: Column(
            children: [
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.only(top: 20),
                child: Text(widget.Msg, style: TextStyle(fontSize: 12, color: Color(0xFF333333))),
              ),
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: 10),
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.center,
                width: 100,
                height: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0xFFF36704),
                ),
                child: Text('我知道了', style: TextStyle(color: Colors.white, fontSize: 13)),
              ),
            ))
      ],
    );
  }
}
