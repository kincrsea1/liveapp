// @dart=2.9
// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, use_full_hex_values_for_flutter_colors, avoid_unnecessary_containers
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:liveapp/common_widget/base_web_view.dart';

import '../../../../../../config/image/local_image_selector.dart';
import '../../../../../../tools/db/hi_cache.dart';

class BaseNavigation extends StatelessWidget implements PreferredSizeWidget{
  String rightIcon;

  String title;


   BaseNavigation(this.title,this.rightIcon,{
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color:Color(0xFF333333) ,
          onPressed: () {
            Navigator.of(context).pop();
          }),
      title: Text(
        title,
        style: TextStyle(color: Color(0xFF333333), fontSize: 18),
      ),
      shadowColor: Colors.white.withAlpha(122),
      centerTitle: true,
      backgroundColor: Colors.white,
      automaticallyImplyLeading: true,
         actions: [
           rightIcon.length != 0 ?
           GestureDetector(
          onTap: () {
            String kefuUrl = HiCache.getInstance().get("kefuURL");
            print(kefuUrl);
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return BaseWebView(
                '客服',
                baseUrl:
                kefuUrl,
              );
            }));
          },
          child: Container(
              margin: EdgeInsets.only(right: 15),
              height: 24,
              width: 24,
              child: Image(
                image: AssetImage(rightIcon),
                // fit: BoxFit.fill,
                height: 40,
              )
          ),
        ):Container()
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize {
    return new Size.fromHeight(44);
  }
}