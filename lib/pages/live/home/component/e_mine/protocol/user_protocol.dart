import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class UserProtocol extends BaseWidget {
  UserProtocol({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _UserProtocol();
  }
}

class _UserProtocol extends BaseWidgetState<UserProtocol> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget PageBody(AppState state) {
    return Container(
      child: Text('用户协议'),
    );
  }
}