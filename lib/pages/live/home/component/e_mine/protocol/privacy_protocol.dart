import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class PrivacyProtocol extends BaseWidget {
  PrivacyProtocol({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _PrivacyProtocol();
  }
}

class _PrivacyProtocol extends BaseWidgetState<PrivacyProtocol> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget PageBody(AppState state) {
    return Container(
      child: Text('隐私协议'),
    );
  }
}