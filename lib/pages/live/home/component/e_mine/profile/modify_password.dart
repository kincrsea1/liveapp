// @dart=2.9
// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, avoid_print, unnecessary_this
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import '../common/base_navigation_view.dart';
import '../login/login_button.dart';
import '../login/login_input.dart';
import '../login/login_phone_input.dart';

class ModifyPassword extends BaseWidget {


  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ModifyPassword();
  }
}

class _ModifyPassword extends BaseWidgetState<ModifyPassword> {
  bool eableClickCode = true;
  String codeTips = "获取验证码";
  bool loginEnable = true;
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        backgroundColor: c_w,
        appBar: BaseNavigation("修改密码", "assets/images/mine/mine_nav_kefu.png"),
        body: Container(child: Container(
          child: Column(children: [
            Container(
              margin: EdgeInsets.only(top: 27.w),
              color: c_white,
              child: LoginPhoneInput(
                '',
                '请输入验证码 ',
                // obscureText: true,
                onChanged: (text) {
                  // vCodeNumver = text;
                  // checkPhoneInput();
                },
                focusChanged: (focus) {
//                    this.setState(() {
////                    protect = focus;
//                    });
                },
                onTapVif: (a) {

                },
                limitLength: 5,
                eableClickCode: eableClickCode,
                codeTips: codeTips,
              ),
            ),
            Container(width: getScreenWidth(),height: 1.w,color:c_b1 ,),
            Container(
              color: c_white,
              child: LoginInput(
                '',
                '请输入新密码',
                key: Key("1"),
                onChanged: (text) {
                  // userName = text;
                  setState(() {});
                  // checkInput();
                },
                deleteChanged: (n) {
                  // userName = "";
                  setState(() {});
                  // checkInput();
                },
              ),
            ),
            Container(width: getScreenWidth(),height: 1.w,color:c_b1 ,),
            Container(
              color: c_white,
              child: LoginInput(
                '',
                '请输入新密码',
                key: Key("2"),
                onChanged: (text) {
                  // userName = text;
                  setState(() {});
                  // checkInput();
                },
                deleteChanged: (n) {
                  // userName = "";
                  setState(() {});
                  // checkInput();
                },
              ),
            ),
            Container(
              height: 22.w,
            ),
            Padding(
                padding: EdgeInsets.only(left: 12.w, right: 12.w, top: 10.w),
                child: LoginButton(
                  '提 交',
                  enable: loginEnable,
                  onPressed: () => send(),
                ))
          ],),
        ),));
  }

  send() {}
}