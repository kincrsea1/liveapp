// @dart=2.9
// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, avoid_print, unnecessary_this
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import '../../../../../../network/dio_util/dio_method.dart';
import '../../../../../../network/dio_util/dio_response.dart';
import '../../../../../../network/dio_util/dio_util.dart';
import '../../../../../../store/session_storage/state_user.dart';
import '../../../../../../tools/db/hi_cache.dart';
import '../common/base_navigation_view.dart';
import '../login/login_button.dart';
import '../login/util/string_util.dart';
import '../tools/common/date.dart';
import '../tools/picker_view.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import '../../../../../../model/user_info_model.dart' as UserModal;

class ProfileDate extends BaseWidget {
  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ProfileDate();
  }
}

class _ProfileDate extends BaseWidgetState<ProfileDate> {
  DateTime date;
  String dateStr;
  bool loginEnable = true;
  bool isshowDailog = false;

  @override
  void initState() {
    super.initState();
    if (dateStr == null) {
      dateStr = MyDate.format('yyyy-MM-dd', date);
    }
  }

  @override
  Widget PageBody(AppState state) {
    return GestureDetector(
      child: Scaffold(
          backgroundColor: c_w,
          appBar: BaseNavigation("绑定出生日期", "assets/images/mine/mine_nav_kefu.png"),
          body: Container(
              child: Stack(
            children: [
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 27.w),
                    color: c_white,
                    child: GestureDetector(
                      child: Container(
                          margin: EdgeInsets.only(left: 20.w, right: 20.w),
                          alignment: Alignment.centerLeft,
                          height: 50.w,
                          width: getScreenWidth(),
                          color: c_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '${dateStr}',
                                style: TextStyle(color: c_6, fontSize: 16.sp),
                              ),
                              Image(height: 17, width: 17, image: AssetImage("assets/images/onlive/mine_polygon.png")),
                            ],
                          )),
                      onTap: () {
                        setState(() {
                          isshowDailog = !isshowDailog;
                        });
                      },
                    ),
                  ),
                  Container(width: getScreenWidth(), height: 1.w, color: c_b1),
                  // dateDialog(),
                  Container(
                    height: 22.w,
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 12.w, right: 12.w, top: 10.w),
                      child: LoginButton(
                        '提 交',
                        enable: loginEnable,
                        onPressed: () => send(),
                      ))
                ],
              ),
              if (isshowDailog) Positioned(left: 20.w, top: 27.w + 50.w + 1.w, child: dateDialog())
            ],
          ))),
      onTap: () {
        setState(() {
          isshowDailog = false;
        });
      },
    );
  }

  Widget dateDialog() {
    return GestureDetector(
      child: Container(
        width: getScreenWidth() - 40.w,
        color: c_white,
        child: MyDatePicker(
          itemHeight: 50,
          isShowHeader: false,
          background: Colors.white,
          color: Colors.black,
          current: date,
          magnification: 1,
          end: MyDate.getNow(),
          squeeze: 1.45,
          offAxisFraction: 0.2,
          onChange: _change('yyyy-MM-dd'),
        ),
      ),
      onTap: () {
        // print("是");
      },
    );
  }

  _change(formatString) {
    print("打印,.,.,.");
    return (_date) {
      print(MyDate.format(formatString, _date));
      setState(() {
        date = _date;
        dateStr = MyDate.format(formatString, _date);
      });
    };
  }

  send() async {
    print(dateStr);
    var res = await DioUtil().request(
      '/api/user/setBirthday?terminal=1',
      method: DioMethod.post,
      data: {'birthday': dateStr},
    );

    print(res.data);
    if (res.code != 0 || res.data['code'] != 0) {
      showToast(res.data['msg']);
      return;
    }

    DioResponse updateResponse = await DioUtil().request('/api/user/getUserInfo?&terminal=1', method: DioMethod.get);
    print(updateResponse.data);
    if (updateResponse.code != 0 || updateResponse.data['code'] != 0) {
      showToast(updateResponse.data['msg']);
      return;
    }
    showToast('修改成功');
    // Map userData = json.decode(HiCache.getInstance().get('userInfoStr'));
    // userData = {...userData, ...updateResponse.data['userInfo']};
    // HiCache.getInstance().setString('userInfoStr', json.encode(userData));

    Map<String,dynamic> userInfo = await localStorage.getItem('userInfo');
    if(userInfo !=null &&isNotEmpty(updateResponse.data['userInfo']["birthday"])){
      print("有效:${updateResponse.data['userInfo']["birthday"]}");

      userInfo = {...userInfo,...{"birthday":updateResponse.data['userInfo']["birthday"]}};
      print(userInfo);
      localStorage.setItem("userInfo", userInfo);
      sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': userInfo}));
    }

    Navigator.pop(context);

    // Map userData = json.decode(HiCache.getInstance().get('userInfoStr'));
  }
}
