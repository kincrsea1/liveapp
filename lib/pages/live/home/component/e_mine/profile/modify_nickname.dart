import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/util/string_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import '../../../../../../network/dio_util/dio_method.dart';
import '../../../../../../network/dio_util/dio_response.dart';
import '../../../../../../network/dio_util/dio_util.dart';
import '../../../../../../store/session_storage/state_user.dart';
import '../../../../../../tools/db/hi_cache.dart';
import '../common/base_navigation_view.dart';
import '../login/login_button.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';

class ModifyNickName extends BaseWidget {
  ModifyNickName({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ModifyNickName();
  }
}

class _ModifyNickName extends BaseWidgetState<ModifyNickName> {
  bool loginEnable = false;
  bool showerrorText = true;
  String nickName = "";
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        backgroundColor: c_w,
        appBar: BaseNavigation("修改昵称", "assets/images/mine/mine_nav_kefu.png"),
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 20.w),
              color: c_white,
              child:Padding(
              padding: EdgeInsets.only(top: 5, left: 5, right: 5),
              child: TextField(
                // controller: _controller,
                cursorColor: Colors.grey,
                // maxLines: 10,
                style: TextStyle(color: c_6),
                decoration: InputDecoration(
                    hintText: "请输入新的昵称",
                    hintStyle:
                    TextStyle(color: c_9,fontSize: 13.sp),
                    border: InputBorder.none,
                    labelStyle: TextStyle(color: Colors.white),
                    contentPadding: EdgeInsets.only(left: 20.w),),
                // maxLength: 200,
                onEditingComplete: () {
                  // log("suggestion desc input complete value ${_controller.text}");
                  // widget.valueChange(_controller.text);
                },
                onChanged: (value) {
                  print("suggestion desc input value $value");
                  // widget.valueChange(value);
                  String regNickname =
                      "^[\u4e00-\u9fa5a-zA-Z0-9]{4,10}\$";
                  bool checkStatus = RegExp(regNickname).hasMatch(value);
                  setState(() {
                    showerrorText = checkStatus;
                    loginEnable = checkStatus;
                    nickName = value;
                  });

                },
              ),
            ), ),
            Container(
              height: 10.w,
            ),
            !showerrorText
                ? Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
              '    输入格式不正确，请重新输入',
              style: TextStyle(
                    color: Color(0XFFF44336),
                    fontSize: 12.sp),
            ),
                )
                : Container(
              height: 0,
            ),
            Container(
              height: 10.w,
            ),
            Padding(
                padding: EdgeInsets.only(left: 12.w, right: 12.w, top: 10.w),
                child: LoginButton(
                  '提 交',
                  enable: loginEnable,
                  onPressed: () => send(),
                ))
          ],
        ));
  }

  send() async{
    DioResponse updateResponse = await DioUtil().request(
      '/splive/app/user/updateNickName?nick=${nickName}&terminal=1',method: DioMethod.post,data:{}
    );
    print(updateResponse.data);
    if (updateResponse.code != 0 || updateResponse.data['code'] != 0) return;

    DioResponse result = await DioUtil()
        .request("/splive/app/user/getUser", method: DioMethod.get);
    if (result.code != 0 || result.data['code'] != 0) return;
    print("打印${result.data}");
    showToast('修改成功');

    // Map userData = json.decode(HiCache.getInstance().get('userInfoStr'));
    // userData = {...userData, ...result.data['data']};
    // HiCache.getInstance().setString('userInfoStr', json.encode(userData));


    Map<String,dynamic> userInfo = await localStorage.getItem('userInfo');
    if(userInfo !=null &&isNotEmpty(result.data['data']["nickName"])){
      print("有效:${result.data['data']["nickName"]}");

      userInfo = {...userInfo,...{"nickName":result.data['data']["nickName"]}};
      print(userInfo);
      localStorage.setItem("userInfo", userInfo);
      sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': userInfo}));
    }
    Navigator.pop(context);
  }
}