// ignore_for_file: file_names, use_key_in_widget_constructors, prefer_const_constructors, non_constant_identifier_names

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/util/string_util.dart';

import '../../../../../../base/base_widget.dart';
import '../../../../../../network/dio_util/dio_method.dart';
import '../../../../../../network/dio_util/dio_response.dart';
import '../../../../../../network/dio_util/dio_util.dart';
import '../../../../../../store/session_storage/state_main.dart';
import '../../../../../../store/session_storage/state_user.dart';
import '../common/base_navigation_view.dart';
import '../login/block_puzzle_captcha.dart';
import '../tools/common/loading_tool.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';

class ModifyPhone extends BaseWidget {
  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ModifyPhone();
  }
}

class _ModifyPhone extends BaseWidgetState<ModifyPhone> {
  String guojia = "中国";
  bool guojiaChoiceVisible = false;
  String phoneNumber = "";
  bool isCorrect = true;
  String vifCode = "";
  bool isCorrectCode = true;
  String captchaVerification = "";
  bool sendCodeIsSuccess = false;
  Timer? _timer;
  bool isLoading = false;
  var DialogContext;
  //倒计时数值
  var _enable = true;
  var _time = 0;
  String? type = "";
  @override
  void dispose() {
    print("销毁....");
    if (_timer != null) {
      _timer?.cancel();
      _timer = null;
    }
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        backgroundColor: c_w,
        appBar: BaseNavigation("手机号码", "assets/images/mine/mine_nav_kefu.png"),
        body: ListView(children: [
          Stack(
            children: [
              Container(
                padding: EdgeInsets.only(left: 10.w, right: 10.w, top: 10.w),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 40.w,
                            child: Stack(
                              children: [
                                TextField(
                                  onChanged: (text) {
                                    phoneNumber = text;
                                    checkInput();
                                  },
                                  inputFormatters: [LengthLimitingTextInputFormatter(guojia == '中国' ? 11 : 10)],
                                  cursorColor: Colors.grey,
                                  style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w300, color: c_6),
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: c_white,
                                    contentPadding: EdgeInsets.only(left: 15, right: 20, top: 5, bottom: 10),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5), borderSide: BorderSide.none),
                                    hintText: '手机号码',
                                    hintStyle: TextStyle(fontSize: 13.sp, color: c_9),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                      child: Text(
                        isCorrect ? "" : "手机号格式不正确",
                        style: TextStyle(fontSize: 12, color: Color(0xFFfd3a2e)),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 40.w,
                            child: Stack(
                              children: [
                                TextField(
                                  onChanged: (text) {
                                    vifCode = text;
                                    checkInputCode();
                                  },
                                  inputFormatters: [LengthLimitingTextInputFormatter(5)],
                                  cursorColor: Colors.grey,
                                  style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w300, color: c_6),
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: c_white,
                                    contentPadding: EdgeInsets.only(left: 15.w, right: 20, top: 5, bottom: 10),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5), borderSide: BorderSide.none),
                                    hintText: '请输入验证码',
                                    hintStyle: TextStyle(fontSize: 13.sp, color: c_9),
                                  ),
                                ),
                                Positioned(
                                  right: 10.0,
                                  top: 15,
                                  child: GestureDetector(
                                    child: Container(
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(),
                                      child: Text(_time == 0 ? "获取验证码" : "${_time}s后重新获取",
                                          style: TextStyle(fontSize: 13.sp, color: c_o)),
                                    ),
                                    onTap: () => setState(() {
                                      if (!isCorrect || phoneNumber.isEmpty) {
                                        showToast('请输入正确的手机号码');
                                        return;
                                      }
                                      if (!_enable) return;
                                      loadingBlockPuzzle(context, () {
                                        _enable ? startCountdown(60) : null;
                                      });
                                    }),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        child: Text(
                          isCorrectCode
                              ? ""
                              : captchaVerification.isEmpty || !sendCodeIsSuccess
                                  ? '请先完成滑块验证'
                                  : '请输入正确的验证码',
                          style: TextStyle(fontSize: 12, color: Color(0xFFfd3a2e)),
                        )),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "为了您的隐私安全，信息在提交后将无法修改。",
                        style: TextStyle(height: 1.9, color: Colors.grey, fontSize: 13),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 30),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "请务必如实填写，以保护您的账户安全。",
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (checkForm()) {
                          _submitChange();
                        }
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        clipBehavior: Clip.antiAlias,
                        child: Container(
                          margin: EdgeInsets.only(left: 12.w, right: 12.w, top: 10.w),
                          height: 40.w,
                          color: checkForm() ? c_o : c_o.withAlpha(100),
                          alignment: Alignment.center,
                          child: isLoading
                              ? SizedBox(
                                  width: 15.w,
                                  height: 15.w,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(Color(0XFF37373A)),
                                    strokeWidth: 2,
                                  ),
                                )
                              : Text(
                                  "提 交",
                                  style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),
                                ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ]));
  }

  // 验证码结构
  Positioned buildPositioned() {
    return Positioned(
      child: Container(
          alignment: Alignment.topLeft,
          child: GestureDetector(
            child: Container(
              padding: EdgeInsets.only(left: 5),
              alignment: Alignment.centerLeft,
              margin: EdgeInsetsDirectional.only(top: 40, start: 25),
              width: 48,
              height: 20,
              decoration: BoxDecoration(
                color: Color.fromRGBO(62, 63, 68, 1).withAlpha(155),
              ),
              child: Text(guojia == "中国" ? "台湾" : "中国", style: TextStyle(fontSize: 10, color: Color(0XFFc1c2c4))),
            ),
            onTap: () {
              setState(() {
                guojia = guojia == "中国" ? "台湾" : "中国";
                guojiaChoiceVisible = false;
                checkInput();
              });
            },
          )),
    );
  }

  // 手机号码正则
  bool phone(String input) {
    if (input == null || input.isEmpty) return false;
    String dlRule = "^((13[0-9])|(15[^4])|(16[0-9])|(17[0-8])|(18[0-9])|(19[0-9])|(14[0-9]))\\d{8}\$";
    String twRule = "^(886)?09\\d{8}\$";
    String regexPhoneNumber = guojia == '中国' ? dlRule : twRule;
    return RegExp(regexPhoneNumber).hasMatch(input);
  }

  // 验证码正则
  bool code(String input) {
    if (input == null || input.isEmpty) return false;
    //手机正则验证
    String regexcode = "^\\d{5}\$";
    return RegExp(regexcode).hasMatch(input);
  }

  // 检测手机号码
  checkInput() {
    setState(() {
      isCorrect = phone(phoneNumber);
    });
  }

  // 验证码检测
  checkInputCode() {
    setState(() {
      isCorrectCode = code(vifCode) && sendCodeIsSuccess;
    });
  }

  //滑动拼图
  void loadingBlockPuzzle(BuildContext context, callback, {barrierDismissible = true}) {
    showDialog<Null>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        DialogContext = context;
        return BlockPuzzleCaptchaPage(
          onSuccess: (v) {
            captchaVerification = v.toString();
            _sendmobileCode(callback);
          },
          onFail: () {},
        );
      },
    );
  }

  // 发送验证码
  void _sendmobileCode(callback) async {
    Loading.showLoading(context);

    Map<String, dynamic> Parms = {
      "captchaVerification": captchaVerification,
      "mobile": phoneNumber,
      "mobileAreaCode": guojia == '中国' ? "86" : "886",
    };
    DioUtil().request('/api/user/sendMobCode?terminal=1', method: DioMethod.post, data: Parms).then((res) {
      Loading.hideLoading(context);
      Navigator.pop(DialogContext);
      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      setState(() {
        sendCodeIsSuccess = true;
      });
      showToast("成功发送验证码");
      callback();
    }).catchError((error) {
      Loading.hideLoading(context);
      print(error);
    });
  }

  // 验证是否全部通过
  bool checkForm() =>
      phoneNumber.isNotEmpty && isCorrect && vifCode.isNotEmpty && isCorrectCode && captchaVerification.isNotEmpty;

  // 修改表单提交
  void _submitChange() async {
    setState(() => isLoading = true);

    Map<String, dynamic> Parms = {
      "captcha": captchaVerification,
      "code": vifCode,
      "mobile": phoneNumber,
    };

    try {
      var res = await DioUtil().request('/api/user/vfyMobCode?terminal=1', method: DioMethod.post, data: Parms);
      Loading.hideLoading(context);
      setState(() => isLoading = false);
      if (res.code != 0 || res.data['code'] != 0) {
        showToast(res.data['msg']);
        return;
      }
      /*
      var userInfoResponse = await HttpManager.getRequestData('/user/getUserInfo?&terminal=1');
      Map userData = json.decode(HiCache.getInstance().get('userInfoStr'));
      userData = {...userData, ...userInfoResponse['userInfo']};
      HiCache.getInstance().setString('userInfoStr', json.encode(userData));
      var store = StoreProvider.of<AppState>(context);
      store.dispatch(UpdateUserAction(payload: {'userInfo': userInfoResponse['userInfo']}));

       */
      DioResponse updateResponse = await DioUtil().request('/api/user/getUserInfo?&terminal=1', method: DioMethod.get);
      print(updateResponse.data);
      if (updateResponse.code != 0 || updateResponse.data['code'] != 0) {
        showToast(updateResponse.data['msg']);
        return;
      }
      // Map? userInfo = await localStorage.getItem('userInfo');
      // if(userInfo !=null &&isNotEmpty(updateResponse.data['userInfo']["mobile"])){
      //   userInfo = {...userInfo,...updateResponse.data['userInfo']["mobile"]};
      //   localStorage.setItem("userInfo", userInfo);
      //   sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': userInfo}));
      // }

      Map<String, dynamic>? userInfo = await localStorage.getItem('userInfo');
      if (userInfo != null && isNotEmpty(updateResponse.data['userInfo']["mobile"])) {
        print("有效:${updateResponse.data['userInfo']["mobile"]}");

        userInfo = {
          ...userInfo,
          ...{"mobile": updateResponse.data['userInfo']["mobile"]}
        };
        print(userInfo);
        localStorage.setItem("userInfo", userInfo);
        sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': userInfo}));
      }

      Navigator.pop(context, true);
      showToast("成功绑定手机号");
    } catch (e) {
      setState(() => isLoading = false);
      print(e);
    }
  }

  //倒计时方法
  void startCountdown(int count) {
    if (!_enable) return;
    setState(() {
      _enable = false;
      _time = count;
    });
    //倒计时时间
    _timer = Timer.periodic(Duration(seconds: 1), (Timer it) {
      setState(() {
        if (it.tick == count) {
          _enable = true;
          it.cancel();
        }
        _time = count - it.tick;
      });
    });
  }
}
