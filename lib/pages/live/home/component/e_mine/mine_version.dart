import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import 'common/base_navigation_view.dart';

class MineVersion extends BaseWidget {
  MineVersion({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _MineVersion();
  }
}

class _MineVersion extends BaseWidgetState<MineVersion> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        backgroundColor: c_w,
        appBar: BaseNavigation("版本号", "assets/images/mine/mine_nav_kefu.png"),
        body: Container(child: Text("版本号")));
  }
}