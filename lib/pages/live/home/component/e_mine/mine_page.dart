// @dart=2.9
// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, use_full_hex_values_for_flutter_colors, avoid_unnecessary_containers, avoid_function_literals_in_foreach_calls, sized_box_for_whitespace
import 'dart:convert';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/bottom_nav_bar.dart';
import 'package:liveapp/common_widget/base_web_view.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/pages/live/home/component/b_onlive/onlive_page.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/live_login.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/suggestion_report.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../../config/date/date_utils.dart';
import '../../../../../config/image/local_image_selector.dart';
import '../../../../../network/dio_util/dio_method.dart';
import '../../../../../network/dio_util/dio_util.dart';
import '../../../../../store/session_storage/state_user.dart';
import '../../../../../tools/db/hi_cache.dart';
import 'mine_about_us.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';

class minePage extends BaseWidget {
  @override
  BaseWidgetState<BaseWidget> getState() {
    return _minePageState();
  }
}

class _minePageState extends BaseWidgetState<minePage> {
  String guanZhu = "0";
  String yuYue = "0";
  final _refreshController = RefreshController();
  final List<MineModel> _list = [
    MineModel(
        iconName: 'assets/images/mine/mine_personal.png',
        title: '个人中心',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "profile"),
    MineModel(
        iconName: 'assets/images/mine/mine_message.png',
        title: '消息中心',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "message"),
    MineModel(
        iconName: 'assets/images/mine/mine_kefu.png',
        title: '在线客服',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "kefu"),
    // MineModel(
    //     iconName: 'assets/images/mine/mine_system.png',
    //     title: '系统设置',
    //     rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
    //     target: "system"),
    MineModel(
        iconName: 'assets/images/mine/mine_report.png',
        title: '意见反馈',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "suggestion"),
    MineModel(
        iconName: 'assets/images/mine/mine_about.png',
        title: '关于我们',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "about_us"),
    MineModel(
        iconName: 'assets/images/mine/mine_version.png',
        title: '版本号',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "login"),
    MineModel(
        iconName: 'assets/images/mine/mine_version.png',
        title: '退出登录',
        rightIconName: 'assets/images/mine/mine_iocn-Unfold.png',
        target: "login"),
  ];

  @override
  void initState() {
    super.initState();
    print("第一次我的页面");
    getData();
    Future.delayed(Duration(seconds: 1), _checkUserInfo);
  }

  @override
  Widget PageBody(AppState state) {
    print("我的页面刷新");
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
              child: Container(
                  width: double.infinity,
                  height: getScreenWidth() * 365 / 750.0,
                  child: Image(
                    image: AssetImage('assets/images/mine/mine_bg_head.png'),
                    fit: BoxFit.fill,
                  ))),
          Container(
            padding: EdgeInsets.only(top: getTopBarHeight() + 10.w, left: 14.w, right: 20.w),
            width: double.infinity,
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Container(
                height: 50.w,
                child: Row(
                  children: [
                    Container(
                        width: 50.w,
                        height: 50.w,
                        child: sessionStorage.state.user.userInfo.logo != null
                            ?
                        // Image.network(
                        //         sessionStorage.state.user.userInfo.logo,
                        //         fit: BoxFit.fill,
                        //       )
                        FadeInImage.assetNetwork(
                          placeholder: 'assets/images/mine/mine_avatar.png',
                          image: sessionStorage.state.user.userInfo.logo,
                          fit: BoxFit.fill,
                        )
                            : Image(
                                image: AssetImage('assets/images/mine/mine_avatar.png'),
                                fit: BoxFit.fill,
                              )),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: sessionStorage.state.user.userInfo.nickName != null
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "${sessionStorage.state.user.userInfo.nickName}",
                                    style: TextStyle(fontSize: 11.sp, color: Colors.white),
                                  ),
                                ),
                                Container(
                                  height: 5,
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "已加入一个直播 ${getJoinDays()}天",
                                    style: TextStyle(fontSize: 11.sp, color: Colors.white),
                                  ),
                                )
                              ],
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                  GestureDetector(
                                    child: Container(
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                          // border: Border.all(),
                                          borderRadius: BorderRadius.circular(100),
                                          color: Colors.white),
                                      width: 80.w,
                                      height: 35.w,
                                      child: Text(
                                        "登录/注册",
                                        style: TextStyle(fontSize: 12.sp, color: c_o, fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                    onTap: () {
                                      goPage(url: "login");
                                    },
                                  )
                                ]),
                    )
                  ],
                ),
              ),
              GestureDetector(
                child: Container(
                    // color: Colors.green,
                    width: 22.w,
                    height: 20.w,
                    child: Image(
                      image: AssetImage('assets/images/mine/mine_head_kefu.png'),
                      fit: BoxFit.fill,
                    )),
                onTap: () {
                  String kefuUrl = HiCache.getInstance().get("kefuURL");
                  print(kefuUrl);
                  Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return BaseWebView(
                      '客服',
                      baseUrl: kefuUrl,
                    );
                  }));
                },
              ),
            ]),
          ),
          Container(
            // color: Colors.red,
            margin: EdgeInsets.only(top: getScreenWidth() * 365 / 750.0),
            // padding: EdgeInsets.only(top: getTopBarHeight() + 10.w),
            width: double.infinity,
            child: Column(children: [
              mineFocusAndRes(),
              mineList()
              // _TabNav,
              // SizedBox(height: 10.w),
              // _viewBody(),
            ]),
          ),
        ],
      ),
    );
  }

  Container mineFocusAndRes() {
    return Container(
        margin: EdgeInsets.only(left: 30.w, right: 30.w),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          GestureDetector(
            child: Row(
              children: [
                Container(
                    width: 38.w,
                    height: 38.w,
                    child: Image(
                      image: AssetImage('assets/images/mine/mine_fucus.png'),
                      fit: BoxFit.fill,
                    )),
                Column(
                  children: [
                    Container(
                      child: Text(
                        guanZhu,
                        style: TextStyle(fontSize: 19.sp, color: Color(0XFFF36704)),
                      ),
                    ),
                    Container(
                      child: Text(
                        "我的关注",
                        style: TextStyle(fontSize: 10.sp, color: c_6),
                      ),
                    )
                  ],
                )
              ],
            ),
            onTap: () {
              if (sessionStorage.state.user.userInfo.logo == null) {
                goPage(url: "login");
              } else {
                goPage(url: 'focus', params: {
                  'type': '1',
                });
              }
            },
          ),
          Container(
            height: 40.w,
            width: 1.w,
            color: Color(0xFFF1E1DA),
          ),
          GestureDetector(
            child: Row(
              children: [
                Container(
                    width: 38.w,
                    height: 38.w,
                    child: Image(
                      image: AssetImage('assets/images/mine/mine_res.png'),
                      fit: BoxFit.fill,
                    )),
                Column(
                  children: [
                    Container(
                      child: Text(
                        yuYue,
                        style: TextStyle(fontSize: 19.sp, color: Color(0XFFF36704)),
                      ),
                    ),
                    Container(
                      child: Text(
                        "我的预约",
                        style: TextStyle(fontSize: 10.sp, color: Color(0XFF666666)),
                      ),
                    )
                  ],
                )
              ],
            ),
            onTap: () {
              if (sessionStorage.state.user.userInfo.logo == null) {
                goPage(url: "login");
              } else {
                goPage(url: 'focus', params: {
                  'type': '0',
                });
              }
            },
          ),
        ]));
  }

  Expanded mineList() {
    return Expanded(
      child: ListView.separated(
          shrinkWrap: true,
          // physics: const ScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            bool isPadding = index == _list.length - 1;
            if (_list[index].title == "退出登录") {
              print("退出");
              if (sessionStorage.state.user.userInfo.nickName != null) {
                return quitLogin();
              } else {
                return Container();
              }
            } else {
              return GestureDetector(
                onTap: () async {
                  if (_list[index].title == "在线客服") {
                  String kefuUrl = HiCache.getInstance().get("kefuURL");
                  print(kefuUrl);
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                      return BaseWebView(
                        '客服',
                        baseUrl: kefuUrl,
                      );
                    }));
                    return;
                  }
                  if (_list[index].title == "版本号") {
                    return;
                  }
                  if (sessionStorage.state.user.userInfo.logo != null || _list[index].title == "关于我们") {
                    goPage(url: _list[index].target);
                  } else {
                    goPage(url: "login");
                  }
                },
                child: Padding(
                  padding: EdgeInsets.only(right: 10, left: 10, bottom: isPadding ? 15 : 0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    clipBehavior: Clip.antiAlias,
                    child: Container(
                      color: c_w,
                      height: 40,
                      child: Row(
                        children: [
                          Container(
                            width: 50,
                            padding: const EdgeInsets.only(left: 10),
                            alignment: Alignment.centerLeft,
                            child: Image(
                              image: AssetImage(_list[index].iconName),
                              width: 18,
                              height: 18,
                            ),
                          ),
                          Expanded(
                              child: Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              _list[index].title,
                              style: TextStyle(color: Color(0xff666666), fontSize: 12.sp, fontWeight: FontWeight.w400),
                            ),
                          )),
                          Container(
                            padding: const EdgeInsets.only(right: 10),
                            width: 100,
                            alignment: Alignment.centerRight,
                            child: _list[index].title == "版本号"
                                ? Container(
                                    margin: EdgeInsets.only(right: 0),
                                    child: Text(
                                      "v${ConfigUtil().APP_VERSION}",
                                      style: TextStyle(color: c_9),
                                    ))
                                : Image(
                                    image: AssetImage(
                                      _list[index].rightIconName,
                                    ),
                                    fit: BoxFit.fitHeight,
                                    width: 5.w,
                                    height: 10.w,
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
          },
          separatorBuilder: (BuildContext context, int index) {
            return const Divider(
              height: 5,
              color: Colors.transparent,
            );
          },
          itemCount: _list.length),
    );
  }

  SliverToBoxAdapter headerView() {
    return SliverToBoxAdapter(
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(image: AssetImage("assets/images/mine/mine_bg_head.png"), fit: BoxFit.fill),
          color: Colors.transparent,
        ),
        height: 191,
        child: Text("SaSa"),
      ),
    );
  }

  Center quitLogin() {
    return Center(
      child: GestureDetector(
        child: Container(
          margin: EdgeInsets.only(top: 40.w),
          alignment: Alignment.center,
          height: 44.w,
          width: 137.w,
          decoration: BoxDecoration(
            color: c_border,
            borderRadius: BorderRadius.circular(100.w),
            border: Border.all(width: 1.w, color: c_o),
          ),
          child: Text(
            "退出登录",
            style: TextStyle(color: c_o),
          ),
        ),
        onTap: () {
          guanZhu = "0";
          yuYue = "0";

          localStorage.deleteItem('userInfo');
          sessionStorage.dispatch(DelUserInfo(payload: {}));
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
            return Bottomnavigationbar(
              selectPage: 1,
            );
          }));
          // goPage(url: "login");
        },
      ),
    );
  }

  void _test() {
    // print(sessionStorage.state.user.test);
    // sessionStorage.dispatch(UpdateTest(payload: {'test': 'hello'}));
    //- 持久话
    setState(() {});
  }

  void getData() async {
    getGuanZhu();
    getYuYue();
  }

  void getGuanZhu() async {
    print("关注");
//POST /app/getUserFollowTotal
    DioUtil().request('/splive/app/getUserFollowTotal', method: DioMethod.post, data: {}).then((res) {
      if (res.code != 0 || res.data['code'] != 0) {
        return;
      }

      print("关注:${res.data}");
      setState(() {
        guanZhu = res.data["total"].toString();
      });
    }).catchError((error) {
      print(error);
    });
  }

  void getYuYue() async {
    // POST /app/getEventResvaTotal
    DioUtil().request('/splive/app/getEventResvaTotal', method: DioMethod.post, data: {}).then((res) {
      if (res.code != 0 || res.data['code'] != 0) {
        return;
      }

      print("预约:${res.data}");
      setState(() {
        yuYue = res.data["total"].toString();
      });
    }).catchError((error) {
      print(error);
    });
  }

  Future _checkUserInfo() async {
    Map userInfo = await localStorage.getItem('userInfo');
    if (userInfo != null) {
      sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': userInfo}));
    }
  }

  String getJoinDays() {
    String registerTime = sessionStorage.state.user.userInfo.registerTime;

    if (registerTime == null) {
      return "1";
    }
    int start = DateTime.parse(registerTime).millisecondsSinceEpoch;
    int end = DateTime.now().millisecondsSinceEpoch;
    int days = (end - start) ~/ (1000 * 24 * 3600);
    return days == 0 ? '1' : days.toString();
  }
}

class MineModel {
  String iconName = '';
  String title = '';
  String rightIconName = '';

  //跳转的目标
  String target;

  MineModel({this.iconName, this.title, this.rightIconName, this.target});

  @override
  String toString() {
    return 'MineModel{iconName: $iconName, title: $title, rightIconName: $rightIconName, target: $target}';
  }
}
