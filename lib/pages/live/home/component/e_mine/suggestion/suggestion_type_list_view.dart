import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/mo/suggest_model.dart';

import '../../../../../../network/dio_util/dio_method.dart';
import '../../../../../../network/dio_util/dio_util.dart';
import 'event_bus_util.dart';


typedef SuggestionTypeListViewSelected = Function(String title, int index);

class SuggestionTypeListView extends StatefulWidget {
  final bool visible;
  final SuggestionTypeListViewSelected click;
  const SuggestionTypeListView(
      {Key? key, required this.visible, required this.click})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _SuggestionTypeListView();
}

class _SuggestionTypeListView extends State<SuggestionTypeListView> {
  late int _index = 1111111;

  List<Data> titles =  List<Data>.empty(growable: true);

  @override
  void initState() {
    // TODO: implement initState
    bus.on(EventBus.CLEARUPSUGGESTIONCONTENT, (arg) {
      setState(() {
        widget.click('', 0);
        setState(() {
          _index = 0;
        });
      });
    });

    getData();
  }

  @override
  Widget build(BuildContext context) {
    double left = (MediaQuery.of(context).size.width - 240) / 2;
    return Visibility(
        visible: widget.visible,
        child: Positioned(
          left: left,
          top: 90,
          child: Container(
              height: 240,
              width: 250,
              color: Colors.white,
              child: ListView.separated(
                itemBuilder: (_, index) {
                  return GestureDetector(
                    child: Container(
                        height: 30,
                        alignment: Alignment.center,
                        color: _index == index
                            ? Color(0XFFF36704)
                            : Colors.transparent,
                        child: Text(
                          "${titles[index].reportName}",
                          style: TextStyle(
                              color: _index == index
                                  ? Colors.white
                                  : Color(0XFFF36704),
                              fontSize: 14),
                        )),
                    onTap: () {
                      widget.click(titles[index].reportName, titles[index].id);
                      setState(() {
                        _index = index;
                      });
                    },
                  );
                },
                separatorBuilder: (_, index) {
                  return Divider(color: Colors.transparent, height: 0);
                },
                itemCount: titles.length,
                shrinkWrap: true,
              )),
        ));
  }

  void getData() async{
    // POST /app/report/listAppSpReportType
    DioUtil().request('/splive/app/report/listAppSpReportType', method: DioMethod.post, data: {}).then((res) {

      if (res.code != 0 || res.data['code'] != 0) {

        return;
      }

      print("列表:${res.data}");
      SuggestModel suggestModel = SuggestModel.fromJson(res.data);
     List<Data> datas = suggestModel.data;
     titles.clear();
     for(int i = 0; i < datas.length; i++){
       if(datas[i].available != 0){
         titles.add(datas[i]);
       }
     }
      setState(() {
        // guanZhu = res.data["total"].toString();
      });


    }).catchError((error) {

      print(error);
    });
  }



}
