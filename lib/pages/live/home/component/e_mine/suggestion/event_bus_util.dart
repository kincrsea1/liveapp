// @dart=2.9

//订阅者回调签名
typedef void EventCallback(arg);

//todo  单独给 充值用
class EventBusRechargeType {
  // 单例公开访问点
  factory EventBusRechargeType() => _sharedInstance();

  // 静态私有成员，没有初始化
  static EventBusRechargeType _instance;

  // 私有构造函数
  EventBusRechargeType._() {
    // 具体初始化代码
  }

  // 静态、同步、私有访问点
  static EventBusRechargeType _sharedInstance() {
    if (_instance == null) {
      _instance = EventBusRechargeType._();
    }
    return _instance;
  }

  int inPop = 0; //充值  一级头部组件下标

  int inDex = 0; // 充值 二级头部组件下标

  String type = "finish"; //回传状态标识

  List list = ['存款', '提款', '交易记录', '银行卡'];
}

class EventBus {
  //todo   充值 新建 Type
  static String WITHDRAWAL_GO_TO_MAIN_ME = 'withdrawalGoMainMe';
  //todo   充值 新建 Type
  static String WITHDRAWAL_GO_TO_BANK = 'withdrawalGoToBank';
  //- 全屏开启聊天监听
  static String SHOWCHATSLIDER = 'showChatSlider';
  //- 直播内容错误描述信息
  static String GENERATEERRORTEXT = 'generateErrorText';
  //- 弹窗开启通知
  static String OPENFLOATING = 'openFloating';
  //- 登录弹窗开启
  static String OPENLOGINTIPDIALOG = 'openLoginTipDialog';

  ///回调刷新widget通知
  static String WiDGETSETSTATE = 'widgetSetState';

  ///清空意见反馈内容
  static String CLEARUPSUGGESTIONCONTENT = 'clearUpSuggestionContent';

  ///=================================================比分相关=========================///
  ///联赛赛选更新最新的数据
  static String SCOREMATCHSHIRTDATANOTICE = 'ScoreMatchShirtDataNotice';

  ///联赛赛选实时刷新赛程赛果的数据
  static String SCORERESULTANDMATCHINGRELOADDATA = 'ScoreResultAndMatchingRelaodData';

  ///联赛赛选实时刷新赛程赛果的数据2
  static String SCORERESULTANDMATCHINGRELOADDATAREQUEST = 'ScoreResultAndMatchingRelaodDataRequesr';

  ///=================================================比分相关=========================///

  //- 弹幕消息(新增监听)
  static String ADDBARRAGEDATA = 'addBarrageData';
  //- 请求失败
  static String LISTREQUESTERROR = 'ListRequestError';

  //- 弹幕消息(开关监听)
  static String WATCHISOPENBARRAGE = 'watchIsOpenBarrage';
  //- 切换媒体按钮重新初始化弹幕状态EventBus
  static String RELOADBARRAGEINFO = 'reloadBarrageInfo';
  //- 详情界面监听小窗口是否关闭
  static String CLOSETARGETPAGE = 'closeTargetPage';
  //- 大屏幕视频 聊天信息发送监听
  static String SENDCHATMESSAGE = 'sendChatMessage';
  //- 跳转其他界面
  static String GOTARGETPAGE = 'goTargetPage';
  //- 循环推荐列表事件监听
  static String WATCHRECOMMENDLOOP = 'watchRecommendLoop';
  //- 更新关注主播内容
  static String RELOADAUDIENCELENGTH = 'reloadAudienceLength';

  ///私有构造函数
  EventBus._internal();

  ///保存单例
  static EventBus _singleton = new EventBus._internal();

  ///工厂构造函数
  factory EventBus() => _singleton;

  //保存事件订阅者队列，key:事件名(id)，value: 对应事件的订阅者队列
  var _emap = new Map<Object, List<EventCallback>>();

  //添加订阅者
  void on(eventName, EventCallback f) {
    if (eventName == null || f == null) return;
    _emap[eventName] ??= new List<EventCallback>();
    _emap[eventName].add(f);
  }

  //移除订阅者
  void off(eventName, [EventCallback f]) {
    var list = _emap[eventName];
    if (eventName == null || list == null) return;
    if (f == null) {
      _emap[eventName] = null;
    } else {
      list.remove(f);
    }
  }

  //触发事件，事件触发后该事件所有订阅者会被调用
  void emit(eventName, [arg]) {
    var list = _emap[eventName];
    if (list == null) return;
    int len = list.length - 1;
    //反向遍历，防止订阅者在回调中移除自身带来的下标错位
    for (var i = len; i > -1; --i) {
      list[i](arg);
    }
  }
}

//定义一个top-level（全局）变量，页面引入该文件后可以直接使用bus
var bus = new EventBus();
