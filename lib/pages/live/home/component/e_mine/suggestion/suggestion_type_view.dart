// ignore_for_file: prefer_const_constructors

import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


import '../../../../../../config/image/local_image_selector.dart';

typedef SuggestionTypeViewClick = void Function(bool);

class SuggestionTypeView extends StatefulWidget {
  /// 点击弹窗的响应事件
  final SuggestionTypeViewClick click;

  /// 选择的类型
  final String? type;
  final bool visible;
  const SuggestionTypeView({Key? key, required this.click, this.type,this.visible = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _SuggestionTypeViewState();
}

class _SuggestionTypeViewState extends State<SuggestionTypeView> {


  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Padding(
            padding:  EdgeInsets.only(left: 14.w, top: 15.w,bottom: 15.w),
            child: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "问题类型：",
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFF666666), fontSize: 14),
              ),
            )),
        Container(
        color:Colors.white,
            child: Container(
              margin: EdgeInsets.only(left: 12, right: 12),
              height: 40,
              decoration: BoxDecoration(
                  // border: Border.all(),
                  borderRadius: BorderRadius.circular(4),
                  color:Colors.white
                      ),
              child: Container(
                child: GestureDetector(
                  onTap: () {
                    log("选择问题类型");
                    setState(() {
                    bool _visible = !widget.visible;
                      print(_visible);
                      widget.click(_visible);
                    });
                  },
                  behavior: HitTestBehavior.opaque,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        (widget.type == null || widget.type?.length == 0)
                            ? "请选择问题类型"
                            : widget.type!,
                        style: TextStyle(
                            color: Color(0xFF919699),
                            fontSize: 14),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: LocalImageSelector.getSingleImage(
                            "report_arrow",
                            imageHeight: 16,
                            imageWidth: 16,
                            bFitFill: BoxFit.contain),
                      )
                    ],
                  ),
                ),
              ),
            )),
      ],
    ));
  }
}
