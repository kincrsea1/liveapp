// ignore_for_file: prefer_const_constructors, import_of_legacy_library_into_null_safe

import 'dart:collection';
import 'dart:developer';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/base_web_view.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/pages/live/home/component/e_mine/common/base_navigation_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/suggestion_description_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/suggestion_select_image_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/suggestion_type_list_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/suggestion_type_view.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import '../tools/common/loading_tool.dart';
import 'event_bus_util.dart';

///意见反馈
class SuggestionReport extends BaseWidget {
  SuggestionReport({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    // TODO: implement getState
    return _SuggestionReportState();
  }
}

class _SuggestionReportState extends BaseWidgetState<SuggestionReport> {
  bool _visible = false;
  String? type;
  String _path = '';
  String _content = '';
  int _index = 0;
  bool buttonEnable = false;
  bool contenteable = false;
  bool showeable = false;
  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      appBar: BaseNavigation("意见反馈", "assets/images/mine/mine_nav_kefu.png"),
      body: Container(
        decoration: BoxDecoration(
            // ignore: prefer_const_literals_to_create_immutables
            color: Color(0xFFFAF7F5)),
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(),
          child: Stack(
            children: [
              ListView(
                shrinkWrap: true,
                children: [
                  SuggestionTypeView(
                    type: type,
                    visible: _visible,
                    click: (value) {
                      setState(() {
                        _visible = value;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 5)),
                  SuggestionDescView(
                    showable: showeable,
                    enable: contenteable,
                    valueChange: (value) {
                      showeable = true;
                      _content = value;
                      checkEnable();
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 5)),
                  SuggestionSelectImageView(
                    click: (path) {
                      setState(() {
                        _path = path;
                        // updateImage();
                      });
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.w, left: 14.w, right: 14.w),
                    child: GestureDetector(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 44.w,
                        alignment: Alignment.center,
                        decoration: buttonEnable == true
                            ? BoxDecoration(
                                color: Color(0XFFF36704),
                                borderRadius: BorderRadius.circular(100),
                              )
                            : BoxDecoration(
                                color: Color(0XFFF36704).withAlpha(110),
                                borderRadius: BorderRadius.circular(100),
                              ),
                        child: Text(
                          "提交",
                          style: TextStyle(color: buttonEnable == true ? Colors.white : Colors.white, fontSize: 14),
                        ),
                      ),
                      onTap: () {
                        log("click submit");
                        if (buttonEnable == true) {
                          updateImage(isEnd: true);
                        }
                        ;
                      },
                    ),
                  )
                ],
              ),
              SuggestionTypeListView(
                visible: _visible,
                click: (title, index) {
                  log("click type index $index");

                  if (title.length > 0) {
                    setState(() {
                      _visible = !_visible;
                      type = title;
                      _index = index;
                      checkEnable();
                    });
                  } else {
                    setState(() {
                      type = title;
                      _index = index;
                      checkEnable();
                    });
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void updateImage({bool isEnd = false}) async {
    Loading.showLoading(context);
    String name = _path.split('/').last;
    log(name);
    // FormData formData = FormData.fromMap({
    //   'uploadMessageFile': MultipartFile.fromFileSync(_path, filename: name),
    // });
    Map<String, dynamic> formDataMap = HashMap();
    if (_path.length > 0) {
      formDataMap = {
        'uploadMessageFile': MultipartFile.fromFileSync(_path, filename: name),
      };
    } else {
      // showToast(' 图片信息不完整');
      report("");
      return;
    }
    FormData formData = FormData.fromMap(formDataMap);

    DioUtil()
        .uploadImage(
      '/api/opinion/messageSend?terminal=1',
      method: DioMethod.post,
      data: formData,
    )
        .then((result) {
      // Loading.hideLoading(context);
      log('update image result $result');
      // showToast("${result.data['msg']}");
      if (result.code != 0 || result.data['code'] != 0) {
        showToast(result.data['msg']);
        return;
      }
      //提交数据
      report(result.data["data"]);
    }).catchError((error) {
      Loading.hideLoading(context);
      print('747行打印：============ ${error}');
      // print(error);
    });
  }

  void report(String imgUrl) async {
    // POST /app/report/saveOrUpdateReportLog
    Map<String, String> parms = {
      "content": _content.trim(),
      "id": "",
      "qq": "",
      "imgUrl": imgUrl,
      "roomId": "",
      "reportType": _index.toString(),
      "username": ""
    };
    print("参数${parms}");
    DioUtil().request('/splive/app/report/saveOrUpdateReportLog', method: DioMethod.post, data: parms).then((res) {
      Loading.hideLoading(context);
      if (res.code != 0 || res.data['code'] != 0) {
        return;
      }

      print("提交:${res.data}");
      showToast("反馈成功");
      Navigator.of(context).pop();
    }).catchError((error) {
      Loading.hideLoading(context);
      print(error);
    });
  }

  void checkEnable() {
    ///问题类型
    bool QuestionType = false;
    if (type != null && type!.length > 0) {
      QuestionType = true;
    }

    ///问题描述
    bool QuestionDes = false;
    contenteable = false;
    if (_content != null && _content.length >= 20) {
      QuestionDes = true;
      contenteable = true;
    }

    if (QuestionType == true && QuestionDes == true) {
      buttonEnable = true;
    } else {
      buttonEnable = false;
    }

    setState(() {});
  }
}
