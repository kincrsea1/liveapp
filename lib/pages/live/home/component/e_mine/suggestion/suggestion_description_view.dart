import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'event_bus_util.dart';


typedef SuggestionDescViewValueChange = Function(String);

/// 问题描述
class SuggestionDescView extends StatefulWidget {
  final SuggestionDescViewValueChange valueChange;
  bool enable;
  bool showable;
  SuggestionDescView(
      {Key? key,
      required this.valueChange,
      required this.enable,
      required this.showable})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _SuggestionDescViewState();
}

class _SuggestionDescViewState extends State<SuggestionDescView> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    bus.on(EventBus.CLEARUPSUGGESTIONCONTENT, (arg) {
      _controller.clear();
      widget.valueChange('');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Padding(
            padding:  EdgeInsets.only(left: 14.w, top: 15.w,bottom: 15.w),
            child: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "问题描述：",
                textAlign: TextAlign.left,
                style:
                    TextStyle(color: Color(0xFF666666), fontSize: 14),
              ),
            )),
        Container(
            // padding: const EdgeInsets.only(left: 14, right: 14),
           color: Colors.white,
            child: Container(
              margin: EdgeInsets.only(left: 12, right: 12),
              height: 170.w,
              decoration: BoxDecoration(
                  // border: Border.all(),
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.white),
              child: Container(
                child: GestureDetector(
                  onTap: () {
                    log("选择问题类型");
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: 5, left: 5, right: 5),
                    child: TextField(
                      controller: _controller,
                      cursorColor: Colors.grey,
                      maxLines: 10,
                      style: TextStyle(color: Color(0xFF666666)),
                      decoration: InputDecoration(
                          counterText: "",
                          hintText: "请输入…(内容介于20-200字)",
                          hintStyle:
                              TextStyle(color: Color(0xFF919699)),
                          border: InputBorder.none,
                          labelStyle: TextStyle(color: Colors.white)),
                      maxLength: 200,
                      onEditingComplete: () {
                        log("suggestion desc input complete value ${_controller.text}");
                        widget.valueChange(_controller.text);
                      },
                      onChanged: (value) {
                        log("suggestion desc input value $value");
                        widget.valueChange(value);
                      },
                    ),
                  ),
                ),
              ),
            )),
        widget.showable == true
            ? widget.enable != true
                ? Padding(
                    padding: EdgeInsets.only(left: 14, top: 3),
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "问题描述内容格式不正确",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.red, fontSize: 14),
                      ),
                    ))
                : Container()
            : Container()
      ],
    ));
  }
}
