// ignore_for_file: prefer_const_constructors, import_of_legacy_library_into_null_safe

import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../../../config/image/local_image_selector.dart';
import 'event_bus_util.dart';

typedef SuggestionSelectImageViewClick = Function(String);

/// 选择图片
class SuggestionSelectImageView extends StatefulWidget {
  final SuggestionSelectImageViewClick click;

  const SuggestionSelectImageView({Key? key, required this.click}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SuggestionSelectImageViewState();
}

class _SuggestionSelectImageViewState extends State<SuggestionSelectImageView> {
  File? image;

  @override
  void initState() {
    // TODO: implement initState
    bus.on(EventBus.CLEARUPSUGGESTIONCONTENT, (arg) {
      setState(() {
        image = null;
        widget.click("");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Padding(
            padding: EdgeInsets.only(left: 14.w, top: 15.w, bottom: 15.w),
            child: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "问题截图：",
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFF666666), fontSize: 14.sp),
              ),
            )),
        Container(
            color: Colors.white,
            child: Container(
              margin: EdgeInsets.only(left: 12, right: 12),
              // height: 162.w,
              decoration: BoxDecoration(
                  // border: Border.all(),
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.white),
              child: Container(
                width: MediaQuery.of(context).size.width - 28,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 5, top: 5, right: 5),
                      child: Text(
                        "*文件格式为PNG，JPG，JPEG，且大小不超过15MB（目前仅支持上传一张）",
                        style: TextStyle(color: Color(0xFF919699), fontSize: 14.sp),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Visibility(
                          child: Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(top: 5, left: 5),
                            child: image != null
                                ? Image.file(
                                    File(image?.path ?? ""),
                                    width: 100,
                                    height: 75,
                                  )
                                : null,
                          ),
                          visible: image != null,
                        ),
                        // Container(
                        //   margin: EdgeInsets.only(right: 20),
                        //   child: Text(
                        //     image != null ? '1/1' : '0/1',
                        //     style: TextStyle(
                        //       color: Color(0xFF919699),
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                    // Padding(padding: EdgeInsets.only(top: 30)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                            onTap: () {
                              getImage().then((value) {
                                // log(value);
                                setState(() {
                                  image = value;
                                  widget.click(image?.path ?? "");
                                });
                              }).catchError((err) {
                                log("get image error ${err}");
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 27.w,bottom: 27.w),
                              alignment: Alignment.topLeft,
                              child: LocalImageSelector.getSingleImage(
                                "report_pic",
                                imageHeight: 55.5.w,
                                // imageWidth: 63.w,
                              ),
                            )),
                        Container(
                          height: 50.5.w,
                          // color: Colors.red,
                          alignment: Alignment.bottomRight,
                          margin: EdgeInsets.only(right: 20),
                          child: Text(
                            image != null ? '1/1' : '0/1',
                            style: TextStyle(
                              color: Color(0xFF919699),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ))
      ],
    ));
  }

  Future getImage() async {
    File? file = await ImagePicker.pickImage(source: ImageSource.gallery);
    log("${file.length().then((value) {
      log("$value");
    })}");
    return file;
    //     .then((value) {
    //   log("selected images : $value");
    //   return value;
    // }).onError((error, stackTrace) {
    //   log("select image error :$error");
    // }).whenComplete(() {
    //   log("select image finish");
    // });
  }
}
