// @dart=2.9
class SuggestModel {
  String msg;
  int code;
  List<Data> data;

  SuggestModel({this.msg, this.code, this.data});

  SuggestModel.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String reportName;
  int available;
  String createTime;
  String updateTime;
  String updateUser;

  Data(
      {this.id,
        this.reportName,
        this.available,
        this.createTime,
        this.updateTime,
        this.updateUser});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reportName = json['reportName'];
    available = json['available'];
    createTime = json['createTime'];
    updateTime = json['updateTime'];
    updateUser = json['updateUser'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['reportName'] = this.reportName;
    data['available'] = this.available;
    data['createTime'] = this.createTime;
    data['updateTime'] = this.updateTime;
    data['updateUser'] = this.updateUser;
    return data;
  }
}