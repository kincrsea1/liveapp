// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/utils/index.dart' as util;
import 'package:m_loading/m_loading.dart';

class VideoItem extends BaseWidget {
  final AnchorList renderItem;
  Function clickCallBack;
  VideoItem({Key? key, required this.renderItem, required this.clickCallBack}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _VideoItemState();
  }
}

class _VideoItemState extends BaseWidgetState<VideoItem> {
  Map _iconMap = {
    '1': '足球',
    '2': '篮球',
    '37': '电竞',
    'other': '其他',
  };
  @override
  Widget PageBody(AppState state) {
    return GestureDetector(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4.w),
        child: Container(
          width: 172.w,
          color: c_white,
          child: Column(children: [
            Stack(
              children: [
                Container(
                  width: double.infinity,
                  constraints: BoxConstraints(minHeight: 96.w, maxHeight: 96.w),
                  height: 96.w,
                  child: widget.renderItem.playUrls == null ||
                          widget.renderItem.playUrls![0].streamImgUrl == null ||
                          !widget.renderItem.playUrls![0].streamImgUrl!.startsWith('http')
                      ? LocalImageSelector.getSingleImage('common_default_bg')
                      : CachedNetworkImage(
                          errorWidget: (BuildContext, String, dynamic) {
                            return LocalImageSelector.getSingleImage('common_default_bg');
                          },
                          placeholder: (BuildContext, String) {
                            return Container(
                              alignment: Alignment.center,
                              color: Color(0xFF2C2C2E),
                              child: Container(
                                width: 30,
                                child: Ball3OpacityLoading(
                                  ballStyle: BallStyle(
                                    size: 4,
                                    color: c_6,
                                  ),
                                ),
                              ),
                            );
                          },
                          fit: BoxFit.fill,
                          imageUrl: widget.renderItem.playUrls![0].streamImgUrl,
                        ),
                ),
                Positioned(
                  left: 5.w,
                  top: 5.w,
                  child: Container(
                    alignment: Alignment.center,
                    decoration:
                        BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0.6), borderRadius: BorderRadius.circular(7.w)),
                    width: 38.w,
                    height: 14.w,
                    child: Text(
                      _iconMap[widget.renderItem.catId] != null ? _iconMap[widget.renderItem.catId] as String : '其他',
                      style: TextStyle(color: c_white, fontSize: 10.sp),
                    ),
                  ),
                ),
                Positioned(
                  right: 5.w,
                  top: 5.w,
                  child: Container(
                    width: 14.w,
                    height: 10.w,
                    child: LocalImageSelector.getSingleImage('home_live_status_icon'),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.only(top: 7.w, bottom: 3.w, left: 5.w, right: 9.w),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 6.w),
                    child: ClipOval(
                      child: widget.renderItem.userLogo != null
                          ? Image(
                              image: NetworkImage(widget.renderItem.userLogo),
                              width: 26.w,
                              height: 26.w,
                              fit: BoxFit.cover,
                            )
                          : LocalImageSelector.getSingleImage('common_team_default',
                              imageWidth: 26.w, imageHeight: 26.w),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Container(
                          constraints: BoxConstraints(maxWidth: 126.w),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '${widget.renderItem.homeName}VS${widget.renderItem.awayName}',
                            style: TextStyle(color: c_6, fontSize: 12.sp, overflow: TextOverflow.ellipsis),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(right: 10.w),
                                  child: Text(
                                    widget.renderItem.nickName ?? '',
                                    style: TextStyle(color: c_6, fontSize: 10.sp, overflow: TextOverflow.ellipsis),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 3.w, left: 5.w),
                                    width: 10.w,
                                    height: 14.w,
                                    child: LocalImageSelector.getSingleImage('home_live_item_icon'),
                                  ),
                                  Text(
                                    widget.renderItem.online != null
                                        ? '${util.filterNum(widget.renderItem.online)}'
                                        : '',
                                    style: TextStyle(color: c_6, fontSize: 12.sp),
                                  )
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ]),
        ),
      ),
      onTap: () => widget.clickCallBack(),
    );
  }
}
