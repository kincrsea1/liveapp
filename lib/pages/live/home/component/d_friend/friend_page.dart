import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class friendPage extends BaseWidget {
  friendPage({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _friendPageState();
  }
}

class _friendPageState extends BaseWidgetState<friendPage> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget PageBody(AppState state) {
    return Container(
      child: Text('friend'),
    );
  }
}
