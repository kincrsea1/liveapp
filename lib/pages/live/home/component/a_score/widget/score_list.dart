// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:liveapp/api/score_req_method.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/config/date/date_utils.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/main.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/mo/score_home_model.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_kind_tabcontroller2.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/style/live/base_color.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListWidget extends BaseWidget {
  ListWidget(this.tabName, {this.tabtype, this.tabModel, this.scrollDirection = Axis.vertical});
  final String tabName;
  final String tabtype;
  final weekTab tabModel;
  final Axis scrollDirection;

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ListWidgetState();
  }
}

class _ListWidgetState extends BaseWidgetState<ListWidget> with RouteAware {
  List<scoreMatchModel> matchList = [];

  List<dynamic> matchName = [];
  bool isLoading = true;

  RefreshController _refreshController = RefreshController(initialRefresh: false);
  int currPage = 1;

  void _onRefresh() async {
    currPage = 1;
    _refreshController.resetNoData();
    _getScoreData();
  }

  void _onLoading() async {
    currPage++;
    _getScoreData();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeViewObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void didPush() {
    print('didPush');
  }

  @override
  void didPopNext() {
    print('didPopNext');
  }

  @override
  void didPushNext() {
    print('didPushNext');
  }

  @override
  void didPop() {
    print('didPop');
  }

  @override
  void initState() {
    ///获取默认值 当天日期
    _getScoreData();
    super.initState();

    bus.on(EventBus.SCORERESULTANDMATCHINGRELOADDATAREQUEST, (arg) {
      _getScoreData();
    });

    bus.on(EventBus.SCOREMATCHSHIRTDATANOTICE, (arg) {
      if (arg['sportType'] == widget.tabtype) {
        _refreshController.resetNoData();
        currPage = 1;
        matchName = arg['matchName'];
        _getScoreData();
      }
    });

    bus.on(EventBus.LISTREQUESTERROR, (arg) {
      if (arg == GetSchedule) {
        // if(currPage == 1){
        //
        // }else{
        //
        // }
        _refreshController.refreshFailed();
        _refreshController.loadFailed();
      }
    });

    // GetLeagueList
  }

  @override
  Widget PageBody(AppState state) {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
        header: ClassicHeader(
          height: 45.0.w,
          releaseText: '松开手刷新',
          refreshingText: '刷新中',
          completeText: '已更新最新比赛！',
          failedText: '刷新失败',
          idleText: '下拉刷新',
        ),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("没有更多");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请重新加载");
            } else {
              body = Text("没有更多");
            }
            return Container(
              height: 55.0.w,
              child: Center(child: body),
            );
          },
        ),
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        controller: _refreshController,
        child: !isLoading
            ? (matchList != null && matchList.length > 0)
                ? widget.tabName == '足球'
                    ? ListView.builder(
                        itemBuilder: (BuildContext c, int i) {
                          return FlatButton(
                            padding: EdgeInsets.symmetric(horizontal: 0),

                            ///点击足球cell
                            onPressed: () async {
                              _refreshController.loadComplete();
                              scoreMatchModel model = matchList[i];
                              model.catId = 1;
                              goPage(url: 'score_detail', params: {'scoreModel': model, 'type': 'scorehome'});
                            },
                            child: Container(
                                height: 75.w,
                                width: MediaQuery.of(context).size.width,
                                child: Container(
                                  margin: EdgeInsets.only(top: 4.w),
                                  child: _footballCell(i),
                                )),
                          );
                        },
                        itemCount: matchList.length,
                        scrollDirection: widget.scrollDirection,
                      )
                    : ListView.builder(
                        itemBuilder: (BuildContext c, int i) {
                          // String type =
                          return FlatButton(
                            padding: EdgeInsets.symmetric(horizontal: 0),

                            ///点击篮球cell
                            onPressed: () async {
                              _refreshController.loadComplete();
                              scoreMatchModel model = matchList[i];
                              model.catId = 2;
                              await Future.delayed(Duration.zero);
                              goPage(url: 'score_detail', params: {'scoreModel': model, 'type': 'scorehome'});
                            },
                            child: Container(
                                height: 85.w,
                                width: MediaQuery.of(context).size.width,
                                child: Container(
                                  color: c_white,
                                  margin: EdgeInsets.only(top: 4.w),
                                  child: _BasketBallCell(i),
                                )),
                          );
                        },
                        itemCount: matchList.length,
                        scrollDirection: widget.scrollDirection,
                      )
                : EmptyContainer(txt: '暂无比分直播')
            : LoadingContainer());
  }

  ///====================================足球状态 ===============================
  Widget _footballCell(int i) {
    scoreMatchModel model = matchList[i];
    return Container(
      color: c_white,
      child: Column(
        children: [_footBallTopPart(i, model), _footBallBottomPart(i, model)],
      ),
    );
  }

  Widget _footBallTopPart(int i, scoreMatchModel model) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(top: 4.w),
          width: 130.w,
          child: Row(
            children: [
              SizedBox(
                width: 14.w,
              ),
              Container(
                width: 70.w,
                decoration: BoxDecoration(
                  color: AppColors.getColorForHex(model.color),
                  borderRadius: BorderRadius.circular(2.w),
                ),
                child: Text(
                  model.leagueChsShort ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 12.sm, color: c_white),
                ),
              ),
              Expanded(
                child: Text(
                  model.time ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 12.sm, color: c_6),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 4.w),
          child: Text(
            model.realTime ?? '',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 12.sm, color: c_6),
          ),
        ),
        SizedBox(
          width: 130,
        ),
      ],
    );
  }

  Widget _footBallBottomPart(int i, scoreMatchModel model) {
    bool isPlayer = false;
    bool isAnimal = false;
    if (model.iconType == '直播') isPlayer = true;
    if (!isPlayer && model.iconType == '动画') isAnimal = true;
    return Container(
      margin: EdgeInsets.only(top: 7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 35.w,
          ),
          _footBallMeddleInfo(i, model),
          (isAnimal)
              ? Container(
                  margin: EdgeInsets.only(right: 15.w),
                  child: LocalImageSelector.getSingleImage(isPlayer ? 'playicon' : 'animalicon',
                      imageWidth: 20.w, imageHeight: 19.w),
                )
              : SizedBox(
                  width: 35.w,
                ),
        ],
      ),
    );
  }

  _footBallMeddleInfo(int i, scoreMatchModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Row(
            children: [
              Container(
                width: 100.w,
                child: Text(
                  model.homeName ?? '',
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.right,
                  style: TextStyle(fontSize: 14.sm, color: c_6),
                ),
              ),
              Text(
                ' ${model.homeScore} - ${model.awayScore} ',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14.sm, color: c_6),
              ),
              Container(
                width: 100.w,
                child: Text(
                  model.awayName ?? '',
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 14.sm, color: c_6),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          child: Text(
            '半 ${model.homeHalfScore}-${model.awayHalfScore} 角 ${model.homeCorner}-${model.awayCorner}',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 10.sm, color: c_6),
          ),
        ),
      ],
    );
  }

  ///====================================篮球状态 ===============================
  _BasketBallCell(int i) {
    scoreMatchModel model = matchList[i];
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 14.w, top: 5.w),
          width: 150.w,
          child: _basketBallLeftPart(i, model),
        ),
        Expanded(
            child: Container(
          margin: EdgeInsets.only(left: 14.w, top: 5.w),
          child: _basketBallRightPart(i, model),
        )),
      ],
    );
  }

  Widget _basketBallLeftPart(int i, scoreMatchModel model) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Text(
                model.leagueChs ?? '',
                style: TextStyle(
                  fontSize: 12.sm,
                  color: AppColors.getColorForHex(model.color),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 5.w),
              child: Text(
                model.time ?? '',
                style: TextStyle(
                  fontSize: 12.sm,
                  color: c_6,
                ),
              ),
            )
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 10.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 20.w,
                    child: Text(
                      model.homeName ?? '',
                      style: TextStyle(
                        fontSize: 14.sm,
                        color: c_6,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Container(
                    height: 20.w,
                    child: Text(
                      model.awayName ?? '',
                      style: TextStyle(
                        fontSize: 14.sm,
                        color: c_6,
                      ),
                    ),
                  )
                ],
              ),
              Center(
                child: Container(
                  width: 1.w,
                  height: 36.w,
                  color: c_b1,
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _basketBallRightPart(int i, scoreMatchModel model) {
    bool isPlayer = false;
    bool isAnimal = false;
    if (model.iconType != null && model.iconType == '直播') isPlayer = true;
    if (!isPlayer && model.iconType != null && model.iconType == '动画') isAnimal = true;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text(
                model.realTime ?? '',
                style: TextStyle(
                  fontSize: 12.sm,
                  color: model.matchState <= 0 ? c_6 : c_o,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                children: [
                  (model.matchState < 0 && model.matchState != -1)
                      ? SizedBox(
                          width: 120.w,
                        )
                      : Container(
                          width: 120.w,
                          child: Row(
                            children: _scoreItem(model, [model.home1, model.home2, model.home3, model.home4]),
                          ),
                        ),
                  model.matchState != 0
                      ? Container(
                          decoration: BoxDecoration(color: c_border, borderRadius: BorderRadius.circular(10.w)),
                          child: Text(
                            model.homeScore.toString(),
                            style: TextStyle(
                              fontSize: 12.sm,
                              color: model.state == 1 ? c_o : c_darkborder,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                children: [
                  (model.matchState < 0 && model.matchState != -1)
                      ? SizedBox(
                          width: 120.w,
                        )
                      : Container(
                          width: 120.w,
                          child: Row(
                            children: _scoreItem(model, [model.away1, model.away2, model.away3, model.away4]),
                          ),
                        ),
                  model.matchState != 0
                      ? Container(
                          decoration: BoxDecoration(color: c_border, borderRadius: BorderRadius.circular(10.w)),
                          child: Text(
                            model.awayScore.toString(),
                            style: TextStyle(
                              fontSize: 12.sm,
                              color: model.state == 1 ? c_o : c_darkborder,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
        (isAnimal)
            ? Container(
                margin: EdgeInsets.only(right: 15.w),
                child: LocalImageSelector.getSingleImage(isPlayer ? 'playicon' : 'animalicon',
                    imageWidth: 20.w, imageHeight: 19.w),
              )
            : SizedBox(
                width: 35,
              )
      ],
    );
  }

  List<Widget> _scoreItem(scoreMatchModel model, List<String> scoreList) {
    return scoreList
        .map((String score) => Container(
              width: 26.w,
              child: Container(
                decoration: BoxDecoration(
                    color: (model.matchState <= 0 || score.toString() == '') ? Colors.transparent : c_border,
                    borderRadius: BorderRadius.circular(10.w)),
                child: Text(
                  model.matchState <= 0
                      ? score.toString() == '0'
                          ? ''
                          : score.toString()
                      : score.toString() == ''
                          ? '0'
                          : score.toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 12.sm,
                    color: c_9,
                  ),
                ),
              ),
            ))
        .toList();
  }

  ///===============数据请求==================================

  void _getScoreData() async {
    isLoading = true;
    String type = '0';
    if (widget.tabName == '篮球') type = '1';

    String scheduleType = '0';
    switch (widget.tabtype) {
      case '全部':
        {
          scheduleType = '0';
          break;
        }
      case '进行中':
        {
          scheduleType = '1';
          break;
        }
      case '赛程':
        {
          scheduleType = '2';
          break;
        }
      case '赛果':
        {
          scheduleType = '3';
          break;
        }
    }

    String date = widget.tabModel == null
        ? DateFormatUtils.getNowDateTimeFormat(DateFormatUtils.PARAM_TIMEZN_FORMAT)
        : widget.tabModel.date;

    List<scoreMatchModel> dateList = await scoreMatchData(
        date: date, type: type, scheduleType: scheduleType, currPage: currPage, scheduleName: matchName);
    if (currPage == 1) {
      matchList.clear();
      matchList = dateList;
      _refreshController.refreshCompleted();
    } else {
      if (dateList.length > 0) {
        matchList.addAll(dateList);
        _refreshController.loadComplete();
      } else {
        currPage -= 1;
        _refreshController.loadNoData();
      }
    }

    isLoading = false;

    if (mounted) setState(() {});
  }
}
