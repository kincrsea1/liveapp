// @dart=2.9
import 'package:flutter/material.dart';
import 'package:liveapp/api/score_req_method.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/extended_tabs/src/tab_indicator.dart';
import 'package:liveapp/common_widget/extended_tabs/src/tab_view.dart';
import 'package:liveapp/config/date/date_utils.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/mo/score_home_model.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_list.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_shrit_config.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class ScoreTabController2 extends BaseWidget {
  String SportName;
  String Sporttype;
  ScoreTabController2(this.SportName,this.Sporttype);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ScoreTabController2State();
  }
}

class _ScoreTabController2State extends BaseWidgetState<ScoreTabController2> with TickerProviderStateMixin{
  TabController tabController2;

  List<weekTab> MatchPlanData = [];

  List<weekTab> MatchResultData = [];





  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    if(widget.Sporttype == '赛果'){
      tabController2 = TabController(initialIndex: 6,length: 7, vsync: this);
    }else{
      tabController2 = TabController(initialIndex: 0,length: 7, vsync: this);
    }

    matchPlan();
    matchResultData();

    ///日期变动更新数据
    bus.on(EventBus.SCORERESULTANDMATCHINGRELOADDATA, (arg) {
      matchPlan();
      matchResultData();
      if(mounted)setState(() {
        ScoreShritConfig.todayDate =  DateFormatUtils.getNowDateTimeFormat(DateFormatUtils.PARAM_TIMEZN_FORMAT);
        bus.emit(EventBus.SCORERESULTANDMATCHINGRELOADDATAREQUEST);
      });
    });

  }

  @override
  void dispose() {
    tabController2.dispose();
    super.dispose();
  }
  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      body:  Container(

    color: c_white,
    child: Column(
      children: <Widget>[
        TabBar(
          labelPadding: EdgeInsets.zero,
          indicator:
          ColorTabIndicator(c_border),

          labelStyle: TextStyle(fontSize: 12.sp),
          labelColor: c_o,
          unselectedLabelColor: c_6,
          //indicatorSize: TabBarIndicatorSize.label,
          tabs: _tabsFromType(widget.SportName,widget.Sporttype),
          controller: tabController2,
        ),
        Expanded(
          child: ExtendedTabBarView(
            children: _MatchplanOrResultList(widget.SportName,tabtype: widget.Sporttype),
            controller: tabController2,
            link: true,
            cacheExtent: 1,
          ),
        )
      ],
    ),
  ),
    );
  }
  List<Tab> _tabsFromType(String name, String type) {
    List<weekTab> tabData = [];
    if(type == '赛程'){
      tabData = MatchPlanData;
    }else{
      tabData = MatchResultData;
    }
    return tabData.map((weekTab model) {
      bool isToday = false;
      if(type == '赛程'){
        if(int.parse(model.index) == 0) isToday = true;

      }else{
        if(int.parse(model.index) == MatchPlanData.length - 1)  isToday = true;

      }
      return Tab(
        child: Container(
          child: isToday
              ? Text(
            "今天",
            textAlign: TextAlign.center,

          )
              : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                model.date.substring(5),
                textAlign: TextAlign.center,
              ),
              Text(
                model.weekday,
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      );
    }
    ).toList();
  }


  List<Widget> _MatchplanOrResultList(String name, {String tabtype}) {
    List<weekTab> tabData = [];
    if(tabtype == '赛程'){
      tabData = MatchPlanData;
    }else{
      tabData = MatchResultData;
    }

    return tabData.map((weekTab model) =>
        Container(

          color: c_w,
          child: ListWidget(name,tabtype: tabtype,tabModel: model),
        )
    ).toList();

  }


  ///赛果数据
  void matchResultData(){
    String now = DateFormatUtils.getNowDateTimeFormat(
        DateFormatUtils.PARAM_TIMEZN_FORMAT);
    ///赛果时间计算逻辑
    List<String> mdata = DateFormatUtils.getTimeStartTimeAndEnd(
        startTime: now,
        dayNumber: 6,
        format: DateFormatUtils.PARAM_TIMEZN_FORMAT);
    List<String> weekdate = mdata.reversed.toList();
    List<String> weekmdata = DateFormatUtils.getWeekTimeStartTimeAndEnd(
        startTime: now,
        dayNumber: 6,
        format: DateFormatUtils.PARAM_DATE_FORMAT);
    List<String> weekNum = weekmdata.reversed.toList();
    List<Map<String, dynamic>> testData = [];
    for (int i = 0; i < 7; i++) {
      Map date = Map<String, dynamic>();
      date['date'] = weekdate[i];
      date['weekday'] = weekNum[i];
      date['index'] = i.toString();
      testData.add(date);
    }

    MatchResultData = new List<weekTab>.empty(growable: true);
    testData.forEach((v) {
      MatchResultData.add(new weekTab.fromJson(v));
    });
  }

  ///赛程数据
  void matchPlan(){
    String now = DateFormatUtils.getNowDateTimeFormat(
        DateFormatUtils.PARAM_TIMEZN_FORMAT);
    ///赛程时间计算逻辑
    List<String> mdata = DateFormatUtils.getTimeStartTimeAndEnd2(
        startTime: now,
        dayNumber: 6,
        format: DateFormatUtils.PARAM_TIMEZN_FORMAT);
    // List<String> weekdate = mdata.reversed.toList();
    List<String> weekmdata = DateFormatUtils.getWeekTimeStartTimeAndEnd2(
        startTime: now,
        dayNumber: 6,
        format: DateFormatUtils.PARAM_DATE_FORMAT);
    // List<String> weekNum = weekmdata.reversed.toList();
    List<Map<String, dynamic>> testData = [];
    for (int i = 0; i < 7; i++) {
      Map date = Map<String, dynamic>();
      date['date'] = mdata[i];
      date['weekday'] = weekmdata[i];
      date['index'] = i.toString();
      testData.add(date);
    }

    MatchPlanData = new List<weekTab>.empty(growable: true);
    testData.forEach((v) {
      MatchPlanData.add(new weekTab.fromJson(v));
    });


  }




}

//分页一周数据字段模型
class weekTab {
  String date;
  String weekday;
  String index;
  weekTab({this.date, this.weekday});
  weekTab.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    index = json['index'];
    weekday = json['weekday'];
  }
}
