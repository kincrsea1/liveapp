// @dart=2.9



import 'package:flutter/material.dart';
import 'package:liveapp/api/score_req_method.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/config/date/date_utils.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/alphabet_list_scroll.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_shrit_config.dart';
import 'package:liveapp/pages/live/home/component/e_mine/common/base_navigation_view.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
///每一行的高度
const double kMatchSigleLineH = 42.0;
///头部高度
const double kMatchHeandH = 60.0;
class ScoreSiftPage extends BaseWidget {

  ScoreSiftPage();

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ScoreSiftPageState();
  }
}

class _ScoreSiftPageState extends BaseWidgetState<ScoreSiftPage> {

  String catId = '1';
  List<String>  chartsList = [];

  Map<String,List<MatchModel>> MatchMap = {
  };
  List<String>  costMatchName = [];

  List<String>  newMatchName = [];

  bool isLoading = true;
  int sum = 0;
  int newSum = 0;

  bool AllSelect = false;
  bool AllNoSelect = false;
  ScrollController _scrollController = ScrollController();
  num columnCount = 3;


  void initState() {
    // _scrollController.addListener(() {
    //   if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
    //     _loadData(loadMore: true);
    //   }
    // });


    super.initState();

    Future.delayed(Duration(seconds: 1), () {
      catId = (ModalRoute.of(this.context).settings.arguments as Map)['catId'] ?? '1';
      getShirtData();
      });



  }

  @override
  void dispose() {
    _scrollController .dispose();
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    Size layoutSize = MediaQuery.of(context).size;
    return  Scaffold(
      backgroundColor: c_w,
      appBar: BaseNavigation("赛事筛选",''),
      body:  MediaQuery.removePadding(
        removeTop: true,
        removeBottom: true,
        removeLeft: true,
        removeRight: true,
        context: context,
        child:!isLoading ? MatchMap.keys.length > 0 ? Column(
      children: [
        Container(width: double.infinity, height: 1.w, color: c_b1,),
        Expanded(child: AlphabetListScrollView(
            showPreview: true,
            useVibration: false,
            keyboardUsage: true,
            headerWidgetList: [
            ],
            highlightTextStyle:
            Theme.of(context).primaryTextTheme.headline1.copyWith(
              fontSize: 12.w,
              color: c_bl,
            ),
            normalTextStyle: Theme.of(context).textTheme.headline3.copyWith(
              fontSize: 10.w,
              color: c_9,
            ),
            strList:chartsList,
            indexedHeight: (i) {
              String key = chartsList[i];

              // if (i == 0){
              //   key = "热门赛事";
              // }

              List<MatchModel> MatchList = MatchMap[key];

              int rows = (MatchList.length / 3).floor();
                  int rows2 =   MatchList.length % 3 > 0 ? 1 : 0;
              double SigleH = kMatchHeandH + (rows+rows2) * kMatchSigleLineH;
              return SigleH ;
            },
            itemBuilder: (BuildContext context, int index) {
              String key = chartsList[index];
              // if (index == 0){
              //   key = "热门赛事";
              // }

              List<MatchModel> MatchList = MatchMap[key];
              int rows = (MatchList.length / 3).floor();
              int rows2 =   MatchList.length % 3 > 0 ? 1 : 0;
              double SigleH = kMatchHeandH + (rows+rows2) * kMatchSigleLineH;
              return ListTile(
                // contentPadding: EdgeInsets.only(left: 0),//
                contentPadding: EdgeInsets.all(0),//
                title: Container(
                  color: c_w,

                  height: SigleH,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _MenuTitleWidget(index),
                      _MenuMatchListWidget(index),
                    ],
                  ),
                ),
              );
            })),
        Container(width: double.infinity, height:50.w , color: c_white,
        child: _bottomToolWidgte(),
        ),
        Container(width: double.infinity, height:MediaQuery.of(context).padding.bottom , color: c_bl,),
      ],
    ) :  EmptyContainer(txt: '暂无数据') :LoadingContainer(),
      ),


    );
  }

  _MenuTitleWidget(int index) {
    String key = chartsList[index];
    // if (index == 0){
    //   key = "热门赛事";
    // }

    return  Container(
      height: kMatchHeandH,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: 4.w,
            height: 20.w,
            decoration: BoxDecoration(
                color: c_o,
                borderRadius:BorderRadius.only(
                  topRight: Radius.circular(10.w),
                  bottomRight: Radius.circular(10.w),
                )),
          ),
          Container(
            margin: EdgeInsets.only(left: 10.w),
            child: Text(
              (key).toUpperCase(),
            style: TextStyle(
            fontSize: 20.sp,
            color: c_6
            ),
            ),
          )
        ],
      ),
    );
  }

  _MenuMatchListWidget(int index) {
    String key = chartsList[index];
    // if (index == 0){
    //   key = "热门赛事";
    // }

    List<MatchModel> MatchList = MatchMap[key];
    int rows = (MatchList.length / 3).floor();
    int rows2 =   MatchList.length % 3 > 0 ? 1 : 0;
    double SigleH = (rows+rows2) * kMatchSigleLineH;


    double cellWidth = (MediaQuery.of(context).size.width / columnCount);
    double desiredCellHeight = 45;
    double childAspectRatio = cellWidth / desiredCellHeight;


    return Container(
      margin: EdgeInsets.only(right: 25.w),
      height:SigleH ,
      child: GridView.count(
        controller: _scrollController,
        // padding: EdgeInsets.only(left: 14, right: 14, top: 10),
        scrollDirection: Axis.vertical,
        crossAxisSpacing: 0,///横轴方向子元素的间距
        crossAxisCount: 3,
        childAspectRatio: childAspectRatio,
        mainAxisSpacing: 0,///主轴方向的间距
        children: _MatchItem(MatchList),
      ),
    );
  }

  ///返回联赛列表widget
  List<Widget>  _MatchItem(List<MatchModel> matchList){
  dynamic MList = matchList.map((MatchModel item) {
    return _matchCellWidget(item);
  }).toList();
  return MList;
  }

  Widget _matchCellWidget(MatchModel item) {

    bool select = true;
    if(AllSelect){
      select = true;
      item.state = true;
    }else if(AllNoSelect ){
      select = false;
      item.state = false;
    }else {
      select =  item.state;
    }


    return Container(
      // color: Colors.red,

      child:  FlatButton(
        padding: EdgeInsets.symmetric(horizontal: 0),

        onPressed: (){
          AllSelect = false;
          AllNoSelect = false;
          item.state = !item.state;
          if (mounted)setState(() {
           if( item.state == true) {
             newSum += 1;
             newMatchName.add(item.title);
           } else{
             newSum -= 1;
             newMatchName.remove(item.title);
           }


          });
        },
        child: Container(
          width: 86.w,
          height: 30.w,
          decoration:

          BoxDecoration(
              color:  select ? c_b1  :c_white,
              border: Border.all(width: 1.w, color: c_b1),
              borderRadius: BorderRadius.circular(6.w)),
          child: Center(
            child: Text(
                item.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 14.sm,
                    color: c_6
                )
            ),
          ),
        )
      ),
    );
  }

  ///底部工具栏
  _bottomToolWidgte() {
    return Row(
      children: [
        creatButton(c_o,'全选',(){
          print("选择全选");
          AllSelect = true;
          AllNoSelect = false;
          if (mounted)setState(() {
            newSum = sum;
            newMatchName = List.from(costMatchName);
          });
        }),

        creatButton(c_6,'反选',(){
          print("反选");
          AllSelect = false;
          AllNoSelect = true;
          if (mounted)setState(() {
            newSum = 0;
            newMatchName.clear();
          });
        }),
        Container(
          height:14.w,
          width: 1.w,
          color: c_b1,
        ),
        Container(
          margin: EdgeInsets.only(left: 11.w),
          child: Row(
            children: [
              creatText(c_6, '已选'),
              creatText(c_o, newSum.toString()),
              creatText(c_6, '/${sum.toString()}场'),
            ],
          ),

        ),
        Expanded(
          child: Container(),
        ),
        Container(
          margin: EdgeInsets.only(right: 10.w),
          child: Center(
            child: Container(
              width: 90.w,
              height: 30.w,
              decoration: BoxDecoration(
                  color: newSum != 0 ?c_o:c_w,
                  borderRadius: BorderRadius.circular(5.w)

              ),
              child: FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 0),
                onPressed: () {

                  if(newSum == 0){
                    showToast('请你至少选择一个联赛');
                    return;
                  }

                  if(newSum == sum && sum != 0){
                    bus.emit(EventBus.SCOREMATCHSHIRTDATANOTICE,{'matchName':[],'sportType':ScoreShritConfig.typeName});
                  }else if(newSum != sum ){
                    bus.emit(EventBus.SCOREMATCHSHIRTDATANOTICE,{'matchName':newMatchName,'sportType':ScoreShritConfig.typeName});
                  }



        // SCOREMATCHSHIRTDATANOTICE
                /// 点击确定
                  Navigator.pop(context);
                },
                child: Text(
                  '确定',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: newSum != 0 ?c_white:c_9, fontSize: 16.sm),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }


  ///公共按钮架子
  Widget creatButton(
      Color TextColor, String btnStr, Function btnClick) {
    return Container(
      width: 50.w,
      height: 50.w,
      child: FlatButton(
        padding: EdgeInsets.symmetric(horizontal: 0),
        onPressed: () {

          btnClick();
        },
        child: Text(
          btnStr,
          textAlign: TextAlign.center,
          style: TextStyle(color: TextColor, fontSize: 14.sm),
        ),
      ),
    );
  }

  Widget creatText(
      Color TextColor, String TextStr) {
    return Container(
      child: Container(
        child: Text(
          TextStr,
          style: TextStyle(color: TextColor, fontSize: 14.sm),
        ),
      ),
    );
  }




  ///===========================联赛接口================================///
  void getShirtData() async {

    isLoading = true;
    sum = 0;
    costMatchName.clear();
    String date = DateFormatUtils.getNowDateTimeFormat(DateFormatUtils.PARAM_TIMEZN_FORMAT);
    Map<String,dynamic> dateMap = Map();
    dateMap =  await getLeagueList(date: date,catId: catId)  ;
    List<String> keyslist = dateMap.keys.toList();
    keyslist.forEach((String key) {
      List<dynamic> matchNames = dateMap[key];
      if(matchNames.length > 0){
        List<MatchModel> matchlist = [];
        matchNames.forEach(( name) {
          MatchModel model = MatchModel(title: name.toString(),state: true);
          matchlist.add(model);
          costMatchName.add(name.toString());
        });
        MatchMap.putIfAbsent(key, () => matchlist);
        sum += matchlist.length;
        chartsList.add(key);
      }

    });
    newSum = sum;
    newMatchName =  List.from(costMatchName);
    isLoading = false;
   if(mounted) setState(() {
    });
  }

}


///数据模型
class MatchModel {
  String title;
  bool state;
 
  MatchModel({this.title,this.state});
  MatchModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    state = json['state'];
  }
}

