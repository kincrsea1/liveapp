// @dart=2.9
import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/extended_tabs/src/tab_view.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_kind_tabcontroller2.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_list.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_shrit_config.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/utils/my_underline_indicator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../../config/date/date_utils.dart';
class ScoreTabController1 extends BaseWidget {
  String gameKind;
  ScoreTabController1(this.gameKind);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ScoreTabController1State();
  }
}

class _ScoreTabController1State extends BaseWidgetState<ScoreTabController1> with TickerProviderStateMixin{

  List<String> topList1 = ['全部', '进行中', '赛程', '赛果'];
  TabController tabController1;
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    ScoreShritConfig.todayDate =  DateFormatUtils.getNowDateTimeFormat(DateFormatUtils.PARAM_TIMEZN_FORMAT);
    super.initState();
    tabController1 = TabController( initialIndex: 0,length: topList1.length, vsync: this);
    tabController1.addListener(() {
      if (!tabController1.indexIsChanging) {
        ScoreShritConfig.typeName = topList1[tabController1.index];

        String todayDate =  DateFormatUtils.getNowDateTimeFormat(DateFormatUtils.PARAM_TIMEZN_FORMAT);
        if(todayDate != ScoreShritConfig.todayDate){
          bus.emit(EventBus.SCORERESULTANDMATCHINGRELOADDATA);
        }

      }
    });

  }

  @override
  void dispose() {
    tabController1.dispose();
    super.dispose();
  }
  @override
  Widget PageBody(AppState state) {
    return Scaffold(
        body: Column(
          children: <Widget>[
            Container(
              color: c_white,
              height: 10.w,
            ),
            Container(
              color: c_white,
              // margin: EdgeInsets.only(top: 10.w),
              height: 30.w,
              child: TabBar(
                tabs:topList1.map((String type) => Container(
                    width: MediaQuery.of(context).size.width / 4,

                    child: Tab(
                      text: type,
                    ))).toList(),
                labelPadding: EdgeInsets.zero,
                // labelPadding: EdgeInsets.symmetric(horizontal: 10.w),
                isScrollable: true,
                labelColor: c_o,
                unselectedLabelColor: c_6,
                indicator: TabSizeIndicator(
                  wantWidth: 30.w,
                  borderSide: BorderSide(width: 4.0.w, color: c_o),
                ),
                labelStyle: TextStyle(fontSize: 14.sp, fontWeight: FontWeight.w600),
                // unselectedLabelStyle: TextStyle(fontSize: 16.sp),
                controller: tabController1,

              ),
            ),
            Container(width: double.infinity, height: 1.w, color: c_b1,),
            Expanded(
              child: ExtendedTabBarView(
                children: _expandtypeList(widget.gameKind),
                controller: tabController1,
              ),
            )
          ],
        ),
    );
  }

  _expandtypeList(String name) {
    return topList1.map((String type) {
      if(type == '全部' || type == '进行中') return Container(
        color: c_w,
        child: ListWidget(name,tabtype: type),
      );
      return ScoreTabController2(name,type);
    }
    ).toList();
  }



}
