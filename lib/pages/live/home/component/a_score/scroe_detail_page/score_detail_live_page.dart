// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/model/tab_data_mod.dart';
import 'package:liveapp/pages/live/home/component/common/video_item.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart' as AnchorListMod;
import 'package:liveapp/api/index.dart' as api;

class ScoreDetailLivePage extends BaseWidget {
  var catId = ''; // 当前直播列表分类
  ScoreDetailLivePage(this.catId);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ScoreDetailLivePageState();
  }
}

class _ScoreDetailLivePageState extends BaseWidgetState<ScoreDetailLivePage> with TickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  List<DataMap> tabs = [
    DataMap(val: '推荐', key: '', contentList: []),
    DataMap(val: '足球', key: '1', contentList: []),
    DataMap(val: '篮球', key: '2', contentList: []),
    DataMap(val: '电竞', key: '37', contentList: []),
    DataMap(val: '其他', key: 'other', contentList: []),
  ];

  List<AnchorListMod.AnchorList> _anchorList = []; //- 主播列表
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  int _pageSize = 10; //- 当前列表长度
  int _pageNo = 1; //- 当前请求页数
  bool isLoading = false; //- 当前是否在进行数据请求
  int _totalCount = 0;
  String reloadType = '';

  ///上拉
  void _onRefresh() async {
    _pageNo = 1;
    _refreshController.resetNoData();
    _initLiveList();
  }

  ///下拉
  void _onLoading() async {
    DataMap data = tabs.firstWhere((e) => e.key == widget.catId);
    if (_totalCount <= data.contentList.length) {
      reloadType = 'loadNoData';
      _refreshController.loadNoData();
      setState(() {});
      return;
    }

    _pageNo++;
    _initLiveList();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _initLiveList();
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      body: !isLoading
          ? SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              // WaterDropHeader、ClassicHeader、CustomHeader、LinkHeader、MaterialClassicHeader、WaterDropMaterialHeader
              header: ClassicHeader(
                height: 45.0,
                releaseText: '松开手刷新',
                refreshingText: '刷新中',
                completeText: '刷新完成',
                failedText: '刷新失败',
                idleText: '下拉刷新',
              ),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text("没有更多");
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text("加载失败，请点击重新加载");
                  } else {
                    body = Text("没有更多");
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),

              onRefresh: _onRefresh,
              onLoading: _onLoading,
              controller: _refreshController,
              child: _anchorList.length == 0
                  ? EmptyContainer(txt: '暂无推荐主播')
                  : SingleChildScrollView(
                      child: Wrap(
                        spacing: 10.w,
                        runSpacing: 17.w,
                        children: cardItem(),
                      ),
                    ),
            )
          : LoadingContainer(),
    );
  }

  //- 卡片列表
  cardItem() {
    return _anchorList
        .map(
          (item) => VideoItem(
              renderItem: item,
              clickCallBack: () {
                if (sessionStorage.state.live.isMiniPlay) {
                  Navigator.popUntil(context, ModalRoute.withName('/index'));
                  goPage(url: 'live_detail', params: {
                    'enterType': 'playNormalVideo',
                    'item': item,
                  });
                } else {
                  Navigator.popUntil(context, ModalRoute.withName('/index'));
                  goPage(url: 'live_detail', params: {
                    'userId': item.userId,
                    'enterType': 'playNormalVideo',
                    'item': item,
                  });
                }
              }),
        )
        .toList();
  }

  ///- 直播列表获取
  Future _initLiveList() async {
    //  isLoading = true;
    //  AnchorListMod.Data data = await api.listMiqEvent({
    //    'pageSize': _pageSize,
    //    'pageNo': _pageNo,
    //    'catId': widget.catId,
    //  });
    //  isLoading = false;
    //  if (_pageNo == 1) {
    //    tabs.firstWhere((e) => e.key == widget.catId).contentList.clear();
    //    tabs.firstWhere((e) => e.key == widget.catId).contentList = data.list;
    //    _refreshController.refreshCompleted();
    //  } else {
    //    if (data.list.length > 0) {
    //      tabs.firstWhere((e) => e.key == widget.catId).contentList.addAll(data.list);
    //      _refreshController.loadComplete();
    //    } else {
    //      _pageNo -= 1;
    //      _refreshController.loadNoData();
    //    }
    //  }
    //  _anchorList = List.from(tabs.firstWhere((e) => e.key == widget.catId).contentList);
    //
    // if(mounted) setState(() {});

    isLoading = true;
    try {
      AnchorListMod.Data data = await api.listMiqEvent({
        'pageSize': _pageSize,
        'pageNo': _pageNo,
        'catId': widget.catId,
        'anchorId': null,
      });
      isLoading = false;
      _totalCount = data.totalCount;
      if (_pageNo == 1) {
        tabs.firstWhere((e) => e.key == widget.catId).contentList.clear();
        tabs.firstWhere((e) => e.key == widget.catId).contentList = data.list;
        reloadType = 'refreshCompleted';
        _refreshController.refreshCompleted();
      } else {
        if (data != null && data.list.length > 0) {
          tabs.firstWhere((e) => e.key == widget.catId).contentList.addAll(data.list);
          reloadType = 'loadComplete';
          _refreshController.loadComplete();
        } else {
          _pageNo -= 1;
          reloadType = 'loadNoData';
          _refreshController.loadNoData();
        }
      }
      _anchorList = List.from(tabs.firstWhere((e) => e.key == widget.catId).contentList);
      if (mounted) setState(() {});
    } catch (e) {
      isLoading = false;
      if (mounted) setState(() {});
    }
  }
}
