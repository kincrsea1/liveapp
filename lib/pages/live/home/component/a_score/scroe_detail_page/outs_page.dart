import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/model/out_page_mod.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:percent_indicator/percent_indicator.dart';

class outsPage extends BaseWidget {
  final String? matchId;
  final int? type;
  dynamic scoreModel;

  outsPage(
      {Key? key,
      required this.scoreModel,
      required this.matchId,
      required this.type})
      : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _outsPageState();
  }
}

class _outsPageState extends BaseWidgetState<outsPage> {
  List _tabs = ["文字直播", "重要事件"];
  List<String> _TopTabs = ["一", "二", "三", "四", "OT", "总分"];
  int isSelected = 0;
  List<String?> _TopTabsName = [];
  List<String?> _TopTabsName2 = [];
  int num = 0;

  List<EventList>? eventList1 = [];
  List<EventList>? eventList2 = [];

  OutPageModal? _OutsPageModal;

  @override
  void initState() {
    _GetOutsPage();
    super.initState();
  }

  CancelToken _cancelToken = CancelToken();

  void _GetOutsPage() async {
    DioUtil.getInstance()?.openLog();
    DioResponse result =
        await DioUtil().request("/splive/clean/live/gameSituation",
            method: DioMethod.get,
            params: {
              'matchId': widget.matchId,
              'type': widget.type,
            },
            cancelToken: _cancelToken);
    if (mounted)
    setState(() {
      _OutsPageModal = OutPageModal.fromJson(result.data);
      if (widget.type == 1) {
        _TopTabsName.add(_OutsPageModal?.data?.home1 ?? "0");
        _TopTabsName.add(_OutsPageModal?.data?.home2 ?? "0");
        _TopTabsName.add(_OutsPageModal?.data?.home3 ?? "0");
        _TopTabsName.add(_OutsPageModal?.data?.home4 ?? "0");
        _TopTabsName.add(_OutsPageModal?.data?.homeOT ?? "0");
        _TopTabsName.add(_OutsPageModal?.data?.homeScore ?? "0");
        _TopTabsName2.add(_OutsPageModal?.data?.away1 ?? "0");
        _TopTabsName2.add(_OutsPageModal?.data?.away2 ?? "0");
        _TopTabsName2.add(_OutsPageModal?.data?.away3 ?? "0");
        _TopTabsName2.add(_OutsPageModal?.data?.away4 ?? "0");
        _TopTabsName2.add(_OutsPageModal?.data?.awayOT ?? "0");
        _TopTabsName2.add(_OutsPageModal?.data?.awayScore ?? "0");
      }
      if (_OutsPageModal != null &&
          _OutsPageModal!.data != null &&
          _OutsPageModal!.data!.eventList != null &&
          _OutsPageModal!.data!.eventList!.length > 0) {
        _OutsPageModal!.data!.eventList!
            .insert(0, EventList(nameChs: "比赛开始", time: "0"));
      }
    });
    print(result);
  }

  @override
  Widget PageBody(AppState state) {
    return widget.type == 0 || widget.type == 1
        ? SingleChildScrollView(
            child: Column(
            children: [
              Container(
                color: c_white,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Visibility(
                        visible: widget.type == 1,
                        child: Container(
                          margin: EdgeInsets.only(right: 35.w),
                          height: 31,
                          child: TabList(),
                        ),
                      ),
                      Visibility(
                          visible: widget.type == 1,
                          child: Container(
                            width: 700,
                            height: 1,
                            margin: EdgeInsets.only(left: 22.w, right: 22.w),
                            child: SizedBox(
                              child: DecoratedBox(
                                decoration:
                                    BoxDecoration(color: Color(0xFFF1E1DA)),
                              ),
                            ),
                          )),
                      Visibility(
                          visible: widget.type == 1,
                          child: Container(
                              alignment: Alignment.centerRight,
                              height: 17.w,
                              margin: EdgeInsets.only(
                                  left: 32.w, right: 35.w, top: 12.w),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(),
                                      alignment: Alignment.centerRight,
                                      width: 3.w,
                                      height: 12.w,
                                      decoration: ShapeDecoration(
                                          color: Color(0xFF6388D2),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5))),
                                    ),
                                    Container(
                                      width: 80.w,
                                      margin: EdgeInsets.only(left: 2.w),
                                      child: Text(
                                        _OutsPageModal?.data?.homeName ?? "主客",
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 12.sp,
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                    ),
                                    Expanded(
                                        child: Container(
                                      alignment: Alignment.centerRight,
                                      // margin: EdgeInsets.only(right: 10),
                                      height: 31,
                                      child: MyList(data: _TopTabsName),
                                    )),
                                  ]))),
                      Visibility(
                          visible: widget.type == 1,
                          child: Container(
                              alignment: Alignment.centerRight,
                              height: 17,
                              margin: EdgeInsets.only(
                                  left: 32.w, right: 35.w, top: 7.w),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(),
                                      alignment: Alignment.centerRight,
                                      width: 3.w,
                                      height: 12.w,
                                      decoration: ShapeDecoration(
                                          color: Color(0xFFD1775E),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5))),
                                    ),
                                    Container(
                                      width: 80.w,
                                      margin: EdgeInsets.only(left: 2.w),
                                      child: Text(
                                        _OutsPageModal?.data?.awayName ?? "客队",
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 12.sp,
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                    ),
                                    Expanded(
                                        child: Container(
                                      alignment: Alignment.centerRight,
                                      // margin: EdgeInsets.only(right: 10),
                                      height: 31.w,
                                      child: MyList(data: _TopTabsName2),
                                    )),
                                  ]))),
                      Visibility(
                          visible: widget.type == 1,
                          child: Container(
                            width: 700,
                            height: 1,
                            margin: EdgeInsets.only(
                                left: 22.w, right: 22.w, top: 9.w),
                            child: SizedBox(
                              child: DecoratedBox(
                                decoration:
                                    BoxDecoration(color: Color(0xFFF1E1DA)),
                              ),
                            ),
                          )),
                      Visibility(
                          visible: widget.type == 0,
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20.w, right: 15.w, top: 15.w),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 6.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homeAttackCount !=
                                                      null
                                              ? _OutsPageModal
                                                      ?.data?.homeAttackCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                      CircularPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        radius: 66.w,
                                        lineWidth: 10.w,
                                        animation: true,
                                        percent:
                                            _OutsPageModal != null &&
                                                    _OutsPageModal?.data !=
                                                        null &&
                                                    _OutsPageModal?.data
                                                            ?.homeAttackCount !=
                                                        null &&
                                                    _OutsPageModal?.data
                                                            ?.awayAttackCount !=
                                                        null &&
                                                    _OutsPageModal?.data
                                                            ?.awayAttackCount !=
                                                        0
                                                ? _OutsPageModal!.data!
                                                        .awayAttackCount! /
                                                    (_OutsPageModal!.data!
                                                            .awayAttackCount! +
                                                        _OutsPageModal!.data!
                                                            .homeAttackCount!)
                                                : 0,
                                        center: Text(
                                          "进攻",
                                          style: TextStyle(fontSize: 14.sp),
                                          textAlign: TextAlign.center,
                                        ),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .awayAttackCount !=
                                                      null
                                              ? _OutsPageModal
                                                      ?.data?.awayAttackCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                    height: 78.w,
                                    child: DecoratedBox(
                                      decoration: BoxDecoration(
                                          color: Color(0xFFF1E1DA)),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 6.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homeAttackCount !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.homeDangerAttackCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                      CircularPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        radius: 66.w,
                                        lineWidth: 10.w,
                                        animation: true,
                                        percent: _OutsPageModal != null &&
                                                _OutsPageModal?.data != null &&
                                                _OutsPageModal?.data
                                                        ?.homeDangerAttackCount !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayDangerAttackCount !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayDangerAttackCount !=
                                                    0
                                            ? _OutsPageModal!.data!
                                                    .awayDangerAttackCount! /
                                                (_OutsPageModal!.data!
                                                        .awayDangerAttackCount! +
                                                    _OutsPageModal!.data!
                                                        .homeDangerAttackCount!)
                                            : 0,
                                        center: Container(
                                            width: 40,
                                            child: Text(
                                              "危险进攻",
                                              style: TextStyle(fontSize: 14.sp),
                                              textAlign: TextAlign.center,
                                              maxLines: 2,
                                            )),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homeAttackCount !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.awayDangerAttackCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                    height: 78.w,
                                    child: DecoratedBox(
                                      decoration: BoxDecoration(
                                          color: Color(0xFFF1E1DA)),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 3.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homePossessionPercentage !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.homePossessionPercentage
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                      CircularPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        radius: 66.w,
                                        lineWidth: 10.w,
                                        animation: true,
                                        percent: _OutsPageModal != null &&
                                                _OutsPageModal?.data != null &&
                                                _OutsPageModal?.data
                                                        ?.homePossessionPercentage !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayPossessionPercentage !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayPossessionPercentage !=
                                                    0
                                            ? _OutsPageModal!.data!
                                                    .awayPossessionPercentage! /
                                                (_OutsPageModal!.data!
                                                        .awayPossessionPercentage! +
                                                    _OutsPageModal!.data!
                                                        .homePossessionPercentage!)
                                            : 0,
                                        center: Container(
                                          width: 50.w,
                                          child: Text(
                                            "控球率(%)",
                                            style: TextStyle(fontSize: 14.sp),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .awayPossessionPercentage !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.awayPossessionPercentage
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ))),
                      Visibility(
                          visible: widget.type == 1,
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20.w, right: 20.w, top: 15.w),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 6.w),
                                        child: Text(
                                          _OutsPageModal
                                                  ?.data?.homeThreePointScore ??
                                              "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                      CircularPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        radius: 66.w,
                                        lineWidth: 10.w,
                                        animation: true,
                                        percent: _OutsPageModal != null &&
                                                _OutsPageModal?.data != null &&
                                                _OutsPageModal?.data
                                                        ?.homeThreePointScore !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayThreePointScore !=
                                                    null
                                            ? int.parse(_OutsPageModal!.data!
                                                    .awayThreePointScore!) /
                                                (int.parse(_OutsPageModal!.data!
                                                        .awayThreePointScore!) +
                                                    int.parse(_OutsPageModal!
                                                        .data!
                                                        .homeThreePointScore!))
                                            : 0,
                                        center: Container(
                                            width: 50.w,
                                            child: Text(
                                              "三分球得分",
                                              style: TextStyle(
                                                fontSize: 14.sp,
                                              ),
                                              textAlign: TextAlign.center,
                                            )),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3.w),
                                        child: Text(
                                          _OutsPageModal
                                                  ?.data?.awayThreePointScore ??
                                              "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                    height: 78.w,
                                    child: DecoratedBox(
                                      decoration: BoxDecoration(
                                          color: Color(0xFFF1E1DA)),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 6.w),
                                        child: Text(
                                          _OutsPageModal
                                                  ?.data?.homeFreeThrowScore ??
                                              "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                      CircularPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        radius: 66.w,
                                        lineWidth: 10.w,
                                        animation: true,
                                        percent: _OutsPageModal != null &&
                                                _OutsPageModal?.data != null &&
                                                _OutsPageModal?.data
                                                        ?.homeFreeThrowScore !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayFreeThrowScore !=
                                                    null
                                            ? int.parse(_OutsPageModal!.data!
                                                    .awayFreeThrowScore!) /
                                                (int.parse(_OutsPageModal!.data!
                                                        .awayFreeThrowScore!) +
                                                    int.parse(_OutsPageModal!
                                                        .data!
                                                        .homeFreeThrowScore!))
                                            : 0,
                                        center: Container(
                                            width: 40,
                                            child: Text(
                                              "罚球得分",
                                              style: TextStyle(fontSize: 14.sp),
                                              textAlign: TextAlign.center,
                                              maxLines: 2,
                                            )),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3.w),
                                        child: Text(
                                          _OutsPageModal
                                                  ?.data?.awayFreeThrowScore ??
                                              "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                    height: 78.w,
                                    child: DecoratedBox(
                                      decoration: BoxDecoration(
                                          color: Color(0xFFF1E1DA)),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 6.w),
                                        child: Text(
                                          _OutsPageModal
                                                  ?.data?.homeFreeThrowHitScorePercentage ??
                                              "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                      CircularPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        radius: 66.w,
                                        lineWidth: 10.w,
                                        animation: true,
                                        percent: _OutsPageModal != null &&
                                                _OutsPageModal?.data != null &&
                                                _OutsPageModal?.data
                                                        ?.homeFreeThrowHitScorePercentage !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayFreeThrowHitScorePercentage !=
                                                    null
                                            ? int.parse(_OutsPageModal!.data!
                                                    .awayFreeThrowHitScorePercentage!) /
                                                (int.parse(_OutsPageModal!.data!
                                                        .awayFreeThrowHitScorePercentage!) +
                                                    int.parse(_OutsPageModal!
                                                        .data!
                                                        .homeFreeThrowHitScorePercentage!))
                                            : 0,
                                        center: Container(
                                          width: 55.w,
                                          child: Text(
                                            "罚球命中率(%)",
                                            style: TextStyle(fontSize: 14.sp),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3.w),
                                        child: Text(
                                          _OutsPageModal
                                                  ?.data?.awayFreeThrowHitScorePercentage ??
                                              "0",
                                          style: TextStyle(fontSize: 12.sp),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ))),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 1,
                        margin:
                            EdgeInsets.only(left: 22.w, right: 22.w, top: 9.w),
                        child: SizedBox(
                          child: DecoratedBox(
                            decoration: BoxDecoration(color: Color(0xFFF1E1DA)),
                          ),
                        ),
                      ),
                      Visibility(
                          visible: widget.type == 0,
                          child: Container(
                            margin: EdgeInsets.only(
                                left: 20.w, right: 10.w, top: 14.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child:
                                            LocalImageSelector.getSingleImage(
                                                'Group2',
                                                imageHeight: 20.w,
                                                imageWidth: 20.w,
                                                bFitFill: BoxFit.fill),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 7),
                                        child:
                                            LocalImageSelector.getSingleImage(
                                                'Group',
                                                imageHeight: 20.w,
                                                imageWidth: 13.w,
                                                bFitFill: BoxFit.fill),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 7.w),
                                        child:
                                            LocalImageSelector.getSingleImage(
                                                'Group1',
                                                imageHeight: 20.w,
                                                imageWidth: 13.w,
                                                bFitFill: BoxFit.fill),
                                      ),
                                      Container(
                                        width: 15.w,
                                        margin: EdgeInsets.only(left: 14.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homeShotsCount !=
                                                      null
                                              ? _OutsPageModal
                                                      ?.data?.homeShotsCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                    ]),
                                Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(bottom: 2.w),
                                        child: Text(
                                          "射门次数",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      LinearPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        width: 156.w,
                                        lineHeight: 8.w,
                                        percent: _OutsPageModal != null &&
                                                _OutsPageModal?.data != null &&
                                                _OutsPageModal?.data
                                                        ?.homeShotsCount !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayShotsCount !=
                                                    null &&
                                                _OutsPageModal!.data!
                                                        .awayShotsCount! !=
                                                    0
                                            ? _OutsPageModal!
                                                    .data!.awayShotsCount! /
                                                (_OutsPageModal!
                                                        .data!.awayShotsCount! +
                                                    _OutsPageModal!
                                                        .data!.homeShotsCount!)
                                            : 0,
                                        isRTL: true,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      )
                                    ]),
                                Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: 15.w,
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .awayShotsCount !=
                                                      null
                                              ? _OutsPageModal
                                                      ?.data?.awayShotsCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 12.w),
                                        child:
                                            LocalImageSelector.getSingleImage(
                                                'Group2',
                                                imageHeight: 20.w,
                                                imageWidth: 20.w,
                                                bFitFill: BoxFit.fill),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 7.w),
                                        child:
                                            LocalImageSelector.getSingleImage(
                                                'Group',
                                                imageHeight: 20.w,
                                                imageWidth: 13.w,
                                                bFitFill: BoxFit.fill),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 7.w),
                                        child:
                                            LocalImageSelector.getSingleImage(
                                                'Group1',
                                                imageHeight: 20.w,
                                                imageWidth: 13.w,
                                                bFitFill: BoxFit.fill),
                                      ),
                                    ]),
                              ],
                            ),
                          )),
                      Visibility(
                          visible: widget.type == 0,
                          child: Container(
                            margin: EdgeInsets.only(
                                left: 20.w,
                                right: 10.w,
                                top: 10.w,
                                bottom: 15.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 15.w,
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!
                                                          .data!.homeCorner !=
                                                      null
                                              ? _OutsPageModal?.data?.homeCorner
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                      Container(
                                        width: 15.w,
                                        margin: EdgeInsets.only(left: 7.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homeRedCardCount !=
                                                      null
                                              ? _OutsPageModal
                                                      ?.data?.homeRedCardCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                      Container(
                                        width: 15.w,
                                        margin: EdgeInsets.only(left: 7.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homeYellowCardCount !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.homeYellowCardCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                      Container(
                                        width: 15.w,
                                        margin: EdgeInsets.only(left: 14.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .homeShootRightCount !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.homeShootRightCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                    ]),
                                Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(bottom: 2.w),
                                        child: Text(
                                          "射正次数",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      LinearPercentIndicator(
                                        addAutomaticKeepAlive: false,
                                        width: 156.w,
                                        lineHeight: 8.w,
                                        percent: _OutsPageModal != null &&
                                                _OutsPageModal?.data != null &&
                                                _OutsPageModal?.data
                                                        ?.homeShootRightCount !=
                                                    null &&
                                                _OutsPageModal?.data
                                                        ?.awayShootRightCount !=
                                                    null &&
                                                _OutsPageModal!.data!
                                                        .awayShootRightCount! !=
                                                    0
                                            ? _OutsPageModal!.data!
                                                    .awayShootRightCount! /
                                                (_OutsPageModal!.data!
                                                        .awayShootRightCount! +
                                                    _OutsPageModal!.data!
                                                        .homeShootRightCount!)
                                            : 0,
                                        isRTL: true,
                                        progressColor: Color(0xFFF36704),
                                        backgroundColor: Color(0xFFFFFAF7),
                                      )
                                    ]),
                                Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: 15.w,
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .awayShootRightCount !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.awayShootRightCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                      Container(
                                        width: 15.w,
                                        margin: EdgeInsets.only(left: 12.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!
                                                          .data!.awayCorner !=
                                                      null
                                              ? _OutsPageModal?.data?.awayCorner
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                      Container(
                                        width: 15.w,
                                        margin: EdgeInsets.only(left: 7.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .awayRedCardCount !=
                                                      null
                                              ? _OutsPageModal
                                                      ?.data?.awayRedCardCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                      Container(
                                        width: 15.w,
                                        margin: EdgeInsets.only(left: 7.w),
                                        child: Text(
                                          _OutsPageModal != null &&
                                                  _OutsPageModal!.data !=
                                                      null &&
                                                  _OutsPageModal!.data!
                                                          .awayYellowCardCount !=
                                                      null
                                              ? _OutsPageModal?.data
                                                      ?.awayYellowCardCount
                                                      .toString() ??
                                                  "0"
                                              : "0",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF666666)),
                                        ),
                                      ),
                                    ]),
                              ],
                            ),
                          )),
                      Container(
                        alignment: Alignment.center,
                        height: 64.w,
                        color: c_w,
                        child: Container(
                            margin: EdgeInsets.only(top: 15.w, bottom: 15.w),
                            height: 44.w,
                            width: 192.w,
                            decoration: BoxDecoration(
                              color: Color(0xFFFFFFFF),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40.0)),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: isSelected == 0
                                          ? Color(0xFFF36704)
                                          : Color(0xFFFFFFFF),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(40.0)),
                                    ),
                                    height: 36.w,
                                    width: 90.w,
                                    alignment: Alignment.center,
                                    child: Text(_tabs[0],
                                        style: TextStyle(
                                          fontSize: 14.sp,
                                          color: isSelected == 0
                                              ? Color(0xFFFFFFFF)
                                              : Color(0xFF666666),
                                        )),
                                  ),
                                  onTap: () => setState(() {
                                    isSelected = 0;
                                    // _loadData();
                                    // _RequestBottomListData(0);
                                  }),
                                ),
                                GestureDetector(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(40.0)),
                                      color: isSelected == 1
                                          ? Color(0xFFF36704)
                                          : Color(0xFFFFFFFF),
                                    ),
                                    height: 36.w,
                                    width: 90.w,
                                    alignment: Alignment.center,
                                    child: Text(_tabs[1],
                                        style: TextStyle(
                                          fontSize: 14.sp,
                                          color: isSelected == 1
                                              ? Color(0xFFFFFFFF)
                                              : Color(0xFF666666),
                                        )),
                                  ),
                                  onTap: () => setState(() {
                                    isSelected = 1;
                                    // _loadData();
                                    // _RequestBottomListData(3);
                                  }),
                                ),
                              ],
                            )),
                      ),
                    ]),
              ),
              Visibility(
                  visible: isSelected == 0,
                  child: _OutsPageModal != null &&
                          _OutsPageModal!.data != null &&
                          _OutsPageModal!.data!.textLiveList != null &&
                          _OutsPageModal!.data!.textLiveList!.isNotEmpty
                      ? Container(
                          color: Color(0xFFFFFFFF),
                          child: TextLiveBroadcastList(
                              textLiveList:
                                  _OutsPageModal!.data!.textLiveList!))
                      : Container(
                          height: 200.w,
                          child: EmptyContainer(txt: '暂无数据'),
                        )),
              Visibility(
                  visible: isSelected == 1,
                  child: _OutsPageModal != null &&
                          _OutsPageModal!.data != null &&
                          _OutsPageModal!.data!.eventList != null &&
                          _OutsPageModal!.data!.eventList!.isNotEmpty
                      ? Container(
                          color: Color(0xFFFFFFFF),
                          child: ImportantEventsScroll(
                              homeLogo: widget.scoreModel != null &&
                                      widget.scoreModel.homeLogo != null
                                  ? widget.scoreModel.homeLogo
                                  : "",
                              awayLogo: widget.scoreModel != null &&
                                      widget.scoreModel.awayLogo != null
                                  ? widget.scoreModel.awayLogo
                                  : "",
                              data: _OutsPageModal?.data,
                              eventList1: _OutsPageModal?.data?.eventList,
                              eventList2: eventList2))
                      : Container(
                          height: 200.w,
                          child: EmptyContainer(txt: '暂无数据'),
                        )),
              // Container(
              //   height: 20.w,
              //   color: Color(0xFFFAF7F5),
              // )
            ],
          ))
        : EmptyContainer(txt: '暂无数据');
  }

  TabList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: 6,
        itemBuilder: _cellForRow);
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      width: 35.w,
      alignment: Alignment.centerRight,
      // margin: EdgeInsets.only(left: 19.w),
      child: Text(
        _TopTabs[index],
        style: TextStyle(
          fontSize: 12.sp,
        ),
      ),
    );
  }
}

class MyList extends StatelessWidget {
  final List<String?> data;

  MyList({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: data.length,
        itemBuilder: _cellForRow);
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      alignment: Alignment.centerRight,
      width: 35.w,
      // margin: EdgeInsets.only(left: 22.w),
      child: Text(
        data[index]!.isNotEmpty ? data[index].toString() : "0",
        style: TextStyle(
          fontSize: 12.sp,
        ),
      ),
    );
  }
}

class TextLiveBroadcastList extends StatelessWidget {
  final List<TextLiveList>? textLiveList;

  TextLiveBroadcastList({
    Key? key,
    required this.textLiveList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: Container(
            margin: EdgeInsets.only(left: 24.w, right: 24.w),
            child: ListView.separated(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: textLiveList != null ? textLiveList!.length : 0,
              physics: NeverScrollableScrollPhysics(),
              separatorBuilder: (BuildContext context, int index) =>
                  Divider(height: 1.w, color: Color(0xFFF1E1DA)),
              itemBuilder: (BuildContext context, int position) {
                return _cellForRow(context, position);
              },
            )));
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      height: 52.w,
      alignment: Alignment.centerLeft,
          child:Container(
            child: Text(
              textLiveList != null && textLiveList!.length > 0
                  ? textLiveList![index].content.toString()
                  : "",
              style: TextStyle(
                fontSize: 12.sp,overflow: TextOverflow.ellipsis,
              ),
              maxLines: 2,
            ),
          ),
    );
  }
}

class ImportantEventsScroll extends StatelessWidget {
  final Data? data;
  final String? homeLogo;
  final String? awayLogo;
  final List<EventList>? eventList1;
  final List<EventList>? eventList2;

  ImportantEventsScroll(
      {Key? key,
      required this.homeLogo,
      required this.awayLogo,
      required this.data,
      required this.eventList1,
      required this.eventList2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Container(
                margin: EdgeInsets.only(top: 12.w, bottom: 7.w),
                width: 4.w,
                height: 20.w,
                decoration: ShapeDecoration(
                  color: Color(0xFFF36704),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10))),
                )),
            Container(
              margin: EdgeInsets.only(left: 10.w, top: 12.w, bottom: 7.w),
              child: Text(
                "场上事件",
                style: TextStyle(
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF666666)),
              ),
            )
          ],
        ),
        Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 108.w,
                child: Column(
                  children: [
                    Container(
                      width: 38.w,
                      height: 38.w,
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.all(7),
                        child: ClipOval(
                          child: Image.network(homeLogo ?? "",
                              width: 24.w,
                              height: 24.w,
                              errorBuilder: (context, error, stackTrace) =>
                                  LocalImageSelector.getSingleImage(
                                      "default-head",
                                      imageHeight: 24.w,
                                      imageWidth: 24.w,
                                      bFitFill: BoxFit.fill)),
                        ),
                      ),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              'assets/images/onlive/online_rectangle.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 7.w),
                      child: Text(
                        data?.homeName ?? "",
                        style: TextStyle(fontSize: 14.sp),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.w),
                child: Text(
                  '${data?.homeScore ?? "0"}-${data?.awayScore ?? "0"}',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 24.sp),
                ),
              ),
              Container(
                width: 108.w,
                child: Column(
                  children: [
                    Container(
                      width: 38.w,
                      height: 38.w,
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.all(7),
                        child: ClipOval(
                          child: Image.network(awayLogo ?? "",
                              width: 24.w,
                              height: 24.w,
                              errorBuilder: (context, error, stackTrace) =>
                                  LocalImageSelector.getSingleImage(
                                      "default-head",
                                      imageHeight: 24.w,
                                      imageWidth: 24.w,
                                      bFitFill: BoxFit.fill)),
                        ),
                      ),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              'assets/images/onlive/online_rectangle.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 7.w),
                      child: Text(
                        data?.awayName ?? "",
                        style: TextStyle(fontSize: 14.sp),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ]),
        Container(
          margin: EdgeInsets.only(top: 5.w, left: 14.w, right: 14.w),
          child: SizedBox(
            width: 500.w,
            height: 1.w,
            child: DecoratedBox(
              decoration: BoxDecoration(color: Color(0xFFF1E1DA)),
            ),
          ),
        ),
        Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Container(
                alignment: Alignment.centerRight,
                child: ImportantEventsLeftList(
                  eventList1: eventList1,
                ),
              )),
            ])
      ],
    );
  }
}

class ImportantEventsLeftList extends StatelessWidget {
  final List<EventList>? eventList1;

  ImportantEventsLeftList({Key? key, required this.eventList1})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: eventList1 != null ? eventList1!.length : 0,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: _cellForRow);
  }

  Widget _cellForRow(BuildContext context, int index) {
    return eventList1 != null &&
            eventList1!.length > 0 &&
            eventList1![index].isHome != null &&
            eventList1![index].isHome!
        ? Row(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Container(
                    alignment: Alignment.topRight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                                alignment: Alignment.topRight,
                                margin:
                                    EdgeInsets.only(left: 20.w, right: 20.w),
                                child: Text(
                                  eventList1 != null
                                      ? "${eventList1![index].nameChs.toString()} ${getKindName(eventList1![index].kind)}"
                                      : "",
                                  style: TextStyle(
                                    fontSize: 12.sp,
                                  ),
                                  textAlign: TextAlign.end,
                                  maxLines: 2,
                                  softWrap: true,
                                  overflow: TextOverflow.ellipsis,
                                ))),
                        Container(
                            alignment: Alignment.topRight,
                            margin: EdgeInsets.only(top: 2.w, right: 4.w),
                            child: Text(
                              eventList1 != null
                                  ? "${eventList1![index].time}’"
                                  : "",
                              style: TextStyle(
                                fontSize: 12.sp,
                              ),
                              textAlign: TextAlign.start,
                            ))
                      ],
                    ),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color(0x00FFFFFF),
                          Color(0xFFF3F3F3),
                        ],
                      ),
                    ),
                  )),
              Expanded(child: Text(""))
            ],
          )
        : Row(
            children: [
              Expanded(child: Text("")),
              Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Container(
                      // height: 20.w,
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              margin: EdgeInsets.only(top: 2.w, left: 4.w),
                              child: Text(
                                eventList1 != null
                                    ? "${eventList1![index].time}’"
                                    : "",
                                style: TextStyle(
                                  fontSize: 12.sp,
                                ),
                                textAlign: TextAlign.start,
                              )),
                          Expanded(
                              child: Container(
                                  margin:
                                      EdgeInsets.only(left: 18.w, right: 20.w),
                                  child: Text(
                                    eventList1 != null
                                        ? "${eventList1![index].nameChs.toString()} ${getKindName(eventList1![index].kind)}"
                                        : "",
                                    style: TextStyle(
                                      fontSize: 12.sp,
                                    ),
                                    textAlign: TextAlign.start,
                                    maxLines: 2,
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                  ))),
                        ],
                      ),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color(0xFFF3F3F3),
                            Color(0x00FFFFFF),
                          ],
                        ),
                      )))
            ],
          );
  }

  getKindName(int? kind) {
    if (kind == null) {
      return "";
    }
    if (kind == 1) {
      return "进球";
    } else if (kind == 2) {
      return "红牌";
    } else if (kind == 3) {
      return "黄牌";
    } else if (kind == 7) {
      return "点球";
    } else if (kind == 8) {
      return "乌龙";
    } else if (kind == 9) {
      return "两黄变红";
    } else if (kind == 11) {
      return "换人";
    } else if (kind == 13) {
      return "射失点球";
    } else if (kind == 14) {
      return "视频裁判（VR裁判）";
    } else {
      return "";
    }
  }
}

class ImportantEventsRightList extends StatelessWidget {
  final List<EventList>? eventList2;

  ImportantEventsRightList({Key? key, required this.eventList2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: eventList2 != null ? eventList2!.length : 0,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: _cellForRow);
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(right: 20.w),
          height: 20.w,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Color(0xFFF3F3F3),
                Color(0x00FFFFFF),
              ],
            ),
          ),
        ),
        Container(
          // width: 130,
          height: 40.w,
          alignment: Alignment.topLeft,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: EdgeInsets.only(top: 2.w, left: 4.w),
                  child: Text(
                    eventList2 != null ? "${eventList2![index].time}’" : "",
                    style: TextStyle(
                      fontSize: 12.sp,
                    ),
                    textAlign: TextAlign.start,
                  )),
              Expanded(
                  child: Container(
                      margin: EdgeInsets.only(left: 18.w, right: 20.w),
                      child: Text(
                        eventList2 != null
                            ? "${eventList2![index].nameChs.toString()} ${getKindName(eventList2![index].kind)}"
                            : "",
                        style: TextStyle(
                          fontSize: 12.sp,
                        ),
                        textAlign: TextAlign.start,
                        maxLines: 2,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                      ))),
            ],
          ),
        )
      ],
    );
  }

  getKindName(int? kind) {
    if (kind == null) {
      return "";
    }
    if (kind == 1) {
      return "进球";
    } else if (kind == 2) {
      return "红牌";
    } else if (kind == 3) {
      return "黄牌";
    } else if (kind == 7) {
      return "点球";
    } else if (kind == 8) {
      return "乌龙";
    } else if (kind == 9) {
      return "两黄变红";
    } else if (kind == 11) {
      return "换人";
    } else if (kind == 13) {
      return "射失点球";
    } else if (kind == 14) {
      return "视频裁判（VR裁判）";
    } else {
      return "";
    }
  }
}
