// @dart=2.9
// ignore_for_file: unused_import

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:liveapp/api/score_req_method.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/common_widget/extended_tabs/src/tab_view.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/model/tab_data_mod.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/lineup.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/mo/score_home_model.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/outs_page.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/score_animal_loading.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/score_detail_live_page.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/score_detail_vids_dialog.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_list.dart';
import 'package:liveapp/pages/live/home/component/common/video_item.dart';
import 'package:liveapp/pages/live/home/component/e_mine/common/base_navigation_view.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/pages/live/home/liveDetail/live_video.dart';
import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/utils/my_underline_indicator.dart';
import 'package:m_loading/m_loading.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'analysis.dart';
import 'for.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

// import 'package:sider_bar/sider_bar.dart';
class ScoreDetailPage extends BaseWidget {
  ScoreDetailPage();

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _ScoreDetailPageState();
  }
}

class _ScoreDetailPageState extends BaseWidgetState<ScoreDetailPage> with TickerProviderStateMixin {
  List<String> MenuList = ['直播', '赛况', '阵容', '分析', '统计'];
  List<String> StateList = ['未开始', '进行中', '已结束', '取消'];
  final String content = "ABCDEFGHIGKLMNOPQRSTUVWXYZ";
  List<String> mDataList;
  TabController MenutabController;

  ///0 初始状态  1 动画  2 播放器
  int detailState = 0;
  bool isAnimal = false;

  String enterType = '';
  // scoreMatchModel scoreModel;
  /// scorehome 比分首页type   search 搜索页type
  String routeType = '';

  dynamic scoreModel;

  var _catId = null;
  @override
  bool get wantKeepAlive => true;

  bool get _isFullScreen => MediaQuery.of(context).orientation == Orientation.landscape;

  var animationUrl = '';
  var PlayUrl = '';

  var FloatPlayUrl = '';
  String leagueName = '';

  bool onPageFinished = false;
  Timer _reloadTimer;

  bool islock = true;
  @override
  void initState() {
    super.initState();
    mDataList = List<String>.generate(content.length, (index) => content[index]);
    MenutabController = TabController(initialIndex: 0, length: MenuList.length, vsync: this);
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    _initData();
  }

  Future<void> _initData() async {
    await Future.delayed(Duration.zero);
    // Future.delayed(const Duration(milliseconds: 860), () {
    //   islock = false;
    //   if (mounted) setState(() {});
    // });

    _checkIsSmallEnter();
    routeType = (ModalRoute.of(this.context).settings.arguments as Map)['type'];
    scoreModel = (ModalRoute.of(this.context).settings.arguments as Map)['scoreModel'];
    if (routeType == 'search') {
      _catId = (scoreModel.catId.toString() != '1' &&
              scoreModel.catId.toString() != '2' &&
              scoreModel.catId.toString() != '37')
          ? ''
          : scoreModel.catId.toString();
      detailState = 2;
      PlayUrl = scoreModel.playUrls.last.id + '.m3u8';
      FloatPlayUrl = scoreModel.playUrls.last.id + '.m3u8';
      sessionStorage.dispatch(SaveFloatVideo(payload: {'floatInfo': FloatPlayUrl}));
    } else if (routeType == 'scorehome') {
      _catId = scoreModel.catId.toString();

      if (scoreModel.vids != null && scoreModel.vids.length > 0) {
        FloatPlayUrl = scoreModel.vids.last.playUrls.first.id + '.m3u8';
        sessionStorage.dispatch(SaveFloatVideo(payload: {'floatInfo': FloatPlayUrl}));
      }
    } else if (routeType == 'reserve') {
      _catId = (scoreModel.catId.toString() != '1' &&
              scoreModel.catId.toString() != '2' &&
              scoreModel.catId.toString() != '37')
          ? ''
          : scoreModel.catId.toString();
      if (scoreModel.playUrls != null) {
        FloatPlayUrl = scoreModel.playUrls.last.id + '.m3u8';
      }

      sessionStorage.dispatch(SaveFloatVideo(payload: {'floatInfo': FloatPlayUrl}));
      scoreModel.realTime = '未开';
      scoreModel.animationUrl == '';
    }

    leagueName = scoreModel != null
        ? (routeType == 'scorehome' || routeType == 'reserve')
            ? scoreModel.catId == 1
                ? scoreModel.leagueChsShort ?? ''
                : scoreModel.leagueChs ?? ''
            : scoreModel.leagueName ?? ''
        : "";

    if (mounted) setState(() {});

    ///比分进入 实时刷新数据
    if (routeType == 'scorehome' &&
        scoreModel.realTime != '完场' &&
        scoreModel.realTime != '腰斩' &&
        scoreModel.realTime != '取消' &&
        scoreModel.realTime != '中断') {
      const time = Duration(seconds: 10);
      if (mounted) {
        _reloadTimer = Timer.periodic(time, (timer) async {
          _getMatchScheduleByMatchId();
        });
      }
    }
  }

  @override
  void dispose() {
    if (mounted) {
      if (_reloadTimer != null && _reloadTimer.isActive) _reloadTimer.cancel();
      MenutabController.dispose();
    }
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    bool isfull = false;
    if (mounted) {
      isfull = MediaQuery.of(context).orientation == Orientation.landscape;
    }

    return Scaffold(
        body: WillPopScope(
      child: Container(
        // color: Colors.black,
        child: Column(
          children: [
            !isfull
                ? Container(
                    height: getTopBarHeight(),
                    color: c_bl,
                  )
                : Container(),

            ///比分详情页头部
            detailState == 2
                ? Stack(
                    children: [
                      Column(
                        children: [
                          !state.live.isMiniPlay
                              ? LiveVideo(
                                  enterType: enterType,
                                  leagueName: leagueName,
                                  playerUrl: PlayUrl,
                                  item: scoreModel,
                                  routeType: routeType,
                                )
                              : Container(
                                  decoration: BoxDecoration(image: LocalImageSelector.getBgImage('common_live_bg')),
                                  height: 210.w,
                                ),
                        ],
                      ),
                      if (state.live.isMiniPlay)
                        Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          child: userAppBar(
                            title: leagueName,
                            rightIcon: 'common_share',
                            rightClickFn: () {
                              print('41行打印：============ ${'share'}');
                              _shareMethod();
                            },
                          ),
                        ),
                    ],
                  )
                : detailState == 1
                    ? Stack(
                        children: [
                          Container(
                            height: 210.w,
                            child: Container(
                              decoration: BoxDecoration(image: LocalImageSelector.getBgImage('common_live_bg')),
                              child: WebView(
                                backgroundColor: Color(0x00000000),
                                initialUrl: scoreModel != null ? animationUrl : "",
                                javascriptMode: JavascriptMode.unrestricted,
                                onPageFinished: (url) {
                                  onPageFinished = true;
                                  if (mounted) setState(() {});
                                },
                              ),
                            ),
                          ),
                          !onPageFinished
                              ? Align(
                                  alignment: Alignment.center,
                                  child: AnimalLoadingContainer(),
                                )
                              : Container(),
                          Positioned(
                            top: 0,
                            left: 0,
                            right: 0,
                            child: userAppBar(
                              title: leagueName,
                              rightIcon: 'common_share',
                              rightClickFn: () {
                                print('41行打印：============ ${'share'}');
                                _shareMethod();
                              },
                            ),
                          ),
                        ],
                      )
                    : Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(image: LocalImageSelector.getBgImage('common_live_bg')),
                            height: 210.w,
                          ),
                          Positioned(
                              top: 0,
                              left: 0,
                              right: 0,
                              // child:!islock?   userAppBar(
                              //   title: scoreModel != null
                              //       ? routeType == 'reserve'
                              //       ? scoreModel.leagueName
                              //       : scoreModel.catId == 1
                              //       ? scoreModel.leagueChsShort ?? ''
                              //       : scoreModel.leagueChs ?? ''
                              //       : "",
                              //   rightIcon: 'common_share',
                              //   rightClickFn: () {
                              //     print('41行打印：============ ${'share'}');
                              //     _shareMethod();
                              //   },
                              // ):Container(),

                              child: userAppBar(
                                title: scoreModel != null
                                    ? routeType == 'reserve'
                                        ? scoreModel.leagueName
                                        : scoreModel.catId == 1
                                            ? scoreModel.leagueChsShort ?? ''
                                            : scoreModel.leagueChs ?? ''
                                    : "",
                                rightIcon: 'common_share',
                                rightClickFn: () {
                                  print('41行打印：============ ${'share'}');
                                  _shareMethod();
                                },
                              )),
                          Positioned(
                            top: 46.w,
                            left: 0,
                            right: 0,
                            child: _scoreHeadMiddleView(),
                          ),
                        ],
                      ),

            !isfull
                ? Container(
                    color: c_white,
                    height: 45.w,
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 20.w,
                        right: 20.w,
                      ),
                      child: TabBar(
                        tabs: MenuList.map((String type) => Container(
                            width: (MediaQuery.of(context).size.width - 40.w) / MenuList.length,
                            child: Tab(
                              text: type,
                            ))).toList(),
                        labelPadding: EdgeInsets.zero,
                        // labelPadding: EdgeInsets.symmetric(horizontal: 10.w),
                        isScrollable: true,
                        labelColor: c_o,
                        unselectedLabelColor: c_6,
                        indicator: TabSizeIndicator(
                          wantWidth: 32.w,
                          borderSide: BorderSide(width: 4.0.w, color: c_o),
                        ),
                        labelStyle: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
                        // unselectedLabelStyle: TextStyle(fontSize: 16.sp),
                        controller: MenutabController,
                      ),
                    ),
                  )
                : Container(),
            !isfull
                ? Container(
                    width: double.infinity,
                    height: 1.w,
                    color: c_b1,
                  )
                : Container(),
            !isfull
                ? SizedBox(
                    height: 10.w,
                  )
                : Container(),
            Expanded(
              child: ExtendedTabBarView(
                children: _expandtypeList(),
                controller: MenutabController,
              ),
            )
          ],
        ),
      ),

      ///关闭返回手势
      // onWillPop: () async{
      //   return false;
      // }
    ));
  }

  // List<String> MenuList = ['直播', '赛况', '阵容', '分析', '统计'];
  ///底部分页内容
  _expandtypeList() {
    return MenuList.map((String TitleName) {
      if (TitleName == '赛况')
        return outsPage(
            scoreModel: scoreModel,
            matchId: (scoreModel != null && scoreModel.matchId != null) ? scoreModel.matchId.toString() : "",
            type: (scoreModel != null && scoreModel.catId != null && scoreModel.catId != '')
                ? int.parse(scoreModel.catId.toString()) == 1
                    ? 0
                    : int.parse(scoreModel.catId.toString()) == 2
                        ? 1
                        : 2
                : 2);
      if (TitleName == '阵容')
        return lineupPage(
            scoreModel: scoreModel,
            matchId: (scoreModel != null && scoreModel.matchId != null) ? scoreModel.matchId.toString() : "",
            type: (scoreModel != null && scoreModel.catId != null && scoreModel.catId != '')
                ? int.parse(scoreModel.catId.toString()) == 1
                    ? 0
                    : 1
                : 0);
      if (TitleName == '分析')
        return analysisPage(
            matchId: (scoreModel != null && scoreModel.matchId != null) ? scoreModel.matchId.toString() : "",
            type: (scoreModel != null && scoreModel.catId != null && scoreModel.catId != '')
                ? int.parse(scoreModel.catId.toString()) == 1
                    ? 0
                    : 1
                : 0);
      if (TitleName == '统计')
        return forPage(
            matchId: (scoreModel != null && scoreModel.matchId != null) ? scoreModel.matchId.toString() : "",
            type: (scoreModel != null && scoreModel.catId != null && scoreModel.catId != '')
                ? int.parse(scoreModel.catId.toString()) == 1
                    ? 0
                    : 1
                : 0);
      return _catId != null ? ScoreDetailLivePage(_catId.toString()) : Container();
    }).toList();
  }

  ///头部比分赛况信息_scoreHeadMiddleView
  _scoreHeadMiddleView() {
    return Container(
      margin: EdgeInsets.only(left: 10.w, right: 10.w),
      child: Row(
        children: [
          _matchInfoWidget(
              scoreModel != null ? scoreModel.homeLogo ?? "" : "", scoreModel != null ? scoreModel.homeName : "--"),
          Expanded(child: _middleStateWidget()),
          _matchInfoWidget(
              scoreModel != null ? scoreModel.awayLogo ?? "" : "", scoreModel != null ? scoreModel.awayName : "--"),
        ],
      ),
    );
  }

  _matchInfoWidget(String iconStr, String matchName) {
    return Container(
      width: 110.w,
      margin: EdgeInsets.only(bottom: 10.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 45.w,
            height: 45.w,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(22.w)),
            ),
            child: Image(
              image: NetworkImage(iconStr),
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) => LocalImageSelector.getSingleImage("default-head",
                  imageHeight: 45.w, imageWidth: 45.w, bFitFill: BoxFit.fill),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.w),
            child: Text(
              matchName,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.sp, color: c_white),
            ),
          )
        ],
      ),
    );
  }

  _middleStateWidget() {
    String timeStr = '--';
    //matchDateTime
    if (scoreModel != null && scoreModel.matchTime.length > 8) {
      timeStr = scoreModel.matchTime.substring(5);
      timeStr = timeStr.substring(0, timeStr.length - 3);
    } else if (routeType == 'reserve') {
      timeStr = scoreModel.matchDateTime.substring(5);
      timeStr = timeStr.substring(0, timeStr.length - 3);
    }

    String homeScore = '0';
    if (scoreModel != null && scoreModel.homeScore != null && scoreModel.homeScore != '') {
      homeScore = scoreModel.homeScore.toString();
    }
    String awayScore = '0';
    if (scoreModel != null && scoreModel.awayScore != null && scoreModel.awayScore != '') {
      awayScore = scoreModel.awayScore.toString();
    }

    return Column(
      children: [
        Container(
          height: 105.w,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 20.w,
                child: Text(
                  timeStr,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 14.sp, color: c_white),
                ),
              ),
              scoreModel != null
                  ? Container(
                      margin: EdgeInsets.only(top: 5),
                      height: 38.w,
                      child: Text(
                        homeScore + '-' + awayScore,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 30.sp, color: c_white),
                      ),
                    )
                  : Container(
                      height: 38.w,
                    ),
              Container(
                height: 18.w,
                child: Text(
                  scoreModel != null ? '${scoreModel.realTime ?? ''}' : "--",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 14.sp, color: c_white),
                ),
              ),
            ],
          ),
        ),
        (scoreModel != null)
            ? Container(
                width: 128.w,
                height: 24.w,
                decoration: BoxDecoration(image: LocalImageSelector.getBgImage('score_detailhead_statebg')),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        flex: 1,
                        child: FlatButton(
                          padding: EdgeInsets.symmetric(horizontal: 0),
                          onPressed: () {
                            if (scoreModel == null ||
                                scoreModel.animationUrl == null ||
                                scoreModel.animationUrl == '') {
                              if (scoreModel.realTime == '完场') {
                                EasyLoading.showToast('比赛已结束');
                              } else {
                                EasyLoading.showToast('暂无比赛动画');
                              }

                              return;
                            }
                            if (mounted)
                              setState(() {
                                animationUrl = scoreModel.animationUrl.toString();
                                detailState = 1;
                              });
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              LocalImageSelector.getSingleImage('score_videoicon', imageWidth: 16.w, imageHeight: 10.w),
                              Container(
                                margin: EdgeInsets.only(left: 5.w),
                                child: Text(
                                  '动画',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12.sm, color: c_white),
                                ),
                              )
                            ],
                          ),
                        )),

                    // Container(
                    //   height: 10.w,
                    //   width: 1.w,
                    //   color: c_white,
                    // ),
                    // Expanded(
                    //     flex: 1,
                    //     child: FlatButton(
                    //       padding: EdgeInsets.symmetric(horizontal: 0),
                    //       onPressed: () {
                    //         if (routeType == 'reserve') {
                    //           if (scoreModel == null ||
                    //               scoreModel.playUrls == null ||
                    //               scoreModel.playUrls.length == 0) {
                    //             if (scoreModel.realTime == '完场') {
                    //               EasyLoading.showToast('比赛已结束');
                    //             } else {
                    //               EasyLoading.showToast('暂无比赛视频');
                    //             }
                    //             return;
                    //           }
                    //
                    //           PlayUrl = scoreModel.playUrls.last.id + '.m3u8';
                    //
                    //           ///临时测试
                    //           // PlayUrl = 'https://www.apple.com/105/media/cn/mac/family/2018/46c4b917_abfd_45a3_9b51_4e3054191797/films/bruce/mac-bruce-tpl-cn-2018_1280x720h.mp4';
                    //           if (mounted)
                    //             setState(() {
                    //               detailState = 2;
                    //             });
                    //         } else {
                    //           if (scoreModel == null || scoreModel.vids == null || scoreModel.vids.length == 0 || scoreModel.vids.first.playUrls.length == 0) {
                    //             if (scoreModel.realTime == '完场') {
                    //               EasyLoading.showToast('比赛已结束');
                    //             } else {
                    //               EasyLoading.showToast('暂无比赛视频');
                    //             }
                    //             return;
                    //           }
                    //
                    //           if (scoreModel.vids.length > 1) {
                    //             showDialog(
                    //                 context: context,
                    //                 builder: (BuildContext context) {
                    //                   return SelectVidsDialog(SelectVidConfirmCallback, scoreModel.vids);
                    //                 });
                    //           } else {
                    //             PlayUrl = scoreModel.vids.first.playUrls.first.id + '.m3u8';
                    //
                    //             ///临时测试
                    //             // PlayUrl = 'https://www.apple.com/105/media/cn/mac/family/2018/46c4b917_abfd_45a3_9b51_4e3054191797/films/bruce/mac-bruce-tpl-cn-2018_1280x720h.mp4';
                    //             if (mounted)
                    //               setState(() {
                    //                 detailState = 2;
                    //               });
                    //           }
                    //         }
                    //       },
                    //       child: Row(
                    //         mainAxisAlignment: MainAxisAlignment.center,
                    //         children: [
                    //           Container(
                    //             margin: EdgeInsets.only(right: 5.w),
                    //             child: Text(
                    //               '视频',
                    //               textAlign: TextAlign.center,
                    //               style: TextStyle(fontSize: 12.sm, color: c_white),
                    //             ),
                    //           ),
                    //           LocalImageSelector.getSingleImage('score_animalicon',
                    //               imageWidth: 16.w, imageHeight: 10.w),
                    //         ],
                    //       ),
                    //     )),
                  ],
                ),
              )
            : Container(
                height: 24.w,
              )
      ],
    );
  }

  ///- 检测是否为小窗口进入
  void _checkIsSmallEnter() async {
    await Future.delayed(Duration.zero);
    if (mounted) {
      enterType = (ModalRoute.of(this.context).settings.arguments as Map)['enterType'];
      setState(() {});
    }
  }

  SelectVidConfirmCallback(Vids playModel) {
    PlayUrl = playModel.playUrls.first.id + '.m3u8';
    if (mounted)
      setState(() {
        detailState = 2;
      });
  }

  ///分享
  void _shareMethod() {
    Clipboard.setData(ClipboardData(text: ConfigUtil().APP_SHARE_URL));
    EasyLoading.showToast('已经成功为你复制地址，快去分享吧！');
  }

  void _getMatchScheduleByMatchId() async {
    Map<String, dynamic> MatchMap =
        await getMatchScheduleByMatchId(matchId: scoreModel.matchId.toString(), catId: _catId);
    if (MatchMap.length > 0) {
      scoreModel.homeScore = MatchMap['homeScore'];
      scoreModel.awayScore = MatchMap['awayScore'];
      scoreModel.realTime = MatchMap['realTime'];
      // scoreModel.vids = MatchMap['vids'];
      // scoreModel.animationUrl = MatchMap['scos'][0]['animationUrl'];
      if (mounted) setState(() {});
    }
  }
}
