// ignore_for_file: import_of_legacy_library_into_null_safe


import 'package:flutter/material.dart';
import 'package:m_loading/m_loading.dart';

class AnimalLoadingContainer extends StatelessWidget {
  final txt;
  const AnimalLoadingContainer({Key? key, String? this.txt}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 210,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 20,
              height: 25,
              child: PouringHourGlassLoading(
                color: Colors.white,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Text('动画正在努力加载中...',style: TextStyle(
                fontSize: 14,
                color: Colors.white
              ),),
            )
          ],
        ),
      ),
    );
  }
}
