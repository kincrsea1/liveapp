// @dart=2.9

class getScheduleData {
  int totalCount;
  int pageSize;
  int totalPage;
  int currPage;
  List<scoreMatchModel> list;

  getScheduleData({this.totalCount, this.pageSize, this.totalPage, this.currPage, this.list});

  getScheduleData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = new List<scoreMatchModel>();
      json['list'].forEach((v) {
        list.add(new scoreMatchModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class scoreMatchModel {
  int catId;
  String weatherEn;
  String leagueEn;
  dynamic groupId;
  String homeName;
  String roundCn;
  String leagueChtShort;
  String subLeagueEn;
  int awayCorner;
  String leagueEnShort;
  int homeRed;
  String awayLogo;
  int leagueId;
  String subLeagueCht;
  String season;
  String explainEn;
  int state;
  int homeHalfScore;
  String homeRankEn;
  String subLeagueId;
  dynamic homeId;
  int awayRed;
  String leagueChsShort;
  int kind;
  String subLeagueChs;
  String matchTime;
  String homeLogo;
  String grouping;
  String homeEn;
  String realTime;
  String awayName;
  String animationUrl;
  String awayChs;
  String awayRankEn;
  String awayCht;
  String weatherCn;
  String roundEn;
  String color;
  int awayHalfScore;
  int homeCorner;
  String extraExplain;
  dynamic updateTime;
  dynamic awayScore;
  String iconType;
  String locationEn;
  String explainCn;
  String homeRankCn;
  String startTime;
  int matchId;
  String temp;
  dynamic isNeutral;
  dynamic homeScore;
  String locationCn;
  bool hasAnimation;
  int awayId;
  int awayYellow;
  int homeYellow;
  String homeChs;
  bool isHidden;
  String homeCht;
  String awayEn;
  String awayRankCn;
  String time;
  String hasLineup;

  String tv;
  String away4;
  dynamic playoffsId;
  String away2;
  String away3;
  String homeOT1;
  String away1;
  String homeOT3;
  String homeOT2;
  String homeTeamCht;
  String homeTeamChs;
  String awayOT3;
  bool hasStats;
  String group;
  String leagueChs;
  int overtimeCount;
  String awayTeamEn;
  String roundTypeChs;
  List<Vids> vids;
  int awayTeamId;
  dynamic cupQualifyId;
  int leagueType;
  int matchState;
  String awayTeamCht;
  String leagueCht;
  String awayTeamChs;
  String home4;
  String home3;
  int homeTeamId;
  String home2;
  String home1;
  String awayOT1;
  String awayOT2;
  String remainTime;
  String roundTypeEn;
  String matchKind;
  String homeTeamEn;

  scoreMatchModel({
    this.weatherEn,
    this.leagueEn,
    this.groupId,
    this.homeName,
    this.roundCn,
    this.leagueChtShort,
    this.subLeagueEn,
    this.awayCorner,
    this.leagueEnShort,
    this.homeRed,
    this.awayLogo,
    this.leagueId,
    this.subLeagueCht,
    this.season,
    this.explainEn,
    this.state,
    this.homeHalfScore,
    this.homeRankEn,
    this.subLeagueId,
    this.homeId,
    this.awayRed,
    this.leagueChsShort,
    this.kind,
    this.subLeagueChs,
    this.matchTime,
    this.homeLogo,
    this.grouping,
    this.homeEn,
    this.realTime,
    this.awayName,
    this.animationUrl,
    this.awayChs,
    this.awayRankEn,
    this.awayCht,
    this.weatherCn,
    this.roundEn,
    this.color,
    this.awayHalfScore,
    this.homeCorner,
    this.extraExplain,
    this.updateTime,
    this.awayScore,
    this.iconType,
    this.locationEn,
    this.explainCn,
    this.homeRankCn,
    this.startTime,
    this.matchId,
    this.temp,
    this.isNeutral,
    this.homeScore,
    this.locationCn,
    this.hasAnimation,
    this.awayId,
    this.awayYellow,
    this.homeYellow,
    this.homeChs,
    this.isHidden,
    this.homeCht,
    this.awayEn,
    this.awayRankCn,
    this.time,
    this.hasLineup,
    this.tv,
    this.away4,
    this.playoffsId,
    this.away2,
    this.away3,
    this.homeOT1,
    this.away1,
    this.homeOT3,
    this.homeOT2,
    this.homeTeamCht,
    this.homeTeamChs,
    this.awayOT3,
    this.hasStats,
    this.group,
    this.leagueChs,
    this.overtimeCount,
    this.awayTeamEn,
    this.roundTypeChs,
    this.vids,
    this.awayTeamId,
    this.cupQualifyId,
    this.leagueType,
    this.matchState,
    this.awayTeamCht,
    this.leagueCht,
    this.awayTeamChs,
    this.home4,
    this.home3,
    this.homeTeamId,
    this.home2,
    this.home1,
    this.awayOT1,
    this.awayOT2,
    this.remainTime,
    this.roundTypeEn,
    this.matchKind,
    this.homeTeamEn,
  });

  scoreMatchModel.fromJson(Map<String, dynamic> json) {
    weatherEn = json['weatherEn'];
    leagueEn = json['leagueEn'];
    groupId = json['groupId'];
    homeName = json['homeName'];
    roundCn = json['roundCn'];
    leagueChtShort = json['leagueChtShort'];
    subLeagueEn = json['subLeagueEn'];
    awayCorner = json['awayCorner'];
    leagueEnShort = json['leagueEnShort'];
    homeRed = json['homeRed'];
    awayLogo = json['awayLogo'];
    leagueId = json['leagueId'];
    subLeagueCht = json['subLeagueCht'];
    season = json['season'];
    explainEn = json['explainEn'];
    state = json['state'];
    homeHalfScore = json['homeHalfScore'];
    homeRankEn = json['homeRankEn'];
    subLeagueId = json['subLeagueId'];
    homeId = json['homeId'];
    awayRed = json['awayRed'];
    leagueChsShort = json['leagueChsShort'];
    kind = json['kind'];
    subLeagueChs = json['subLeagueChs'];
    matchTime = json['matchTime'];
    homeLogo = json['homeLogo'];
    grouping = json['grouping'];
    homeEn = json['homeEn'];
    realTime = json['realTime'];
    awayName = json['awayName'];
    animationUrl = json['animationUrl'];
    awayChs = json['awayChs'];
    awayRankEn = json['awayRankEn'];
    awayCht = json['awayCht'];
    weatherCn = json['weatherCn'];
    roundEn = json['roundEn'];
    color = json['color'];
    awayHalfScore = json['awayHalfScore'];
    homeCorner = json['homeCorner'];
    extraExplain = json['extraExplain'];
    updateTime = json['update_time'];
    awayScore = json['awayScore'];
    iconType = json['iconType'];
    locationEn = json['locationEn'];
    explainCn = json['explainCn'];
    homeRankCn = json['homeRankCn'];
    startTime = json['startTime'];
    matchId = json['matchId'];
    temp = json['temp'];
    isNeutral = json['isNeutral'];
    homeScore = json['homeScore'];
    locationCn = json['locationCn'];
    hasAnimation = json['hasAnimation'];
    awayId = json['awayId'];
    awayYellow = json['awayYellow'];
    homeYellow = json['homeYellow'];
    homeChs = json['homeChs'];
    isHidden = json['isHidden'];
    homeCht = json['homeCht'];
    awayEn = json['awayEn'];
    awayRankCn = json['awayRankCn'];
    time = json['time'];
    hasLineup = json['hasLineup'];

    tv = json['tv'];
    away4 = json['away4'];
    playoffsId = json['playoffsId'];
    away2 = json['away2'];
    away3 = json['away3'];
    homeOT1 = json['homeOT1'];
    away1 = json['away1'];
    homeOT3 = json['homeOT3'];
    homeOT2 = json['homeOT2'];
    homeTeamCht = json['homeTeamCht'];
    homeTeamChs = json['homeTeamChs'];
    awayOT3 = json['awayOT3'];
    hasStats = json['hasStats'];
    group = json['group'];
    leagueChs = json['leagueChs'];
    overtimeCount = json['overtimeCount'];
    awayTeamEn = json['awayTeamEn'];
    roundTypeChs = json['roundTypeChs'];
    if (json['vids'] != null) {
      vids = new List<Vids>();
      json['vids'].forEach((v) {
        vids.add(new Vids.fromJson(v));
      });
    }
    awayTeamId = json['awayTeamId'];
    cupQualifyId = json['cupQualifyId'];
    leagueType = json['leagueType'];
    matchState = json['matchState'];
    awayTeamCht = json['awayTeamCht'];
    leagueCht = json['leagueCht'];
    home4 = json['home4'];
    home3 = json['home3'];
    homeTeamId = json['homeTeamId'];
    home2 = json['home2'];
    home1 = json['home1'];
    awayOT1 = json['awayOT1'];
    awayOT2 = json['awayOT2'];
    remainTime = json['remainTime'];
    roundTypeEn = json['roundTypeEn'];
    matchKind = json['matchKind'];
    homeTeamEn = json['homeTeamEn'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['weatherEn'] = this.weatherEn;
    data['leagueEn'] = this.leagueEn;
    data['groupId'] = this.groupId;
    data['homeName'] = this.homeName;
    data['roundCn'] = this.roundCn;
    data['leagueChtShort'] = this.leagueChtShort;
    data['subLeagueEn'] = this.subLeagueEn;
    data['awayCorner'] = this.awayCorner;
    data['leagueEnShort'] = this.leagueEnShort;
    data['homeRed'] = this.homeRed;
    data['awayLogo'] = this.awayLogo;
    data['leagueId'] = this.leagueId;
    data['subLeagueCht'] = this.subLeagueCht;
    data['season'] = this.season;
    data['explainEn'] = this.explainEn;
    data['state'] = this.state;
    data['homeHalfScore'] = this.homeHalfScore;
    data['homeRankEn'] = this.homeRankEn;
    data['subLeagueId'] = this.subLeagueId;
    data['homeId'] = this.homeId;
    data['awayRed'] = this.awayRed;
    data['leagueChsShort'] = this.leagueChsShort;
    data['kind'] = this.kind;
    data['subLeagueChs'] = this.subLeagueChs;
    data['matchTime'] = this.matchTime;
    data['homeLogo'] = this.homeLogo;
    data['grouping'] = this.grouping;
    data['homeEn'] = this.homeEn;
    data['realTime'] = this.realTime;
    data['awayName'] = this.awayName;
    data['animationUrl'] = this.animationUrl;
    data['awayChs'] = this.awayChs;
    data['awayRankEn'] = this.awayRankEn;
    data['awayCht'] = this.awayCht;
    data['weatherCn'] = this.weatherCn;
    data['roundEn'] = this.roundEn;
    data['color'] = this.color;
    data['awayHalfScore'] = this.awayHalfScore;
    data['homeCorner'] = this.homeCorner;
    data['extraExplain'] = this.extraExplain;
    data['update_time'] = this.updateTime;
    data['awayScore'] = this.awayScore;
    data['iconType'] = this.iconType;
    data['locationEn'] = this.locationEn;
    data['explainCn'] = this.explainCn;
    data['homeRankCn'] = this.homeRankCn;
    data['startTime'] = this.startTime;
    data['matchId'] = this.matchId;
    data['temp'] = this.temp;
    data['isNeutral'] = this.isNeutral;
    data['homeScore'] = this.homeScore;
    data['locationCn'] = this.locationCn;
    data['hasAnimation'] = this.hasAnimation;
    data['awayId'] = this.awayId;
    data['awayYellow'] = this.awayYellow;
    data['homeYellow'] = this.homeYellow;
    data['homeChs'] = this.homeChs;
    data['isHidden'] = this.isHidden;
    data['homeCht'] = this.homeCht;
    data['awayEn'] = this.awayEn;
    data['awayRankCn'] = this.awayRankCn;
    data['time'] = this.time;
    data['hasLineup'] = this.hasLineup;

    data['tv'] = this.tv;

    data['away4'] = this.away4;
    data['playoffsId'] = this.playoffsId;
    data['homeName'] = this.homeName;
    data['away2'] = this.away2;
    data['away3'] = this.away3;
    data['homeOT1'] = this.homeOT1;
    data['away1'] = this.away1;
    data['homeOT3'] = this.homeOT3;
    data['homeOT2'] = this.homeOT2;

    data['homeTeamCht'] = this.homeTeamCht;

    data['homeTeamChs'] = this.homeTeamChs;

    data['awayOT3'] = this.awayOT3;
    data['hasStats'] = this.hasStats;
    data['group'] = this.group;
    data['leagueChs'] = this.leagueChs;

    data['overtimeCount'] = this.overtimeCount;
    data['awayTeamEn'] = this.awayTeamEn;

    data['roundTypeChs'] = this.roundTypeChs;
    if (this.vids != null) {
      data['vids'] = this.vids.map((v) => v.toJson()).toList();
    }

    data['awayTeamId'] = this.awayTeamId;
    data['cupQualifyId'] = this.cupQualifyId;

    data['leagueType'] = this.leagueType;
    data['matchState'] = this.matchState;
    data['awayTeamCht'] = this.awayTeamCht;
    data['leagueCht'] = this.leagueCht;
    data['awayTeamChs'] = this.awayTeamChs;
    data['home4'] = this.home4;
    data['home3'] = this.home3;
    data['homeTeamId'] = this.homeTeamId;
    data['home2'] = this.home2;
    data['home1'] = this.home1;
    data['awayOT1'] = this.awayOT1;
    data['awayOT2'] = this.awayOT2;
    data['remainTime'] = this.remainTime;
    data['roundTypeEn'] = this.roundTypeEn;
    data['matchKind'] = this.matchKind;
    data['homeTeamEn'] = this.homeTeamEn;
    return data;
  }
}

class Vids {
  int matchType;
  String matchId;
  String groupId;
  int play;
  List<PlayUrls> playUrls;
  String matchName;

  Vids({this.matchType, this.matchId, this.groupId, this.play, this.playUrls, this.matchName});

  Vids.fromJson(Map<String, dynamic> json) {
    matchType = json['matchType'];
    matchId = json['matchId'];
    groupId = json['groupId'];
    play = json['play'];
    if (json['playUrls'] != null) {
      playUrls = new List<PlayUrls>();
      json['playUrls'].forEach((v) {
        playUrls.add(new PlayUrls.fromJson(v));
      });
    }
    matchName = json['matchName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['matchType'] = this.matchType;
    data['matchId'] = this.matchId;
    data['groupId'] = this.groupId;
    data['play'] = this.play;
    if (this.playUrls != null) {
      data['playUrls'] = this.playUrls.map((v) => v.toJson()).toList();
    }
    data['matchName'] = this.matchName;
    return data;
  }
}

class PlayUrls {
  String id;
  String type;
  int lang;
  String name;
  dynamic streamImgUrl;
  int isFullPlayUrl;

  PlayUrls({this.id, this.type, this.lang, this.name, this.streamImgUrl, this.isFullPlayUrl});

  PlayUrls.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    lang = json['lang'];
    name = json['name'];
    streamImgUrl = json['streamImgUrl'];
    isFullPlayUrl = json['isFullPlayUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['lang'] = this.lang;
    data['name'] = this.name;
    data['streamImgUrl'] = this.streamImgUrl;
    data['isFullPlayUrl'] = this.isFullPlayUrl;
    return data;
  }
}
