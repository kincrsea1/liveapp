import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/model/outs_page_mod.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../../../../../config/image/local_image_selector.dart';

class forPage extends BaseWidget {
  final String? matchId;
  final int? type;

  forPage({Key? key, required this.matchId, required this.type})
      : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _forPagePageState();
  }
}

class _forPagePageState extends BaseWidgetState<forPage> {
  OutsPageModal? _OutsPageModal;

  bool isStatArr = false;
  bool isHomePlayerScores = false;

  @override
  void initState() {
    _GetOutsPage();
    super.initState();
  }

  CancelToken _cancelToken = CancelToken();

  void _GetOutsPage() async {
    DioUtil.getInstance()?.openLog();
    DioResponse result = await DioUtil().request("/splive/clean/live/technic",
        method: DioMethod.get,
        params: {
          'matchId': widget.matchId,
          'type': widget.type,
        },
        cancelToken: _cancelToken);
    setState(() {
      _OutsPageModal = OutsPageModal.fromJson(result.data);
      if (_OutsPageModal != null &&
          _OutsPageModal!.data != null &&
          _OutsPageModal?.data?.statArr != null &&
          _OutsPageModal!.data!.statArr!.length > 0) {
        _OutsPageModal!.data!.statArr!.forEach((item) {
          if (item.home != null &&
              item.away != null &&
              item.home != 0 &&
              item.away != 0) {
            isStatArr = true;
          }
        });
      }
      if (_OutsPageModal != null &&
          _OutsPageModal!.data != null &&
          _OutsPageModal!.data!.homePlayerScores != null &&
          _OutsPageModal!.data!.homePlayerScores!.length > 0) {
        isHomePlayerScores = true;
      }
    });
    print(result);
  }

  @override
  Widget PageBody(AppState state) {
    return isHomePlayerScores && isStatArr
        ? SingleChildScrollView(
            child: Container(
              color: c_white,
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                Container(
                  margin: EdgeInsets.only(top: 14.w, bottom: 19.w),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 108.w,
                          child: Column(
                            children: [
                              Container(
                                width: 38.w,
                                height: 38.w,
                                alignment: Alignment.center,
                                child: Container(
                                  margin: EdgeInsets.all(7),
                                  child: ClipOval(
                                    child: Image.network(
                                        _OutsPageModal?.data?.homeTeamLogo ??
                                            "",
                                        width: 24.w,
                                        height: 24.w,
                                        errorBuilder: (context, error,
                                                stackTrace) =>
                                            LocalImageSelector.getSingleImage(
                                                "default-head",
                                                imageHeight: 24.w,
                                                imageWidth: 24.w,
                                                bFitFill: BoxFit.fill)),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/onlive/online_rectangle.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 7.w),
                                child: Text(
                                  _OutsPageModal?.data?.homeTeamName ?? "",
                                  style: TextStyle(fontSize: 12.sp),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 9.w),
                          child: Text(
                            'VS',
                            style: TextStyle(
                                color: Color(0xFF666666), fontSize: 16.sp),
                          ),
                        ),
                        Container(
                          width: 108.w,
                          child: Column(
                            children: [
                              Container(
                                width: 38.w,
                                height: 38.w,
                                alignment: Alignment.center,
                                child: Container(
                                  margin: EdgeInsets.all(7),
                                  child: ClipOval(
                                    child: Image.network(
                                        _OutsPageModal?.data?.awayTeamLogo ??
                                            "",
                                        width: 24.w,
                                        height: 24.w,
                                        errorBuilder: (context, error,
                                                stackTrace) =>
                                            LocalImageSelector.getSingleImage(
                                                "default-head",
                                                imageHeight: 24.w,
                                                imageWidth: 24.w,
                                                bFitFill: BoxFit.fill)),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/onlive/online_rectangle.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(top: 7.w),
                                child: Text(
                                  _OutsPageModal?.data?.awayTeamName ?? "",
                                  style: TextStyle(fontSize: 12.sp),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
                Visibility(
                    visible: widget.type == 1 &&
                            _OutsPageModal != null &&
                            _OutsPageModal!.data != null &&
                            _OutsPageModal!.data!.homePlayerScores != null &&
                            _OutsPageModal!.data!.homePlayerScores!.length > 0
                        ? true
                        : false,
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.only(left: 0),
                            width: 4.w,
                            height: 20.w,
                            decoration: ShapeDecoration(
                              color: Color(0xFFF36704),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                            )),
                        Expanded(
                            child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(left: 10.w),
                          child: Text(
                            "球员得分",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 16.sp, color: Color(0xFF666666)),
                          ),
                        ))
                      ],
                    )),
                Visibility(
                    visible: widget.type == 1 &&
                            _OutsPageModal != null &&
                            _OutsPageModal!.data != null &&
                            _OutsPageModal!.data!.homePlayerScores != null &&
                            _OutsPageModal!.data!.homePlayerScores!.length > 0
                        ? true
                        : false,
                    child: Container(
                      margin:
                          EdgeInsets.only(top: 10.w, left: 22.w, right: 22.w),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 1.w,
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Color(0xFFF1E1DA)),
                        ),
                      ),
                    )),
                Visibility(
                    visible: widget.type == 1 &&
                            _OutsPageModal != null &&
                            _OutsPageModal!.data != null &&
                            _OutsPageModal!.data!.homePlayerScores != null &&
                            _OutsPageModal!.data!.homePlayerScores!.length > 0
                        ? true
                        : false,
                    child: Container(
                        margin:
                            EdgeInsets.only(left: 28.w, right: 28.w, top: 13.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "编号",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      color: Color(0xFF999999)),
                                  textAlign: TextAlign.center,
                                  maxLines: 1,
                                  softWrap: true,
                                  overflow: TextOverflow.ellipsis,
                                )),
                            Container(
                                child: Text(
                              "主场球队",
                              style: TextStyle(
                                  fontSize: 12.sp, color: Color(0xFF999999)),
                              textAlign: TextAlign.center,
                            )),
                            Container(
                                child: Text(
                              "得分",
                              style: TextStyle(
                                  fontSize: 12.sp, color: Color(0xFF999999)),
                              textAlign: TextAlign.center,
                            )),
                            Container(
                                child: Text(
                              "客场球队",
                              style: TextStyle(
                                  fontSize: 12.sp, color: Color(0xFF999999)),
                              textAlign: TextAlign.center,
                            )),
                          ],
                        ))),
                Visibility(
                    visible: widget.type == 1 &&
                            _OutsPageModal != null &&
                            _OutsPageModal!.data != null &&
                            _OutsPageModal!.data!.homePlayerScores != null &&
                            _OutsPageModal!.data!.homePlayerScores!.length > 0
                        ? true
                        : false,
                    child: Container(
                      margin:
                          EdgeInsets.only(left: 28.w, right: 28.w, top: 9.w),
                      child: ImportantEventsList(data: _OutsPageModal?.data),
                    )),
                Visibility(
                    visible: widget.type == 1 &&
                            _OutsPageModal != null &&
                            _OutsPageModal!.data != null &&
                            _OutsPageModal!.data!.homePlayerScores != null &&
                            _OutsPageModal!.data!.homePlayerScores!.length > 0
                        ? true
                        : false,
                    child: Container(
                      margin: EdgeInsets.only(
                          bottom: 14.w, left: 22.w, right: 22.w),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 1.w,
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Color(0xFFF1E1DA)),
                        ),
                      ),
                    )),
                Row(
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 0),
                        width: 4.w,
                        height: 20.w,
                        decoration: ShapeDecoration(
                          color: Color(0xFFF36704),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10))),
                        )),
                    Expanded(
                        child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 10.w),
                      child: Text(
                        "技术统计",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16.sp, color: Color(0xFF666666)),
                      ),
                    ))
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.w, left: 22.w, right: 22.w),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 1.w,
                    child: DecoratedBox(
                      decoration: BoxDecoration(color: Color(0x268E95BB)),
                    ),
                  ),
                ),
                Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                          child: Container(
                        alignment: Alignment.centerRight,
                        child: ImportantEventsLeftList(
                            data: _OutsPageModal?.data?.statArr),
                      )),
                      Container(
                        width: 100.w,
                        alignment: Alignment.center,
                        child: ImportantEventsCenterList(
                            data: _OutsPageModal?.data?.statArr),
                      ),
                      Expanded(
                          child: Container(
                        alignment: Alignment.centerLeft,
                        child: ImportantEventsRightList(
                            data: _OutsPageModal?.data?.statArr),
                      )),
                    ]),
              ]),
            ),
          )
        : EmptyContainer(txt: '暂无数据');
  }
}

class ImportantEventsList extends StatelessWidget {
  final Data? data;

  ImportantEventsList({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        ListView.separated(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: data != null && data!.homePlayerScores != null
              ? data!.homePlayerScores!.length
              : 0,
          separatorBuilder: (BuildContext context, int index) =>
              Divider(height: 1.w, color: Color(0xFFF1E1DA)),
          itemBuilder: (BuildContext context, int position) {
            return _cellForRow(context, position);
          },
        ),
      ],
    ));
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      height: 42.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            data != null &&
                    data!.homePlayerScores != null &&
                    data!.homePlayerScores!.length > index &&
                    data!.homePlayerScores![index].playerNumber != null
                ? data!.homePlayerScores![index].playerNumber.toString()
                : "#0",
            style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
            textAlign: TextAlign.center,
            maxLines: 1,
          ),
          Container(
            alignment: Alignment.centerLeft,
              width: 80.w,
              child: Text(
                data != null &&
                        data!.homePlayerScores != null &&
                        data!.homePlayerScores!.length > index &&
                        data!.homePlayerScores![index].playerName != null
                    ? data!.homePlayerScores![index].playerName.toString()
                    : "",
                style: TextStyle(
                    fontSize: 12.sp,
                    color: Color(0xFF666666),
                    overflow: TextOverflow.ellipsis),
                maxLines: 1,
                textAlign: TextAlign.start,
              )),
          Row(
            children: [
              Container(
                  child: Text(
                data != null &&
                        data!.homePlayerScores != null &&
                        data!.homePlayerScores!.length > index &&
                        data!.homePlayerScores![index].playerScore != null
                    ? "${data!.homePlayerScores![index].playerScore ?? "0"}|"
                    : "0|",
                style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
                textAlign: TextAlign.start,
              )),
              Container(
                  child: Text(
                data != null &&
                        data!.awayPlayerScores != null &&
                        data!.awayPlayerScores!.length > index &&
                        data!.awayPlayerScores![index].playerScore != null
                    ? data!.homePlayerScores![index].playerScore.toString()
                    : "0",
                style: TextStyle(fontSize: 12.sp, color: Color(0xFFF36704)),
                textAlign: TextAlign.start,
              )),
            ],
          ),
          Container(
              width: 80.w,
              child: Text(
                data != null &&
                        data!.awayPlayerScores != null &&
                        data!.awayPlayerScores!.length > index &&
                        data!.awayPlayerScores![index].playerName != null
                    ? data!.awayPlayerScores![index].playerName.toString()
                    : "",
                style: TextStyle(
                    fontSize: 12.sp,
                    color: Color(0xFF666666),
                    overflow: TextOverflow.ellipsis),
                maxLines: 1,
                textAlign: TextAlign.end,
              )),
        ],
      ),
    );
  }
}

class ImportantEventsRightList extends StatelessWidget {
  final List<GrowableList>? data;

  ImportantEventsRightList({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: data != null ? data!.length : 0,
        itemBuilder: _cellForRow);
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      height: 40.w,
      alignment: Alignment.topLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              margin: EdgeInsets.only(top: 6.w),
              child: LinearPercentIndicator(
                width: 98.w,
                lineHeight: 8.w,
                percent: data != null &&
                        data!.length > 0 &&
                        data![index].home != null &&
                        data![index].away != null &&
                        data![index].away! != 0
                    ? data![index].away! /
                        (data![index].home! + data![index].away!)
                    : 0,
                isRTL: false,
                progressColor: Color(0xFFF36704),
                backgroundColor: Color(0xFFFFFAF7),
              )),
          Expanded(
              child: Container(
                  margin: EdgeInsets.only(right: 24.w, top: 4.w),
                  child: Text(
                    data != null && data!.length > 0
                        ? data![index].away.toString()
                        : "",
                    style: TextStyle(
                      fontSize: 12.sp,
                    ),
                    textAlign: TextAlign.end,
                  ))),
        ],
      ),
    );
  }
}

class ImportantEventsLeftList extends StatelessWidget {
  final List<GrowableList>? data;

  ImportantEventsLeftList({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: data != null ? data!.length : 0,
        itemBuilder: _cellForRow);
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      height: 40.w,
      alignment: Alignment.topRight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: Container(
                  padding: EdgeInsets.only(left: 20.w, right: 0.w),
                  child: Text(
                    data != null && data!.length > 0
                        ? data![index].home.toString()
                        : "",
                    style: TextStyle(
                      fontSize: 12.sp,
                    ),
                    maxLines: 1,
                  ))),
          Container(
              margin: EdgeInsets.only(top: 5.w),
              child: LinearPercentIndicator(
                width: 98.w,
                lineHeight: 8.w,
                percent: data != null &&
                        data!.length > 0 &&
                        data![index].home != null &&
                        data![index].away != null &&
                        data![index].home! != 0
                    ? data![index].home! /
                        (data![index].home! + data![index].away!)
                    : 0,
                isRTL: true,
                progressColor: Color(0xFF6792D3),
                backgroundColor: Color(0xFFFFFAF7),
              ))
        ],
      ),
    );
  }
}

class ImportantEventsCenterList extends StatelessWidget {
  final List<GrowableList>? data;

  ImportantEventsCenterList({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: data != null ? data!.length : 0,
        itemBuilder: _cellForRow);
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      height: 40.w,
      alignment: Alignment.center,
      child: Container(
        height: 40.w,
        margin: EdgeInsets.only(left: 10.w, right: 10.w),
        child: Text(
          data != null && data!.length > 0 ? data![index].title.toString() : "",
          style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
          // textAlign: TextAlign.center,
          maxLines: 1,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
