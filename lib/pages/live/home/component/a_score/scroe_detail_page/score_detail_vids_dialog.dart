
// ignore: file_names
// @dart=2.9

// ignore_for_file: file_names, prefer_typing_uninitialized_variables, prefer_const_constructors, must_be_immutable, use_key_in_widget_constructors, avoid_unnecessary_containers, unnecessary_new



import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import 'mo/score_home_model.dart';
class SelectVidsDialog  extends BaseWidget {
  List<Vids> vidList = [];
  final confirmCallback;


  SelectVidsDialog(this.confirmCallback,this.vidList);


  @override
  BaseWidgetState<BaseWidget> getState() => _SelectVidsDialogState();
}

class _SelectVidsDialogState extends BaseWidgetState<SelectVidsDialog> {
  @override
  Widget PageBody(AppState state){
  return MediaQuery.removePadding(
      removeTop: true,
      removeBottom: true,
      context: context,
      child:  SimpleDialog(
        titlePadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      title: Container(
        width: MediaQuery.of(context).size.width - 50,
        child:  Container(
          alignment: Alignment.center,
          width:double.infinity ,
          height: 45.w,
          child: Text('请选择直播源线路',
              textAlign: TextAlign.center,
              style:  TextStyle(
                color: c_bl,
                fontSize: 14.sp,
              )
          ),
        ),
      ),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.w)),
      children: [
        Container(
          child: Column(
            children: _selectVidWidget(),
          ),
        ),
        Container(
            child:Column(
              children: [
                Container(
                  width: double.infinity,
                  height: 1.w,
                  color: c_b1,
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      height: 45.w,
                      child:Text('取消',
                          style:
                          TextStyle(color: c_9, fontSize: 15.sm))
                  ),
                )
              ],
            )
        )
      ],
    ));
  }

 List<Widget> _selectVidWidget() {
    
    return widget.vidList.map((Vids e) {
      if( e.playUrls.length == 0){
        return Container();
      }
      return _vidWidget(e);
    }).toList();
 }

 Widget _vidWidget(Vids e) {
    return  Container(

      height: 45.w,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 1.w,
            color: c_b1,
          ),
          Expanded(child: FlatButton(
            padding: EdgeInsets.symmetric(horizontal: 0),
            ///点击足球cell
            onPressed: () {
              widget.confirmCallback(e);
              Navigator.pop(context);
            },
            child: Container(
                width: double.infinity,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(  e.playUrls.length == 0? '': e.playUrls.first.name ?? '',
                 style: TextStyle(
                   color:c_bl ,
                   fontSize: 14.sm,
                 ),
                  ),
                )
            ),
          ))
        ],
      ),
    );
  }
}