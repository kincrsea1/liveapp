import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/model/analy_page_mod.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import '../../../../../../config/image/local_image_selector.dart';

class analysisPage extends BaseWidget {
  final String? matchId;
  final int? type;

  analysisPage({Key? key, required this.matchId, required this.type})
      : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _analysisPagePageState();
  }
}

class _analysisPagePageState extends BaseWidgetState<analysisPage> {
  AnalyPageModal? _AnalyPageModal;

  int isSelected = 0;

  @override
  void initState() {
    _GetOutsPage();
    super.initState();
  }

  CancelToken _cancelToken = CancelToken();

  void _GetOutsPage() async {
    DioUtil.getInstance()?.openLog();
    DioResponse result = await DioUtil().request("/splive/clean/live/analy",
        method: DioMethod.get,
        params: {
          'matchId': widget.matchId,
          'type': widget.type,
        },
        cancelToken: _cancelToken);
    setState(() {
      _AnalyPageModal = AnalyPageModal.fromJson(result.data);
    });
    print(result);
  }

  @override
  Widget PageBody(AppState state) {
    return _AnalyPageModal != null &&
            _AnalyPageModal!.data != null &&
            _AnalyPageModal!.data!.homeHistoryMatches != null &&
            _AnalyPageModal!.data!.awayHistoryMatches != null &&
            _AnalyPageModal!.data!.homeHistoryMatches!.length > 0 &&
            _AnalyPageModal!.data!.awayHistoryMatches!.length > 0
        ? SingleChildScrollView(
            child: Container(
            color: c_white,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              Container(
                margin: EdgeInsets.only(top: 14.w),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        child: Container(
                          width: 108.w,
                          child: Column(
                            children: [
                              Container(
                                width: 38.w,
                                height: 38.w,
                                alignment: Alignment.center,
                                child: Container(
                                  margin: EdgeInsets.all(7),
                                  child: ClipOval(
                                    child: Image.network(
                                        _AnalyPageModal?.data?.homeLogo ?? "",
                                        width: 24.w,
                                        height: 24.w,
                                        errorBuilder: (context, error,
                                                stackTrace) =>
                                            LocalImageSelector.getSingleImage(
                                                "default-head",
                                                imageHeight: 24.w,
                                                imageWidth: 24.w,
                                                bFitFill: BoxFit.fill)),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/onlive/online_rectangle.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 7.w),
                                child: Text(
                                  _AnalyPageModal?.data?.homeName ?? "",
                                  style: TextStyle(fontSize: 14.sp),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                        onTap: () => setState(() {
                          isSelected = 0;
                        }),
                      ),
                      (isSelected == 0
                              ? _AnalyPageModal != null &&
                                  _AnalyPageModal!.data != null &&
                                  _AnalyPageModal!
                                          .data!.homeWinningPercentage !=
                                      null &&
                                  _AnalyPageModal!
                                      .data!.homeWinningPercentage!.isNotEmpty
                              : _AnalyPageModal != null &&
                                  _AnalyPageModal?.data != null &&
                                  _AnalyPageModal!
                                          .data!.awayWinningPercentage !=
                                      null &&
                                  _AnalyPageModal!
                                      .data!.awayWinningPercentage!.isNotEmpty)
                          ? Container(
                              height: 20.w,
                              margin: EdgeInsets.only(top: 5.w),
                              child: Row(
                                children: [
                                  Text(
                                    '胜率',
                                    style:
                                        TextStyle(color: c_6, fontSize: 16.sp),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 3.w),
                                    alignment: Alignment.center,
                                    child: Text(
                                        isSelected == 0
                                            ? _AnalyPageModal?.data
                                                    ?.homeWinningPercentage ??
                                                ""
                                            : _AnalyPageModal?.data
                                                    ?.awayWinningPercentage ??
                                                "",
                                        style: TextStyle(
                                            color: Color(0xFF00CCCC),
                                            fontSize: 16.sp)),
                                  )
                                ],
                              ),
                            )
                          : Container(
                              height: 20.w,
                              margin: EdgeInsets.only(top: 5.w),
                              child: Text(
                                isSelected == 0
                                    ? _AnalyPageModal?.data?.homeResult ?? ""
                                    : _AnalyPageModal?.data?.awayResult ?? "",
                                style: TextStyle(color: c_6, fontSize: 16.sp),
                              )),
                      GestureDetector(
                        child: Container(
                          width: 108.w,
                          child: Column(
                            children: [
                              Container(
                                width: 38.w,
                                height: 38.w,
                                alignment: Alignment.center,
                                child: Container(
                                  margin: EdgeInsets.all(7),
                                  child: ClipOval(
                                    child: Image.network(
                                        _AnalyPageModal?.data?.awayLogo ?? "",
                                        width: 24.w,
                                        height: 24.w,
                                        errorBuilder: (context, error,
                                                stackTrace) =>
                                            LocalImageSelector.getSingleImage(
                                                "default-head",
                                                imageHeight: 24.w,
                                                imageWidth: 24.w,
                                                bFitFill: BoxFit.fill)),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/onlive/online_rectangle.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(top: 7.w),
                                child: Text(
                                  _AnalyPageModal?.data?.awayName ?? "",
                                  style: TextStyle(fontSize: 14.sp),
                                ),
                              ),
                            ],
                          ),
                        ),
                        onTap: () => setState(() {
                          isSelected = 1;
                        }),
                      ),
                    ]),
              ),
              Container(
                  margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 20.w),
                  child: Row(
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width / 5 - 8.w,
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "时间",
                            style: TextStyle(
                                fontSize: 12.sp, color: c_9),
                            textAlign: TextAlign.start,
                            maxLines: 1,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width / 5 - 8.w,
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "主场球队",
                            style: TextStyle(
                                fontSize: 12.sp, color: c_9),
                            textAlign: TextAlign.start,
                          )),
                      Container(
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width / 5 - 8.w,
                          child: Text(
                            "比分",
                            style: TextStyle(
                                fontSize: 12.sp, color: c_9),
                            textAlign: TextAlign.start,
                          )),
                      Container(
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width / 5 - 8.w,
                          child: Text(
                            "客场球队",
                            style: TextStyle(
                                fontSize: 12.sp, color: c_9),
                            textAlign: TextAlign.start,
                          )),
                      Container(
                          alignment: Alignment.centerRight,
                          width: MediaQuery.of(context).size.width / 5 - 8.w,
                          child: Text(
                            "赛果",
                            style: TextStyle(
                                fontSize: 12.sp, color: c_9),
                            textAlign: TextAlign.start,
                          )),
                    ],
                  )),
              Container(
                margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 10.w),
                child: ImportantEventsList(),
              )
            ]),
          ))
        : EmptyContainer(txt: '暂无数据');
  }

  ImportantEventsList() {
    return (isSelected == 0
            ? _AnalyPageModal != null &&
                _AnalyPageModal!.data != null &&
                _AnalyPageModal!.data!.homeHistoryMatches != null &&
                _AnalyPageModal!.data!.homeHistoryMatches!.length > 0
            : _AnalyPageModal != null &&
                _AnalyPageModal!.data != null &&
                _AnalyPageModal!.data!.awayHistoryMatches != null &&
                _AnalyPageModal!.data!.awayHistoryMatches!.length > 0)
        ? MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.separated(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: isSelected == 0
                  ? _AnalyPageModal != null &&
                          _AnalyPageModal!.data != null &&
                          _AnalyPageModal!.data!.homeHistoryMatches != null
                      ? _AnalyPageModal!.data!.homeHistoryMatches!.length
                      : 0
                  : _AnalyPageModal != null &&
                          _AnalyPageModal!.data != null &&
                          _AnalyPageModal!.data!.awayHistoryMatches != null
                      ? _AnalyPageModal!.data!.awayHistoryMatches!.length
                      : 0,
              separatorBuilder: (BuildContext context, int index) =>
                  Divider(height: 1.w, color: c_b1),
              itemBuilder: (BuildContext context, int position) {
                return _cellForRow(
                    context,
                    position,
                    isSelected == 0
                        ? _AnalyPageModal!.data!.homeHistoryMatches
                        : _AnalyPageModal!.data!.awayHistoryMatches,
                    isSelected == 0
                        ? _AnalyPageModal!.data!.homeName
                        : _AnalyPageModal!.data!.awayName);
              },
            ))
        : Container();
  }

  Widget _cellForRow(BuildContext context, int index,
      List<HomeHistoryMatches>? HistoryMatches, var name) {
    return Container(
      height: 42.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
              alignment: Alignment.centerLeft,
              width: MediaQuery.of(context).size.width / 5 - 8.w,
              child: Text(
                HistoryMatches?[index].matchDate ?? "",
                style: TextStyle(fontSize: 12.sp, color: c_6),
                maxLines: 1,
              )),
          Container(
              // margin: EdgeInsets.only(left: 11.w,right: 17.w),
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 5 - 8.w,
              child: Text(
                HistoryMatches?[index].homeName ?? "",
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 12.sp, color: c_6),
                textAlign: TextAlign.center,
              )),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width / 5 - 8.w,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    alignment: Alignment.center,
                    child: Text(
                      HistoryMatches?[index].homeScore.toString() ?? "0",
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: _GetTextColor(index, HistoryMatches, name)),
                      textAlign: TextAlign.center,
                    )),
                Container(
                    alignment: Alignment.center,
                    child: Text(
                      "|",
                      style: TextStyle(fontSize: 12.sp, color: c_6),
                      textAlign: TextAlign.center,
                    )),
                Container(
                    alignment: Alignment.center,
                    child: Text(
                      HistoryMatches?[index].awayScore.toString() ?? "0",
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: _GetTextColor1(index, HistoryMatches, name)),
                      textAlign: TextAlign.center,
                    )),
              ],
            ),
          ),
          Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 5 - 8.w,
              child: Text(
                HistoryMatches != null && HistoryMatches.length > index
                    ? HistoryMatches[index].awayName ?? ""
                    : "",
                style: TextStyle(fontSize: 12.sp, color: c_6),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              )),
          Container(
              alignment: Alignment.centerRight,
              width: MediaQuery.of(context).size.width / 5 - 8.w,
              child: Text(
                HistoryMatches != null && HistoryMatches.length > index
                    ? HistoryMatches[index].result ?? ""
                    : "",
                style: TextStyle(fontSize: 12.sp, color: c_6),
                textAlign: TextAlign.end,
              )),
        ],
      ),
    );
  }

  Color _GetTextColor(
      int index, List<HomeHistoryMatches>? homeHistoryMatches, name) {
    if (name != null && homeHistoryMatches != null) {
      if (homeHistoryMatches[index].homeName! == name) {
        return c_o;
      } else {
        return c_6;
      }
    }
    return c_6;
  }

  Color _GetTextColor1(
      int index, List<HomeHistoryMatches>? homeHistoryMatches, name) {
    if (name != null && homeHistoryMatches != null) {
      if (homeHistoryMatches[index].awayName! == name) {
        return c_o;
      } else {
        return c_6;
      }
    }
    return c_6;
  }
}
