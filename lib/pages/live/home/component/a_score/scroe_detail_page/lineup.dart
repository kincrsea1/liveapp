import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/empty_container.dart';
import 'package:liveapp/model/lineup_page_mod.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import '../../../../../../config/image/local_image_selector.dart';

class lineupPage extends BaseWidget {
  final String? matchId;
  final int? type;
  dynamic scoreModel;

  lineupPage({
    Key? key,
    required this.scoreModel,
    required this.matchId,
    required this.type,
  }) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _lineupPagePageState();
  }
}

class _lineupPagePageState extends BaseWidgetState<lineupPage> {

  LineupPageModal? _LineupPageModal;
  List<int> mAdList = [];
  List<int> mAdListAway = [];

  List<List<GrowableList>>? positionALL = [];
  List<GrowableList>? position1 = [];
  List<GrowableList>? position2 = [];
  List<GrowableList>? position3 = [];
  List<GrowableList>? position4 = [];
  List<GrowableList>? position5 = [];
  List<GrowableList>? position6 = [];
  List<GrowableList>? position7 = [];

  List<List<GrowableList>>? positionALLAway = [];
  List<GrowableList>? positionAway1 = [];
  List<GrowableList>? positionAway2 = [];
  List<GrowableList>? positionAway3 = [];
  List<GrowableList>? positionAway4 = [];
  List<GrowableList>? positionAway5 = [];
  List<GrowableList>? positionAway6 = [];
  List<GrowableList>? positionAway7 = [];

  @override
  void initState() {
    _GetOutsPage();
    super.initState();
  }

  CancelToken _cancelToken = CancelToken();

  void _GetOutsPage() async {
    DioUtil.getInstance()?.openLog();
    DioResponse result = await DioUtil().request("/splive/clean/live/lineup",
        method: DioMethod.get,
        params: {
          'matchId': widget.matchId,
          'type': widget.type,
        },
        cancelToken: _cancelToken);
    if (mounted)
      setState(() {
        _LineupPageModal = LineupPageModal.fromJson(result.data);
        if (widget.type == 0) {
          _LineupPageModal?.data?.home?.lineup?.forEach((item) {
            if (item.position == "守门员") {
              position1?.add(item);
            } else if (item.position == "后卫") {
              position2?.add(item);
            } else if (item.position == "后腰") {
              position3?.add(item);
            } else if (item.position == "中场") {
              position4?.add(item);
            } else if (item.position == "前腰") {
              position5?.add(item);
            } else if (item.position == "中锋") {
              position6?.add(item);
            } else if (item.position == "前锋") {
              position7?.add(item);
            }
          });
          if (position2 != null && position2!.length > 0) {
            mAdList.add(position2!.length);
            positionALL?.add(position2!);
          }
          if (position3 != null && position3!.length > 0) {
            mAdList.add(position3!.length);
            positionALL?.add(position3!);
          }
          if (position4 != null && position4!.length > 0) {
            mAdList.add(position4!.length);
            positionALL?.add(position4!);
          }
          if (position5 != null && position5!.length > 0) {
            mAdList.add(position5!.length);
            positionALL?.add(position5!);
          }
          if (position6 != null && position6!.length > 0) {
            mAdList.add(position6!.length);
            positionALL?.add(position6!);
          }
          if (position7 != null && position7!.length > 0) {
            mAdList.add(position7!.length);
            positionALL?.add(position7!);
          }
        }

        _LineupPageModal?.data?.away?.lineup?.forEach((item) {
          if (item.position == "守门员") {
            positionAway1?.add(item);
          } else if (item.position == "后卫") {
            positionAway2?.add(item);
          } else if (item.position == "后腰") {
            positionAway3?.add(item);
          } else if (item.position == "中场") {
            positionAway4?.add(item);
          } else if (item.position == "前腰") {
            positionAway5?.add(item);
          } else if (item.position == "中锋") {
            positionAway6?.add(item);
          } else if (item.position == "前锋") {
            positionAway7?.add(item);
          }
        });
        if (positionAway7 != null && positionAway7!.length > 0) {
          mAdListAway.add(positionAway7!.length);
          positionALLAway?.add(positionAway7!);
        }
        if (positionAway6 != null && positionAway6!.length > 0) {
          mAdListAway.add(positionAway6!.length);
          positionALLAway?.add(positionAway6!);
        }
        if (positionAway5 != null && positionAway5!.length > 0) {
          mAdListAway.add(positionAway5!.length);
          positionALLAway?.add(positionAway5!);
        }
        if (positionAway4 != null && positionAway4!.length > 0) {
          mAdListAway.add(positionAway4!.length);
          positionALLAway?.add(positionAway4!);
        }
        if (positionAway3 != null && positionAway3!.length > 0) {
          mAdListAway.add(positionAway3!.length);
          positionALLAway?.add(positionAway3!);
        }
        if (positionAway2 != null && positionAway2!.length > 0) {
          mAdListAway.add(positionAway2!.length);
          positionALLAway?.add(positionAway2!);
        }
        if (positionAway1 != null && positionAway1!.length > 0) {
          mAdListAway.add(positionAway1!.length);
          positionALLAway?.add(positionAway1!);
        }

        _tabs.add(_LineupPageModal?.data?.home?.homeName ?? "");
        _tabs.add(_LineupPageModal?.data?.away?.awayName ?? "");
      });
    print(result);
  }

  List _tabs = [];
  int isSelected = 0;

  @override
  Widget PageBody(AppState state) {
    return _LineupPageModal != null &&
            _LineupPageModal?.data != null &&
            _LineupPageModal?.data?.away != null &&
            _LineupPageModal?.data?.home != null &&
            _LineupPageModal?.data?.away?.lineup != null &&
            _LineupPageModal?.data?.home?.lineup != null
        ? SingleChildScrollView(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              Visibility(
                  visible: widget.type == 0,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 14.w),
                              width: 38.w,
                              height: 38.w,
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin: EdgeInsets.all(7),
                                child: ClipOval(
                                  child: Image.network(
                                    widget.scoreModel != null &&
                                            widget.scoreModel.homeLogo != null
                                        ? widget.scoreModel.homeLogo
                                        : "",
                                    width: 24.w,
                                    height: 24.w,
                                  ),
                                ),
                              ),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/onlive/online_rectangle.png'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w, top: 7.w),
                              child: Text(
                                _LineupPageModal?.data?.home?.homeName ?? "",
                                style: TextStyle(fontSize: 14.sp),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Text(''),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(right: 10.w, top: 7.w),
                              child: Text(
                                _LineupPageModal?.data?.away?.awayName ?? "",
                                style: TextStyle(fontSize: 14.sp),
                                textAlign: TextAlign.end,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(right: 14.w),
                              width: 38.w,
                              height: 38.w,
                              alignment: Alignment.centerRight,
                              child: Container(
                                margin: EdgeInsets.all(7),
                                child: ClipOval(
                                  child: Image.network(
                                    widget.scoreModel != null &&
                                            widget.scoreModel.awayLogo != null
                                        ? widget.scoreModel.awayLogo
                                        : "",
                                    width: 24.w,
                                    height: 24.w,
                                  ),
                                ),
                              ),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/onlive/online_rectangle.png'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ])),
              Visibility(
                  visible: widget.type == 0 && positionALL!.isNotEmpty,
                  child: Container(
                      margin: EdgeInsets.only(top: 10.w),
                      color: c_white,
                      width: MediaQuery.of(context).size.width,
                      // height: 700,
                      child: Container(
                          margin: EdgeInsets.only(left: 14.w, right: 14.w),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 10.w),
                                width: MediaQuery.of(context).size.width,
                                // height: 600.w,
                                alignment: Alignment.center,
                                child: Column(
                                  children: [
                                    Container(
                                        margin: EdgeInsets.only(top: 10.w),
                                        width: 70.w,
                                        child: Column(children: [
                                          Container(
                                            width: 28.w,
                                            height: 28.w,
                                            alignment: Alignment.center,
                                            child: Container(
                                              width: 24.w,
                                              height: 24.w,
                                              alignment: Alignment.center,
                                              child: Text(
                                                position1!.length > 0
                                                    ? position1![0].number ?? ""
                                                    : "",
                                                style: TextStyle(
                                                    color: c_white,
                                                    fontSize: 14.sp),
                                              ),
                                              decoration: ShapeDecoration(
                                                  color: Color(0xFFD55F5F),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadiusDirectional
                                                              .circular(20))),
                                            ),
                                            decoration: ShapeDecoration(
                                                color: c_white,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadiusDirectional
                                                            .circular(20))),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(
                                                  right: 5.w, top: 2.w),
                                              child: Text(
                                                position1!.length > 0
                                                    ? position1![0].name ?? ""
                                                    : "",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: TextStyle(
                                                    color: c_white,
                                                    fontSize: 10.sp),
                                              )),
                                        ])),
                                    ColumnList()
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/onlive/gree_bg_1.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.center,
                                child: BColumnList(),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/onlive/gree_bg_2.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 14.w),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 24.w),
                                      width: 24.w,
                                      height: 24.w,
                                      alignment: Alignment.center,
                                      child: Image.network(
                                          widget.scoreModel != null &&
                                                  widget.scoreModel.homeLogo !=
                                                      null
                                              ? widget.scoreModel.homeLogo
                                              : "",
                                          width: 24.w,
                                          height: 24.w,
                                          errorBuilder: (context, error,
                                                  stackTrace) =>
                                              LocalImageSelector.getSingleImage(
                                                  "default-head",
                                                  imageHeight: 24.w,
                                                  imageWidth: 24.w,
                                                  bFitFill: BoxFit.fill)),
                                    ),
                                    Text(
                                      "替补阵容",
                                      style: TextStyle(
                                          color: Color(0xFF666666),
                                          fontSize: 14.sp),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(right: 24.w),
                                      width: 24.w,
                                      height: 24.w,
                                      alignment: Alignment.center,
                                      child: Image.network(
                                          widget.scoreModel != null &&
                                                  widget.scoreModel.awayLogo !=
                                                      null
                                              ? widget.scoreModel.awayLogo
                                              : "",
                                          width: 24.w,
                                          height: 24.w,
                                          errorBuilder: (context, error,
                                                  stackTrace) =>
                                              LocalImageSelector.getSingleImage(
                                                  "default-head",
                                                  imageHeight: 24.w,
                                                  imageWidth: 24.w,
                                                  bFitFill: BoxFit.fill)),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 14.w),
                                  child: SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    height: 1.w,
                                    child: DecoratedBox(
                                      decoration: BoxDecoration(
                                          color: Color(0xFFF1E1DA)),
                                    ),
                                  )),
                            ],
                          )))),
              Visibility(
                  visible: widget.type == 0 &&
                      _LineupPageModal?.data?.home?.backup != null &&
                      _LineupPageModal?.data?.away?.backup != null,
                  child: Container(
                      padding: EdgeInsets.only(top: 10.w),
                      color: c_white,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 14.w, right: 14.w, bottom: 14.w),
                                  child: Container(
                                      alignment: Alignment.centerLeft,
                                      decoration: BoxDecoration(
                                        color: c_white,
                                        border: Border.all(
                                            width: 1.w,
                                            color: Color(0xFFF1E1DA)),
                                      ),
                                      child: Container(
                                          margin: EdgeInsets.only(bottom: 10.w),
                                          child: benchLeftList(
                                              backup: _LineupPageModal
                                                  ?.data?.home?.backup))))),
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(
                                      left: 14.w, right: 14.w, bottom: 14.w),
                                  child: Container(
                                      padding: EdgeInsets.only(left: 20),
                                      alignment: Alignment.centerRight,
                                      decoration: BoxDecoration(
                                        color: c_white,
                                        border: Border.all(
                                            width: 1.w,
                                            color: Color(0xFFF1E1DA)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 10.w),
                                        child: benchRightList(
                                            backup: _LineupPageModal
                                                ?.data?.away?.backup),
                                      )))),
                        ],
                      ))),
              Visibility(
                  visible: widget.type == 1,
                  child: Container(
                      margin: EdgeInsets.only(top: 15.w, bottom: 15.w),
                      height: 44.w,
                      width: 192.w,
                      decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        borderRadius: BorderRadius.all(Radius.circular(40.0)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                              decoration: BoxDecoration(
                                color: isSelected == 0
                                    ? Color(0xFFF36704)
                                    : Color(0xFFFFFFFF),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(40.0)),
                              ),
                              height: 36.w,
                              width: 90.w,
                              alignment: Alignment.center,
                              child: Text(_tabs.length > 0 ? _tabs[0] : "",
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      color: isSelected == 0
                                          ? Color(0xFFFFFFFF)
                                          : Color(0xFF666666),
                                      overflow: TextOverflow.ellipsis)),
                            ),
                            onTap: () => setState(() {
                              isSelected = 0;
                              // _loadData();
                              // _RequestBottomListData(0);
                            }),
                          ),
                          GestureDetector(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(40.0)),
                                color: isSelected == 1
                                    ? Color(0xFFF36704)
                                    : Color(0xFFFFFFFF),
                              ),
                              height: 36.w,
                              width: 90.w,
                              alignment: Alignment.center,
                              child: Text(_tabs.length > 0 ? _tabs[1] : "",
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      color: isSelected == 1
                                          ? Color(0xFFFFFFFF)
                                          : Color(0xFF666666),
                                      overflow: TextOverflow.ellipsis)),
                            ),
                            onTap: () => setState(() {
                              isSelected = 1;
                              // _loadData();
                              // _RequestBottomListData(3);
                            }),
                          ),
                        ],
                      ))),
              Visibility(
                  visible: isSelected == 0 && widget.type == 1,
                  child: Container(
                    color: Color(0xFFFFFFFF),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(top: 15.w),
                                width: 4.w,
                                height: 20.w,
                                decoration: ShapeDecoration(
                                  color: Color(0xFFF36704),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10),
                                          bottomRight: Radius.circular(10))),
                                )),
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(left: 14.w, top: 15.w),
                              child: Text(
                                "首发阵容",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.sp, color: Color(0xFF666666)),
                              ),
                            )
                          ],
                        ),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.home != null &&
                                    _LineupPageModal!.data!.home!.lineup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.home!.lineup!.length >
                                        0
                                ? false
                                : true,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 30.w),
                              child: EmptyContainer(txt: '暂无数据'),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.home != null &&
                                    _LineupPageModal!.data!.home!.lineup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.home!.lineup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(
                                  left: 21.w, right: 22.w, top: 12.w),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: 1.w,
                                child: DecoratedBox(
                                  decoration:
                                      BoxDecoration(color: Color(0xFFF1E1DA)),
                                ),
                              ),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.home != null &&
                                    _LineupPageModal!.data!.home!.lineup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.home!.lineup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                                margin: EdgeInsets.only(
                                    left: 33.w, right: 36.w, top: 14.w),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "球员",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF999999)),
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          softWrap: true,
                                          overflow: TextOverflow.ellipsis,
                                        )),
                                    Container(
                                        child: Text(
                                      "球衣",
                                      style: TextStyle(
                                          fontSize: 12.sp,
                                          color: Color(0xFF999999)),
                                      textAlign: TextAlign.center,
                                    )),
                                    Container(
                                        child: Text(
                                      "位置",
                                      style: TextStyle(
                                          fontSize: 12.sp,
                                          color: Color(0xFF999999)),
                                      textAlign: TextAlign.center,
                                    )),
                                  ],
                                ))),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.home != null &&
                                    _LineupPageModal!.data!.home!.lineup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.home!.lineup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 14.w),
                              child: TextLiveBroadcastList(
                                  backup: _LineupPageModal?.data?.home?.lineup),
                            )),
                        Container(
                          height: 10.w,
                          color: c_w,
                        ),
                        Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(top: 15.w),
                                width: 4.w,
                                height: 20.w,
                                decoration: ShapeDecoration(
                                  color: Color(0xFFF36704),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10),
                                          bottomRight: Radius.circular(10))),
                                )),
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(left: 14.w, top: 15.w),
                              child: Text(
                                "替补阵容",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.sp, color: Color(0xFF666666)),
                              ),
                            )
                          ],
                        ),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.home != null &&
                                    _LineupPageModal!.data!.home!.backup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.home!.backup!.length >
                                        0
                                ? false
                                : true,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 30.w),
                              child: EmptyContainer(txt: '暂无数据'),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.home != null &&
                                    _LineupPageModal!.data!.home!.backup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.home!.backup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(
                                  left: 21.w, right: 22.w, top: 12.w),
                              child: SizedBox(
                                width: 500.w,
                                height: 1.w,
                                child: DecoratedBox(
                                  decoration:
                                      BoxDecoration(color: Color(0xFFF1E1DA)),
                                ),
                              ),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.home != null &&
                                    _LineupPageModal!.data!.home!.backup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.home!.backup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 14.w),
                              child: ReserveLineupList(
                                  backup: _LineupPageModal?.data?.home?.backup),
                            )),
                      ],
                    ),
                  )),
              Visibility(
                  visible: isSelected == 1 && widget.type == 1,
                  child: Container(
                    color: Color(0xFFFFFFFF),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(top: 15.w),
                                width: 4.w,
                                height: 20.w,
                                decoration: ShapeDecoration(
                                  color: Color(0xFFF36704),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10),
                                          bottomRight: Radius.circular(10))),
                                )),
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(left: 14.w, top: 15.w),
                              child: Text(
                                "首发阵容",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.sp, color: Color(0xFF666666)),
                              ),
                            )
                          ],
                        ),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.away != null &&
                                    _LineupPageModal!.data!.away!.lineup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.away!.lineup!.length >
                                        0
                                ? false
                                : true,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 30.w),
                              child: EmptyContainer(txt: '暂无数据'),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.away != null &&
                                    _LineupPageModal!.data!.away!.lineup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.away!.lineup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(
                                  left: 21.w, right: 22.w, top: 12.w),
                              child: SizedBox(
                                width: 500.w,
                                height: 1.w,
                                child: DecoratedBox(
                                  decoration:
                                      BoxDecoration(color: Color(0xFFF1E1DA)),
                                ),
                              ),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                _LineupPageModal!.data != null &&
                                _LineupPageModal!.data!.away != null &&
                                _LineupPageModal!.data!.away!.lineup !=
                                    null &&
                                _LineupPageModal!
                                    .data!.away!.lineup!.length >
                                    0
                                ? true
                                : false,
                            child: Container(
                                margin: EdgeInsets.only(
                                    left: 33.w, right: 36.w, top: 14.w),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "球员",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF999999)),
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          softWrap: true,
                                          overflow: TextOverflow.ellipsis,
                                        )),
                                    Container(
                                        child: Text(
                                          "球衣",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF999999)),
                                          textAlign: TextAlign.center,
                                        )),
                                    Container(
                                        child: Text(
                                          "位置",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: Color(0xFF999999)),
                                          textAlign: TextAlign.center,
                                        )),
                                  ],
                                ))),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.away != null &&
                                    _LineupPageModal!.data!.away!.lineup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.away!.lineup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 14.w),
                              child: TextLiveBroadcastList(
                                  backup: _LineupPageModal?.data?.away?.lineup),
                            )),
                        Container(
                          height: 10.w,
                          color: c_w,
                        ),
                        Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(top: 15.w),
                                width: 4.w,
                                height: 20.w,
                                decoration: ShapeDecoration(
                                  color: Color(0xFFF36704),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10),
                                          bottomRight: Radius.circular(10))),
                                )),
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(left: 14.w, top: 15.w),
                              child: Text(
                                "替补阵容",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.sp, color: Color(0xFF666666)),
                              ),
                            )
                          ],
                        ),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.away != null &&
                                    _LineupPageModal!.data!.away!.backup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.away!.backup!.length >
                                        0
                                ? false
                                : true,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 30.w),
                              child: EmptyContainer(txt: '暂无数据'),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.away != null &&
                                    _LineupPageModal!.data!.away!.backup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.away!.backup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(
                                  left: 21.w, right: 22.w, top: 12.w),
                              child: SizedBox(
                                width: 500.w,
                                height: 1.w,
                                child: DecoratedBox(
                                  decoration:
                                      BoxDecoration(color: Color(0xFFF1E1DA)),
                                ),
                              ),
                            )),
                        Visibility(
                            visible: _LineupPageModal != null &&
                                    _LineupPageModal!.data != null &&
                                    _LineupPageModal!.data!.away != null &&
                                    _LineupPageModal!.data!.away!.backup !=
                                        null &&
                                    _LineupPageModal!
                                            .data!.away!.backup!.length >
                                        0
                                ? true
                                : false,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 14.w),
                              child: ReserveLineupList(
                                  backup: _LineupPageModal?.data?.away?.backup),
                            )),
                      ],
                    ),
                  )),
            ]),
          )
        : EmptyContainer(txt: '暂无数据');
  }

  RowList(List<GrowableList> list) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      itemCount: list.length,
      itemBuilder: (BuildContext context, int position) {
        return _cellForRow(context, position, list);
      },
    );
  }

  Widget _cellForRow(BuildContext context, int index, List<GrowableList> list) {
    return Container(
        width: 70.w,
        child: Column(children: [
          Container(
            width: 28.w,
            height: 28.w,
            alignment: Alignment.center,
            child: Container(
              width: 24.w,
              height: 24.w,
              alignment: Alignment.center,
              child: Text(
                list[index].number ?? "",
                style: TextStyle(color: c_white, fontSize: 14.sp),
              ),
              decoration: ShapeDecoration(
                  color: Color(0xFFD55F5F),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(20))),
            ),
            decoration: ShapeDecoration(
                color: c_white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(20))),
          ),
          Container(
              margin: EdgeInsets.only(right: 5.w, top: 2.w),
              child: Text(
                list[index].name ?? "",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(color: c_white, fontSize: 10.sp),
              )),
        ]));
  }

  Widget _BcellForRow(
      BuildContext context, int index, List<GrowableList> list) {
    return Container(
        width: 70.w,
        child: Column(children: [
          Container(
            width: 28.w,
            height: 28.w,
            alignment: Alignment.center,
            child: Container(
              child: Container(
                width: 24.w,
                height: 24.w,
                alignment: Alignment.center,
                child: Text(
                  list[index].number ?? "",
                  style: TextStyle(color: c_white, fontSize: 14.sp),
                ),
                decoration: ShapeDecoration(
                    color: Color(0xFF6260D7),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadiusDirectional.circular(20))),
              ),
            ),
            decoration: ShapeDecoration(
                color: c_white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(20))),
          ),
          Container(
              margin: EdgeInsets.only(right: 5.w, top: 2.w),
              child: Text(
                list[index].name ?? "",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                  color: c_white,
                  fontSize: 10.sp,
                ),
                textAlign: TextAlign.center,
              )),
        ]));
  }

  ColumnList() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      itemCount: positionALL != null ? positionALL!.length : 0,
      itemBuilder: (BuildContext context, int position) {
        return _Column(context, position);
      },
    );
  }

  Widget _Column(BuildContext context, int index) {
    return Container(
      height: 50.w,
      alignment: Alignment.center,
      child: RowList(positionALL![index]),
    );
  }

  BColumnList() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      itemCount: positionALLAway != null ? positionALLAway!.length : 0,
      itemBuilder: (BuildContext context, int position) {
        return _BColumn(context, position);
      },
    );
  }

  Widget _BColumn(BuildContext context, int index) {
    return Container(
      height: 50.w,
      alignment: Alignment.center,
      child: BRowList(positionALLAway![index]),
    );
  }

  BRowList(List<GrowableList> list) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      itemCount: list.length,
      itemBuilder: (BuildContext context, int position) {
        return _BcellForRow(context, position, list);
      },
    );
  }
}

class benchLeftList extends StatelessWidget {
  final List<GrowableList>? backup;

  benchLeftList({Key? key, required this.backup}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: backup != null ? backup!.length : 0,
          itemBuilder: (BuildContext context, int position) {
            return _cellForRow(context, position);
          },
        ));
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      margin: EdgeInsets.only(left: 10.w),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
              margin: EdgeInsets.only(top: 10.w),
              width: 133.w,
              child: Text(
                backup != null ? backup![index].name ?? "" : "",
                style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
                textAlign: TextAlign.start,
              )),
          Container(
              child: Text(
            backup != null
                ? "${backup![index].number}号 ${backup![index].position}"
                : "",
            style: TextStyle(fontSize: 10.sp, color: Color(0xFF999999)),
            textAlign: TextAlign.start,
          )),
        ],
      ),
    );
  }
}

class benchRightList extends StatelessWidget {
  final List<GrowableList>? backup;

  benchRightList({Key? key, required this.backup}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: backup != null ? backup!.length : 0,
          itemBuilder: (BuildContext context, int position) {
            return _cellForRow(context, position);
          },
        ));
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
      margin: EdgeInsets.only(right: 10.w),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
              margin: EdgeInsets.only(top: 10.w),
              width: 133.w,
              child: Text(
                backup != null ? backup![index].name ?? "" : "",
                style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
                textAlign: TextAlign.end,
              )),
          Container(
              child: Text(
            backup != null
                ? "${backup![index].number}号 ${backup![index].position}"
                : "",
            style: TextStyle(fontSize: 10.sp, color: Color(0xFF999999)),
            textAlign: TextAlign.start,
          )),
        ],
      ),
    );
  }
}

class TextLiveBroadcastList extends StatelessWidget {
  final List<GrowableList>? backup;

  TextLiveBroadcastList({Key? key, required this.backup}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.separated(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: backup != null ? backup!.length : 0,
          physics: NeverScrollableScrollPhysics(),
          separatorBuilder: (BuildContext context, int index) =>
              Divider(height: 1.w, color: Color(0xFFFFFFFF)),
          itemBuilder: (BuildContext context, int position) {
            return _cellForRow(context, position);
          },
        ));
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
        margin: EdgeInsets.only(left: 33.w, right: 36.w, top: 8.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                width: MediaQuery.of(context).size.width / 4,
                alignment: Alignment.centerLeft,
                child: Text(
                  backup != null ? backup![index].name ?? "" : "",
                  style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
                  textAlign: TextAlign.center,
                  // maxLines: 1,
                  // softWrap: true,
                  // overflow: TextOverflow.ellipsis,
                )),
            Container(
                width: MediaQuery.of(context).size.width / 4,
                alignment: Alignment.center,
                child: Text(
                  backup != null ? backup![index].number ?? "" : "",
                  style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
                  textAlign: TextAlign.center,
                )),
            Container(
                width: MediaQuery.of(context).size.width / 4,
                alignment: Alignment.centerRight,
                child: Text(
                  backup != null ? backup![index].position ?? "" : "",
                  style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
                  textAlign: TextAlign.center,
                )),
          ],
        ));
  }
}

class ReserveLineupList extends StatelessWidget {
  final List<GrowableList>? backup;

  ReserveLineupList({Key? key, required this.backup}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 0,
              mainAxisSpacing: 3,
              childAspectRatio: 7),
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: backup != null ? backup!.length : 0,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {
            return _cellForRow(context, position);
          },
        ));
  }

  Widget _cellForRow(BuildContext context, int index) {
    return Container(
        margin: EdgeInsets.only(left: 33.w, right: 33.w, top: 8.w),
        child: Text(
          backup != null ? backup![index].name ?? "" : "",
          style: TextStyle(fontSize: 12.sp, color: Color(0xFF666666)),
          textAlign: isOdd(index) ? TextAlign.start : TextAlign.right,
        ));
  }
}

isOdd(n) {
  if (n % 2 == 0) {
    return true;
  }
  if (n % 2 == 1) {
    return false;
  }
}
