// @dart=2.9

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:liveapp/api/score_req_method.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/common_widget/extended_tabs/extended_tabs.dart';
import 'package:liveapp/config/date/date_utils.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/main.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/mo/score_home_model.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_kind_tabcontroller1.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_list.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/tabindicator_custom.dart';
import 'package:liveapp/router/route_watch.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/utils/my_underline_indicator.dart';

class scorePage extends BaseWidget {
  scorePage();

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _scorePageState();
  }
}

class _scorePageState extends BaseWidgetState<scorePage> with TickerProviderStateMixin, RouteAware {
  List<String> topList = ['足球', '篮球'];

  TabController tabController;
  int _selectedIndex = 0;



  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeViewObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void didPush() {
    print('didPush');
  }

  @override
  void didPopNext() {
    print('didPopNext');
  }
  @override
  void didPushNext() {
    print('didPushNext');
  }

  @override
  void didPop() {
    print('didPop');
  }


  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: topList.length, vsync: this);
    tabController.addListener(() {
      if (tabController.index == tabController.animation.value) {
        setState(() {
          _selectedIndex = tabController.index;
        });
      }
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    routeViewObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  Widget PageBody(AppState state) {
    return MediaQuery.removePadding(
      removeTop: true,
      removeBottom: true,
      removeLeft: true,
      removeRight: true,
      context: context,
      child: Container(
        color: c_white,
        child: Container(
          margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          child: Directionality(
            textDirection: TextDirection.ltr,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 50.w,
                    ),
                    //  Container(),
                    Center(
                      child: Container(
                        width: 220.w,
                        height: 32.w,
                        decoration: BoxDecoration(color: c_o, borderRadius: BorderRadius.circular(10.w)),
                        child: ExtendedTabBar(
                          indicator: CustomUnderlineTabIndicator(),
                          labelColor: Colors.black,
                          unselectedLabelColor: c_white,
                          tabs: topList
                              .map((String name) => Container(
                                  width: 110.w,
                                  child: Tab(
                                    text: name,
                                  )))
                              .toList(),
                          labelStyle: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
                          unselectedLabelStyle: TextStyle(fontSize: 16.sp),
                          controller: tabController,
                          labelPadding: EdgeInsets.zero,
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        ),
                      ),
                    ),
                    Container(
                      width: 50.w,
                      child: FlatButton(
                        padding: EdgeInsets.symmetric(horizontal: 0),
                        ///跳转联赛筛选
                        onPressed: () {
                          goPage(url: 'score_siftpage',params: {'catId':(_selectedIndex+1).toString()});
                        },
                        child: LocalImageSelector.getSingleImage('Union', imageWidth: 20.w, imageHeight: 18.w),
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: ExtendedTabBarView(
                    children: _expandList(),
                    controller: tabController,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _expandList() {
    return topList
        .map(
          (String name) => ScoreTabController1(name),
        )
        .toList();
  }




}
