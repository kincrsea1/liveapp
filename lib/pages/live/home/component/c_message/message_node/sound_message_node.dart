import 'package:flutter/cupertino.dart';

import '../enums/message_node_type.dart';
import 'package:liveapp/pages/live/home/component/c_message/message_node/message_node.dart';

class SoundMessageNode extends MessageNode {
  String? uuid;
  String? path;

  int? duration;
  int? dataSize;

  SoundMessageNode({
    @required this.path,
    @required this.duration,
  }) : super(MessageNodeType.Sound);

  SoundMessageNode.fromJson(Map<String, dynamic> json)
      : super(MessageNodeType.Sound) {
    uuid = json['uuid'];
    path = json['path'];
    duration = json['duration'];
    dataSize = json['dataSize'];
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data["path"] = this.path;
    data["duration"] = this.duration;
    return data;
  }
}
