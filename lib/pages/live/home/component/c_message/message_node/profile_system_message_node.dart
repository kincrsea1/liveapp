

import 'package:liveapp/pages/live/home/component/c_message/enums/message_node_type.dart';
import 'package:liveapp/pages/live/home/component/c_message/message_node/message_node.dart';

/// 用户资料变更系统通知节点
class ProfileSystemMessageNode extends MessageNode {
  /// 资料变更类型
  int? subType;

  /// 资料变更的用户名
  String? fromUser;

  /// 资料变更的昵称
  String? nickName;

  ProfileSystemMessageNode.fromJson(Map<String, dynamic> json) : super(MessageNodeType.GroupSystem) {
    this.subType = json["subType"];
    this.fromUser = json["fromUser"];
    this.nickName = json["nickName"];
  }
}
