
import '../enums/message_node_type.dart';
import 'package:liveapp/pages/live/home/component/c_message/message_node/message_node.dart';

/// 自定义消息节点
class CustomMessageNode extends MessageNode {
  /// 自定义数据
  String? data;

  CustomMessageNode({
    this.data,
  }) : super(MessageNodeType.Custom);

  CustomMessageNode.fromJson(Map<String, dynamic> json)
      : super(MessageNodeType.Custom) {
    data = json['data'];
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data["data"] = this.data;
    return data;
  }
}
