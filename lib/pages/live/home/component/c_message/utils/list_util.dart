
import 'package:liveapp/pages/live/home/component/c_message/utils/entity_factory.dart';

class ListUtil {
  /// 根据泛型生成集合
  static List<T> generateOBJList<T>(arr) {
    List<T> data = [];
    for (var item in arr) {
      if(EntityFactory.generateOBJ<T>(item) != null){
        data.add(EntityFactory.generateOBJ<T>(item)!);
      }
    }
    return data;
  }
}
