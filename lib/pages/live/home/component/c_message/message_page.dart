import 'package:flutter/material.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/pages/live/home/component/c_message/friend_list.dart';
import 'package:liveapp/pages/live/home/component/c_message/friend_list2.dart';
import 'package:liveapp/pages/live/home/component/c_message/message_mail_list_page.dart';
import 'package:liveapp/pages/live/home/component/c_message/message_message_page.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

class messagePage extends BaseWidget {
  messagePage({Key? key}) : super(key: key);

  @override
  BaseWidgetState<BaseWidget> getState() {
    return _messagePageState();
  }
}

class _messagePageState extends BaseWidgetState<messagePage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  Widget PageBody(AppState state) {
    return Scaffold(
      appBar: AppBar(
        title: Text('顶部tab切换'),
        bottom: TabBar(
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.directions_bike),
            ),
            Tab(
              icon: Icon(Icons.directions_boat),
            ),
          ],
          controller: _tabController,
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          messageMessagePage(),
          messageMailListPage(),
        ],
      ),
    );
  }
}
