// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:flutter/material.dart';
import 'package:liveapp/bottom_nav_bar.dart';
import 'package:liveapp/pages/live/home/component/a_score/score_page.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/score_detail_page.dart';
import 'package:liveapp/pages/live/home/component/a_score/widget/score_match_sift_page.dart';
import 'package:liveapp/pages/live/home/component/b_onlive/onlive_page.dart';
import 'package:liveapp/pages/live/home/component/d_friend/friend_page.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/live_login.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mine_about_us.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mine_kefu.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mine_message.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mine_page.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mine_system.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mine_version.dart';
import 'package:liveapp/pages/live/home/component/e_mine/profile/modify_nickname.dart';
import 'package:liveapp/pages/live/home/component/e_mine/profile/modify_password.dart';
import 'package:liveapp/pages/live/home/component/e_mine/profile/modify_phone.dart';
import 'package:liveapp/pages/live/home/component/e_mine/protocol/privacy_protocol.dart';
import 'package:liveapp/pages/live/home/component/e_mine/protocol/user_protocol.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/suggestion_report.dart';
import 'package:liveapp/pages/live/home/focus.dart';
import 'package:liveapp/pages/live/home/liveDetail/live_detail.dart';
import 'package:liveapp/pages/live/home/live_search.dart';

import '../../pages/live/home/component/e_mine/mine_profile.dart';
import '../../pages/live/home/component/e_mine/profile/profile_date.dart';

class onliveRouter {
  ///获取直播路由配置
  static Map<String, WidgetBuilder> getRouter() {
    return {
      '/index': (BuildContext context) => Bottomnavigationbar(),
      '/onlive/live_search': (BuildContext context) => LiveSearchPage(),
      '/onlive/focus': (BuildContext context, {arguments}) => FocusPage(), //- 关注界面
      '/onlive/live_detail': (BuildContext context, {arguments}) => LiveDetail(), //- 直播详情界面
      '/onlive/score': (BuildContext context) => scorePage(),
      '/onlive/live': (BuildContext context) => OnLivePage(),
      '/onlive/score_siftpage': (BuildContext context, {arguments}) => ScoreSiftPage(),
      '/onlive/score_detail': (BuildContext context, {arguments}) => ScoreDetailPage(),
      '/onlive/friend': (BuildContext context) => friendPage(),
      '/onlive/mine': (BuildContext context) => minePage(),
      '/onlive/login': (BuildContext context) => LiveLogin(),
      '/onlive/profile': (BuildContext context) => MineProfile(),
      '/onlive/suggestion': (BuildContext context) => SuggestionReport(),
      '/onlive/userProtocol': (BuildContext context) => UserProtocol(),
      '/onlive/privacyProtocol': (BuildContext context) => PrivacyProtocol(),
      '/onlive/message': (BuildContext context) => MineMessage(),
      '/onlive/system': (BuildContext context) => MineSystem(),
      '/onlive/version': (BuildContext context) => MineVersion(),
      '/onlive/modify_nickname': (BuildContext context) => ModifyNickName(),
      '/onlive/modify_password': (BuildContext context) => ModifyPassword(),
      '/onlive/date': (BuildContext context) => ProfileDate(),
      '/onlive/kefu': (BuildContext context) => Minekefu(),
      '/onlive/about_us': (BuildContext context) => MineAboutUs(),
      '/onlive/modify_phone': (BuildContext context) => ModifyPhone(),
    };
  }
}
