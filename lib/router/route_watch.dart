import 'package:flutter/material.dart';
import 'package:liveapp/pages/live/home/component/e_mine/suggestion/event_bus_util.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:liveapp/store/session_storage/state_user.dart';
import 'package:redux/redux.dart';

class MyRouteObserver<R extends Route<dynamic>> extends RouteObserver<R> {
  Store<AppState> store;
  MyRouteObserver(this.store);
  get _currentRouteName => store.state.user.currentRouteName;
  @override
  void didPush(Route route, Route? previousRoute) {
    if (previousRoute != null) {}

    super.didPush(route, previousRoute);
    if (route.settings.name != null) {
      store.dispatch(UpDateCurrentRouteName(payload: {'currentRouteName': route.settings.name}));
    }
    if (previousRoute != null && previousRoute.settings.name == '/index' && store.state.chat.tabBarIndex == 1) {
      bus.emit(EventBus.WATCHRECOMMENDLOOP, 'stop');
    }
  }

  @override
  void didPop(Route route, Route? previousRoute) {
    super.didPop(route, previousRoute);
    //- 当前不一致存储上一个路由名称
    if (route.settings.name != null && route.settings.name == _currentRouteName) {
      store.dispatch(UpDateCurrentRouteName(payload: {'currentRouteName': previousRoute!.settings.name}));
    }

    //- 视频详情界面跳转
    if (route.settings.name == '/onlive/login' && previousRoute!.settings.name == '/onlive/live_detail') {
      bus.emit(EventBus.GOTARGETPAGE, 'return'); //todo moore 提出暂停问题
    }

    if (route.settings.name == '/onlive/live_detail') {
      bus.emit(EventBus.WATCHRECOMMENDLOOP, 'start');
    }
  }

  @override
  void didReplace({Route? newRoute, Route? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    print('didReplace newRoute: $newRoute,oldRoute:$oldRoute');
  }

  @override
  void didRemove(Route route, Route? previousRoute) {
    super.didRemove(route, previousRoute);
    print('didRemove route: $route,previousRoute:$previousRoute');
  }

  @override
  void didStartUserGesture(Route route, Route? previousRoute) {
    super.didStartUserGesture(route, previousRoute);
    print('didStartUserGesture route: $route,previousRoute:$previousRoute');
  }

  @override
  void didStopUserGesture() {
    super.didStopUserGesture();
    print('didStopUserGesture');
  }
}
