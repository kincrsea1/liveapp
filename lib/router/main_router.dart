import 'package:flutter/material.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/router/live/live.dart';

class MainRouter {
  static Map<String, WidgetBuilder> getRoute() {
    switch (ConfigUtil().APP_TEMPLATE) {
      case 'onlive':
        return onliveRouter.getRouter();
      case 'betob8':
        return onliveRouter.getRouter();
      default:
        return onliveRouter.getRouter();
        ;
    }
  }
}
