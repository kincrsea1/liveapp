// @dart=2.9
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
typedef void EventChannelCallback(arg);
class MyEventChannel{
// 初始化一个广播流从channel中接收数据，返回的Stream调用listen方法完成注册，需要在页面销毁时调用Stream的cancel方法取消监听
  StreamSubscription _streamSubscription;

  static MyEventChannel _myEventChannel;
  static instance(){
    if(_myEventChannel==null){
      _myEventChannel = MyEventChannel();
    }
    return _myEventChannel;
  }

  //这里一个回调函数，用来注册的使用给调用者使用的
  void config(EventChannelCallback eventChannelCallback) {
    _streamSubscription = EventChannel('EventChannel')
        .receiveBroadcastStream()
        .listen(eventChannelCallback, onError: _onToDartError);
  }
  void dispose() {
    if (_streamSubscription != null) {
      _streamSubscription.cancel();
      _streamSubscription = null;
    }
  }
  void _onToDartError(error) {
    print("_onToDartError ===> " + error);
  }

}