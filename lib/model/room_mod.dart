class RoomMod {
  String? msg;
  int? code;
  Data? data;

  RoomMod({this.msg, this.code, this.data});

  RoomMod.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? userId;
  int? roomId;
  String? roomName;
  String? username;
  var expiredTime;
  var createTime;
  String? nickName;
  var password;
  var email;
  var mobile;
  var salt;
  var createUserId;
  String? description;
  int? status;
  var userLogo;
  int? follow;
  var roleId;
  var roleName;

  Data(
      {this.userId,
      this.roomId,
      this.roomName,
      this.username,
      this.expiredTime,
      this.createTime,
      this.nickName,
      this.password,
      this.email,
      this.mobile,
      this.salt,
      this.createUserId,
      this.description,
      this.status,
      this.userLogo,
      this.follow,
      this.roleId,
      this.roleName});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    roomId = json['roomId'];
    roomName = json['roomName'];
    username = json['username'];
    expiredTime = json['expiredTime'];
    createTime = json['createTime'];
    nickName = json['nickName'];
    password = json['password'];
    email = json['email'];
    mobile = json['mobile'];
    salt = json['salt'];
    createUserId = json['createUserId'];
    description = json['description'];
    status = json['status'];
    userLogo = json['userLogo'];
    follow = json['follow'];
    roleId = json['roleId'];
    roleName = json['roleName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['roomId'] = this.roomId;
    data['roomName'] = this.roomName;
    data['username'] = this.username;
    data['expiredTime'] = this.expiredTime;
    data['createTime'] = this.createTime;
    data['nickName'] = this.nickName;
    data['password'] = this.password;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['salt'] = this.salt;
    data['createUserId'] = this.createUserId;
    data['description'] = this.description;
    data['status'] = this.status;
    data['userLogo'] = this.userLogo;
    data['follow'] = this.follow;
    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    return data;
  }
}
