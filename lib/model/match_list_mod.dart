class MatchListModal {
  String? msg;
  int? code;
  Data? data;

  MatchListModal({this.msg, this.code, this.data});

  MatchListModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? totalCount;
  int? pageSize;
  int? totalPage;
  int? currPage;
  List<MatchList>? list;

  Data({this.totalCount, this.pageSize, this.totalPage, this.currPage, this.list});

  Data.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = <MatchList>[];
      json['list'].forEach((v) {
        list!.add(new MatchList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MatchList {
  int? SportType;
  String? matchDate;
  List<PlayUrls>? playUrls;
  String? groupId;
  String? homeName;
  var homeNameEn;
  var leagueNameEn;
  String? matchState;
  var awayNameEn;
  String? type;
  var userLogo;
  var roomId;
  String? leagueName;
  String? awayLogo;
  String? awayScore;
  BetEventInfo? betEventInfo;
  MatchAVBS? matchAVBS;
  String? homeId;
  String? matchId;
  String? homeScore;
  String? matchDateTime;
  String? awayId;
  String? homeLogo;
  String? matchTime;
  var realTime;
  var animationUrl;
  var qtId;
  var userName;
  var userId;
  String? liveStreams;
  var roomName;
  String? imgUrl;
  String? catId;
  String? awayName;
  var matchScheduleId;
  var online;
  var resva;

  MatchList(
      {this.matchDate,
      this.playUrls,
      this.SportType,
      this.groupId,
      this.homeName,
      this.homeNameEn,
      this.leagueNameEn,
      this.matchState,
      this.awayNameEn,
      this.type,
      this.userLogo,
      this.roomId,
      this.leagueName,
      this.awayLogo,
      this.awayScore,
      this.betEventInfo,
      this.matchAVBS,
      this.homeId,
      this.matchId,
      this.homeScore,
      this.matchDateTime,
      this.awayId,
      this.homeLogo,
      this.matchTime,
      this.qtId,
      this.userName,
      this.userId,
      this.liveStreams,
      this.roomName,
      this.imgUrl,
      this.catId,
      this.awayName,
      this.matchScheduleId,
      this.realTime,
        this.animationUrl,
      this.online,
      this.resva});

  MatchList.fromJson(Map<String, dynamic> json) {
    matchDate = json['matchDate'];
    if (json['playUrls'] != null) {
      playUrls = <PlayUrls>[];
      json['playUrls'].forEach((v) {
        playUrls!.add(new PlayUrls.fromJson(v));
      });
    }
    groupId = json['groupId'];
    homeName = json['homeName'];
    SportType = json['SportType'];
    homeNameEn = json['homeNameEn'];
    leagueNameEn = json['leagueNameEn'];
    matchState = json['matchState'];
    awayNameEn = json['awayNameEn'];
    type = json['type'];
    userLogo = json['userLogo'];
    realTime = json['realTime'];
    animationUrl = json['animationUrl'];
    roomId = json['roomId'];
    leagueName = json['leagueName'];
    awayLogo = json['awayLogo'];
    awayScore = json['awayScore'];
    betEventInfo = json['betEventInfo'] != null ? new BetEventInfo.fromJson(json['betEventInfo']) : null;
    matchAVBS = json['matchAVBS'] != null ? new MatchAVBS.fromJson(json['matchAVBS']) : null;
    homeId = json['homeId'];
    matchId = json['matchId'];
    homeScore = json['homeScore'];
    matchDateTime = json['matchDateTime'];
    awayId = json['awayId'];
    homeLogo = json['homeLogo'];
    matchTime = json['matchTime'];
    qtId = json['qtId'];
    userName = json['userName'];
    userId = json['userId'];
    liveStreams = json['liveStreams'];
    roomName = json['roomName'];
    imgUrl = json['imgUrl'];
    catId = json['catId'];
    awayName = json['awayName'];
    matchScheduleId = json['matchScheduleId'];
    online = json['online'];
    resva = json['resva'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['matchDate'] = this.matchDate;
    if (this.playUrls != null) {
      data['playUrls'] = this.playUrls!.map((v) => v.toJson()).toList();
    }
    data['groupId'] = this.groupId;
    data['homeName'] = this.homeName;
    data['homeNameEn'] = this.homeNameEn;
    data['leagueNameEn'] = this.leagueNameEn;
    data['matchState'] = this.matchState;
    data['awayNameEn'] = this.awayNameEn;
    data['type'] = this.type;
    data['SportType'] = this.SportType;
    data['userLogo'] = this.userLogo;
    data['roomId'] = this.roomId;
    data['leagueName'] = this.leagueName;
    data['awayLogo'] = this.awayLogo;
    data['realTime'] = this.realTime;
    data['animationUrl'] = this.animationUrl;
    data['awayScore'] = this.awayScore;
    if (this.betEventInfo != null) {
      data['betEventInfo'] = this.betEventInfo!.toJson();
    }
    if (this.matchAVBS != null) {
      data['matchAVBS'] = this.matchAVBS!.toJson();
    }
    data['homeId'] = this.homeId;
    data['matchId'] = this.matchId;
    data['homeScore'] = this.homeScore;
    data['matchDateTime'] = this.matchDateTime;
    data['awayId'] = this.awayId;
    data['homeLogo'] = this.homeLogo;
    data['matchTime'] = this.matchTime;
    data['qtId'] = this.qtId;
    data['userName'] = this.userName;
    data['userId'] = this.userId;
    data['liveStreams'] = this.liveStreams;
    data['roomName'] = this.roomName;
    data['imgUrl'] = this.imgUrl;
    data['catId'] = this.catId;
    data['awayName'] = this.awayName;
    data['matchScheduleId'] = this.matchScheduleId;
    data['online'] = this.online;
    data['resva'] = this.resva;
    return data;
  }
}

class PlayUrls {
  String? streamImgUrl;
  String? name;
  String? id;
  int? lang;
  var type;
  var isFullPlayUrl;

  PlayUrls({this.streamImgUrl, this.name, this.id, this.lang, this.type, this.isFullPlayUrl});

  PlayUrls.fromJson(Map<String, dynamic> json) {
    streamImgUrl = json['streamImgUrl'];
    name = json['name'];
    id = json['id'];
    lang = json['lang'];
    type = json['type'];
    isFullPlayUrl = json['isFullPlayUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['streamImgUrl'] = this.streamImgUrl;
    data['name'] = this.name;
    data['id'] = this.id;
    data['lang'] = this.lang;
    data['type'] = this.type;
    data['isFullPlayUrl'] = this.isFullPlayUrl;
    return data;
  }
}

class BetEventInfo {
  String? fjAwayName;
  String? siteCode;
  String? fjLeagueName;
  String? homeName;
  String? fjMatchId;
  String? qtId;
  var userId;
  String? leagueName;
  String? awayName;
  String? betMatchId;
  String? createTime;
  String? fjHomeName;
  int? id;
  String? videoMatchId;

  BetEventInfo(
      {this.fjAwayName,
      this.siteCode,
      this.fjLeagueName,
      this.homeName,
      this.fjMatchId,
      this.qtId,
      this.userId,
      this.leagueName,
      this.awayName,
      this.betMatchId,
      this.createTime,
      this.fjHomeName,
      this.id,
      this.videoMatchId});

  BetEventInfo.fromJson(Map<String, dynamic> json) {
    fjAwayName = json['fjAwayName'];
    siteCode = json['siteCode'];
    fjLeagueName = json['fjLeagueName'];
    homeName = json['homeName'];
    fjMatchId = json['fjMatchId'];
    qtId = json['qtId'];
    userId = json['userId'];
    leagueName = json['leagueName'];
    awayName = json['awayName'];
    betMatchId = json['betMatchId'];
    createTime = json['createTime'];
    fjHomeName = json['fjHomeName'];
    id = json['id'];
    videoMatchId = json['videoMatchId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fjAwayName'] = this.fjAwayName;
    data['siteCode'] = this.siteCode;
    data['fjLeagueName'] = this.fjLeagueName;
    data['homeName'] = this.homeName;
    data['fjMatchId'] = this.fjMatchId;
    data['qtId'] = this.qtId;
    data['userId'] = this.userId;
    data['leagueName'] = this.leagueName;
    data['awayName'] = this.awayName;
    data['betMatchId'] = this.betMatchId;
    data['createTime'] = this.createTime;
    data['fjHomeName'] = this.fjHomeName;
    data['id'] = this.id;
    data['videoMatchId'] = this.videoMatchId;
    return data;
  }
}

class MatchAVBS {
  List<Vids>? vids;
  int? id;
  String? matchDate;
  String? matchTime;
  String? matchDateTime;
  var matchId;
  String? catId;
  String? catName;
  String? leagueName;
  String? homeName;
  String? awayName;
  String? leagueNameEn;
  String? homeNameEn;
  String? awayNameEn;
  String? leagueLogo;
  String? homeLogo;
  String? awayLogo;
  String? homeScore;
  String? awayScore;
  var resva;
  String? matchState;
  var realTime;
  var animationUrl;
  var state;
  List<dynamic>? ancs;
  List<Bets>? bets;
  List<Scos>? scos;

  MatchAVBS(
      {this.vids,
      this.id,
      this.matchDate,
      this.matchTime,
      this.matchDateTime,
      this.matchId,
      this.catId,
      this.catName,
      this.leagueName,
      this.homeName,
      this.awayName,
      this.leagueNameEn,
      this.homeNameEn,
      this.awayNameEn,
      this.leagueLogo,
      this.homeLogo,
      this.awayLogo,
      this.homeScore,
      this.awayScore,
      this.resva,
      this.matchState,
      this.realTime,
        this.animationUrl,
      this.state,
      this.ancs,
      this.bets,
      this.scos});

  MatchAVBS.fromJson(Map<String, dynamic> json) {
    if (json['vids'] != null) {
      vids = <Vids>[];
      json['vids'].forEach((v) {
        vids!.add(new Vids.fromJson(v));
      });
    }
    id = json['id'];
    matchDate = json['matchDate'];
    matchTime = json['matchTime'];
    matchDateTime = json['matchDateTime'];
    matchId = json['matchId'];
    catId = json['catId'];
    catName = json['catName'];
    leagueName = json['leagueName'];
    homeName = json['homeName'];
    awayName = json['awayName'];
    leagueNameEn = json['leagueNameEn'];
    homeNameEn = json['homeNameEn'];
    awayNameEn = json['awayNameEn'];
    leagueLogo = json['leagueLogo'];
    homeLogo = json['homeLogo'];
    awayLogo = json['awayLogo'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    resva = json['resva'];
    matchState = json['matchState'];
    realTime = json['realTime'];
    animationUrl = json['animationUrl'];
    state = json['state'];
    if (json['ancs'] != null) {
      ancs = <dynamic>[];
      json['ancs'].forEach((v) {
        ancs!.add((v));
      });
    }
    if (json['bets'] != null) {
      bets = <Bets>[];
      json['bets'].forEach((v) {
        bets!.add(new Bets.fromJson(v));
      });
    }
    if (json['scos'] != null) {
      scos = <Scos>[];
      json['scos'].forEach((v) {
        scos!.add(new Scos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.vids != null) {
      data['vids'] = this.vids!.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    data['matchDate'] = this.matchDate;
    data['matchTime'] = this.matchTime;
    data['matchDateTime'] = this.matchDateTime;
    data['matchId'] = this.matchId;
    data['catId'] = this.catId;
    data['catName'] = this.catName;
    data['leagueName'] = this.leagueName;
    data['homeName'] = this.homeName;
    data['awayName'] = this.awayName;
    data['leagueNameEn'] = this.leagueNameEn;
    data['homeNameEn'] = this.homeNameEn;
    data['awayNameEn'] = this.awayNameEn;
    data['leagueLogo'] = this.leagueLogo;
    data['homeLogo'] = this.homeLogo;
    data['awayLogo'] = this.awayLogo;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['resva'] = this.resva;
    data['matchState'] = this.matchState;
    data['realTime'] = this.realTime;
    data['animationUrl'] = this.animationUrl;

    data['state'] = this.state;
    if (this.ancs != null) {
      data['ancs'] = this.ancs!.map((v) => v.toJson()).toList();
    }
    if (this.bets != null) {
      data['bets'] = this.bets!.map((v) => v.toJson()).toList();
    }
    if (this.scos != null) {
      data['scos'] = this.scos!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Vids {
  List<PlayUrls>? playUrls;
  String? matchId;
  int? matchType;
  String? groupId;
  int? play;
  String? matchName;

  Vids({this.playUrls, this.matchId, this.matchType, this.groupId, this.play, this.matchName});

  Vids.fromJson(Map<String, dynamic> json) {
    if (json['playUrls'] != null) {
      playUrls = <PlayUrls>[];
      json['playUrls'].forEach((v) {
        playUrls!.add(new PlayUrls.fromJson(v));
      });
    }
    matchId = json['matchId'];
    matchType = json['matchType'];
    groupId = json['groupId'];
    play = json['play'];
    matchName = json['matchName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.playUrls != null) {
      data['playUrls'] = this.playUrls!.map((v) => v.toJson()).toList();
    }
    data['matchId'] = this.matchId;
    data['matchType'] = this.matchType;
    data['groupId'] = this.groupId;
    data['play'] = this.play;
    data['matchName'] = this.matchName;
    return data;
  }
}

class PlayUrls1 {
  String? streamImgUrl;
  String? name;
  String? id;
  int? lang;
  String? type;
  int? isFullPlayUrl;

  PlayUrls1({this.streamImgUrl, this.name, this.id, this.lang, this.type, this.isFullPlayUrl});

  PlayUrls1.fromJson(Map<String, dynamic> json) {
    streamImgUrl = json['streamImgUrl'];
    name = json['name'];
    id = json['id'];
    lang = json['lang'];
    type = json['type'];
    isFullPlayUrl = json['isFullPlayUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['streamImgUrl'] = this.streamImgUrl;
    data['name'] = this.name;
    data['id'] = this.id;
    data['lang'] = this.lang;
    data['type'] = this.type;
    data['isFullPlayUrl'] = this.isFullPlayUrl;
    return data;
  }
}

class Bets {
  int? matchType;
  String? matchId;
  String? catId;
  String? groupId;

  Bets({this.matchType, this.matchId, this.catId, this.groupId});

  Bets.fromJson(Map<String, dynamic> json) {
    matchType = json['matchType'];
    matchId = json['matchId'];
    catId = json['catId'];
    groupId = json['groupId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['matchType'] = this.matchType;
    data['matchId'] = this.matchId;
    data['catId'] = this.catId;
    data['groupId'] = this.groupId;
    return data;
  }
}

class Scos {
  int? matchType;
  String? matchId;
  String? groupId;
  String? animationUrl;

  Scos({this.matchType, this.matchId, this.groupId, this.animationUrl});

  Scos.fromJson(Map<String, dynamic> json) {
    matchType = json['matchType'];
    matchId = json['matchId'];
    groupId = json['groupId'];
    animationUrl = json['animationUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['matchType'] = this.matchType;
    data['matchId'] = this.matchId;
    data['groupId'] = this.groupId;
    data['animationUrl'] = this.animationUrl;
    return data;
  }
}
