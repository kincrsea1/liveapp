class AnchorVideoListModal {
  String? msg;
  int? code;
  Data? data;

  AnchorVideoListModal({this.msg, this.code, this.data});

  AnchorVideoListModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  var totalCount;
  var pageSize;
  var totalPage;
  var currPage;
  List<AnchorList>? list;

  Data({this.totalCount, this.pageSize, this.totalPage, this.currPage, this.list});

  Data.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = <AnchorList>[];
      json['list'].forEach((v) {
        list!.add(new AnchorList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AnchorList {
  String? matchDate;
  List<PlayUrls>? playUrls;
  String? groupId;
  String? homeName;
  var homeNameEn;
  var leagueNameEn;
  String? matchState;
  var awayNameEn;
  String? type;
  var userLogo;
  var roomId;
  String? leagueName;
  String? awayLogo;
  String? awayScore;
  var betEventInfo;
  MatchAVBS? matchAVBS;
  String? fakeState;
  String? homeId;
  String? matchId;
  String? homeScore;
  String? matchDateTime;
  String? nickName;
  String? awayId;
  String? homeLogo;
  String? matchTime;
  var qtId;
  var userName;
  var userId;
  String? liveStreams;
  var roomName;
  String? imgUrl;
  String? catId;
  String? awayName;
  var matchScheduleId;
  var online;
  var resva;

  AnchorList(
      {this.matchDate,
      this.playUrls,
      this.groupId,
      this.homeName,
      this.homeNameEn,
      this.leagueNameEn,
      this.matchState,
      this.awayNameEn,
      this.type,
      this.userLogo,
      this.roomId,
      this.leagueName,
      this.awayLogo,
      this.awayScore,
      this.betEventInfo,
      this.matchAVBS,
      this.homeId,
      this.matchId,
      this.homeScore,
      this.matchDateTime,
      this.awayId,
      this.homeLogo,
      this.matchTime,
      this.fakeState,
      this.qtId,
      this.userName,
      this.userId,
      this.liveStreams,
      this.roomName,
      this.imgUrl,
      this.catId,
      this.awayName,
      this.nickName,
      this.matchScheduleId,
      this.online,
      this.resva});

  AnchorList.fromJson(Map<String, dynamic> json) {
    matchDate = json['matchDate'];
    if (json['playUrls'] != null) {
      playUrls = <PlayUrls>[];
      json['playUrls'].forEach((v) {
        playUrls!.add(new PlayUrls.fromJson(v));
      });
    }
    groupId = json['groupId'];
    homeName = json['homeName'];
    homeNameEn = json['homeNameEn'];
    leagueNameEn = json['leagueNameEn'];
    matchState = json['matchState'];
    awayNameEn = json['awayNameEn'];
    nickName = json['nickName'];
    type = json['type'];
    userLogo = json['userLogo'];
    roomId = json['roomId'];
    leagueName = json['leagueName'];
    awayLogo = json['awayLogo'];
    awayScore = json['awayScore'];
    betEventInfo = json['betEventInfo'];
    matchAVBS = json['matchAVBS'] != null ? new MatchAVBS.fromJson(json['matchAVBS']) : null;
    homeId = json['homeId'];
    matchId = json['matchId'];
    homeScore = json['homeScore'];
    matchDateTime = json['matchDateTime'];
    awayId = json['awayId'];
    homeLogo = json['homeLogo'];
    matchTime = json['matchTime'];
    qtId = json['qtId'];
    userName = json['userName'];
    fakeState = json['fakeState'];
    userId = json['userId'];
    liveStreams = json['liveStreams'];
    roomName = json['roomName'];
    imgUrl = json['imgUrl'];
    catId = json['catId'];
    awayName = json['awayName'];
    matchScheduleId = json['matchScheduleId'];
    online = json['online'];
    resva = json['resva'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['matchDate'] = this.matchDate;
    if (this.playUrls != null) {
      data['playUrls'] = this.playUrls!.map((v) => v.toJson()).toList();
    }
    data['groupId'] = this.groupId;
    data['homeName'] = this.homeName;
    data['homeNameEn'] = this.homeNameEn;
    data['leagueNameEn'] = this.leagueNameEn;
    data['matchState'] = this.matchState;
    data['awayNameEn'] = this.awayNameEn;
    data['type'] = this.type;
    data['nickName'] = this.nickName;
    data['userLogo'] = this.userLogo;
    data['roomId'] = this.roomId;
    data['leagueName'] = this.leagueName;
    data['fakeState'] = this.fakeState;
    data['awayLogo'] = this.awayLogo;
    data['awayScore'] = this.awayScore;
    data['betEventInfo'] = this.betEventInfo;
    if (this.matchAVBS != null) {
      data['matchAVBS'] = this.matchAVBS!.toJson();
    }
    data['homeId'] = this.homeId;
    data['matchId'] = this.matchId;
    data['homeScore'] = this.homeScore;
    data['matchDateTime'] = this.matchDateTime;
    data['awayId'] = this.awayId;
    data['homeLogo'] = this.homeLogo;
    data['matchTime'] = this.matchTime;
    data['qtId'] = this.qtId;
    data['userName'] = this.userName;
    data['userId'] = this.userId;
    data['liveStreams'] = this.liveStreams;
    data['roomName'] = this.roomName;
    data['imgUrl'] = this.imgUrl;
    data['catId'] = this.catId;
    data['awayName'] = this.awayName;
    data['matchScheduleId'] = this.matchScheduleId;
    data['online'] = this.online;
    data['resva'] = this.resva;
    return data;
  }
}

class PlayUrls {
  String? streamImgUrl;
  String? name;
  String? id;
  int? lang;
  var type;
  var isFullPlayUrl;

  PlayUrls({this.streamImgUrl, this.name, this.id, this.lang, this.type, this.isFullPlayUrl});

  PlayUrls.fromJson(Map<String, dynamic> json) {
    streamImgUrl = json['streamImgUrl'];
    name = json['name'];
    id = json['id'];
    lang = json['lang'];
    type = json['type'];
    isFullPlayUrl = json['isFullPlayUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['streamImgUrl'] = this.streamImgUrl;
    data['name'] = this.name;
    data['id'] = this.id;
    data['lang'] = this.lang;
    data['type'] = this.type;
    data['isFullPlayUrl'] = this.isFullPlayUrl;
    return data;
  }
}

class MatchAVBS {
  List<Vids>? vids;

  MatchAVBS({this.vids});

  MatchAVBS.fromJson(Map<String, dynamic> json) {
    if (json['vids'] != null) {
      vids = <Vids>[];
      json['vids'].forEach((v) {
        vids!.add(new Vids.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.vids != null) {
      data['vids'] = this.vids!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Vids {
  List<PlayUrls>? playUrls;
  String? matchId;

  Vids({this.playUrls, this.matchId});

  Vids.fromJson(Map<String, dynamic> json) {
    if (json['playUrls'] != null) {
      playUrls = <PlayUrls>[];
      json['playUrls'].forEach((v) {
        playUrls!.add(new PlayUrls.fromJson(v));
      });
    }
    matchId = json['matchId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.playUrls != null) {
      data['playUrls'] = this.playUrls!.map((v) => v.toJson()).toList();
    }
    data['matchId'] = this.matchId;
    return data;
  }
}
