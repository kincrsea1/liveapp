class UserInfoModel {
  String? msg;
  UserInfo? userInfo;
  int? code;
  int? expire;
  String? token;

  UserInfoModel({this.msg, this.userInfo, this.code, this.expire, this.token});

  UserInfoModel.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    userInfo = json['userInfo'] != null ? new UserInfo.fromJson(json['userInfo']) : null;
    code = json['code'];
    expire = json['expire'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    if (this.userInfo != null) {
      data['userInfo'] = this.userInfo!.toJson();
    }
    data['code'] = this.code;
    data['expire'] = this.expire;
    data['token'] = this.token;
    return data;
  }
}

/* 
available: 1
betSettingsType: 0
createTime: "2021-10-14 17:40:35"
isgGroupId: 1
logo: "https://download.injiepor.com/FjyPHelBrbeqsJIP6MR-i-41iP7i"
nickName: "asdasd"
updateTime: "2022-07-21 09:55:04"
userId: 9483
username: "alan9558"
 */

class UserInfo {
  String? gender;
  String? token;
  String? birthday;
  int? userId;
  String? loginName;
  String? nickName;
  int? depositLock;
  String? realName;
  var logo;
  var username;
  String? registerTime;
  double? balance;
  int? available;
  String? mobile;
  String? email;
  String? weChat;
  String? loginPwd;
  var securePwd;
  String? qq;
  int? userInfoMeasure;
  String? groupName;
  int? mbrLevel;
  String? levelName;
  int? freeWalletSwitch;
  int? settlementSwitch;
  int? settlementBtnShow;
  int? egSanGongFlg;
  int? egSanGong;
  int? egSanGongPromotionUrl;
  List<int>? depositCondition;
  int? withDrawalAudit;

  UserInfo(
      {this.gender,
      this.birthday,
      this.userId,
      this.loginName,
      this.nickName,
      this.depositLock,
      this.realName,
      this.registerTime,
      this.balance,
      this.available,
      this.mobile,
      this.email,
      this.weChat,
      this.loginPwd,
      this.securePwd,
      this.qq,
      this.logo,
      this.username,
      this.userInfoMeasure,
      this.groupName,
      this.mbrLevel,
      this.levelName,
      this.freeWalletSwitch,
      this.settlementSwitch,
      this.settlementBtnShow,
      this.egSanGongFlg,
      this.token,
      this.egSanGong,
      this.egSanGongPromotionUrl,
      this.depositCondition,
      this.withDrawalAudit});

  UserInfo.fromJson(Map<String, dynamic> json) {
    gender = json['gender'];
    birthday = json['birthday'];
    userId = json['userId'];
    loginName = json['loginName'];
    nickName = json['nickName'];
    depositLock = json['depositLock'];
    realName = json['realName'];
    registerTime = json['registerTime'];
    balance = json['balance'];
    available = json['available'];
    mobile = json['mobile'];
    email = json['email'];
    weChat = json['weChat'];
    logo = json['logo'];
    username = json['username'];
    loginPwd = json['loginPwd'];
    securePwd = json['securePwd'];
    qq = json['qq'];
    userInfoMeasure = json['userInfoMeasure'];
    groupName = json['groupName'];
    mbrLevel = json['mbrLevel'];
    levelName = json['levelName'];
    freeWalletSwitch = json['freeWalletSwitch'];
    settlementSwitch = json['settlementSwitch'];
    settlementBtnShow = json['settlementBtnShow'];
    egSanGongFlg = json['egSanGongFlg'];
    egSanGong = json['egSanGong'];
    token = json['token'];
    egSanGongPromotionUrl = json['egSanGongPromotionUrl'];
    depositCondition = json['depositCondition'].cast<int>();
    withDrawalAudit = json['withDrawalAudit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['gender'] = this.gender;
    data['birthday'] = this.birthday;
    data['userId'] = this.userId;
    data['loginName'] = this.loginName;
    data['nickName'] = this.nickName;
    data['depositLock'] = this.depositLock;
    data['realName'] = this.realName;
    data['registerTime'] = this.registerTime;
    data['balance'] = this.balance;
    data['available'] = this.available;
    data['mobile'] = this.mobile;
    data['email'] = this.email;
    data['weChat'] = this.weChat;
    data['loginPwd'] = this.loginPwd;
    data['securePwd'] = this.securePwd;
    data['logo'] = this.logo;
    data['username'] = this.username;
    data['qq'] = this.qq;
    data['userInfoMeasure'] = this.userInfoMeasure;
    data['groupName'] = this.groupName;
    data['mbrLevel'] = this.mbrLevel;
    data['levelName'] = this.levelName;
    data['freeWalletSwitch'] = this.freeWalletSwitch;
    data['settlementSwitch'] = this.settlementSwitch;
    data['settlementBtnShow'] = this.settlementBtnShow;
    data['egSanGongFlg'] = this.egSanGongFlg;
    data['egSanGong'] = this.egSanGong;
    data['egSanGongPromotionUrl'] = this.egSanGongPromotionUrl;
    data['depositCondition'] = this.depositCondition;
    data['withDrawalAudit'] = this.withDrawalAudit;
    data['token'] = this.token;
    return data;
  }
}
