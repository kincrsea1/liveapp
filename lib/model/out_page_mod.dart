class OutPageModal {
  String? msg;
  int? code;
  Data? data;

  OutPageModal({this.msg, this.code, this.data});

  OutPageModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? homeAttackCount;
  int? awayAttackCount;
  int? homeDangerAttackCount;
  int? awayDangerAttackCount;
  int? homePossessionPercentage;
  int? awayPossessionPercentage;
  int? homeShotsCount;
  int? awayShotsCount;
  int? homeShootRightCount;
  int? awayShootRightCount;
  int? homeRedCardCount;
  int? awayRedCardCount;
  int? homeYellowCardCount;
  int? awayYellowCardCount;
  int? homeCorner;
  int? awayCorner;
  String? homeName;
  String? awayName;
  var homeNameEn;
  var awayNameEn;
  var homeLogo;
  var awayLogo;
  var homeScore;
  var awayScore;
  String? home1;
  String? home2;
  String? home3;
  String? home4;
  String? homeOT;
  String? away1;
  String? away2;
  String? away3;
  String? away4;
  String? awayOT;

  var homeThreePointScore;
  var homeFreeThrowScore;
  var homeFreeThrowHitScorePercentage;
  var homeScoreMax;
  var homeReboundMax;
  var homeAssistMax;

  var awayThreePointScore;
  var awayFreeThrowScore;
  var awayFreeThrowHitScorePercentage;
  var awayScoreMax;
  var awayReboundMax;
  var awayAssistMax;

  List<EventList>? eventList;
  List<TextLiveList>? textLiveList;
  List<StatArr>? statArr;

  Data.fromJson(Map<String, dynamic> json) {
    homeAttackCount = json['homeAttackCount'];
    awayAttackCount = json['awayAttackCount'];
    homeDangerAttackCount = json['homeDangerAttackCount'];
    awayDangerAttackCount = json['awayDangerAttackCount'];
    homePossessionPercentage = json['homePossessionPercentage'];
    awayPossessionPercentage = json['awayPossessionPercentage'];
    homeShotsCount = json['homeShotsCount'];
    awayShotsCount = json['awayShotsCount'];
    homeShootRightCount = json['homeShootRightCount'];
    awayShootRightCount = json['awayShootRightCount'];
    homeRedCardCount = json['homeRedCardCount'];
    awayRedCardCount = json['awayRedCardCount'];
    homeYellowCardCount = json['homeYellowCardCount'];
    awayYellowCardCount = json['awayYellowCardCount'];
    homeName = json['homeName'];
    awayName = json['awayName'];
    homeNameEn = json['homeNameEn'];
    awayNameEn = json['awayNameEn'];
    homeLogo = json['homeLogo'];
    awayLogo = json['awayLogo'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    homeCorner = json['homeCorner'];
    awayCorner = json['awayCorner'];

    home1 = json['home1'];
    home2 = json['home2'];
    home3 = json['home3'];
    home4 = json['home4'];
    homeOT = json['homeOT'];
    away1 = json['away1'];
    away2 = json['away2'];
    away3 = json['away3'];
    away4 = json['away4'];
    awayOT = json['awayOT'];

    homeThreePointScore = json['homeThreePointScore'];
    homeFreeThrowScore = json['homeFreeThrowScore'];
    homeFreeThrowHitScorePercentage = json['homeFreeThrowHitScorePercentage'];
    homeScoreMax = json['homeScoreMax'];
    homeReboundMax = json['homeReboundMax'];
    homeAssistMax = json['homeAssistMax'];
    awayThreePointScore = json['awayThreePointScore'];
    awayFreeThrowScore = json['awayFreeThrowScore'];
    awayFreeThrowHitScorePercentage = json['awayFreeThrowHitScorePercentage'];
    awayScoreMax = json['awayScoreMax'];
    awayReboundMax = json['awayReboundMax'];
    awayAssistMax = json['awayAssistMax'];

    if (json['eventList'] != null) {
      eventList = <EventList>[];
      json['eventList'].forEach((v) {
        eventList!.add(EventList.fromJson(v));
      });
    }
    if (json['textLiveList'] != null) {
      textLiveList = <TextLiveList>[];
      json['textLiveList'].forEach((v) {
        textLiveList!.add(TextLiveList.fromJson(v));
      });
    }
    if (json['statArr'] != null) {
      statArr = <StatArr>[];
      json['statArr'].forEach((v) {
        statArr!.add(StatArr.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['homeAttackCount'] = this.homeAttackCount;
    data['awayAttackCount'] = this.awayAttackCount;
    data['homeDangerAttackCount'] = this.homeDangerAttackCount;
    data['awayDangerAttackCount'] = this.awayDangerAttackCount;
    data['homePossessionPercentage'] = this.homePossessionPercentage;
    data['awayPossessionPercentage'] = this.awayPossessionPercentage;
    data['homeShotsCount'] = this.homeShotsCount;
    data['awayShotsCount'] = this.awayShotsCount;
    data['homeShootRightCount'] = this.homeShootRightCount;
    data['awayShootRightCount'] = this.awayShootRightCount;
    data['awayRedCardCount'] = this.awayRedCardCount;
    data['homeRedCardCount'] = this.homeRedCardCount;
    data['homeYellowCardCount'] = this.homeYellowCardCount;
    data['awayYellowCardCount'] = this.awayYellowCardCount;
    data['homeName'] = this.homeName;
    data['awayName'] = this.awayName;
    data['homeNameEn'] = this.homeNameEn;
    data['awayNameEn'] = this.awayNameEn;
    data['homeLogo'] = this.homeLogo;
    data['awayLogo'] = this.awayLogo;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['homeCorner'] = this.homeCorner;
    data['awayCorner'] = this.awayCorner;

    data['home1'] = this.home1;
    data['home2'] = this.home2;
    data['home3'] = this.home3;
    data['home4'] = this.home4;
    data['homeOT'] = this.homeOT;
    data['away1'] = this.away1;
    data['away2'] = this.away2;
    data['away3'] = this.away3;
    data['away4'] = this.away4;
    data['awayOT'] = this.awayOT;

    data['homeThreePointScore'] = this.homeThreePointScore;
    data['homeFreeThrowScore'] = this.homeFreeThrowScore;
    data['homeFreeThrowHitScorePercentage'] = this.homeFreeThrowHitScorePercentage;
    data['homeScoreMax'] = this.homeScoreMax;
    data['homeReboundMax'] = this.homeReboundMax;
    data['homeAssistMax'] = this.homeAssistMax;
    data['awayThreePointScore'] = this.awayThreePointScore;
    data['awayFreeThrowScore'] = this.awayFreeThrowScore;
    data['awayFreeThrowHitScorePercentage'] = this.awayFreeThrowHitScorePercentage;
    data['awayScoreMax'] = this.awayScoreMax;
    data['awayReboundMax'] = this.awayReboundMax;
    data['awayAssistMax'] = this.awayAssistMax;

    if (this.eventList != null) {
      data['eventList'] = this.eventList!.map((v) => v.toJson()).toList();
    }
    if (this.textLiveList != null) {
      data['textLiveList'] = this.textLiveList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class EventList {
  String? nameChs;
  // 1：入球
  // 2：红牌
  // 3：黄牌
  // 7：点球
  // 8：乌龙
  // 9：两黄变红
  // 11：换人
  // 13：射失点球
  // 14：视频裁判（VR裁判）
  var kind;
  String? nameCht;
  String? overtime;
  bool? isHome;
  int? id;
  String? time;
  String? nameEn;
  String? playerId2;
  String? playerId;
  EventList(
      {this.nameChs,
        this.time,});

  EventList.fromJson(Map<String, dynamic> json) {
    nameChs = json['nameChs'];
    kind = json['kind'];
    nameCht = json['nameCht'];
    overtime = json['overtime'];
    isHome = json['isHome'];
    id = json['id'];
    time = json['time'];
    nameEn = json['nameEn'];
    playerId2 = json['playerId2'];
    playerId = json['playerId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nameChs'] = this.nameChs;
    data['kind'] = this.kind;
    data['nameCht'] = this.nameCht;
    data['overtime'] = this.overtime;
    data['isHome'] = this.isHome;
    data['id'] = this.id;
    data['time'] = this.time;
    data['nameEn'] = this.nameEn;
    data['playerId2'] = this.playerId2;
    data['playerId'] = this.playerId;
    return data;
  }
}

class TextLiveList {
  int? recordId;
  String? score;
  var kind;
  String? matchState;
  String? remainTime;
  int? matchId;
  String? content;

  TextLiveList.fromJson(Map<String, dynamic> json) {
    recordId = json['recordId'];
    score = json['score'];
    kind = json['kind'];
    matchState = json['matchState'];
    remainTime = json['remainTime'];
    matchId = json['matchId'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['recordId'] = this.recordId;
    data['score'] = this.score;
    data['nameCht'] = this.kind;
    data['matchState'] = this.matchState;
    data['remainTime'] = this.remainTime;
    data['matchId'] = this.matchId;
    data['content'] = this.content;
    return data;
  }
}

class AwayHistoryMatches {
  String? result;
  String? leagueChsShort;
  String? leagueEnShort;
  String? matchDate;
  String? awayName;
  var homeScore;
  var awayScore;
  String? homeName;
  String? homeNameEn;
  String? awayNameEn;

  AwayHistoryMatches.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    leagueChsShort = json['leagueChsShort'];
    leagueEnShort = json['leagueEnShort'];
    matchDate = json['matchDate'];
    awayName = json['awayName'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    homeName = json['homeName'];
    homeNameEn = json['homeNameEn'];
    awayNameEn = json['awayNameEn'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['leagueChsShort'] = this.leagueChsShort;
    data['leagueEnShort'] = this.leagueEnShort;
    data['matchDate'] = this.matchDate;
    data['awayName'] = this.awayName;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['homeName'] = this.homeName;
    data['homeNameEn'] = this.homeNameEn;
    data['awayNameEn'] = this.awayNameEn;
    return data;
  }
}

class StatArr {
  String? title;
  int? away;
  int? home;

  StatArr.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    away = json['away'];
    home = json['home'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['away'] = this.away;
    data['home'] = this.home;
    return data;
  }
}
