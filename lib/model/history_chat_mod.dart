class HistoryChatModal {
  String? msg;
  int? code;
  List<Data>? data;

  HistoryChatModal({this.msg, this.code, this.data});

  HistoryChatModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String? id;
  String? groupId;
  String? groupType;
  String? prefix;
  String? type;
  String? ref;
  String? random;
  String? msgTime;
  String? msgType;
  var msgContent;
  String? operatorAccount;
  String? fromAccount;
  String? nickName;
  var msgSeq;
  var available;
  String? createTime;
  String? ident;
  bool? isFace;
  bool? isFirstEnter;
  bool? isGift;
  var sortTime;

  Data({
    this.id,
    this.groupId,
    this.groupType,
    this.prefix,
    this.type,
    this.ref,
    this.random,
    this.msgTime,
    this.msgType,
    this.msgContent,
    this.operatorAccount,
    this.fromAccount,
    this.nickName,
    this.msgSeq,
    this.available,
    this.createTime,
    this.ident,
    this.isFace,
    this.isFirstEnter,
    this.isGift,
    this.sortTime,
  });

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    groupId = json['groupId'];
    groupType = json['groupType'];
    prefix = json['prefix'];
    type = json['type'];
    ref = json['ref'];
    random = json['random'];
    msgTime = json['msgTime'];
    msgType = json['msgType'];
    msgContent = json['msgContent'];
    operatorAccount = json['operatorAccount'];
    fromAccount = json['fromAccount'];
    nickName = json['nickName'];
    msgSeq = json['msgSeq'];
    available = json['available'];
    createTime = json['createTime'];
    sortTime = json['sortTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['groupId'] = this.groupId;
    data['groupType'] = this.groupType;
    data['prefix'] = this.prefix;
    data['type'] = this.type;
    data['ref'] = this.ref;
    data['random'] = this.random;
    data['msgTime'] = this.msgTime;
    data['msgType'] = this.msgType;
    data['msgContent'] = this.msgContent;
    data['operatorAccount'] = this.operatorAccount;
    data['fromAccount'] = this.fromAccount;
    data['nickName'] = this.nickName;
    data['msgSeq'] = this.msgSeq;
    data['available'] = this.available;
    data['createTime'] = this.createTime;
    data['sortTime'] = this.sortTime;
    return data;
  }
}
