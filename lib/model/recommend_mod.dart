import 'package:liveapp/model/anchor_event_anchorId_mod.dart';

class RecommendMod {
  String? msg;
  int? code;
  Data? data;

  RecommendMod({this.msg, this.code, this.data});

  RecommendMod.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? totalCount;
  int? pageSize;
  int? totalPage;
  int? currPage;
  List<AnchorList>? list;

  Data({this.totalCount, this.pageSize, this.totalPage, this.currPage, this.list});

  Data.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = <AnchorList>[];
      json['list'].forEach((v) {
        list!.add(new AnchorList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AnchorList {
  var id;
  String? matchDate;
  String? matchTime;
  String? matchDateTime;
  String? matchId;
  String? catId;
  String? catName;
  String? leagueName;
  String? homeName;
  String? awayName;
  var leagueNameEn;
  var homeNameEn;
  var awayNameEn;
  String? leagueLogo;
  String? homeLogo;
  String? awayLogo;
  String? homeScore;
  String? awayScore;
  int? resva;
  String? matchState;
  String? realTime;
  var state;
  List<PlayUrls>? playUrls;
  var animationUrl;
  var ancs;
  var vids;
  var bets;
  var scos;

  AnchorList(
      {this.id,
      this.matchDate,
      this.matchTime,
      this.matchDateTime,
      this.matchId,
      this.catId,
      this.catName,
      this.leagueName,
      this.homeName,
      this.awayName,
      this.leagueNameEn,
      this.homeNameEn,
      this.awayNameEn,
      this.leagueLogo,
      this.homeLogo,
      this.awayLogo,
      this.homeScore,
      this.awayScore,
      this.resva,
      this.matchState,
      this.realTime,
      this.state,
        this.playUrls,
        this.animationUrl,
      this.ancs,
      this.vids,
      this.bets,
      this.scos});

  AnchorList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    matchDate = json['matchDate'];
    matchTime = json['matchTime'];
    matchDateTime = json['matchDateTime'];
    matchId = json['matchId'];
    catId = json['catId'];
    catName = json['catName'];
    leagueName = json['leagueName'];
    homeName = json['homeName'];
    awayName = json['awayName'];
    leagueNameEn = json['leagueNameEn'];
    homeNameEn = json['homeNameEn'];
    awayNameEn = json['awayNameEn'];
    leagueLogo = json['leagueLogo'];
    homeLogo = json['homeLogo'];
    awayLogo = json['awayLogo'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    resva = json['resva'];
    matchState = json['matchState'];
    realTime = json['realTime'];
    state = json['state'];
    ancs = json['ancs'];
    vids = json['vids'];
    bets = json['bets'];
    scos = json['scos'];
    playUrls = json['playUrls'];
    animationUrl =  json['playUrls'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['matchDate'] = this.matchDate;
    data['matchTime'] = this.matchTime;
    data['matchDateTime'] = this.matchDateTime;
    data['matchId'] = this.matchId;
    data['catId'] = this.catId;
    data['catName'] = this.catName;
    data['leagueName'] = this.leagueName;
    data['homeName'] = this.homeName;
    data['awayName'] = this.awayName;
    data['leagueNameEn'] = this.leagueNameEn;
    data['homeNameEn'] = this.homeNameEn;
    data['awayNameEn'] = this.awayNameEn;
    data['leagueLogo'] = this.leagueLogo;
    data['homeLogo'] = this.homeLogo;
    data['awayLogo'] = this.awayLogo;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['resva'] = this.resva;
    data['matchState'] = this.matchState;
    data['realTime'] = this.realTime;
    data['state'] = this.state;
    data['ancs'] = this.ancs;
    data['vids'] = this.vids;
    data['bets'] = this.bets;
    data['scos'] = this.scos;
    data['playUrls'] = this.playUrls;
    data['animationUrl'] = this.animationUrl;
    return data;
  }
}
