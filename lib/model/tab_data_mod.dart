class DataMap {
  String val;
  bool? isShowLine = false;
  var key;
  List? contentList = [];
  DataMap({required this.val, required this.key, bool? this.isShowLine, this.contentList});
}
