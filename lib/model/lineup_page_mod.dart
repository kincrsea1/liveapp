class LineupPageModal {
  String? msg;
  int? code;
  Data? data;

  LineupPageModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  Home? home;
  Away? away;

  Data.fromJson(Map<String, dynamic> json) {
    home = json['home'] != null ? new Home.fromJson(json['home']) : null;
    away = json['away'] != null ? new Away.fromJson(json['away']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.home != null) {
      data['home'] = this.home!.toJson();
    }
    if (this.away != null) {
      data['away'] = this.away!.toJson();
    }
    return data;
  }
}

class Home {
  String? homeName;
  List<GrowableList>? backup;
  List<GrowableList>? lineup;
  int? homeId;

  Home.fromJson(Map<String, dynamic> json) {
    homeName = json['homeName'];
    if (json['backup'] != null) {
      backup = <GrowableList>[];
      json['backup'].forEach((v) {
        backup!.add(GrowableList.fromJson(v));
      });
    }
    if (json['lineup'] != null) {
      lineup = <GrowableList>[];
      json['lineup'].forEach((v) {
        lineup!.add(GrowableList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['homeName'] = this.homeName;
    if (this.backup != null) {
      data['backup'] = this.backup!.map((v) => v.toJson()).toList();
    }
    if (this.lineup != null) {
      data['lineup'] = this.lineup!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Away {
  String? awayName;
  List<GrowableList>? backup;
  List<GrowableList>? lineup;
  int? homeId;

  Away.fromJson(Map<String, dynamic> json) {
    awayName = json['awayName'];
    if (json['backup'] != null) {
      backup = <GrowableList>[];
      json['backup'].forEach((v) {
        backup!.add(GrowableList.fromJson(v));
      });
    }
    if (json['lineup'] != null) {
      lineup = <GrowableList>[];
      json['lineup'].forEach((v) {
        lineup!.add(GrowableList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['awayName'] = this.awayName;
    if (this.backup != null) {
      data['backup'] = this.backup!.map((v) => v.toJson()).toList();
    }
    if (this.lineup != null) {
      data['lineup'] = this.lineup!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
class GrowableList {
  String? number;
  String? name;
  var position;

  GrowableList.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    name = json['name'];
    position = json['position'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this.number;
    data['name'] = this.name;
    data['position'] = this.position;
    return data;
  }
}