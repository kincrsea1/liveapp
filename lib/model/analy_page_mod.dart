class AnalyPageModal {
  String? msg;
  int? code;

  // List<GrowableList>? data;
  Data? data;

  AnalyPageModal({this.msg, this.code, this.data});

  AnalyPageModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;

    // if (json['data'] != null) {
    //   data = <GrowableList>[];
    //   json['data'].forEach((v) {
    //     data!.add(GrowableList.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    // if (this.data != null) {
    //   data['data'] = this.data!.map((v) => v.toJson()).toList();
    // }
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  var leagueId;
  var homeTeamId;
  var awayTeamId;
  var homeName;
  var awayName;
  var homeLogo;
  var awayLogo;
  var homeResult;
  var awayResult;
  var homeNameEn;
  var awayNameEn;
  String? awayWinningPercentage;
  String? homeWinningPercentage;
  String? matchTime;
  List<HomeHistoryMatches>? homeHistoryMatches;
  List<HomeHistoryMatches>? awayHistoryMatches;

  Data.fromJson(Map<String, dynamic> json) {
    leagueId = json['leagueId'];
    homeTeamId = json['homeTeamId'];
    awayTeamId = json['awayTeamId'];
    homeName = json['homeName'];
    awayName = json['awayName'];
    homeLogo = json['homeLogo'];
    awayLogo = json['awayLogo'];
    homeResult = json['homeResult'];
    awayResult = json['awayResult'];
    homeNameEn = json['homeNameEn'];
    awayNameEn = json['awayNameEn'];
    homeWinningPercentage = json['homeWinningPercentage'];
    awayWinningPercentage = json['awayWinningPercentage'];
    matchTime = json['matchTime'];
    if (json['homeHistoryMatches'] != null) {
      homeHistoryMatches = <HomeHistoryMatches>[];
      json['homeHistoryMatches'].forEach((v) {
        homeHistoryMatches!.add(HomeHistoryMatches.fromJson(v));
      });
    }
    if (json['awayHistoryMatches'] != null) {
      awayHistoryMatches = <HomeHistoryMatches>[];
      json['awayHistoryMatches'].forEach((v) {
        awayHistoryMatches!.add(HomeHistoryMatches.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['leagueId'] = this.leagueId;
    data['homeTeamId'] = this.homeTeamId;
    data['awayTeamId'] = this.awayTeamId;
    data['homeName'] = this.homeName;
    data['awayName'] = this.awayName;
    data['homeLogo'] = this.homeLogo;
    data['awayLogo'] = this.awayLogo;
    data['homeResult'] = this.homeResult;
    data['awayResult'] = this.awayResult;
    data['homeNameEn'] = this.homeNameEn;
    data['awayNameEn'] = this.awayNameEn;
    data['homeWinningPercentage'] = this.homeWinningPercentage;
    data['awayWinningPercentage'] = this.awayWinningPercentage;
    data['matchTime'] = this.matchTime;

    if (this.homeHistoryMatches != null) {
      data['homeHistoryMatches'] = this.homeHistoryMatches!.map((v) => v.toJson()).toList();
    }
    if (this.awayHistoryMatches != null) {
      data['awayHistoryMatches'] = this.awayHistoryMatches!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeHistoryMatches {
  String? result;
  String? leagueChsShort;
  String? leagueEnShort;
  String? matchDate;
  String? awayName;
  var homeScore;
  var awayScore;
  String? homeName;
  String? homeNameEn;
  String? awayNameEn;

  HomeHistoryMatches.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    leagueChsShort = json['leagueChsShort'];
    leagueEnShort = json['leagueEnShort'];
    matchDate = json['matchDate'];
    awayName = json['awayName'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    homeName = json['homeName'];
    homeNameEn = json['homeNameEn'];
    awayNameEn = json['awayNameEn'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['leagueChsShort'] = this.leagueChsShort;
    data['leagueEnShort'] = this.leagueEnShort;
    data['matchDate'] = this.matchDate;
    data['awayName'] = this.awayName;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['homeName'] = this.homeName;
    data['homeNameEn'] = this.homeNameEn;
    data['awayNameEn'] = this.awayNameEn;
    return data;
  }
}

class AwayHistoryMatches {
  String? result;
  String? leagueChsShort;
  String? leagueEnShort;
  String? matchDate;
  String? awayName;
  var homeScore;
  var awayScore;
  String? homeName;
  String? homeNameEn;
  String? awayNameEn;

  AwayHistoryMatches.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    leagueChsShort = json['leagueChsShort'];
    leagueEnShort = json['leagueEnShort'];
    matchDate = json['matchDate'];
    awayName = json['awayName'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    homeName = json['homeName'];
    homeNameEn = json['homeNameEn'];
    awayNameEn = json['awayNameEn'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['leagueChsShort'] = this.leagueChsShort;
    data['leagueEnShort'] = this.leagueEnShort;
    data['matchDate'] = this.matchDate;
    data['awayName'] = this.awayName;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['homeName'] = this.homeName;
    data['homeNameEn'] = this.homeNameEn;
    data['awayNameEn'] = this.awayNameEn;
    return data;
  }
}
