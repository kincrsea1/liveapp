class ReserveModal {
  String? msg;
  int? code;
  Data? data;

  ReserveModal({this.msg, this.code, this.data});

  ReserveModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? totalCount;
  int? pageSize;
  int? totalPage;
  int? currPage;
  List<ReserveList>? list;

  Data({this.totalCount, this.pageSize, this.totalPage, this.currPage, this.list});

  Data.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = <ReserveList>[];
      json['list'].forEach((v) {
        list!.add(new ReserveList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReserveList {
  String? matchId;
  var roomId;
  String? catId;
  var userName;
  var matchScheduleId;
  String? matchDateTime;
  String? matchDate;
  String? matchTime;
  String? leagueName;
  var leagueNameEn;
  String? homeId;
  String? homeName;
  var homeNameEn;
  String? awayId;
  String? awayName;
  var awayNameEn;
  String? homeScore;
  String? awayScore;
  String? matchState;
  var roomName;
  var qtId;
  var userLogo;
  String? groupId;
  List<PlayUrls>? playUrls;
  String? type;
  var awayLogo;
  var homeLogo;
  var resva;
  var userId;
  var online;
  var betEventInfo;

  ReserveList(
      {this.matchId,
      this.roomId,
      this.catId,
      this.userName,
      this.matchScheduleId,
      this.matchDateTime,
      this.matchDate,
      this.matchTime,
      this.leagueName,
      this.leagueNameEn,
      this.homeId,
      this.homeName,
      this.homeNameEn,
      this.awayId,
      this.awayName,
      this.awayNameEn,
      this.homeScore,
      this.awayScore,
      this.matchState,
      this.roomName,
      this.qtId,
      this.userLogo,
      this.groupId,
      this.playUrls,
      this.type,
      this.awayLogo,
      this.homeLogo,
      this.resva,
      this.userId,
      this.online,
      this.betEventInfo});

  ReserveList.fromJson(Map<String, dynamic> json) {
    matchId = json['matchId'];
    roomId = json['roomId'];
    catId = json['catId'];
    userName = json['userName'];
    matchScheduleId = json['matchScheduleId'];
    matchDateTime = json['matchDateTime'];
    matchDate = json['matchDate'];
    matchTime = json['matchTime'];
    leagueName = json['leagueName'];
    leagueNameEn = json['leagueNameEn'];
    homeId = json['homeId'];
    homeName = json['homeName'];
    homeNameEn = json['homeNameEn'];
    awayId = json['awayId'];
    awayName = json['awayName'];
    awayNameEn = json['awayNameEn'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    matchState = json['matchState'];
    roomName = json['roomName'];
    qtId = json['qtId'];
    userLogo = json['userLogo'];
    groupId = json['groupId'];
    if (json['playUrls'] != null) {
      playUrls = <PlayUrls>[];
      json['playUrls'].forEach((v) {
        playUrls!.add(new PlayUrls.fromJson(v));
      });
    }
    type = json['type'];
    awayLogo = json['awayLogo'];
    homeLogo = json['homeLogo'];
    resva = json['resva'];
    userId = json['userId'];
    online = json['online'];
    betEventInfo = json['betEventInfo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['matchId'] = this.matchId;
    data['roomId'] = this.roomId;
    data['catId'] = this.catId;
    data['userName'] = this.userName;
    data['matchScheduleId'] = this.matchScheduleId;
    data['matchDateTime'] = this.matchDateTime;
    data['matchDate'] = this.matchDate;
    data['matchTime'] = this.matchTime;
    data['leagueName'] = this.leagueName;
    data['leagueNameEn'] = this.leagueNameEn;
    data['homeId'] = this.homeId;
    data['homeName'] = this.homeName;
    data['homeNameEn'] = this.homeNameEn;
    data['awayId'] = this.awayId;
    data['awayName'] = this.awayName;
    data['awayNameEn'] = this.awayNameEn;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['matchState'] = this.matchState;
    data['roomName'] = this.roomName;
    data['qtId'] = this.qtId;
    data['userLogo'] = this.userLogo;
    data['groupId'] = this.groupId;
    if (this.playUrls != null) {
      data['playUrls'] = this.playUrls!.map((v) => v.toJson()).toList();
    }
    data['type'] = this.type;
    data['awayLogo'] = this.awayLogo;
    data['homeLogo'] = this.homeLogo;
    data['resva'] = this.resva;
    data['userId'] = this.userId;
    data['online'] = this.online;
    data['betEventInfo'] = this.betEventInfo;
    return data;
  }
}

class PlayUrls {
  String? id;
  var type;
  int? lang;
  String? name;
  String? streamImgUrl;
  var isFullPlayUrl;

  PlayUrls({this.id, this.type, this.lang, this.name, this.streamImgUrl, this.isFullPlayUrl});

  PlayUrls.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    lang = json['lang'];
    name = json['name'];
    streamImgUrl = json['streamImgUrl'];
    isFullPlayUrl = json['isFullPlayUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['lang'] = this.lang;
    data['name'] = this.name;
    data['streamImgUrl'] = this.streamImgUrl;
    data['isFullPlayUrl'] = this.isFullPlayUrl;
    return data;
  }
}
