class FollowListMod {
  int? userId;
  var roomId;
  var roomName;
  String? username;
  var expiredTime;
  var createTime;
  String? nickName;
  var password;
  var email;
  var mobile;
  var salt;
  var createUserId;
  String? description;
  var status;
  var userLogo;
  String? fakeState;
  int? follow;
  var roleId;
  var roleName;

  FollowListMod(
      {this.userId,
      this.roomId,
      this.roomName,
      this.username,
      this.expiredTime,
      this.createTime,
      this.nickName,
      this.password,
      this.email,
      this.mobile,
      this.salt,
      this.createUserId,
      this.description,
      this.status,
      this.userLogo,
      this.fakeState,
      this.follow,
      this.roleId,
      this.roleName});

  FollowListMod.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    roomId = json['roomId'];
    roomName = json['roomName'];
    username = json['username'];
    expiredTime = json['expiredTime'];
    createTime = json['createTime'];
    nickName = json['nickName'];
    password = json['password'];
    email = json['email'];
    mobile = json['mobile'];
    salt = json['salt'];
    createUserId = json['createUserId'];
    description = json['description'];
    status = json['status'];
    userLogo = json['userLogo'];
    follow = json['follow'];
    roleId = json['roleId'];
    fakeState = json['fakeState'];
    roleName = json['roleName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['roomId'] = this.roomId;
    data['roomName'] = this.roomName;
    data['username'] = this.username;
    data['expiredTime'] = this.expiredTime;
    data['createTime'] = this.createTime;
    data['nickName'] = this.nickName;
    data['password'] = this.password;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['salt'] = this.salt;
    data['createUserId'] = this.createUserId;
    data['description'] = this.description;
    data['status'] = this.status;
    data['userLogo'] = this.userLogo;
    data['follow'] = this.follow;
    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    data['fakeState'] = this.fakeState;
    return data;
  }
}
