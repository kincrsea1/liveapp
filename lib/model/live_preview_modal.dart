class LivePreviewModal {
  String? msg;
  int? code;
  Data? data;

  LivePreviewModal({this.msg, this.code, this.data});

  LivePreviewModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? totalCount;
  int? pageSize;
  int? totalPage;
  int? currPage;
  List<PrevList>? list;

  Data({this.totalCount, this.pageSize, this.totalPage, this.currPage, this.list});

  Data.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    pageSize = json['pageSize'];
    totalPage = json['totalPage'];
    currPage = json['currPage'];
    if (json['list'] != null) {
      list = <PrevList>[];
      json['list'].forEach((v) {
        list!.add(new PrevList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    data['pageSize'] = this.pageSize;
    data['totalPage'] = this.totalPage;
    data['currPage'] = this.currPage;
    if (this.list != null) {
      data['list'] = this.list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PrevList {
  String? matchId;
  String? roomId;
  String? catId;
  var userName;
  var matchScheduleId;
  String? matchDateTime;
  String? matchDate;
  String? matchTime;
  String? leagueName;
  var leagueNameEn;
  String? homeId;
  String? homeName;
  var homeNameEn;
  String? awayId;
  String? awayName;
  var awayNameEn;
  var homeScore;
  var awayScore;
  String? matchState;
  var roomName;
  var qtId;
  var userLogo;
  var groupId;
  var playUrls;
  var type;
  String? awayLogo;
  String? homeLogo;
  var resva;
  var userId;
  var online;
  var betEventInfo;

  PrevList(
      {this.matchId,
      this.roomId,
      this.catId,
      this.userName,
      this.matchScheduleId,
      this.matchDateTime,
      this.matchDate,
      this.matchTime,
      this.leagueName,
      this.leagueNameEn,
      this.homeId,
      this.homeName,
      this.homeNameEn,
      this.awayId,
      this.awayName,
      this.awayNameEn,
      this.homeScore,
      this.awayScore,
      this.matchState,
      this.roomName,
      this.qtId,
      this.userLogo,
      this.groupId,
      this.playUrls,
      this.type,
      this.awayLogo,
      this.homeLogo,
      this.resva,
      this.userId,
      this.online,
      this.betEventInfo});

  PrevList.fromJson(Map<String, dynamic> json) {
    matchId = json['matchId'];
    roomId = json['roomId'];
    catId = json['catId'];
    userName = json['userName'];
    matchScheduleId = json['matchScheduleId'];
    matchDateTime = json['matchDateTime'];
    matchDate = json['matchDate'];
    matchTime = json['matchTime'];
    leagueName = json['leagueName'];
    leagueNameEn = json['leagueNameEn'];
    homeId = json['homeId'];
    homeName = json['homeName'];
    homeNameEn = json['homeNameEn'];
    awayId = json['awayId'];
    awayName = json['awayName'];
    awayNameEn = json['awayNameEn'];
    homeScore = json['homeScore'];
    awayScore = json['awayScore'];
    matchState = json['matchState'];
    roomName = json['roomName'];
    qtId = json['qtId'];
    userLogo = json['userLogo'];
    groupId = json['groupId'];
    playUrls = json['playUrls'];
    type = json['type'];
    awayLogo = json['awayLogo'];
    homeLogo = json['homeLogo'];
    resva = json['resva'];
    userId = json['userId'];
    online = json['online'];
    betEventInfo = json['betEventInfo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['matchId'] = this.matchId;
    data['roomId'] = this.roomId;
    data['catId'] = this.catId;
    data['userName'] = this.userName;
    data['matchScheduleId'] = this.matchScheduleId;
    data['matchDateTime'] = this.matchDateTime;
    data['matchDate'] = this.matchDate;
    data['matchTime'] = this.matchTime;
    data['leagueName'] = this.leagueName;
    data['leagueNameEn'] = this.leagueNameEn;
    data['homeId'] = this.homeId;
    data['homeName'] = this.homeName;
    data['homeNameEn'] = this.homeNameEn;
    data['awayId'] = this.awayId;
    data['awayName'] = this.awayName;
    data['awayNameEn'] = this.awayNameEn;
    data['homeScore'] = this.homeScore;
    data['awayScore'] = this.awayScore;
    data['matchState'] = this.matchState;
    data['roomName'] = this.roomName;
    data['qtId'] = this.qtId;
    data['userLogo'] = this.userLogo;
    data['groupId'] = this.groupId;
    data['playUrls'] = this.playUrls;
    data['type'] = this.type;
    data['awayLogo'] = this.awayLogo;
    data['homeLogo'] = this.homeLogo;
    data['resva'] = this.resva;
    data['userId'] = this.userId;
    data['online'] = this.online;
    data['betEventInfo'] = this.betEventInfo;
    return data;
  }
}
