class OutsPageModal {
  String? msg;
  int? code;
  Data? data;

  OutsPageModal({this.msg, this.code, this.data});

  OutsPageModal.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  var homeTeamName;
  var awayTeamName;
  var homeTeamLogo;
  var awayTeamLogo;

  List<PlayerScoresList>? homePlayerScores;
  List<PlayerScoresList>? awayPlayerScores;
  List<GrowableList>? statArr;

  Data.fromJson(Map<String, dynamic> json) {
    homeTeamName = json['homeTeamName'];
    awayTeamName = json['awayTeamName'];
    homeTeamLogo = json['homeTeamLogo'];
    awayTeamLogo = json['awayTeamLogo'];
    // homePlayerScores = json['homePlayerScores'];
    // awayPlayerScores = json['awayPlayerScores'];
    if (json['homePlayerScores'] != null) {
      homePlayerScores = <PlayerScoresList>[];
      json['homePlayerScores'].forEach((v) {
        homePlayerScores!.add(PlayerScoresList.fromJson(v));
      });
    }
    if (json['awayPlayerScores'] != null) {
      awayPlayerScores = <PlayerScoresList>[];
      json['awayPlayerScores'].forEach((v) {
        awayPlayerScores!.add(PlayerScoresList.fromJson(v));
      });
    }
    if (json['statArr'] != null) {
      statArr = <GrowableList>[];
      json['statArr'].forEach((v) {
        statArr!.add(GrowableList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['homeTeamName'] = this.homeTeamName;
    data['awayTeamName'] = this.awayTeamName;
    data['homeTeamLogo'] = this.homeTeamLogo;
    data['awayTeamLogo'] = this.awayTeamLogo;
    // data['homePlayerScores'] = this.homePlayerScores;
    // data['awayPlayerScores'] = this.awayPlayerScores;
    if (this.statArr != null) {
      data['statArr'] = this.statArr!.map((v) => v.toJson()).toList();
    }
    if (this.homePlayerScores != null) {
      data['homePlayerScores'] = this.homePlayerScores!.map((v) => v.toJson()).toList();
    }
    if (this.awayPlayerScores != null) {
      data['awayPlayerScores'] = this.awayPlayerScores!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GrowableList {
  int? away;
  String? title;
  int? home;

  GrowableList.fromJson(Map<String, dynamic> json) {
    away = json['away'];
    title = json['title'];
    home = json['home'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['away'] = this.away;
    data['title'] = this.title;
    data['home'] = this.home;
    return data;
  }
}

class PlayerScoresList {
  String? playerNumber;
  String? playerName;
  int? playerScore;

  PlayerScoresList.fromJson(Map<String, dynamic> json) {
    playerScore = json['playerScore'];
    playerName = json['playerName'];
    playerNumber = json['playerNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['playerScore'] = this.playerScore;
    data['playerName'] = this.playerName;
    data['playerNumber'] = this.playerNumber;
    return data;
  }
}