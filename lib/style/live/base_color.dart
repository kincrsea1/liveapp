import 'package:flutter/material.dart';

/// @describe: 颜色管理
class AppColors {
  Color c_w = Color(0XFFFAF7F5); //背景色
  Color c_bl = Color(0xFF000000); //黑色
  Color c_white = Color(0XFFFFFFFF); //白色
  Color c_o = Color(0XFFF36704); //主色调
  Color c_3 = Color(0XFF333333); //文字主色调-加重
  Color c_6 = Color(0XFF666666); //文字主色调
  Color c_9 = Color(0XFF999999); //次要辅助色
  Color c_b1 = Color(0XFFF1E1DA); // 主分割线颜色
  Color c_border = Color(0XFFFFFAF7); //次分割线颜色
  Color c_darkborder = Color(0XFF99590B); //次分割线颜色 深棕色


  ///字符串转16进制颜色
  static Color getColorForHex(String hex) {
    String ColorStr = hex;
    if (hex.contains('0x') || hex.contains('#')) {
      ColorStr = hex.replaceAll("0x", "");
      ColorStr = hex.replaceAll("#", "");
    }
    return Color(int.parse('0xff' + ColorStr));
  }

//- 渐变色配置
  // LinearGradient gradient1 = LinearGradient(colors: [Color(0XFFEEE0BD), Color(0XFFD4C8A9), Color(0XFFC4B593)]);
  // LinearGradient gradient2 = LinearGradient(colors: [Color(0XFF3C3E41), Color(0XFF37373A)]);
}
