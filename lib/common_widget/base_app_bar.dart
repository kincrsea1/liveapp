import 'package:flutter/material.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget title;
  final String? iconUrl;
  dynamic params;
  final Widget? rightContent;
  bool isShowBackIcon = true;

  BaseAppBar({
    required this.title,
    this.iconUrl,
    this.params,
    this.rightContent,
    required this.isShowBackIcon,
  });

  @override
  Size get preferredSize {
    return Size.fromHeight(55.0);
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: isShowBackIcon
          ? GestureDetector(
              child: Icon(Icons.arrow_back_ios, color: Color(0xff666666)),
              onTap: () => Navigator.of(context).pop(params),
            )
          : null,
      elevation: 0,
      title: title,
      centerTitle: isShowBackIcon,
      backgroundColor: Color(0XFFFFFFFF),
      automaticallyImplyLeading: isShowBackIcon,
      actions: [if (rightContent != null) rightContent as Widget],
    );
  }
}
