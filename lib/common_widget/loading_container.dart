// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:flutter/material.dart';
import 'package:m_loading/m_loading.dart';

class LoadingContainer extends StatelessWidget {
  final txt;
  const LoadingContainer({Key? key, String? this.txt}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 40,
            width: 40,
            child: BallCircleOpacityLoading(
              ballStyle: BallStyle(
                size: 5,
                color: Color(0xff666666),
                ballType: BallType.solid,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: Text(txt ?? '正在加载中...', style: TextStyle(color: Color(0xff666666), fontSize: 14)),
          )
        ],
      ),
    );
  }
}
