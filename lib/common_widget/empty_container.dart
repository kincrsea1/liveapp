import 'package:flutter/material.dart';
import 'package:liveapp/config/image/local_image_selector.dart';

class EmptyContainer extends StatelessWidget {
  final String txt;
  EmptyContainer({Key? key, required this.txt}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LocalImageSelector.getSingleImage('common_empty_icon', imageWidth: 110, imageHeight: 100),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              txt,
              style: TextStyle(fontSize: 14, color: Color(0xff999999)),
            ),
          )
        ],
      ),
    );
  }
}
