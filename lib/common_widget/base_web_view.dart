// @dart=2.9
// ignore_for_file: file_names, non_constant_identifier_names, prefer_is_empty, prefer_const_constructors

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:liveapp/common_widget/loading_container.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../config/image/local_image_selector.dart';


class BaseWebView extends StatefulWidget {
  String title;
  String baseUrl;
  String htmlContent;
  Map<String, String> mapData;
  @override
  _SdyFlutterWebViewState createState() => _SdyFlutterWebViewState();

  BaseWebView(this.title,
      {this.baseUrl, this.htmlContent = '',  this.mapData});
}

class _SdyFlutterWebViewState extends State<BaseWebView> {
  bool _loading = true;
  Offset offsetA = Offset(10, kToolbarHeight + 10); //按钮的初始位置
  Map<String, String> mapData = new Map();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    if (widget.mapData != null && widget.mapData.length != 0) {
      mapData.addAll(widget.mapData);
    }
    final width = size.width;
    final height = size.height;

    Uri uri = Uri.dataFromString(widget.htmlContent, mimeType: 'text/html', encoding: Encoding.getByName('utf-8'));

    var contentBase64 = base64Encode(const Utf8Encoder().convert("""<!DOCTYPE html>
    <html>
      <head><meta name="viewport" http-equiv="Content-Type"  content="width=device-width, initial-scale=1.0,text/html,charset=utf-8;"></head>
     <body style="background-color:black;">
        <div>
          ${widget.htmlContent}
        </div>
      </body>
    </html>"""));
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: widget.title.isNotEmpty
          ? AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color:Color(0xFF333333) ,
            onPressed: () {
              if (mapData != null && mapData.length != 0) {
                Navigator.pop(context, mapData['isGo']);
              } else {
                Navigator.of(context).pop();
              }
            }),
        backgroundColor: Color(0XFFFAF7F5),
        title: Text(widget.title,style: TextStyle(color: Color(0xFF333333), fontSize: 18)),
        centerTitle: true,
      )
          : null,
      body: Stack(children: [
        Positioned(
          child: WebView(
            initialUrl: widget.htmlContent.length > 0 ? 'data:text/html;base64,$contentBase64' : widget.baseUrl,
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (url) {
              _loading = false;
              Future.delayed(Duration(seconds: 2), () {
                setState(() {});
              });
            },
          ),
        ),
        (_loading == false)
            ? Container()
            : LoadingContainer(),
      ]),
    );
  }

}
