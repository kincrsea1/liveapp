import 'package:liveapp/model/reserve_list_mod.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';

//- 获取用户预约列表
Future listResvaEvent(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/listResvaEvent?terminal=1", method: DioMethod.post, data: params);
  if (result.code != 0 || result.data['code'] != 0) return;
  ReserveModal reseveData = ReserveModal.fromJson(result.data);
  return reseveData.data;
}

//- 预约当前比赛
Future resva(params) async {
  DioResponse result = await DioUtil().request("/splive/app/resva?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return;
}

//- 取消预约比赛
Future cancleResva(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/cancleResva?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
  return true;
}

//- 用户主播关注列表
Future listFollow(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/listFollow?terminal=1", method: DioMethod.post, data: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
  return result.data['data']['list'];
}

//- 是否关注了当前直播间主播
Future isFollow(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/isFollow?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return null;
  return result.data;
}

//- 关注主播
Future follow(params) async {
  DioResponse result = await DioUtil().request("/splive/app/follow?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
  return result.data;
}

//- 取消关注主播
Future cancleFollow(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/cancleFollow?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
  return result.data;
}

//- 更新直播间人数
Future updateAnchorTotal(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/updateAnchorTotal?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
}
