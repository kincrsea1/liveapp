import 'package:liveapp/model/anchor_event_anchorId_mod.dart';
import 'package:liveapp/model/anchor_search_mod.dart';
import 'package:liveapp/model/anchor_video_list_mod.dart';
import 'package:liveapp/model/banner_mod.dart';
import 'package:liveapp/model/live_preview_modal.dart';
import 'package:liveapp/model/match_list_mod.dart';
import 'package:liveapp/model/recommend_mod.dart';
import 'package:liveapp/model/room_mod.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';

//- 获取主播赛事列表
Future listMiqEvent(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/listMiqAnchorEvent?terminal=1", method: DioMethod.post, data: params);
  if (result.code != 0 || result.data['code'] != 0) return;
  AnchorVideoListModal anchorData = AnchorVideoListModal.fromJson(result.data);
  return anchorData.data;
}

//- 页面轮播图获取
Future getBannerList() async {
  DioResponse result = await DioUtil().request("/api/sys/indexNoticeAndAdv",
      method: DioMethod.get, params: {'terminal': 1, 'pageNo': 1, 'pageSize': 100, 'advType': 1, 'evebNum': 1});
  if (result.code != 0 || result.data['code'] != 0) return;

  BannerMod bannerData = BannerMod.fromJson(result.data);
  return bannerData.page;
}

//- 获取主播关注数量
Future getFollowOfTotalByUserId(params) async {
  DioResponse result = await DioUtil().request("/splive/app/getFollowOfTotalByUserId", method: DioMethod.post, params: {
    'userId': params['userId'],
    'terminal': 1,
    'fakeState': params['fakeState'],
  });
  if (result.code != 0 || result.data['code'] != 0) return;
  return result.data['followTotal'];
}

//- 主播赛事通过主播id查询
Future listMiqAnchorEventByAnchorId(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/listMiqAnchorEventByAnchorId", method: DioMethod.get, params: params);
  if (result.code != 0 || result.data['code'] != 0) return;
  ListMiqAnchorEventByAnchorIdModal anchorData = ListMiqAnchorEventByAnchorIdModal.fromJson(result.data);
  return anchorData.data;
}

//- 查询主播详情
Future getAnchorDetails(params) async {
  DioResponse result = await DioUtil().request("/splive/app/getAnchorDetails",
      method: DioMethod.post, params: {'userId': params['userId'], 'fakeState': params['fakeState'], 'terminal': 1});
  if (result.code != 0 || result.data['code'] != 0) return;
  RoomMod roomData = RoomMod.fromJson(result.data);
  return roomData.data;
}

//- 主播直播预告
Future listMiqAnchorLivePreview(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/listMiqAnchorLivePreview?terminal=1", method: DioMethod.post, data: params);
  if (result.code != 0 || result.data['code'] != 0) return;
  LivePreviewModal liveData = LivePreviewModal.fromJson(result.data);
  return liveData.data;
}

//-米奇主播相关视频
Future listMiqAnchorRelatedVideos(params) async {
  DioResponse result = await DioUtil().request(
    "/splive/app/listMiqAnchorRelatedVideos?terminal=1",
    method: DioMethod.post,
    data: params,
  );
  if (result.code != 0 || result.data['code'] != 0) return;
  LivePreviewModal liveData = LivePreviewModal.fromJson(result.data);
  return liveData.data;
}

//- 主播赛事列表
Future listMiqAllEvent(params) async {
  DioResponse result = await DioUtil().request(
    "/splive/app/listMiqAllEvent?terminal=1",
    method: DioMethod.post,
    data: params,
  );
  if (result.code != 0 || result.data['code'] != 0) return;
  MatchListModal liveData = MatchListModal.fromJson(result.data);
  return liveData.data;
}

//- 主播列表查询
Future listMiqAnchor(params) async {
  DioResponse result = await DioUtil().request(
    "/splive/app/listMiqAnchor?terminal=1",
    method: DioMethod.post,
    data: params,
  );
  if (result.code != 0 || result.data['code'] != 0) return;
  AnchorSearchMod anchorData = AnchorSearchMod.fromJson(result.data);
  return anchorData.data;
}

//- 推荐比赛列表
Future getReserveLiveEventList(params) async {
  DioResponse result = await DioUtil().request(
    "/splive/app/getReserveLiveEventList?terminal=1",
    method: DioMethod.post,
    data: params,
  );
  if (result.code != 0 || result.data['code'] != 0) return;
  RecommendMod anchorData = RecommendMod.fromJson(result.data);
  return anchorData.data;
}
