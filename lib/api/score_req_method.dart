// @dart=2.9
//- 主播赛事通过主播id查询
import 'dart:developer';

import 'package:liveapp/config/date/date_utils.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';
import 'package:liveapp/pages/live/home/component/a_score/scroe_detail_page/mo/score_home_model.dart';

///获取赛程 赛果数据
const String GetSchedule = '/splive/clean/live/getSchedule';

///联赛筛选列表接口
const String GetLeagueList = '/splive/clean/live/getLeagueList';

///联赛筛选列表接口
const String GetMatchScheduleByMatchId = '/splive/app/getMatchScheduleByMatchId';

///获取赛程 赛果数据  /日期,格式为yyyy-MM-dd    /体育类型 0足球 1篮球         /赛程类型 0-全部 1-进行中 2-赛程 3-赛果
Future scoreMatchData({String date, String type, String scheduleType, int currPage, List<dynamic> scheduleName}) async {
  // DioUtil.getInstance()?.openLog();
  // String todayTime =  DateFormatUtils.getNowDateTimeFormat(DateFormatUtils.PARAM_TIMEZN_FORMAT);

  Map<String, dynamic> params = {
    'date': date,
    'type': type,
    'scheduleType': scheduleType,
    'currPage': currPage.toString(),
  };
  if (scheduleName.length > 0) params.putIfAbsent('scheduleName', () => scheduleName);

  DioResponse result = await DioUtil().request(GetSchedule, method: DioMethod.get, params: params);

  if (result.code != 0 || result.data['code'] != 0) return;
  getScheduleData anchorData = getScheduleData.fromJson(result.data['data']);
  return anchorData.list;
}

///联赛筛选列表接口
Future getLeagueList({String date, String catId}) async {
  // DioUtil.getInstance()?.openLog();
  // String todayTime =  DateFormatUtils.getNowDateTimeFormat(DateFormatUtils.PARAM_TIMEZN_FORMAT);
  DioResponse result = await DioUtil().request(GetLeagueList, method: DioMethod.get, params: {
    'date': date,
    'catId': catId,
  });
  log('38行打印：============ ${result.data}');
  if (result.code != 0 || result.data['code'] != 0) return;
  Map<String, dynamic> LeagueList = Map.from(result.data['data']);
  return LeagueList;
}

///联赛筛选列表接口
Future getMatchScheduleByMatchId({String matchId, String catId}) async {
  DioResponse result = await DioUtil().request(GetMatchScheduleByMatchId, method: DioMethod.get, params: {
    'matchId': matchId,
    'type': '3',
    'matchType': '20',
    'catId': catId,
  });
  log('60行打印：============ ${result.data}');
  if (result.code != 0 || result.data['code'] != 0) return;
  if (result.data['data'] == '') return;
  Map<String, dynamic> MatchMap = Map.from(result.data['data']);
  return MatchMap;
}
