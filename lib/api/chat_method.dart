import 'package:liveapp/model/history_chat_mod.dart';
import 'package:liveapp/network/dio_util/dio_method.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/dio_util.dart';

//- 获取用户预约列表
Future getSdkAppId() async {
  DioResponse result = await DioUtil().request("/splive/im/getSdkAppId?terminal=1", method: DioMethod.get);
  if (result.code != 0 || result.data['code'] != 0) return;
  return result.data['imSdkAppID'];
}

//- 检测用户群组
Future checkGroup(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/checkGroup?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return {};
  return result.data;
}

//- 获取用户签名
Future getUserSig(params) async {
  DioResponse result =
      await DioUtil().request("/splive/app/getUserSig?terminal=1", method: DioMethod.get, params: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
  return result.data['userSig'] ?? '';
}

//- 获取当前聊天室历史聊天信息
Future getHistoryChatList(params) async {
  DioResponse result =
      await DioUtil().request("/splive/im/listChatLog/?terminal=1", method: DioMethod.post, data: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
  HistoryChatModal res = HistoryChatModal.fromJson(result.data);
  return res.data;
}

//- 获取聊天室敏感词汇
Future getSensitiveWordsReq() async {
  DioResponse result = await DioUtil().request("/splive/app/getSensitiveWords/?terminal=1", method: DioMethod.post);
  if (result.code != 0 || result.data['code'] != 0) return false;
  return result.data['data'];
}

//- 获取禁言列表啊
Future getGroupShut(params) async {
  DioResponse result =
      await DioUtil().request("/splive/im/getGroupShut/?terminal=1", method: DioMethod.post, params: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
}

//-进群发送用户账号信息接口 (二期)
Future addAccount(params) async {
  DioResponse result =
      await DioUtil().request("/splive/im/addAccount/?terminal=1", method: DioMethod.post, data: params);
  if (result.code != 0 || result.data['code'] != 0) return false;
}

//-聊天室是否可以发送外部链接
Future getOutsideLinkAllow() async {
  DioResponse result = await DioUtil().request("/splive/app/getOutsideLinkAllow/?terminal=1", method: DioMethod.post);
  if (result.code != 0 || result.data['code'] != 0) return false;
  return result.data['data'];
}
