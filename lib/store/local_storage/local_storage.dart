import 'package:localstore/localstore.dart';

const DBNAME = 'live';

class localStorage {
  //- 设置集合
  static setItem(String key, dynamic val) {
    final db = Localstore.instance;
    db.collection(DBNAME).doc(key).set(val);
  }

  //- 获取集合
  static getItem(String key) async {
    final db = Localstore.instance;
    return await db.collection(DBNAME).doc(key).get();
  }

  //- 删除信息
  static deleteItem(String key) async {
    final db = Localstore.instance;
    return await db.collection(DBNAME).doc(key).delete();
  }
}
