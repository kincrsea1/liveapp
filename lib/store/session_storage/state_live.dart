import 'package:liveapp/model/anchor_video_list_mod.dart';
import 'package:redux/redux.dart';
import 'package:liveapp/model/anchor_event_anchorId_mod.dart' as ListMiqAnchorEventByAnchorIdMod;

class LiveState {
  bool isMiniPlay; //- 小窗口开发
  ListMiqAnchorEventByAnchorIdMod.Data roomInfo;
  String floatInfo;
  bool isGoLiveDetail;
  AnchorList videoDetail;

  LiveState({
    required this.isMiniPlay, //- 主播播放状态
    required this.roomInfo, //- 直播间基础信息
    required this.floatInfo,
    required this.isGoLiveDetail,
    required this.videoDetail,
  });

  static initialState() => LiveState(
        isMiniPlay: false,
        roomInfo: ListMiqAnchorEventByAnchorIdMod.Data(),
        floatInfo: '',
        isGoLiveDetail: false,
        videoDetail: AnchorList(),
      );
}

Function liveReducer = combineReducers<LiveState>(
  [
    TypedReducer<LiveState, UpdateIsMiniPlay>(UpdateIsMiniPlay.handler),
    TypedReducer<LiveState, SaveRoomInfo>(SaveRoomInfo.handler),
    TypedReducer<LiveState, SaveFloatVideo>(SaveFloatVideo.handler),
    TypedReducer<LiveState, UpdateIsGoLiveDetail>(UpdateIsGoLiveDetail.handler),
    TypedReducer<LiveState, saveVideoDetail>(saveVideoDetail.handler),
  ],
);

//- 播放状态改变
class UpdateIsMiniPlay {
  final Map payload;
  UpdateIsMiniPlay({required this.payload});
  static LiveState handler(LiveState state, UpdateIsMiniPlay action) {
    print('44行打印：============ ${action.payload['isMiniPlay']}');
    state.isMiniPlay = action.payload['isMiniPlay'];
    return state;
  }
}

//- 保存聊天信息
class SaveRoomInfo {
  final Map payload;
  SaveRoomInfo({required this.payload});
  static LiveState handler(LiveState state, SaveRoomInfo action) {
    state.roomInfo = action.payload['roomInfo'];
    return state;
  }
}

//- 保存浮动视频信息
class SaveFloatVideo {
  final Map payload;
  SaveFloatVideo({required this.payload});
  static LiveState handler(LiveState state, SaveFloatVideo action) {
    state.floatInfo = action.payload['floatInfo'];
    return state;
  }
}

//- 强制跳转详情页面
class UpdateIsGoLiveDetail {
  final Map payload;
  UpdateIsGoLiveDetail({required this.payload});
  static LiveState handler(LiveState state, UpdateIsGoLiveDetail action) {
    state.isGoLiveDetail = action.payload['isGoLiveDetail'];
    return state;
  }
}

//- 存储当前聊天室信息
class saveVideoDetail {
  final Map payload;
  saveVideoDetail({required this.payload});
  static LiveState handler(LiveState state, saveVideoDetail action) {
    state.videoDetail = action.payload['videoDetail'];
    return state;
  }
}
