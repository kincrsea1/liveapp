import 'package:redux/redux.dart';
import 'package:liveapp/model/history_chat_mod.dart' as HistoryListMod;

class ChatState {
  List<HistoryListMod.Data> historyList;
  int outsideLinkAllow;
  int tabBarIndex;
  bool charDisable; //- 直播间是否为禁言状态
  List welcomeListArr; //- 进入直播间用户列表
  bool isShowWelcomeInfo;

  ChatState({
    required this.historyList,
    required this.outsideLinkAllow,
    required this.tabBarIndex,
    required this.charDisable,
    required this.welcomeListArr,
    required this.isShowWelcomeInfo,
  });

  static initialState() => ChatState(
        historyList: [],
        outsideLinkAllow: 1,
        tabBarIndex: 1,
        charDisable: false,
        welcomeListArr: [],
        isShowWelcomeInfo: false,
      );
}

Function chatReducer = combineReducers<ChatState>(
  [
    TypedReducer<ChatState, SaveHistoryList>(SaveHistoryList.handler),
    TypedReducer<ChatState, SetOutsideLinkAllow>(SetOutsideLinkAllow.handler),
    TypedReducer<ChatState, SaveTabBarIndex>(SaveTabBarIndex.handler),
    TypedReducer<ChatState, UpdateChatDisable>(UpdateChatDisable.handler),
    TypedReducer<ChatState, UpdateWelcomeListArr>(UpdateWelcomeListArr.handler),
    TypedReducer<ChatState, UpdateIsShowWelcomeInfo>(UpdateIsShowWelcomeInfo.handler),
  ],
);

//- 保存当前聊天室历史记录
class SaveHistoryList {
  final Map payload;
  SaveHistoryList({required this.payload});
  static ChatState handler(ChatState state, SaveHistoryList action) {
    state.historyList = action.payload['historyList'];
    return state;
  }
}

//- 聊天室内是否可以进行外部链接发送
class SetOutsideLinkAllow {
  final Map payload;
  SetOutsideLinkAllow({required this.payload});
  static ChatState handler(ChatState state, SetOutsideLinkAllow action) {
    state.outsideLinkAllow = action.payload['outsideLinkAllow'];
    return state;
  }
}

//- 保存当前点击tabBar索引
class SaveTabBarIndex {
  final Map payload;
  SaveTabBarIndex({required this.payload});
  static ChatState handler(ChatState state, SaveTabBarIndex action) {
    state.tabBarIndex = action.payload['tabBarIndex'];
    return state;
  }
}

//- 保存直播间是否为禁言状态

class UpdateChatDisable {
  final Map payload;
  UpdateChatDisable({required this.payload});
  static ChatState handler(ChatState state, UpdateChatDisable action) {
    state.charDisable = action.payload['charDisable'];
    return state;
  }
}

//- 修改进入直播间用户
class UpdateWelcomeListArr {
  final Map payload;
  UpdateWelcomeListArr({required this.payload});
  static ChatState handler(ChatState state, UpdateWelcomeListArr action) {
    state.welcomeListArr = action.payload['welcomeListArr'];
    return state;
  }
}

//- 是否显示用户欢迎语
class UpdateIsShowWelcomeInfo {
  final Map payload;
  UpdateIsShowWelcomeInfo({required this.payload});
  static ChatState handler(ChatState state, UpdateIsShowWelcomeInfo action) {
    state.isShowWelcomeInfo = action.payload['isShowWelcomeInfo'];
    return state;
  }
}
