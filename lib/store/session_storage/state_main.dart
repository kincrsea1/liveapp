import 'package:liveapp/store/session_storage/state_live.dart';
import 'package:liveapp/store/session_storage/state_user.dart';
import 'package:liveapp/store/session_storage/state_chat.dart';

class AppState {
  UserState user;
  LiveState live;
  ChatState chat;

  AppState({
    required this.user,
    required this.live,
    required this.chat,
  });

  static initialState() {
    AppState state = AppState(
      user: UserState.initialState(),
      live: LiveState.initialState(),
      chat: ChatState.initialState(),
    );
    return state;
  }
}

// 全局reducer
AppState commonReducer(AppState state, dynamic action) {
  return AppState(
    user: userReducer(state.user, action),
    live: liveReducer(state.live, action),
    chat: chatReducer(state.chat, action),
  );
}
