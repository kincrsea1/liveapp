import 'package:liveapp/model/user_info_model.dart' as UserInfoMol;
import 'package:redux/redux.dart';

class UserState {
  UserInfoMol.UserInfo userInfo;
  bool isFirstLogin;
  String currentRouteName;

  UserState({
    required this.userInfo,
    required this.isFirstLogin,
    required this.currentRouteName,
  });

  static initialState() => UserState(
        userInfo: UserInfoMol.UserInfo(),
        isFirstLogin: false,
        currentRouteName: '/',
      );
}

Function userReducer = combineReducers<UserState>(
  [
    TypedReducer<UserState, UpdateUserInfo>(UpdateUserInfo.handler),
    TypedReducer<UserState, UpdateFirstLoginStatus>(UpdateFirstLoginStatus.handler),
    TypedReducer<UserState, UpDateCurrentRouteName>(UpDateCurrentRouteName.handler),
    TypedReducer<UserState, DelUserInfo>(DelUserInfo.handler),
  ],
);

///- 保存首页Xm接口登录信息
class UpdateUserInfo {
  final Map payload;
  UpdateUserInfo({required this.payload});
  static UserState handler(UserState state, UpdateUserInfo action) {
    Map oldInfo = state.userInfo.toJson();
    state.userInfo = UserInfoMol.UserInfo.fromJson({...oldInfo, ...action.payload['userInfo']});
    return state;
  }
}

///- 用户退出登录删除用户信息
class DelUserInfo {
  final Map payload;
  DelUserInfo({required this.payload});
  static UserState handler(UserState state, DelUserInfo action) {
    state.userInfo = UserInfoMol.UserInfo();
    return state;
  }
}

///- 修改当前路由名称
class UpDateCurrentRouteName {
  final Map payload;
  UpDateCurrentRouteName({required this.payload});
  static UserState handler(UserState state, UpDateCurrentRouteName action) {
    state.currentRouteName = action.payload['currentRouteName'];
    return state;
  }
}

//- 修改用户初次登陆显示状态
class UpdateFirstLoginStatus {
  final Map payload;
  UpdateFirstLoginStatus({required this.payload});
  static UserState handler(UserState state, UpdateFirstLoginStatus action) {
    state.isFirstLogin = action.payload['isFirstLogin'];
    return state;
  }
}
