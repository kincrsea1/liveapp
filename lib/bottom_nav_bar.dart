// @dart=2.9
// ignore_for_file: unnecessary_this, prefer_const_constructors, prefer_const_literals_to_create_immutables, sized_box_for_whitespace

import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liveapp/base/base_widget.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/pages/live/home/component/a_score/score_page.dart';
import 'package:liveapp/pages/live/home/component/b_onlive/onlive_page.dart';
import 'package:liveapp/pages/live/home/component/c_message/message_page.dart';
import 'package:liveapp/pages/live/home/component/d_friend/friend_page.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/util/string_util.dart';
import 'package:liveapp/pages/live/home/component/e_mine/login/util/update_modal.dart';
import 'package:liveapp/pages/live/home/component/e_mine/mine_page.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/store/session_storage/state_chat.dart';
import 'package:liveapp/store/session_storage/state_main.dart';

import 'package:liveapp/store/session_storage/state_user.dart';
import 'package:liveapp/tools/db/hi_cache.dart';
import 'package:umeng_common_sdk/umeng_common_sdk.dart';
import 'package:umeng_push_sdk/umeng_push_sdk.dart';

import 'network/dio_util/dio_method.dart';
import 'network/dio_util/dio_response.dart';
import 'network/dio_util/dio_util.dart';

class Bottomnavigationbar extends BaseWidget {
  final selectPage;

  String type; //todo 回传状态
  Bottomnavigationbar({int this.selectPage, this.type});

  @override
  BaseWidgetState<BaseWidget> getState() => _BottomnavigationbarState();
}

class _BottomnavigationbarState extends BaseWidgetState<Bottomnavigationbar> {
  PageController pageController;
  int page = 1;
  int num = 4;
  bool unShowLoading = false;
  String type = ""; //todo 回传状态
  List<String> NomalImg = ["score_nomal", "onlive_nomal", "message_nomal", "vector_nomal", "mine_nomal"];
  List<String> SeletImg = ["score_select", "onlive_select", "message_select", "vector_select", "mine_select"];

  @override
  void initState() {
    if (widget.selectPage != null) {
      onPageChanged(widget.selectPage);
    }
    _checkUserIsFirstLogin();
    pageController = PageController(initialPage: this.page);
    getkefuUrl();
    Future.delayed(Duration(seconds: 1), getUpdateInfo);
    super.initState();
  }

  @override
  Widget PageBody(AppState state) {
    getBottomSafeAreaHeight();
    getTopBarHeight();

    return Scaffold(
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        children: <Widget>[
          scorePage(),
          OnLivePage(),
          messagePage(),
          friendPage(),
          minePage(),
        ],
        controller: pageController,
        onPageChanged: onPageChanged,
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: kBottomNavigationBarHeight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 0),
                minWidth: MediaQuery.of(context).size.width / num,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  onTap(0);
                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    LocalImageSelector.getSingleImage(getImageStr(0),
                        imageWidth: 22.w, imageHeight: 22.w, bFitFill: BoxFit.fill),
                    SizedBox(height: 2),
                    Text(
                      "比分",
                      style: TextStyle(
                        color: getColor(0),
                        fontSize: 10.w,
                      ),
                    )
                  ],
                ),
              ),
              FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 0),
                minWidth: MediaQuery.of(context).size.width / num,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  onTap(1);
                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    LocalImageSelector.getSingleImage(getImageStr(1),
                        imageWidth: 22.w, imageHeight: 22.w, bFitFill: BoxFit.fill),
                    SizedBox(height: 2),
                    Text("直播",
                        style: TextStyle(
                          color: getColor(1),
                          fontSize: 10.w,
                        ))
                  ],
                ),
              ),
              FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 0),
                minWidth: MediaQuery.of(context).size.width / num,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  onTap(2);
                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    LocalImageSelector.getSingleImage((getImageStr(2)),
                        imageWidth: 22.w, imageHeight: 22.w, bFitFill: BoxFit.fill),
                    SizedBox(height: 2),
                    Text("消息",
                        style: TextStyle(
                          color: getColor(1),
                          fontSize: 10.w,
                        ))
                  ],
                ),
              ),
              // GestureDetector(
              //     onTap: () => onTap(3),
              //     child: Column(
              //       mainAxisSize: MainAxisSize.min,
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: <Widget>[
              //         LocalImageSelector.getSingleImage(getImageStr(3), imageWidth: 22, imageHeight: 22),
              //         SizedBox(height: 2),
              //         Text("好友",
              //             style: TextStyle(
              //               color: getColor(3),
              //               fontSize: 10.w,
              //             ))
              //       ],
              //     )),
              FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 0),
                minWidth: MediaQuery.of(context).size.width / num,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  onTap(4);
                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    LocalImageSelector.getSingleImage(getImageStr(4),
                        imageWidth: 22.w, imageHeight: 22.w, bFitFill: BoxFit.fill),
                    SizedBox(height: 2),
                    Text("我的",
                        style: TextStyle(
                          color: getColor(4),
                          fontSize: 10.w,
                        ))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Color getColor(int value) {
    return this.page == value ? c_o : c_9;
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  void onTap(int index) {
    sessionStorage.dispatch(SaveTabBarIndex(payload: {'tabBarIndex': index}));
    pageController.jumpToPage(index);
  }

  void onPageChanged(int page) {
    if (mounted)
      setState(() {
        this.page = page;
        type = widget.type;
        log('---->>>---->>>---->>>>');
      });
  }

  String getImageStr(int value) {
    return this.page == value ? SeletImg[value] : NomalImg[value];
  }

  //- 检测用户是否为第一次登录
  void _checkUserIsFirstLogin() {
    Future.delayed(Duration(seconds: 1), () async {
      Map userInfo = await localStorage.getItem('userInfo');
      //- 存储用户登录数据到sessionStorage
      if (userInfo != null && sessionStorage.state.user.userInfo.token == null) {
        sessionStorage.dispatch(UpdateUserInfo(payload: {'userInfo': userInfo}));
      }

      ///隐藏用户协议
      if (!sessionStorage.state.user.isFirstLogin) {
        // showFadeDialog(
        //   isShowTitle: true,
        //   title: '用户协议以及隐私协议概要',
        //   isShowCancelBtn: true,
        //   cancelTxt: '不同意',
        //   confirmTxt: '同意',
        //   isShowConfirmBtn: true,
        //   cancelTextColor: Color(0xff666666),
        //   content: Text.rich(
        //     TextSpan(
        //       style: TextStyle(color: c_6, fontSize: 14.sp),
        //       children: [
        //         TextSpan(text: '欢迎使用米球体育APP，我们将通过'),
        //         TextSpan(
        //           text: '《用户协议》',
        //           style: TextStyle(color: c_o),
        //           recognizer: TapGestureRecognizer()
        //             ..onTap = () {
        //               goPage(url: 'userProtocol');
        //               Navigator.of(context);
        //             },
        //         ),
        //         TextSpan(text: '以及'),
        //         TextSpan(
        //           text: '《隐私协议》',
        //           style: TextStyle(color: c_o),
        //           recognizer: TapGestureRecognizer()
        //             ..onTap = () {
        //               goPage(url: 'privacyProtocol');
        //               Navigator.of(context);
        //             },
        //         ),
        //         TextSpan(
        //             text:
        //                 '帮助您进一步了解如何使用米球体育APP，接下来的文字之后我会汇总好，这里就是一个示意图。接下来的文字之后我会汇总好，这里就是一个示意图。接下来的文字之后我会汇总好，这里就是一个示意图。'),
        //       ],
        //     ),
        //   ),
        //   confirmCallback: () {
        //     //todo
        //   },
        // );
        sessionStorage.dispatch(UpdateFirstLoginStatus(payload: {'isFirstLogin': true}));
      }
    });
  }

  void getkefuUrl() async {
    DioResponse updateResponse = await DioUtil().request('/api/sys//getSerUrl', method: DioMethod.get);
    print(updateResponse.data);
    if (updateResponse.code != 0 || updateResponse.data['code'] != 0) {
      showToast(updateResponse.data['msg']);
      return;
    }
    if (isNotEmpty(updateResponse.data["data"])) {
      HiCache.getInstance().setString("kefuURL", updateResponse.data["data"]);
    }
  }

  void getUpdateInfo() async {
    print("更新弹框");
    DioUtil.getInstance()?.openLog();
    DioResponse updateResponse = await DioUtil().request('/splive/app/sp/config/getAppDownloadUrlList', method: DioMethod.get);
    if (updateResponse.code != 0 || updateResponse.data['code'] != 0) {
      showToast(updateResponse.data['msg']);
      return;
    }

    print(updateResponse.data);

    String newVersion  = updateResponse.data['data']['newVersion'];

    String androidDownloadUrlEnabled  = updateResponse.data['data']['androidDownloadUrlEnabled'];


    String iosDownloadUrlEnabled  = updateResponse.data['data']['iosDownloadUrlEnabled'];

    String forceVersion  = updateResponse.data['data']['forceVersion'];

    String newVersionState  = updateResponse.data['data']['newVersionState'];

    List updateContent = updateResponse.data['data']['updateContent'];

    if(updateContent.length.isOdd){
      updateContent.add("   ");
    }

    List updateContent1 = List.empty(growable: true);
    List updateContent2 = List.empty(growable: true);
    for(int i = 0; i < updateContent.length ;i++){
      if(i < updateContent.length/2) {
        updateContent1.add(updateContent[i]);
      }else{
        updateContent2.add(updateContent[i]);
      }
    }

    ///判断当前设备
    String loginType = '';
    String dev = '';
    if (Platform.isIOS) {
      loginType = '2';
      dev = 'iOS';
    } else if (Platform.isAndroid) {
      loginType = '3';
      dev = 'Android';
    } else {
      loginType = '1';
      dev = 'wap';
    }
    //判断服务器版本是否大于本地版本
    if(!haveNewVersion(newVersion,ConfigUtil().APP_VERSION)){
     // 服务版本小于本地版本
      return;
    }

    //服务器版本大于本地版本

    if(newVersionState == "1"){
    //强制更新
      if(loginType == '2' || loginType == '3') {
       String url =  loginType == '2'?iosDownloadUrlEnabled:androidDownloadUrlEnabled;
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) {
              return UpdateDialog(true, {
                "updateContent1": updateContent1,
                "updateContent2": updateContent2,
                "newVersion": newVersion,
                "url": url
              });
            });
      }
    }else{
    //根据强制跟新版本号来判断是否强制更新
      if(haveNewVersion(forceVersion, ConfigUtil().APP_VERSION)) {
        //强制更新版本 比本地版本高  强制更新
        if (loginType == '2' || loginType == '3') {
          String url = loginType == '2'
              ? iosDownloadUrlEnabled
              : androidDownloadUrlEnabled;
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) {
                return UpdateDialog(true, {
                  "updateContent1": updateContent1,
                  "updateContent2": updateContent2,
                  "newVersion": newVersion,
                  "url": url
                });
              });
        }
      }else{
        //强制更新版本 比本地版本低    新版本 比 本地版本高   选择更新
        if (loginType == '2' || loginType == '3') {
          String url = loginType == '2'
              ? iosDownloadUrlEnabled
              : androidDownloadUrlEnabled;
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) {
                return UpdateDialog(false, {
                  "updateContent1": updateContent1,
                  "updateContent2": updateContent2,
                  "newVersion": newVersion,
                  "url": url
                });
              });
        }
      }
    }

    // if(loginType == '2' || loginType == '3') {
    //   showDialog(
    //       barrierDismissible: false,
    //       context: context,
    //       builder: (context) {
    //         return UpdateDialog(newVersionState == "1",{"updateContent":updateContent,"newVersion":newVersion,"url":loginType == '2'?iosDownloadUrlEnabled:androidDownloadUrlEnabled});
    //       });
    // }

  }



  bool haveNewVersion(String newVersion, String old) {
    if (newVersion == null || newVersion.isEmpty || old == null || old.isEmpty)
      return false;
    int newVersionInt, oldVersion;
    var newList = newVersion.split('.');
    var oldList = old.split('.');
    if (newList.length == 0 || oldList.length == 0) {
      return false;
    }
    for (int i = 0; i < newList.length; i++) {
      newVersionInt = int.parse(newList[i]);
      oldVersion = int.parse(oldList[i]);
      if (newVersionInt > oldVersion) {
        return true;
      } else if (newVersionInt < oldVersion) {
        return false;
      }
    }

    return false;
  }
}
