// @dart=2.9

//import 'package:encrypt/encrypt.dart';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart' as Encrypt;
import 'package:encrypt/encrypt.dart';
import 'package:steel_crypt/steel_crypt.dart';

const String _key = '1234123412ABCDEF';
const String _iv = 'ABCDEF1234123412';

class EncryptUtil {
  ///aes加密
  /// [key]AesCrypt加密key
  /// [content] 需要加密的内容字符串
  static String aesEncode({String key, String content}) {
    var aesEncrypter = AesCrypt(key, 'ecb', 'pkcs7');
    return aesEncrypter.encrypt(content);
  }

  ///aes解密
  /// [key]aes解密key
  /// [content] 需要加密的内容字符串
  static String aesDecode({String key, String content}) {
    var aesEncrypter = AesCrypt(key, 'ecb', 'pkcs7');
    return aesEncrypter.decrypt(content);
  }

  ///aes加密函数 赛果
  static String MatchaesEncode(String content) {
    //加密key
    final key = Encrypt.Key.fromUtf8(_key);
    //偏移量
    final iv = Encrypt.IV.fromUtf8(_iv);

    //设置cbc模式
    final encrypter = Encrypt.Encrypter(
        Encrypt.AES(key, mode: Encrypt.AESMode.cbc, padding: 'PKCS7'));
    //加密
    final encrypted = encrypter.encrypt(content, iv: iv);
    //解密
    final decrypted = encrypter.decrypt(encrypted, iv: iv);

    print(decrypted);
    Uint8List bytedate = encrypted.bytes;
    String reslut = formatBytesAsHexString(bytedate);
    print(reslut);
    return reslut;
  }

  /// 解密函数
  static String MatchaesDecrypted(String data) {
    //加密key
    final key = Encrypt.Key.fromUtf8(_key);
    //偏移量
    final iv = Encrypt.IV.fromUtf8(_iv);

    //设置cbc模式
    final encrypter = Encrypt.Encrypter(
        Encrypt.AES(key, mode: Encrypt.AESMode.cbc, padding: 'PKCS7'));
    //解密
    print("解密后的结果:" + encrypter.decrypt(Encrypted.fromBase64(data), iv: iv));
    return encrypter.decrypt(Encrypted.fromBase64(data), iv: iv);
  }

  static String formatBytesAsHexString(Uint8List bytes) {
    if (bytes == null) throw new ArgumentError("The list is null");

    var result = new StringBuffer();
    for (var i = 0; i < bytes.lengthInBytes; i++) {
      var part = bytes[i];
      result.write('${part < 16 ? '0' : ''}${part.toRadixString(16)}');
    }

    return result.toString();
  }
}
