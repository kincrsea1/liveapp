import 'package:dio/dio.dart';
import 'package:liveapp/config/env_config.dart';
import 'package:liveapp/network/dio_util/dio_response.dart';
import 'package:liveapp/network/dio_util/encrypt_util.dart';
import 'package:liveapp/store/local_storage/local_storage.dart';
import 'package:liveapp/tools/db/hi_cache.dart';
import 'dart:io';

class DioInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    String SToken = HiCache.getInstance().get("SToken");
    options.headers["stoken"] = SToken == null ? ConfigUtil().APP_DEFAULT_STOKEN : SToken;
    //- 添加用户token
    Map? userInfo = await localStorage.getItem('userInfo');
    if (userInfo != null) {
      options.headers["token"] = userInfo['token'];
    }

    String timeStep = ConfigUtil().APP_DEFAULT_SPTVTOKEN + DateTime.now().millisecondsSinceEpoch.toString();
    String sptvstokenStr = EncryptUtil.MatchaesEncode(timeStep);

    options.headers["sptvstoken"] = sptvstokenStr;

    ///判断当前设备
    String loginType = '';
    String dev = '';
    if (Platform.isIOS) {
      loginType = '2';
      dev = 'iOS';
    } else if (Platform.isAndroid) {
      loginType = '3';
      dev = 'Android';
    } else {
      loginType = '1';
      dev = 'wap';
    }
    options.headers["domain"] = ConfigUtil().DEFAULT_DOMAIN;
    options.headers["loginType"] = loginType;
    options.headers["dev"] = dev;

    handler.next(options);

    /// super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    /// 请求成功是对数据做基本处理
    if (response.statusCode == 200) {
      response.data = DioResponse(code: 0, message: "请求成功啦", data: response.data);
    } else {
      response.data = DioResponse(code: 1, message: "请求失败啦", data: response.data);
    }

    /// 对某些单独的url返回数据做特殊处理
    if (response.requestOptions.baseUrl.contains("???????")) {
      ///....
    }

    /// 根据公司的业务需求进行定制化处理

    // 重点
    handler.next(response);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    switch (err.type) {

      /// 连接服务器超时
      case DioErrorType.connectTimeout:
        {
          // 根据自己的业务需求来设定该如何操作,可以是弹出框提示/或者做一些路由跳转处理
        }
        break;

      /// 响应超时
      case DioErrorType.receiveTimeout:
        {
          // 根据自己的业务需求来设定该如何操作,可以是弹出框提示/或者做一些路由跳转处理
        }
        break;

      /// 发送超时
      case DioErrorType.sendTimeout:
        {
          /// 根据自己的业务需求来设定该如何操作,可以是弹出框提示/或者做一些路由跳转处理
        }
        break;

      /// 请求取消
      case DioErrorType.cancel:
        {
          /// 根据自己的业务需求来设定该如何操作,可以是弹出框提示/或者做一些路由跳转处理
        }
        break;

      /// 404/503错误
      case DioErrorType.response:
        {
          /// 根据自己的业务需求来设定该如何操作,可以是弹出框提示/或者做一些路由跳转处理
        }
        break;

      /// other 其他错误类型
      case DioErrorType.other:
        {}
        break;
    }
    super.onError(err, handler);
  }
}
