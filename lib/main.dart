// @dart=2.9
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:liveapp/base/splash_screen.dart';
import 'package:liveapp/config/image/local_image_selector.dart';
import 'package:liveapp/event_channel.dart';
import 'package:liveapp/pages/live/home/liveDetail/event.dart';
import 'package:liveapp/router/main_router.dart';
import 'package:liveapp/router/route_watch.dart';
import 'package:liveapp/store/session_storage/state_main.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:provider/provider.dart';
import 'package:redux/redux.dart';
import 'package:tencent_im_sdk_plugin/enum/V2TimSDKListener.dart';
import 'package:tencent_im_sdk_plugin/enum/log_level.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_callback.dart';
import 'package:tencent_im_sdk_plugin/models/v2_tim_user_full_info.dart';
import 'package:tencent_im_sdk_plugin/tencent_im_sdk_plugin.dart';
import 'package:umeng_common_sdk/umeng_common_sdk.dart';
import 'package:umeng_push_sdk/umeng_push.dart';

import 'im/GenerateTestUserSig.dart';
// import 'package:sentry_flutter/sentry_flutter.dart';
final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

// final transaction = Sentry.startTransaction('processOrderBatch()', 'task');

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  requestPermissions();
  Store<AppState> store = Store(commonReducer, initialState: AppState.initialState());
  MyRouteObserver<PageRoute> routeObserver = MyRouteObserver<PageRoute>(store);
  LocalImageSelector.init();
  MyEventChannel myEventChannel = MyEventChannel.instance();
  myEventChannel.config(receiveRemoteMsg);

  // await SentryFlutter.init(
  //       (options) {
  //     options.dsn = 'http://d1fe9d74270f4a4c8297d53d2c5f796a@35.75.88.170:9000/8';
  //     // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
  //     // We recommend adjusting this value in production.
  //     options.tracesSampleRate = 1.0;
  //   },
  //   appRunner: () => runApp(MultiProvider(providers: [
  //     ChangeNotifierProvider(create: (_) => Event()),
  //   ], child: MyApp(store, routeObserver))),
  // );


  runApp(
    MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => Event()),
    ], child: MyApp(store, routeObserver)),
  );

  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }

  initSDK();

  // try {
  //   await processOrderBatch(transaction);
  // } catch (exception) {
  //   transaction.throwable = exception;
  //   transaction.status = SpanStatus.internalError();
  // } finally {
  //   await transaction.finish();
  // }
}
//
// Future<void> processOrderBatch(ISentrySpan span) async {
//   // span operation: task, span description: operation
//   final innerSpan = span.startChild('task', description: 'operation');
//
//   try {
//     // omitted code
//   } catch (exception) {
//     innerSpan.throwable = exception;
//     innerSpan.status = SpanStatus.notFound();
//   } finally {
//     await innerSpan.finish();
//   }
// }

void initSDK() {
  // 1. 从即时通信 IM 控制台获取应用 SDKAppID。
  int sdkAppID = 1400701255;
// 2. 添加 V2TimSDKListener 的事件监听器，sdkListener 是 V2TimSDKListener 的实现类
  V2TimSDKListener sdkListener = V2TimSDKListener(
    onConnectFailed: (code, error) {},
    onConnectSuccess: () {},
    onConnecting: () {},
    onKickedOffline: () {},
    onSelfInfoUpdated: (V2TimUserFullInfo info) {},
    onUserSigExpired: () {},
  );

// 3.初始化，成功之后可以注册事件。
  TencentImSDKPlugin.v2TIMManager.initSDK(
    sdkAppID: sdkAppID,
    loglevel: LogLevel.V2TIM_LOG_DEBUG,
    listener: sdkListener,
  );
  login();
}

login() async {
  String userID = "12345";
  // 正式环境请在服务端计算userSIg
  String userSig = new GenerateTestUserSig(
    sdkappid: 1400701255,
    key: "bf05ee788c28325efc12383ccda7e1650cb364772d1bd02e61839921db61e7a1",
  ).genSig(
    identifier: userID,
    expire: 7 * 24 * 60 * 1000, // userSIg有效期
  );
  V2TimCallback res = await TencentImSDKPlugin.v2TIMManager.login(
    userID: userID,
    userSig: userSig,
  );
  if (res.code == 0) {
    // 登录成功逻辑
  } else {
    // 登录失败逻辑
  }
}

//接收到的推送 ，根据自己的业务可以做对应的处理，我这里是页面跳转
void receiveRemoteMsg(msg) {
  print("receiveRemoteMsg ===> " + msg);
}

Future<void> umengCommon() async {
  ///安卓key  iOSkey
  UmengCommonSdk.initCommon('62f1d98105844627b5166581', '62f1b1b288ccdf4b7efa080e', 'openinstall');
  UmengPushSdk.register();
  await UmengPushSdk.setPushEnable(true);
  var registeredId = await UmengPushSdk.getRegisteredId();
  print('registeredId ${registeredId}');

//   // 发送自定义事件（目前属性值支持字符、整数、浮点、长整数，暂不支持NULL、布尔、MAP、数组）
//   UmengCommonSdk.onEvent("VideoPlay", {"userID":"神秘账号", "channel":"wx", "playTime":10});
//
// // 登录用户账号
//   UmengCommonSdk.onProfileSignIn("user_id");
//
// // 登出用户账号
//   UmengCommonSdk.onProfileSignOff();

// 手动采集页面信息
  UmengCommonSdk.setPageCollectionModeManual();

// 自动采集页面信息
  UmengCommonSdk.setPageCollectionModeAuto();

// // 进入页面统计（手动采集时才可设置）
//   UmengCommonSdk.onPageStart("viewName");
//
// // 离开页面统计（手动采集时才可设置）
//   UmengCommonSdk.onPageEnd("viewName");
}

void requestPermissions() async {
  Map<Permission, PermissionStatus> statuses = await [
    Permission.storage,
  ].request();
  print('存储权限：${statuses[Permission.storage]}');
}

RouteObserver<PageRoute> routeViewObserver = RouteObserver<PageRoute>();

class MyApp extends StatelessWidget {
  Store<AppState> store;
  final MyRouteObserver<PageRoute> routeObserver;

  MyApp(this.store, this.routeObserver);

  @override
  Widget build(BuildContext context) {
    ///添加全局点击背景收起键盘
    return StoreProvider(
      store: store,
      child: GestureDetector(
        onTap: () {
          hideKeyboard(context);
        },
        child: new MaterialApp(
          navigatorKey: navigatorKey,
          debugShowCheckedModeBanner: false,
          title: '一个直播',
          theme: new ThemeData(
            platform: TargetPlatform.iOS,
            scaffoldBackgroundColor: Color(0XFFFAF7F5),
          ),
          home: SplashPage(),

          ///添加路由
          routes: MainRouter.getRoute(),
          navigatorObservers: [routeObserver, routeViewObserver],

          onGenerateRoute: (RouteSettings settings) {
            print(settings.name);
            return null;
          },
          builder: EasyLoading.init(),

          ///未找到跳转页面时跳转
          onUnknownRoute: (RouteSettings settings) {
            return MaterialPageRoute(builder: (BuildContext context) => emptyPage(htitle: "404"));
          },
        ),
      ),
    );
  }

  ///添加全局点击背景收起键盘判断
  void hideKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus.unfocus();
    }
  }

  ///处理错误路径
  emptyPage({String htitle}) {}



}
