package com.cleanlive.liveapp;

import android.content.Context;

import androidx.multidex.MultiDex;

import com.operainstall.api.OperaInstall;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.commonsdk.utils.UMUtils;

import io.flutter.app.FlutterApplication;
import io.flutter.view.FlutterMain;

public class App extends FlutterApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
        Beta.installTinker();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FlutterMain.startInitialization(this);
        Bugly.init(this, "4e805fd35f", true);

        //友盟日志输出开关
        UMConfigure.setLogEnabled(true);
        //预初始化SDK，不采集信息
        PushHelper.preInit(this);
        boolean isMainProcess = UMUtils.isMainProgress(this);
        if (isMainProcess) {
            OperaInstall.init(getApplicationContext());
            new Thread(() -> PushHelper.init(getApplicationContext())).start();
        }else {
            PushHelper.init(getApplicationContext());
        }
        android.util.Log.i("UMLog", "UMConfigure.init@MultiDexApplication");
    }
}
