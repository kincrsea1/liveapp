package com.cleanlive.liveapp;

import android.content.Context;
import android.util.Log;

import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.PushAgent;
import com.umeng.message.api.UPushRegisterCallback;

public class PushHelper {
    public static void preInit(Context context) {
        UMConfigure.preInit(context, "62f1d98105844627b5166581", "Umeng");
    }

    public static void init(Context context) {

        UMConfigure.init(context,
                "62f1d98105844627b5166581",
                "Umeng",
                UMConfigure.DEVICE_TYPE_PHONE,
                "96176712c3a792acdd0e4cb0fea96c07");
        //注册推送
        PushAgent.getInstance(context).register(new UPushRegisterCallback() {

            @Override
            public void onSuccess(String deviceToken) {
                //注册成功后返回deviceToken，deviceToken是推送消息的唯一标志
                Log.i("UMLog", "注册成功 deviceToken:" + deviceToken);
            }

            @Override
            public void onFailure(String errCode, String errDesc) {
                Log.e("UMLog", "注册失败 " + "code:" + errCode + ", desc:" + errDesc);
            }
        });
        UMConfigure.setLogEnabled(true);
        // 选用LEGACY_AUTO页面采集模式
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
        // 支持在子进程中统计自定义事件
        UMConfigure.setProcessEvent(true);

        ///获取消息推送代理示例
//        PushAgent mPushagent = PushAgent.getInstance(context);
//        mPushagent.setPushIntentServiceClass(YouMengPushIntentService.class);
    }
}