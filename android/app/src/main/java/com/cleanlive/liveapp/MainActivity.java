package com.cleanlive.liveapp;

import static android.util.Log.d;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.operainstall.api.OperaInstall;
import com.operainstall.api.listener.AppWakeUpAdapter;
import com.operainstall.api.model.AppData;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
    private String release = "1";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        PushAgent.getInstance(this).onAppStart();
        android.util.Log.i("UMLog", "onCreate@MainActivity");
        OperaInstall.getWakeUp(getIntent(), wakeUpAdapter);
        MethodChannel _methodChannel = new MethodChannel(Objects.requireNonNull(getFlutterEngine()).getDartExecutor(), "login_page/method");
        _methodChannel.setMethodCallHandler((call, result) -> {
            switch (call.method) {
                case "regiest":
                    d("日志", "(MainActivity.java:50)  register  ");
                    OperaInstall.reportRegister();
                    break;
                case "miniPlay":
                    d("日志", "(MainActivity.java:54)   miniPlay ");
                    String url = call.argument("url");
                    Intent intent = new Intent(this, VoeActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                    break;
                case "closePage":
                    d("日志", "(MainActivity.java:60)    closePage");
                    //    EventBus.getDefault().post(new MessageEvent(""));
                    break;
                default:
                    result.notImplemented();
                    break;

            }
//            if ("regiest".equals(call.method)) {
//                Log.e("-------","regiest");
//                OperaInstall.reportRegister();
//            } else if ("miniPlay".equals(call.method)) {
//
//            } else if ("closePage".equals(call.method)) {
//
//
////                new voeActivity().methodB();
////                ActivityManager manager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
////                manager.restartPackage("com.cleanlive.liveapp.voeActivity");
//            } else {
//                result.notImplemented();
//                Log.e("-------","notImplemented");
//            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onNewIntent(@NotNull Intent intent) {
        super.onNewIntent(intent);
        // 此处要调用，否则App在后台运行时，会无法获取
        OperaInstall.getWakeUp(intent, wakeUpAdapter);
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.getString("release").equals("2")) {
                Handler mHandler = new Handler(getMainLooper());
                mHandler.post(() -> new MethodChannel(Objects.requireNonNull(getFlutterEngine()).getDartExecutor(), "login_page/method").invokeMethod("bigPlay", "", new MethodChannel.Result() {
                    @Override
                    public void success(@Nullable @org.jetbrains.annotations.Nullable Object o) {
                        d("日志", "(MainActivity.java:108)   成功发送  ");
                    }

                    @Override
                    public void error(@NonNull @NotNull String s, @Nullable @org.jetbrains.annotations.Nullable String s1, @Nullable @org.jetbrains.annotations.Nullable Object o) {
                        d("日志", "(MainActivity.java:115)    成功失败");
                    }

                    @Override
                    public void notImplemented() {
                        d("日志", "(MainActivity.java:119)   notImplemented ");
                    }
                }));
            }
        }
    }

    AppWakeUpAdapter wakeUpAdapter = new AppWakeUpAdapter() {
        @Override
        public void onWakeUp(AppData appData) {
            // 打印数据便于调试
            d("OperaInstall", "getWakeUp : wakeupData = " + appData.toString());
            // 获取渠道数据
            String channelCode = appData.getChannel();
            // 获取绑定数据
            String bindData = appData.getData();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wakeUpAdapter = null;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    //    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == 1000 && resultCode == RESULT_OK) {
//            super.onActivityResult(requestCode, resultCode, data);
//            new MethodChannel(Objects.requireNonNull(getFlutterEngine()).getDartExecutor(), "login_page/method").invokeMethod("inner_other", "", new MethodChannel.Result() {
//                @Override
//                public void success(@Nullable @org.jetbrains.annotations.Nullable Object o) {
//                    Log.e("-------", "成功发送");
//                }
//
//                @Override
//                public void error(@NonNull @NotNull String s, @Nullable @org.jetbrains.annotations.Nullable String s1, @Nullable @org.jetbrains.annotations.Nullable Object o) {
//                    Log.e("-------", "成功失败");
//                }
//
//                @Override
//                public void notImplemented() {
//                    Log.e("-------", "成功失败");
//                }
//            });
//        }
//    }
}
