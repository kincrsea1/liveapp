//package com.cleanlive.liveapp;
//
//import android.content.Context;
//import android.util.Log;
//
//import androidx.multidex.MultiDex;
//
//import com.cleanlive.liveapp.PushHelper;
//import com.operainstall.api.OperaInstall;
//import com.umeng.analytics.MobclickAgent;
//import com.umeng.commonsdk.UMConfigure;
//import com.umeng.commonsdk.utils.UMUtils;
//import com.umeng.message.IUmengRegisterCallback;
//import com.umeng.message.PushAgent;
//
//import io.flutter.app.FlutterApplication;
//
//public class MultiDexApplication extends FlutterApplication {
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        //友盟日志输出开关
//        UMConfigure.setLogEnabled(true);
//        //预初始化SDK，不采集信息
//        PushHelper.preInit(this);
//        boolean isMainProcess = UMUtils.isMainProgress(this);
//        if (isMainProcess) {
//            OperaInstall.init(getApplicationContext());
//            new Thread(() -> PushHelper.init(getApplicationContext())).start();
//        }else {
//            PushHelper.init(getApplicationContext());
//        }
//        OperaInstall.reportRegister();
//        android.util.Log.i("UMLog", "UMConfigure.init@MultiDexApplication");
//    }
//
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this) ;
//    }
//}