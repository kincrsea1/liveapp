package com.cleanlive.liveapp

import android.app.Activity
import android.app.PictureInPictureParams
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Rational
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.PlayerView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class VoeActivity : Activity() {
    private var player: SimpleExoPlayer? = null
    private var playerView: PlayerView? = null

    //    private var player: SimpleExoPlayer? = null
    var videoUri = ""

    //    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
//        GeneratedPluginRegistrant.registerWith(flutterEngine)
//    }
    var instance: Activity? = null


    private var mPlayer: ExoPlayer? = null
    private var mBuilder: PictureInPictureParams.Builder? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.a)
        instance = this;
        //找到播放组件
        playerView = findViewById(R.id.player_view)
        //设置播放器
        player = SimpleExoPlayer.Builder(this).build()
        playerView?.player = player
        //播放视频
        if (intent.getStringExtra("url") != null) {
            videoUri = intent.getStringExtra("url")!!
        } else {
            return;
        }
        // Build the media item.
        val mediaItem = MediaItem.fromUri(videoUri)
        // Set the media item to be played.
        player?.setMediaItem(mediaItem)
        // Prepare the player.
        player?.prepare()
        // Start the playback.
        player?.play()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
            && packageManager.hasSystemFeature(PackageManager.FEATURE_PICTURE_IN_PICTURE)
        ) {
            mBuilder = PictureInPictureParams.Builder()
        }
        if (mBuilder != null) {
            val rational = Rational(192, 108)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mBuilder!!.setAspectRatio(rational).build()
                if (Build.VERSION.SDK_INT >= 31) {
                    mBuilder!!.setSeamlessResizeEnabled(true)
                }
                enterPictureInPictureMode(mBuilder!!.build())
            }
        } else {
            Toast.makeText(this, "不能开启画中画", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }
//    override fun onResume() {
//        super.onResume()
//        mPlayer?.play()
//    }

    override fun onStop() {
        super.onStop()
        player?.stop()
        player?.release()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this) // 解绑
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        methodB()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onPictureInPictureModeChanged(
        isInPictureInPictureMode: Boolean,
        newConfig: Configuration?,
    ) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig)
        if (isInPictureInPictureMode) {
            Log.d("日志", "(VoeActivity.kt:116) 画中画    on/开")
        } else {

            player?.stop()
            player?.release()

            val mIntent: Intent? =
                packageManager.getLaunchIntentForPackage("com.cleanlive.liveapp")
            mIntent?.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP

            if (mIntent != null) {
                mIntent.putExtra("release", "2")
                startActivity(mIntent)
            }

//            val intent = Intent(
//                this@voeActivity,
//                MainActivity::class.java
//            )
//
//            this@voeActivity.setResult(RESULT_OK, intent)

            this@VoeActivity.finish()
            Log.d("日志", "(VoeActivity.kt:138)  画中画   off/关")
        }
    }

    fun methodB() {
        player?.stop()
        player?.release()
        this@VoeActivity.finish()
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        mPlayer?.release()
//    }
//
//    override fun onPause() {
//        super.onPause()
//        mPlayer?.pause()
//    }
}