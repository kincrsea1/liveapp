## Add project specific ProGuard rules here.
## You can control the set of applied configuration files using the
## proguardFiles setting in build.gradle.
##
## For more details, see
##   http://developer.android.com/guide/developing/tools/proguard.html
#
## If your project uses WebView with JS, uncomment the following
## and specify the fully qualified class name to the JavaScript interface
## class:
##-keepclassmembers class fqcn.of.javascript.interface.for.webview {
##   public *;
##}
#
## Uncomment this to preserve the line number information for
## debugging stack traces.
##-keepattributes SourceFile,LineNumberTable
#
## If you keep the line number information, uncomment this to
## hide the original source file name.
##-renamesourcefileattribute SourceFile
##直播
#-dontwarn com.gensee.**
#-keep class com.gensee.**{*;}
##1.基本指令区
#-optimizationpasses 5
#-dontusemixedcaseclassnames
#-dontskipnonpubliclibraryclasses
#-dontskipnonpubliclibraryclassmembers
#-dontpreverify
#-verbose
#-ignorewarning
#-printmapping proguardMapping.txt
#-optimizations !code/simplification/cast,!field/*,!class/merging/*
#-keepattributes *Annotation*,InnerClasses
#-keepattributes Signature
#-keepattributes SourceFile,LineNumberTable
#-keepattributes EnclosingMethod
#
##2.默认保留区
#-keep public class * extends android.app.Activity
#-keep public class * extends android.app.Application
#-keep public class * extends android.app.Service
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends android.app.backup.BackupAgentHelper
#-keep public class * extends android.preference.Preference
#-keep public class * extends android.view.View
#-keep class android.support.** {*;}
#
#-keepclasseswithmembernames class * {
#    native <methods>;
#}
#-keepclassmembers class * extends android.app.Activity{
#    public void *(android.view.View);
#}
#-keepclassmembers enum * {
#    public static **[] values();
#    public static ** valueOf(java.lang.String);
#}
#-keep public class * extends android.view.View{
#    *** get*();
#    void set*(***);
#    public <init>(android.content.Context);
#    public <init>(android.content.Context, android.util.AttributeSet);
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet);
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#-keep class * implements android.os.Parcelable {
#  public static final android.os.Parcelable$Creator *;
#}
#
#-keepclassmembers class * implements java.io.Serializable {
#    static final long serialVersionUID;
#    private static final java.io.ObjectStreamField[] serialPersistentFields;
#    private void writeObject(java.io.ObjectOutputStream);
#    private void readObject(java.io.ObjectInputStream);
#    java.lang.Object writeReplace();
#    java.lang.Object readResolve();
#}
#-keep class **.R$* {
# *;
#}
#-keepclassmembers class * {
#    void *(**On*Event);
#}
##轮播图
#-keep class com.daimajia.slider.library.** { *; }
#
##ijkPlayer
#-keep class tv.danmaku.ijk.media.**{*;}
#
## RxJava RxAndroid
#-dontwarn rx.*
#-dontwarn sun.misc.**
#-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
#    long producerIndex;
#    long consumerIndex;
#}
####################################### for picasso ###########################################
#-dontwarn com.squareup.okhttp.**
#
#
####################################### for umeng push ###########################################
#-dontwarn com.taobao.**
#-dontwarn anet.channel.**
#-dontwarn anetwork.channel.**
#-dontwarn org.android.**
#-dontwarn org.apache.thrift.**
#-dontwarn com.xiaomi.**
#-dontwarn com.huawei.**
#-dontwarn com.meizu.**
#
#-keepattributes *Annotation*
#
#-keep class com.taobao.** {*;}
#-keep class org.android.** {*;}
#-keep class anet.channel.** {*;}
#-keep class com.umeng.** {*;}
#-keep class com.xiaomi.** {*;}
#-keep class com.huawei.** {*;}
#-keep class com.meizu.** {*;}
#-keep class org.apache.thrift.** {*;}
#
#-keep class com.alibaba.sdk.android.**{*;}
#-keep class com.ut.**{*;}
#-keep class com.ta.**{*;}
#
#-keep public class **.R$*{
#   public static final int *;
#}
#
##（可选）避免Log打印输出
#-assumenosideeffects class android.util.Log {
#   public static *** v(...);
#   public static *** d(...);
#   public static *** i(...);
#   public static *** w(...);
# }
#
####################################### for tinker hot fix ###########################################
## help us to debug
#-renamesourcefileattribute SourceFile
#-keepattributes Exceptions
#-keepattributes SourceFile,LineNumberTable,keepattributes
#-keepattributes InnerClasses
#-keepattributes EnclosingMethod
#-keepattributes Signature
#-keepattributes *Annotation*
#-dontshrink
#
## Config from tinker
#-dontwarn com.tencent.tinker.anno.AnnotationProcessor
#-keep @com.tencent.tinker.anno.DefaultLifeCycle public class *
#-keep public class * extends android.app.Application {
#    *;
#}
#
#-keep public class com.tencent.tinker.loader.app.ApplicationLifeCycle {
#    *;
#}
#-keep public class * implements com.tencent.tinker.loader.app.ApplicationLifeCycle {
#    *;
#}
#
#-keep public class com.tencent.tinker.loader.TinkerLoader {
#    *;
#}
#-keep public class * extends com.tencent.tinker.loader.TinkerLoader {
#    *;
#}
#-keep public class com.tencent.tinker.loader.TinkerTestDexLoad {
#    *;
#}
#-keep public class com.tencent.tinker.loader.TinkerTestAndroidNClassLoader {
#    *;
#}
#
##your dex.loader patterns here
#-keep class com.tencent.tinker.loader.**
#
#
##okhttp混淆配置
#-keep class com.squareup.okhttp.** { *;}
#-dontwarn com.squareup.okhttp.**
#-dontwarn okio.**
#
## Gson--been类
#-keep class com.zhongye.jinjishi.httpbean.**{*;} # 自定义数据模型的bean目录
#-keep class com.zhongye.jinjishi.been.**{*;} # 自定义数据模型的bean目录
#-dontwarn com.google.**
#-keep class com.google.gson.** {*;}
#
##butterknife    ######7.0以下
#-keep class butterknife.** { *; }
#-dontwarn butterknife.internal.**
#-keep class **$$ViewInjector{ *; }
#-keepclasseswithmembernames class * {
#    @butterknife.* <fields>;
#}
#-keepclasseswithmembernames class * {
#    @butterknife.* <methods>;
#}
##butterknife    #####7.0以上
##-keep class butterknife.** { *; }
##-dontwarn butterknife.internal.**
##-keep class **$$ViewBinder { *; }
##-keepclasseswithmembernames class * {
##    @butterknife.* <fields>;
##}
##-keepclasseswithmembernames class * {
##    @butterknife.* <methods>;
##}
#
#-keep class com.umeng.commonsdk.** {*;}
#
#-dontshrink
#-dontoptimize
#-dontwarn com.google.android.maps.**
#-dontwarn android.webkit.WebView
#-dontwarn com.umeng.**
#-dontwarn com.tencent.weibo.sdk.**
#-dontwarn com.facebook.**
#-keep public class javax.**
#-keep public class android.webkit.**
#-dontwarn android.support.v4.**
#-keep enum com.facebook.**
#-keepattributes Exceptions,InnerClasses,Signature
#-keepattributes *Annotation*
#-keepattributes SourceFile,LineNumberTable
#
#-keep public interface com.facebook.**
#-keep public interface com.tencent.**
#-keep public interface com.umeng.socialize.**
#-keep public interface com.umeng.socialize.sensor.**
#-keep public interface com.umeng.scrshot.**
#
#-keep public class com.umeng.socialize.* {*;}
#-keep class com.tencent.mm.opensdk.** {
#   *;
#}
#-keep class com.tencent.wxop.** {
#   *;
#}
#-keep class com.tencent.mm.sdk.** {
#   *;
#}
#-dontwarn twitter4j.**
#-keep class twitter4j.** { *; }
#
#-keep class com.tencent.** {*;}
#-dontwarn com.tencent.**
#-keep class com.kakao.** {*;}
#-dontwarn com.kakao.**
#-keep public class com.umeng.com.umeng.soexample.R$*{
#    public static final int *;
#}
#-keep public class com.linkedin.android.mobilesdk.R$*{
#    public static final int *;
#}
#-keepclassmembers enum * {
#    public static **[] values();
#    public static ** valueOf(java.lang.String);
#}
#
## banner 的混淆代码
#-keep class com.youth.banner.** {
#    *;
# }
#-keepnames class * implements android.os.Parcelable {
#    public static final ** CREATOR;
#}
#
#-keep class com.linkedin.** { *; }
#-keep class com.android.dingtalk.share.ddsharemodule.** { *; }
#-keepattributes Signature
#
#
################################ alipay ##################################
#
#-keep class com.alipay.android.phone.mrpc.core.** { *; }
#-keep class com.alipay.apmobilesecuritysdk.** { *; }
#-keep class com.alipay.mobile.framework.service.annotation.** { *; }
#-keep class com.alipay.mobilesecuritysdk.face.** { *; }
#-keep class com.alipay.tscenter.biz.rpc.** { *; }
#-keep class org.json.alipay.** { *; }
#-keep class com.alipay.tscenter.** { *; }
#-keep class com.ta.utdid2.** { *;}
#-keep class com.ut.device.** { *;}
#
##直播
#-dontwarn com.gensee.**
#-keep class com.gensee.**{*;}
#
#-dontwarn android.net.**
#-keep class android.net.SSLCertificateSocketFactory{*;}
##-----
#-dontwarn com.ut.mini.**
#-dontwarn okio.**
#-dontwarn com.xiaomi.**
#-dontwarn com.squareup.wire.**
#-dontwarn android.support.v4.**
#
#-keepattributes *Annotation*
#
#-keep class android.support.v4.** { *; }
#-keep interface android.support.v4.app.** { *; }
#
#-keep class okio.** {*;}
#-keep class com.squareup.wire.** {*;}
#
#-keep class com.umeng.message.protobuffer.* {
#         public <fields>;
#         public <methods>;
#}
#
#-keep class com.umeng.message.* {
#         public <fields>;
#         public <methods>;
#}
#
#-keep class org.android.agoo.impl.* {
#         public <fields>;
#         public <methods>;
#}
#
#-keep class org.android.agoo.service.* {*;}
#
#-keep class org.android.spdy.**{*;}
#
#-keep public class com.zhongye.jinjishi.R$*{
#    public static final int *;
#}
#
#
####jmdns
#-keep class javax.jmdns.** { *; }
#-dontwarn javax.jmdns.**
#
####CyberGarage-upnp
#-keep class org.cybergarage.** { *; }
#-dontwarn org.cybergarage.**
#
####plist
#-keep class com.dd.plist.** { *; }
#-dontwarn com.dd.plist.**
#
####kxml
#-keep class org.kxml2.** { *; }
#-keep class org.xmlpull.** { *; }
#-dontwarn org.kxml2.**
#-dontwarn org.xmlpull.**
#
####Lebo
#-keep class com.hpplay.**{*;}
#-keep class com.hpplay.**$*{*;}
#-dontwarn com.hpplay.**
