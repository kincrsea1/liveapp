//
//  JPipVController.m
//  Runner
//
//  Created by jordon on 2022/9/5.
//

#import "JPipVController.h"
#import "VideoPlayer.h"
#import <AVKit/AVKit.h>
#import "AppDelegate.h"
@interface JPipVController ()<AVPictureInPictureControllerDelegate>
@property (strong, nonatomic) VideoPlayer *player;
@property (nonatomic, strong) AVPictureInPictureController *pipController;
@property (weak, nonatomic) UINavigationController *navCtr;
@end

@implementation JPipVController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isBack = NO;
    // Do any additional setup after loading the view.
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    [self setUpView];
  
}

-(void)setUpView{
    NSString *str27 = self.url;
    self.player = [[VideoPlayer alloc]initWithUrl:[NSURL URLWithString:str27]];
    self.player.frame = CGRectMake(self.view.frame.size.width -100, -100, 100, 100);
    [self.view addSubview:self.player];
    [self.player play];
    
//    if ([AVPictureInPictureController isPictureInPictureSupported]) {
//        self.pipController = [[AVPictureInPictureController alloc] initWithPlayerLayer:self.player.avPlayerLayer];
//        self.pipController.delegate = self;
//        [self.pipController setValue:[NSNumber numberWithInt:1] forKey:@"controlsStyle"];
//        self.player.autoControlBackground = NO;
//    }

    __weak typeof(self) weakSelf = self;
    self.player.PipStatr = ^{
        [weakSelf startPip];
    };
}

-(void)startPip{
    if ([AVPictureInPictureController isPictureInPictureSupported]) {
        if (self.pipController.isPictureInPictureActive) {
            [self.pipController stopPictureInPicture];
            self.player.autoControlBackground = YES;
        } else {
            self.pipController = [[AVPictureInPictureController alloc] initWithPlayerLayer:self.player.avPlayerLayer];
            self.pipController.delegate = self;
            [self.pipController setValue:[NSNumber numberWithInt:1] forKey:@"controlsStyle"];
            __weak typeof(self) weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [weakSelf.pipController startPictureInPicture];
                weakSelf.player.autoControlBackground = NO;
            });
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navCtr = self.navigationController;
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if ([delegate.PipViewC isKindOfClass:self.class]) {
        [self.pipController stopPictureInPicture];
    }
}

- (void)dealloc {
    NSLog(@"%s_ 释放了",__func__);
}

#pragma mark ------- 画中画代理，和画中画状态有关的逻辑 在代理中处理
// 将开启画中画
- (void)pictureInPictureControllerWillStartPictureInPicture:(AVPictureInPictureController *)pictureInPictureController {
    // 处理 pipBtn 的选中状态、储存当前控制器
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.PipViewC = self;
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

// 将关闭画中画
- (void)pictureInPictureControllerWillStopPictureInPicture:(AVPictureInPictureController *)pictureInPictureController {
    //
}

// 已经关闭画中画
- (void)pictureInPictureControllerDidStopPictureInPicture:(AVPictureInPictureController *)pictureInPictureController {
    [self.player pause];
    [self.player stop];
    [self.player removeFromSuperview];
    self.player = nil;
    // 处理 pipBtn 的选中状态、当前控制器置空
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        if (self.closeblock) {
            self.closeblock(self.isBack);
        }
    for (id view in self.view.subviews) {
          [view removeFromSuperview];
      }
    delegate.PipViewC = nil;
    NSLog(@"%@",delegate.PipViewC);
    

}

// 点击视频悬浮窗的复原按钮打开控制器
- (void)pictureInPictureController:(AVPictureInPictureController *)pictureInPictureController restoreUserInterfaceForPictureInPictureStopWithCompletionHandler:(void (^)(BOOL))completionHandler {
    self.isBack = YES;
    // 处理控制器的跳转等
    if (self.navCtr != nil && [self.navCtr.viewControllers containsObject:self] == NO) {
      
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        delegate.PipViewC = nil;
        [self.navCtr pushViewController:self animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            completionHandler(YES);
        });
        
        return;
    }
    
    completionHandler(YES);
}


#pragma mark-获取当前根控制器
//获取当前屏幕显示的viewcontroller
- (UIViewController *)ToolsgetCurrentVC
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    
    return currentVC;
}

- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        
        rootVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
        
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
        
    } else {
        // 根视图为非导航类
        
        currentVC = rootVC;
    }
    
    return currentVC;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
