//
//  PipModel.m
//  Runner
//
//  Created by jordon on 2022/9/5.
//

#import "PipModel.h"
#import "VideoPlayer.h"
#import <AVKit/AVKit.h>
#import "AppDelegate.h"

@implementation PipModel
+ (instancetype)sharedInstance{
    static PipModel *myInstance = nil;
    if(myInstance == nil){
        myInstance = [[PipModel alloc]init];
    }
    return myInstance;
}



#pragma mark-获取当前根控制器
//获取当前屏幕显示的viewcontroller
- (UIViewController *)ToolsgetCurrentVC
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    
    return currentVC;
}

- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        
        rootVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
        
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
        
    } else {
        // 根视图为非导航类
        
        currentVC = rootVC;
    }
    
    return currentVC;
    
}
@end
