//
//  PipModel.h
//  Runner
//
//  Created by jordon on 2022/9/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PipModel : NSObject
+ (instancetype)sharedInstance;
@end

NS_ASSUME_NONNULL_END
