//
//  JPipVController.h
//  Runner
//
//  Created by jordon on 2022/9/5.
//

#import <UIKit/UIKit.h>
typedef void(^closePipBlock)(bool isback);
@interface JPipVController : UIViewController
@property (nonatomic,copy)closePipBlock closeblock;
@property (nonatomic,copy)NSString * url;
@property (nonatomic,assign)bool isBack;
@end


