#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"

//#import "channels/NSString+AESEncrypt.h"
#import <UMPush/UMessage.h>
#import <UMCommon/UMCommon.h>
#import <Flutter/FlutterChannels.h>
#import "OperaInstallSDK.h"
#import "OperaInstallData.h"
#import <UMCommonLog/UMCommonLogHeaders.h>
#import "PipViewController.h"

static NSString *const kEventChannelName = @"EventChannel";

@interface AppDelegate ()<UIApplicationDelegate,UNUserNotificationCenterDelegate,FlutterStreamHandler,OperaInstallDelegate>

@property(nonatomic,strong)FlutterEventChannel *eventChannel;
// FlutterEventSink：传输数据的载体
@property (nonatomic) FlutterEventSink eventSink;
@property (nonatomic,strong)  NSDictionary * userInfo;
@property (nonatomic, strong, nullable) FlutterMethodChannel *methodChannel;
@property (nonatomic, strong, nullable) FlutterMethodChannel *VideoChannel;

@end
@implementation AppDelegate
{
    UIBackgroundTaskIdentifier *bgTask;
}



- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GeneratedPluginRegistrant registerWithRegistry:self];
  // Override point for customization after application launch.
    [self keepIdleTimerDisabled];
    [OperaInstallSDK initWithDelegate:self];
    [[OperaInstallSDK defaultManager] getInstallParmsCompleted:^(OperainstallData*_Nullable appData) {
        //在主线程中回调
        if (appData.data) {//(动态安装参数)
           //e.g.如免填邀请码建立邀请关系、自动加好友、自动进入某个群组或房间等
        }
        if (appData.channelCode) {//(通过渠道链接或二维码安装会返回渠道编号)
            //e.g.可自己统计渠道相关数据等
        }
        NSLog(@"OperaInstallSDK:\n动态参数：%@;\n渠道编号：%@",appData.data,appData.channelCode);
        
//        UIAlertView *alerts = [[UIAlertView alloc]initWithTitle:@"当前渠道编号" message:appData.channelCode delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [alerts show];
    }];
    
    [self mychannel];
    [self myeventchennel];
    
    [self initUMcommon];
    [self configUMPush:launchOptions];
    
    NSLog(@"launchOptions==>%@",launchOptions);
    if(launchOptions){
        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        NSLog(@"%@",userInfo);
        NSString *page = [NSString stringWithFormat:@"%@",userInfo[@"type"]];
        NSLog(@"type = %@",page);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self sendMessageByEventChannel:page];
        });
    }
    
    FlutterViewController * vc = (FlutterViewController *)self.window.rootViewController;
      self.methodChannel = [FlutterMethodChannel methodChannelWithName:@"login_page/method" binaryMessenger:vc];

    __weak typeof(self)Weekself = self;
      [self.methodChannel setMethodCallHandler:^(FlutterMethodCall * _Nonnull call, FlutterResult  _Nonnull result) {
//          [OperaInstallSDK reportRegister];
//          UIAlertView *alerts = [[UIAlertView alloc]initWithTitle:@"已注册" message:nil delegate:Weekself cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//          [alerts show];
          
          if ([call.method isEqualToString:@"regiest"]) {
              //用户注册成功后调用
              [OperaInstallSDK reportRegister];

          }else if([call.method isEqualToString:@"miniPlay"]){
              [Weekself gotoPipPageWithUrl:call.arguments[@"url"]];
          }
      }];
    
    
//    self.VideoChannel = [FlutterMethodChannel methodChannelWithName:@"pip" binaryMessenger:self.vc];
//
//    [self.VideoChannel setMethodCallHandler:^(FlutterMethodCall * _Nonnull call, FlutterResult  _Nonnull result) {
////          [OperaInstallSDK reportRegister];
////          UIAlertView *alerts = [[UIAlertView alloc]initWithTitle:@"已注册" message:nil delegate:Weekself cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
////          [alerts show];
//
//        if ([call.method isEqualToString:@"miniPlay"]) {
//
//        }else if([call.method isEqualToString:@"gotoPipPage"]) {
//            [Weekself dismissPipPage];
//        }
//    }];
    
    [self avPipConfig];
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
    
}

#pragma mark- 画中画部分
-(void)avPipConfig{
//    self.PipViewC = PipViewController.new;
    
}


-(void)gotoPipPageWithUrl:(NSString *)url{
    
    [self jump:url];
    
//
//    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
//    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    if (delegate.PipViewC) {
//        [keyWindow.rootViewController presentViewController:delegate.PipViewC animated:NO completion:nil];
//        return;
//    }
//    JPipVController *PipViewC = [[JPipVController alloc]init];
//    PipViewC.modalPresentationStyle = UIModalPresentationCustom;
//    [keyWindow.rootViewController presentViewController:PipViewC animated:NO completion:nil];
  
}

-(void)jump:(NSString *)url{
    self.PipViewC  = [[JPipVController alloc] init];
    __weak typeof(self) weakSelf = self;
    self.PipViewC.closeblock = ^(bool isBack){
        [weakSelf sendMessageWith:isBack];
        [weakSelf.PipViewC dismissViewControllerAnimated:NO completion:^{
            weakSelf.PipViewC = nil;
        }];
        
     
       
    };
    
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.PipViewC.modalPresentationStyle = UIModalPresentationCustom;
    self.PipViewC.url = url;
    [keyWindow.rootViewController presentViewController: self.PipViewC animated:NO completion:nil];
}

-(void)sendMessageWith:(BOOL) isBack{
    NSString * methodName = isBack == YES ? @"bigPlay":@"closePage";
    [self.methodChannel invokeMethod:methodName arguments:methodName result:^(id  _Nullable result) {
        NSLog(@"ios: %@", result);
    }];
}

-(void)dismissPipPage{
    [self.PipViewC dismissViewControllerAnimated:NO completion:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    UIApplication*app = [UIApplication sharedApplication];

        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }];

        // Start the long-running task and return immediately.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            // Do the work associated with the task.
            //[self startTimerAction];
            [app endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        });
}


#pragma mark- 永久不熄屏
- (void)keepIdleTimerDisabled {
    [UIApplication sharedApplication].idleTimerDisabled=YES;
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    [[UIApplication sharedApplication] addObserver:self forKeyPath:@"idleTimerDisabled" options:NSKeyValueObservingOptionNew context:nil];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (![UIApplication sharedApplication].idleTimerDisabled) {
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
}

#pragma mark- =============openinstall================
//适用目前所有iOS版本
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    //处理通过operainstall URL scheme唤起App的数据
    [OperaInstallSDK handLinkURL:url];
    
    //其他第三方回调；
    return YES;
}

//iOS9以上，会优先走这个方法
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(nonnull NSDictionary *)options{
    //处理通过operainstall URL scheme唤起App的数据
    [OperaInstallSDK handLinkURL:url];
    //其他第三方回调；
     return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler{
    //处理通过一键唤起App时传递的数据
    [OperaInstallSDK continueUserActivity:userActivity];
    //其他第三方回调；
     return YES;
}

//通过平台获取已经安装App被唤醒时的参数（如果是通过渠道页面唤醒App时，会返回渠道编号）
-(void)getWakeUpParams:(OperainstallData *)appData{
    if (appData.data) {//(动态唤醒参数)
    }
    if (appData.channelCode) {//(通过渠道链接或二维码唤醒会返回渠道编号)
        //e.g.可自己统计渠道相关数据等
       
    }
 
    NSLog(@"OperaInstallSDK:\n动态参数：%@;\n渠道编号：%@",appData.data,appData.channelCode);
}


#pragma mark- =============openinstall================


-(void)myeventchennel {
    FlutterViewController *vc = (FlutterViewController *)self.window.rootViewController;
    self.eventChannel = [FlutterEventChannel eventChannelWithName:kEventChannelName binaryMessenger:vc];
    //设置消息处理器的代理
    [self.eventChannel setStreamHandler:self];
}
#pragma mark----------------------------------FlutterStreamHandler
#pragma mark - <FlutterStreamHandler>
//这个onListen是Flutter端开始监听这个channel时的回调，第二个参数 EventSink是用来传数据的载体
- (FlutterError* _Nullable)onListenWithArguments:(id _Nullable)arguments eventSink:(FlutterEventSink)eventSink {
    // arguments flutter给native的参数
    // 回调给flutter， 建议使用实例指向，因为该block可以使用多次
    self.eventSink = eventSink;
    return nil;
}

/// flutter不再接收
- (FlutterError* _Nullable)onCancelWithArguments:(id _Nullable)arguments {
    // arguments flutter给native的参数
    self.eventSink = nil;
    return nil;
}
- (void)sendMessageByEventChannel:(NSString *)message{
    if (self.eventSink) {
        self.eventSink(message);
    }
}


#pragma mark----------------------------------mychannel
-(void)mychannel{
    FlutterViewController *vc = (FlutterViewController *)self.window.rootViewController;
    FlutterMethodChannel *channel = [FlutterMethodChannel methodChannelWithName:@"sales_assist_channel" binaryMessenger:vc];
    
    __weak typeof(self) weakself = self;
    [channel setMethodCallHandler:^(FlutterMethodCall * _Nonnull call, FlutterResult  _Nonnull result) {
        NSLog(@"%@",call);
        if([call.method isEqualToString:@"getAesBCB"]){
            NSDictionary *dic =(NSDictionary *)call.arguments;
            NSString *pwd = dic[@"value"];
//            NSString *res = [pwd aci_encryptWithAES];
            result(@"");
        }else if([call.method isEqualToString:@"AddTags"]){
            NSDictionary *dic =(NSDictionary *)call.arguments;
            NSString *pwd = dic[@"value"];
            [weakself addTags:pwd];
            result(pwd);
        }else if([call.method isEqualToString:@"AddAdvisorAlias"]){
            NSDictionary *dic =(NSDictionary *)call.arguments;
            NSString *pwd = dic[@"value"];
            [weakself addAlias:pwd];
            result(pwd);
        }else if([call.method isEqualToString:@"UMpusheLoginOut"]){
            NSDictionary *dic =(NSDictionary *)call.arguments;
            NSString *pwd = dic[@"value"];
//            NSString *pwd2 = dic[@"value2"];
            [weakself removeTags];
            [weakself removeAlias:pwd];
            result(pwd);
        }
        
    }];
}



#pragma mark----------------------------------友盟推送
-(void)initUMcommon {
    [UMCommonLogManager setUpUMCommonLogManager];
    [UMConfigure setEncryptEnabled:NO];//打开加密传输
    [UMConfigure setLogEnabled:YES];//设置打开日志
    [UMConfigure initWithAppkey:@"62f1b1b288ccdf4b7efa080e" channel:@"openinstall"];
   
}

-(void)configUMPush:(NSDictionary *)launchOptions{
    // Push组件基本功能配置
    UMessageRegisterEntity * entity = [[UMessageRegisterEntity alloc] init];
    //type是对推送的几个参数的选择，可以选择一个或者多个。默认是三个全部打开，即：声音，弹窗，角标
    entity.types = UMessageAuthorizationOptionBadge|UMessageAuthorizationOptionSound|UMessageAuthorizationOptionAlert;
    [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions Entity:entity     completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            
        }else{
            
        }
    }];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    //  若发现无法收到deviceToken，可以加上此方法
    [application registerForRemoteNotifications];
}
- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // 系统会根据UIBackgroundFetchResult来判断后台处理的有效性,如果后台处理效率较低,会延迟发送后台推送通知
    completionHandler (UIBackgroundFetchResultNewData);
}

//iOS10以下使用这两个方法接收通知
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [UMessage setAutoAlert:NO];
    if([[[UIDevice currentDevice] systemVersion]intValue] < 10){
        [UMessage didReceiveRemoteNotification:userInfo];
        [self handleRemoteNotification:userInfo];
    }
    completionHandler(UIBackgroundFetchResultNewData);
}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [UMessage sendClickReportForRemoteNotification:self.userInfo];
}

//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    NSDictionary * userInfo = notification.request.content.userInfo;
    self.userInfo = userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [UMessage setAutoAlert:NO];
        //应用处于前台时的远程推送接受
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        NSLog(@"xjy前台时的远程推送接受:%@", userInfo);
               
        [self handleRemoteNotification:userInfo];
    }else{
        //应用处于前台时的本地推送接受
        
               NSLog(@"xjy处于前台时的本地推送接受:%@", userInfo);
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个
    completionHandler(UNNotificationPresentationOptionSound |
                      UNNotificationPresentationOptionBadge |
                      UNNotificationPresentationOptionAlert);
    
 
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler{
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于后台时的远程推送接受
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        
        NSLog(@"%@",userInfo);
        NSString *page = [NSString stringWithFormat:@"%@",userInfo[@"type"]];
        NSLog(@"type = %@",page);
        [self sendMessageByEventChannel:page];
        [self handleRemoteNotification:userInfo];
        
    }else{
        //应用处于后台时的本地推送接受
        NSLog(@"xjy处于后台时的本地推送接受:%@", userInfo);
    }
    
   
    
}

-(void)handleRemoteNotification:(NSDictionary *)info{
    
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    
//    NSString *deviceTokenString2 = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""]
//                                     stringByReplacingOccurrencesOfString:@">" withString:@""]
//                                    stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"==========================================》deviceTokenString2：%@", deviceTokenString2);
    
    if (![deviceToken isKindOfClass:[NSData class]]) return;
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    NSLog(@"deviceToken:%@",hexToken);
    
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    NSLog(@"Regist fail%@",error);
}


-(void)addTags:(NSString *)comIDAndUserName{
    [self removeTags];
    //添加标签
    [UMessage addTags:comIDAndUserName response:^(id  _Nonnull responseObject, NSInteger remain, NSError * _Nonnull error) {
        NSLog(@"添加标签");
        NSLog(@"%@",responseObject);
        NSLog(@"%@",error);
    }];

}
-(void)removeTags{
    //获取所有标签
    [UMessage getTags:^(NSSet * _Nonnull responseTags, NSInteger remain, NSError * _Nonnull error) {
        NSLog(@"responseTags==>%@",responseTags);
        for(NSString *tag in responseTags){
            //删除标签
            [UMessage deleteTags:tag response:^(id  _Nonnull responseObject, NSInteger remain, NSError * _Nonnull error) {
            
            }];
        }
    
    }];
}

-(void)addAlias:(NSString *)comIDAndUserName{
    //绑定别名
    [UMessage addAlias:comIDAndUserName type:@"consultant" response:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
        NSLog(@"绑定别名");
        NSLog(@"%@",responseObject);
        NSLog(@"%@",error);

    }];
}
-(void)removeAlias:(NSString *)comIDAndUserName{
    //移除别名
    [UMessage removeAlias:comIDAndUserName type:@"consultant" response:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
        NSLog(@"移除别名");
        NSLog(@"%@",responseObject);
        NSLog(@"%@",error);
    }];
}
@end
